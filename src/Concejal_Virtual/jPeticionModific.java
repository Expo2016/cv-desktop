/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Extras.jBajarFtp;
import Extras.jComboBox;
import Extras.jTool;
import MVC.sAuditoria;
import MVC.sCategoria;
import MVC.sComentarios;
import MVC.sConcejales;
import MVC.sConectar;
import MVC.sConecxion;
import MVC.sDocumentos;
import MVC.sInfo;
import MVC.sPerfil;
import MVC.sPeticiones;
import Extras.jSubirFtp;
import Hilos._Acciones;
import Hilos._Control;
import Hilos._Hilos;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Toshiba
 */
public class jPeticionModific extends javax.swing.JPanel {

    /**
     * Creates new form jPeticionModific
     */
    
    String direccion = "";
    
    public static ArrayList<String> listaFile = new ArrayList<String>();
    
    javax.swing.JPanel btnDocument;
    
    jComboBox spDoc = new jComboBox();
    
    //Cancelar el doc
    javax.swing.JPanel btnCancelDoc;
    
    sPeticiones pet;
    
    //Saber si a adjuntado un archivo
    private boolean doc = false;
    
    //Saber que posicion le toca al elemento
    int heightDialog = 0;
    
    //Propiedades fundamentales
    boolean stepEstado[] = { false, false, false, false, false };
    
    //Imagenes
    private ImageIcon imgOff[] = { new ImageIcon("Img/Menu/oneOff.png"), new ImageIcon("Img/Menu/twoOff.png"), new ImageIcon("Img/Menu/threeOff.png"), new ImageIcon("Img/Menu/fourOff.png"), new ImageIcon("Img/Menu/fiveOff.png") };
    private ImageIcon imgOn[] = { new ImageIcon("Img/Menu/oneOnDark.png"), new ImageIcon("Img/Menu/twoOnDark.png"), new ImageIcon("Img/Menu/threeOnDark.png"), new ImageIcon("Img/Menu/fourOnDark.png"), new ImageIcon("Img/Menu/fiveOnDark.png") };
    
    //Paneles
    jPanelInfoStep step = new jPanelInfoStep(this);
    
    //Peticion
    jPeticion peticion;
    
    //Tamano del panel
    public int width, height, heightComent, heightImg, heightImgAbsolute;
    private int sizeEstandar = 34, btnSize = 24;
    
    //Dibujar la imagen
    javax.swing.JPanel jImg;
    
    //nombre de usuario, categoria y descripcion
    String name = "Nombre Usuario Apellido Usuario";
    String categoria = "Categoria seleccionada por el usuario";
    String descripcion = "Aquí resumiremos lo que estamos solicitando. Podemos Utilizar alguna fórmula como: “por lo expuesto… Pido:”, “Por lo anteriormente expuesto, A Usted, C. (aquí a la persona a quien nos dirigimos), Atenta y respetuosamente pido:”, etc.";
    
    File urlTxt = null;
    
    //Tmanao para la informacion
    javax.swing.JPanel  panelPeticion;
    
    boolean estado = false;
    
    //numerico
    int heightBox, widthBox;
    
    int mitad;
    
    //singo
    byte sign = -1;
    final byte ONE = 1, TWO = 2, THREE = 3, FOUR = 4, FIVE = 5;
    byte btn = ONE;
    final byte vel = 4;
    boolean can = true;
    
    //Alto del dialogo
    int heightAbsolute;
    
    //Tamano del boton
    int panelHeight;

    //Lista de elementos 
    ArrayList<jInfoDialog> jdList = new ArrayList<jInfoDialog>();
    
    String imgx = "Img/modPeticiones/user.jpg";
    
    int posXNew = 10;
    
    //timer
    Timer timeEfect = new Timer(10, new ActionListener()
    {
        public void actionPerformed(ActionEvent e) {
            //Timer
            switch(btn)
            {
                case ONE:
                    btnOne.setBounds(btnOne.getX(), btnOne.getY() + (vel * sign), btnSize, btnSize);
                    
                    if (sign < 0)
                    {
                        //Al ser negativo
                        if(btnOne.getY() < mitad)
                        {
                            btn = TWO;
                            btnOne.setBounds(btnProgres.getX() + (btnSize + (width / 200)), mitad, btnSize, btnSize);
                        }
                    }
                    else
                    {
                        if(btnOne.getY() > (mitad + sizeEstandar))
                        {
                            btnOne.setBounds(btnTwo.getX() - (btnSize + (width / 200)), btnTwo.getY() , btnSize, btnSize);
                            sign = -1;
                            stoped();
                        }
                    }
                    break;
                    
                case TWO:
                    btnTwo.setBounds(btnTwo.getX(), btnTwo.getY() + (vel * sign), btnSize, btnSize);
                    
                    if (sign < 0)
                    {
                        //Al ser negativo
                        if(btnTwo.getY() < mitad)
                        {
                            btn = THREE;
                            btnTwo.setBounds(btnOne.getX() + (btnSize + (width / 200)), btnOne.getY() , btnSize, btnSize);
                        }
                    }
                    else
                    {
                        if(btnTwo.getY() > (mitad + sizeEstandar))
                        {
                            btnTwo.setBounds(btnThree.getX() - (btnSize + (width / 200)), btnThree.getY() , btnSize, btnSize);
                            btn = ONE;
                        }
                    }
                    break;
                    
                case THREE:
                    btnThree.setBounds(btnThree.getX(), btnThree.getY() + (vel * sign), btnSize, btnSize);
                    
                    if (sign < 0)
                    {
                        //Al ser negativo
                        if(btnThree.getY() < mitad)
                        {
                            btn = FOUR;
                            btnThree.setBounds(btnTwo.getX() + (btnSize + (width / 200)), btnTwo.getY() , btnSize, btnSize);
                        }
                    }
                    else
                    {
                        if(btnThree.getY() > (mitad + sizeEstandar))
                        {
                            btnThree.setBounds(btnFour.getX() - (btnSize + (width / 200)), btnFour.getY() , btnSize, btnSize);
                            btn = TWO;
                        }
                    }
                    break;
                    
                case FOUR:
                    btnFour.setBounds(btnFour.getX(), btnFour.getY() + (vel * sign), btnSize, btnSize);
                    
                    if (sign < 0)
                    {
                        //Al ser negativo
                        if(btnFour.getY() < mitad)
                        {
                            btnFour.setBounds(btnThree.getX() + (btnSize + (width / 200)), btnThree.getY() , btnSize, btnSize);
                            sign = 1;
                            stoped();
                        }
                    }
                    else
                    {
                        if(btnFour.getY() > (mitad + sizeEstandar))
                        {
                            btnFour.setBounds(btnFour.getX(), mitad + sizeEstandar, btnSize, btnSize);
                            btn = THREE;
                        }
                    }
                    break;
            }
        }
    });
    
    //Peticiones
    ArrayList<String> dialogos = new ArrayList<String>();
    
    //Paneles de dibujo
    
    //El valor relativo para mover los paneles
    int valorRelativo = 4;
    
    //Indice de las peticiones
    int indice = 0, widthDialogos, heightDialogos;
    
    public void setComentarios()
    {
        String sql = sComentarios.getAll(pet.getId_peticion());
            sComentarios cm = new sComentarios();
            
            ArrayList<sInfo> list = sConecxion.getInformation(cm, sConectar.conectar(), sql);
            
            boolean resp = jScrollBarDialogos.isVisible();
            
            resetValuesNone();
            
            for (int i = 0; i < list.size(); i++) {
                sComentarios com = (sComentarios)list.get(i);
                
                int number = new Random().nextInt(50);
                
                //Crear nuevos paneles    
                //Creamos el objeto del dialogo
                jInfoDialog jD = new jInfoDialog(com, width, this);

                //Le damos las dimenciones
                jD.setBounds(0, heightDialog + posXNew, jD.getWidth(), jD.getHeight());

                //Le posicionamos en los comentarios
                jComentarios.add(jD);

                //Le ponemos en la lista
                jdList.add(jD);

                //Obtenemos la nueva posicion
                heightDialog += jD.getHeight() + posXNew;

                //Poner el scroll
                setValueScroll();
                
                if((jdList.size() - 2) >= 0)
                    changeList();
                
                //Lo repintamos
                jComentarios.repaint();
            }
            
        if (resp) {
            if (!jScrollBarDialogos.isVisible()) {
                jScrollBarDialogos.setValue(0);
            }
        }
    }
    
    //Prueba para los dialogos__________________________________________________
    Timer tmBeta = new Timer(2000, new ActionListener()
    {
        public void actionPerformed(ActionEvent ae) {
           
            setComentarios();
            //JOptionPane.showMessageDialog(null, "Ejecutado");
            //if(new Random().nextInt(100) == 10)
            //{
                //int number = new Random().nextInt(50);
                
                //Crear nuevos paneles    
                //Creamos el objeto del dialogo
                //jInfoDialog jD = new jInfoDialog("Querido consejal este es un mensaje de prueba para el funcionamiento del texto " + jInfoDialog.linea + " Esta es una nueva linea para abajo",width, new Color(61,76,186), (number > 40), false);

                //Le damos las dimenciones
                //jD.setBounds(0, heightDialog, jD.getWidth(), jD.getHeight());

                //Le posicionamos en los comentarios
                //jComentarios.add(jD);

                //Le ponemos en la lista
                //jdList.add(jD);

                //Obtenemos la nueva posicion
                //heightDialog += jD.getHeight();

                //Poner el scroll
                //setValueScroll();
                
                //if((jdList.size() - 2) >= 0)
                    //changeList();
                
                //Lo repintamos
                //jComentarios.repaint();
            //}
        }
    });
    //__________________________________________________________________________
    
    //Posicionar el label del estado
    public void posLbl()
    {
        Integer ILimit = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sPeticiones.getEstadoPet(pet.getId_peticion()));
        
        switch(ILimit)
        {
            case 1:
                lblEstado.setText("Estado: Registrada");
                break;
            
            case 2:
                lblEstado.setText("Estado: Aceptada");
                break;
                
            case 3:
                lblEstado.setText("Estado: Asignada");
                break;
            
            case 4:
                lblEstado.setText("Estado: Finalizada");
                break;
                
            case 5:
                lblEstado.setText("Estado: Rechazada");
                break;
        }
        
        //Tamaño
        int width = this.width;
        
        //Metrica
        FontMetrics fm = this.getFontMetrics(this.lblEstado.getFont());
        
        //Tamaño del lbl
        int widthLbl = 0;
        
        for (int i = 0; i < lblEstado.getText().length(); i++) {
            String wX = ""+lblEstado.getText().charAt(i);
            
            widthLbl += fm.stringWidth(wX);
        }
        
        widthLbl += widthLbl / 4;
        
        lblEstado.setBounds(jOpcion.getWidth() - widthLbl, btnImg.getY(), widthLbl, this.lblNombre.getHeight());
    }
    
    //Parar
    private void stoped()
    {
        timeEfect.stop();
        can = true;
    }
    _Control control = new _Control();
    //peticion
    public void setPeticion(jPeticion peticion)
    {
        this.peticion = peticion;
    }
    
    public void inicio() {
        initComponents();
        
        //Peticiones
        this.peticion = new jPeticion(this);
        
        //Escogemos las disposiciones
        this.setLayout(null);
        jHeader.setLayout(null);
        jOpcion.setLayout(null);
        jComentarios.setLayout(null);
        jEscribir.setLayout(null);
        
        //tamano de la paginas
        width = (Toolkit.getDefaultToolkit().getScreenSize().width) * 4 / 5;
        height = (Toolkit.getDefaultToolkit().getScreenSize().height) * 4 / 5;
        
        //Tamano sin las cabezera
        heightComent = height - (sizeEstandar * 2);
        
        //calculamos los comentarios
        heightComent = heightComent * 3 / 5;
        
        //El alto de las imagenes
        heightImg = height - ((btnSize * 3) + heightComent);
        heightImgAbsolute = heightImg;
        heightImg = heightImg * 2 / 3;
        
        
        //Posicionamos los paneles
        jHeader.setBounds(0,0,width, sizeEstandar);
        
        //Posicionarl el segundo panel
        jComentarios.setBounds(0,height - heightComent - sizeEstandar, width, heightComent + sizeEstandar);
    
        //Progreso
        jOpcion.setBounds(0, (jComentarios.getY() - sizeEstandar), width, sizeEstandar);
        
        //Dibujar las peticiones
        
        //Dibujar la imagen
        jImg = new javax.swing.JPanel(){
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                
                try
                {
                    if (!imgx.trim().equals("n/a")){
                        jBajarFtp.setPrimerPath("/FTP");
                        jBajarFtp.setSecundPath("Img");
                        jBajarFtp.setNombreArchivo(imgx);
                        File info = jBajarFtp.descargarArchivo();
                        //JOptionPane.showMessageDialog(null, imgx);
                        
                        direccion = info.getAbsolutePath();
                        
                        listaFile.add(info.getAbsolutePath());
                        g.drawImage(ImageIO.read(info), 0, 0, heightImg, heightImg, null);
                    }
                    else
                    {
                        g.drawImage(ImageIO.read(new File("Img/modPeticiones/user.jpg")), 0, 0, heightImg, heightImg, null);
                        direccion = "";
                    }
                }
                catch(Exception e){}
            }
        };
        
        //Caracteristicas del panel
        jImg.setBounds(width / 100, (sizeEstandar) + (width / 100), heightImg, heightImg);
        jImg.setBackground(this.getBackground());
        this.add(jImg);
        
        //Posicionar los label
        lblUserName.setFont(new Font("Consolas", Font.PLAIN, heightImgAbsolute / 13));
        lblUserName.setText(name);
        lblDate.setFont(lblUserName.getFont());
        
        lblAsunto.setFont(lblUserName.getFont());
        
        lblCategory.setFont(lblUserName.getFont());
        lblCategory.setText(categoria);
        
        lblConcejal.setFont(lblUserName.getFont());
        
        lblUserName.setBounds(jImg.getX() + (jImg.getX() / 2), jImg.getY() + jImg.getHeight() + (jImg.getY() / 4), width / 4, heightImgAbsolute / 13);
        lblCategory.setBounds(lblUserName.getX() + lblUserName.getWidth() + (width / 100), lblUserName.getY(), lblUserName.getWidth(), lblUserName.getHeight());
        lblConcejal.setBounds(lblCategory.getX() + lblCategory.getWidth() + (width / 100), lblCategory.getY(), lblCategory.getWidth(), lblCategory.getHeight());
                
                
        //Clcular el panel
        heightBox = jImg.getHeight() - (lblUserName.getHeight() + (lblUserName.getHeight() / 2)) ;
        widthBox = width - (jImg.getWidth() + (jImg.getX() * 2) + (jImg.getX()));
        
        //Peticion
        panelPeticion = new javax.swing.JPanel(){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                
                int sizeLeter = (heightImgAbsolute / 13) / 2;
                
                Font fontL = new Font("Consolas", Font.PLAIN, sizeLeter +  (sizeLeter * 3 / 4));
                
                 //Funete
                g.setFont(fontL);
                
                //Color
                g.setColor(Color.white);
                
                int x = 0;
                int y = g.getFontMetrics().getHeight();
                
                for (int i = 0; i < descripcion.length(); i++)
                {
                    String stepInfo = ""+descripcion.charAt(i);
                    
                    int widthInfo = g.getFontMetrics().stringWidth(stepInfo);
                    
                    g.drawString(stepInfo, x, y);
                    
                    if ((widthInfo + x) < widthBox) {
                        x += widthInfo;
                    }
                    else
                    {
                        x = 0;
                        y += g.getFontMetrics().getHeight();
                    }
                    
                    System.out.print(stepInfo + " - " + x);
                }
            }
        };
        
        //Ajustamos la peticion
        panelPeticion.setBounds(jImg.getX() + jImg.getWidth() + (jImg.getX()), lblUserName.getY() + (lblUserName.getHeight() + (lblUserName.getHeight() / 2)), widthBox, heightBox);
        panelPeticion.setBackground(this.getBackground());
        this.add(panelPeticion);
        
        //Botones
        jTool.btnTransparente(btnImg,  btnIcon, btnExit, btnBack, btnProgres, btnOne, btnOne, btnTwo, btnThree, btnFour);
        
        //Img
        btnIcon.setIcon(new ImageIcon("Img/Menu/Opcion24.png"));
        btnExit.setIcon(new ImageIcon("Img/Peticiones/Close.png"));
        btnBack.setIcon(new ImageIcon("Img/Peticiones/back.png"));
        btnProgres.setIcon(new ImageIcon("Img/Peticiones/Progres.png"));
        //--------------------------------------------------------------
        btnOne.setIcon(imgOff[0]);
        btnTwo.setIcon(imgOff[1]);
        btnThree.setIcon(imgOff[2]);
        btnFour.setIcon(imgOff[3]);
        //---------------------------------------------------------------
        
        //----------------------------------------------------------------------
        btnImg.setIcon(new ImageIcon("Img/Peticiones/archive.png"));
        
        mitad = (sizeEstandar / 2) - (btnSize / 2);
        
        //Posicionarlas
        btnIcon.setBounds(width / 200, mitad, btnSize, btnSize);
        btnExit.setBounds(width - (btnSize + (width / 200)), mitad, btnSize, btnSize);
        btnBack.setBounds(btnExit.getX()- (btnSize + (width / 200)), mitad , btnSize, btnSize);
        //----------------------------------------------------------------------
        
        //----------------------------------------------------------------------
        btnImg.setBounds(width / 200, mitad, btnSize, btnSize);
        
        //Posicionar el panel
        int heightTxt = (jComentarios.getHeight() * 1 / 4);
        int sep = heightTxt / 3;
        panelHeight = sep;
        
        //Caracteristicas del txtArea
        jEscribir.setBounds(0, jComentarios.getHeight() - heightTxt, width, heightTxt);
        jScrollInfo.setBounds(0, sep, jEscribir.getWidth(), heightTxt - sep);
        jTxtArea.setLineWrap(true); 
        jTxtArea.setWrapStyleWord(true);
        
        //Nueva separacion
        sep = sep * 2 / 3;
        
        //Caracteristicas del label
        lblAccion.setFont(new Font("Consolas", Font.PLAIN, sep));
        lblAccion.setForeground(Color.white);
        lblAccion.setText("Escribir respuesta");
        lblAccion.setBounds(width / 100, jScrollInfo.getY() - sep, width / 3, sep);
    
        //Crear un boton para cancelar un archivo adjunto
        btnCancelDoc = new javax.swing.JPanel(){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
                try {
                    g.drawImage(ImageIO.read(new File("Img/Peticiones/docCancel.png")), 0, 0, panelHeight, panelHeight, null);
                }
                catch (IOException ex){}
            }
        };
        
        //Creamos el panel para enviar la info
        javax.swing.JPanel btnEnivar = new javax.swing.JPanel()
        {
            protected void paintComponent(Graphics g) {
                super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
                try {
                    g.drawImage(ImageIO.read(new File("Img/Peticiones/paper.png")), 0, 0, panelHeight, panelHeight, null);
                } catch (IOException ex) {}
            }
        };
        
        btnEnivar.setBounds(width - panelHeight - (width / 150), 0, panelHeight, panelHeight);
        btnEnivar.setBackground(jEscribir.getBackground());
        
        //Evento
        btnEnivar.addMouseListener(new MouseListener(){
            public void mouseClicked(MouseEvent e) {
                if (!jTxtArea.getText().trim().equals("")) {
                    
                    String archivo = "";
                    
                    if (doc)
                        archivo = urlTxt.getName();
                    else
                        archivo = "n/a";
                    
                    Long idPerfil = new Long(mConcejalVirtual.perfil.getIdPerfil());
                    Integer idPeticion = new Integer(pet.getId_peticion());
                    String Comentario = jTxtArea.getText().trim();
                    
                    boolean r = sConecxion.insertAll(sConectar.conectar(), sComentarios.insertComentario(), idPerfil, Comentario, archivo, idPeticion);
                    
                    if (r) {
                        //Cambiar el valor de doc
                        doc = false;
                        
                        if (!archivo.equals("n/a")){
                            jSubirFtp.setDebug(true);
                            jSubirFtp.setLocation("/FTP");
                            jSubirFtp.setFile(urlTxt);
                            jSubirFtp.subierArchivo();
                        }
                        
                        //Lo aparecemos
                        lblNombre.setVisible(false);
                        btnCancelDoc.setVisible(false);

                        jTxtArea.setText("");

                        setComentarios();
                        
                        sConecxion.insertAll(sConectar.conectar(), sAuditoria.allInfo(), pet.getId_peticion(), new Integer(5), mConcejalVirtual.perfil.getIdPerfil(), new Integer(2));
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Por favor escriba un mensaje");
                }

                //Crear dialogos
                
                //Creamos el objeto del dialogo
                //jInfoDialog jD = new jInfoDialog(jTxtArea.getText(),width, new Color(5,142,200), doc, true);
                
                //Cambiar el valor de doc
                //doc = false;
                
                //Lo aparecemos
                //lblNombre.setVisible(false);
                //btnCancelDoc.setVisible(false);
                
                //Le damos las dimenciones
                //jD.setBounds(0, heightDialog, jD.getWidth(), jD.getHeight());
                
                //Le posicionamos en los comentarios
                //jComentarios.add(jD);
                
                //Le ponemos en la lista
                //jdList.add(jD);
                
                //Obtenemos la nueva posicion
                //heightDialog += jD.getHeight();
                
                //Poner el scroll
                //setValueScroll();
                
                //Resetear el texto
                //jTxtArea.setText("");
                
                //if((jdList.size() - 2) >= 0)
                    //changeList();
                
                //Lo repintamos
                //jComentarios.repaint();
            }
            public void mousePressed(MouseEvent e) {}
            public void mouseReleased(MouseEvent e) {}
            public void mouseEntered(MouseEvent e) {}
            public void mouseExited(MouseEvent e) {}
        });
        
        jEscribir.add(btnEnivar);
        
        //Crear un boton para adjuntar un archivo
        btnDocument = new javax.swing.JPanel(){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
                try {
                    g.drawImage(ImageIO.read(new File("Img/Peticiones/newDoc.png")), 0, 0, panelHeight, panelHeight, null);
                } catch (IOException ex) {}
            }
        };
        
        //propiedader extra
        btnDocument.setBounds(btnEnivar.getX() - ((width / 150) + panelHeight), 0, panelHeight, panelHeight);
        btnDocument.setBackground(jEscribir.getBackground());
        
        //Evento
        btnDocument.addMouseListener(new MouseListener(){
            public void mouseClicked(MouseEvent e) {
                //Imgresamos un documento
                JFileChooser selectorArchivos = new JFileChooser();
                
                FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos:", "pdf");
                selectorArchivos.setFileFilter(filtro);
                
                selectorArchivos.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                int resultado = selectorArchivos.showOpenDialog(null);
                File archivo = selectorArchivos.getSelectedFile();
                
                
                if ((archivo == null) || (archivo.getName().equals(""))) {
                    JOptionPane.showMessageDialog(null, "Nombre de archivo inválido", "Nombre de archivo inválido", JOptionPane.ERROR_MESSAGE);
                    doc = false;
                }
                else
                {
                    doc = true;
                    
                    urlTxt = archivo;
                    
                    lblNombre.setText(archivo.getName());
                    lblNombre.setVisible(true);
                    
                    //Lo aparecemos
                    btnCancelDoc.setVisible(true);
                    
                    String newPath = "pdf/"+archivo.getName()+"("+pet.getId_peticion()+")";
                }
            }
            public void mousePressed(MouseEvent e) {}
            public void mouseReleased(MouseEvent e) {}
            public void mouseEntered(MouseEvent e) {}
            public void mouseExited(MouseEvent e) {}
        });
        
        jEscribir.add(btnDocument);
        
        //propiedader extra
        btnCancelDoc.setBounds(btnDocument.getX() - ((width / 150) + panelHeight), 0, panelHeight, panelHeight);
        btnCancelDoc.setBackground(jEscribir.getBackground());
        
        lblNombre.setFont(new Font("Consolas", Font.PLAIN, sep));
        lblNombre.setForeground(Color.white);
        lblNombre.setVisible(false);
        lblNombre.setBounds(btnCancelDoc.getX() - ((width * 2 / 3) + (width / 100)), jScrollInfo.getY() - sep, width * 2 / 3, sep);
        
        //Evento
        btnCancelDoc.addMouseListener(new MouseListener(){
            public void mouseClicked(MouseEvent e) {
                //Imgresamos un documento
                
                doc = false;
                lblNombre.setVisible(false);
                btnCancelDoc.setVisible(false);
            }
            public void mousePressed(MouseEvent e) {}
            public void mouseReleased(MouseEvent e) {}
            public void mouseEntered(MouseEvent e) {}
            public void mouseExited(MouseEvent e) {}
        });
        
        //Le quitamos la visibilidad
        btnCancelDoc.setVisible(false);
        
        jEscribir.add(btnCancelDoc);
        
        //Le quitamos la visibilidad
        btnCancelDoc.setVisible(false);
        
        //Absolute
        heightAbsolute = jComentarios.getHeight() - jEscribir.getHeight();
        
        //Obtenemos los dialogos
        widthDialogos = jEscribir.getWidth();
        heightDialogos = heightAbsolute / 4;
        
        //Le damos las dimensiones
        step.setLocationWindow(height, width);
        
        //Detalles extras
        step.setLocation(btnOne.getX(), btnSize / 6);
        
        //Le ponemos la decoraciones
        step.setIcon(false, jPanelInfoStep.stepOne);
        
        //Quitarle la visibilidad
        step.setVisible(false);
        
        //Le añadimos a panel
        jComentarios.add(step);
        
        //Peticiones Title------------------------------------------------------
        int fontHeight = jHeader.getHeight() / 2;
        Font fuente = new Font("Consolas", Font.PLAIN, fontHeight);
        lblTitle.setFont(fuente);
        lblTitle.setBounds((btnIcon.getX() * 2) + btnIcon.getWidth(), fontHeight - (fontHeight / 2), fontHeight * lblTitle.getText().length(), fontHeight);
    
        jOpcion.add(this.spDoc);
        spDoc.setBounds((btnImg.getX() * 2) + (btnImg.getWidth()), btnImg.getY(), (btnImg.getWidth() * 8), btnImg.getHeight());
    
        spDoc.addPopupMenuListener(new PopupMenuListener() {

            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent pme) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            
                estado = true;
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent pme) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            
                estado = false;
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent pme) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        spDoc.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent ae) {
                //Aqui sera el elejido
                if (estado) {
                    JFileChooser files = new JFileChooser();
                    files.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    int resultado = files.showOpenDialog(null);

                    String destino = files.getSelectedFile().getAbsolutePath();
                    destino += File.separator + "Concejal virtual";

                    File prueba = null;

                    if (resultado == 0) {
                        String dox = (String)spDoc.getSelectedItem();

                        File f = new File(destino);
                        f.mkdir();

                        jBajarFtp.setPrimerPath("/FTP");
                        jBajarFtp.setSecundPath(destino);
                        jBajarFtp.setNombreArchivo(dox);
                        prueba = jBajarFtp.descargarArchivo();

                        if (prueba != null)
                            JOptionPane.showMessageDialog(null, "Archivo descargado exitosamente");
                    }
                }
            }
            
        });
        
        lblEstadoProgres.setText("Estado de la peticion:");
        
        FontMetrics fm = this.getFontMetrics(lblEstadoProgres.getFont());
        
        int wLbl = 0;
        
        for (int i = 0; i < lblEstadoProgres.getText().length(); i++) {
            String x = ""+lblEstadoProgres.getText().charAt(i);
            
            wLbl += fm.stringWidth(x);
        }
        
        lblEstadoProgres.setBounds(spDoc.getX() + spDoc.getWidth() + (width / 200), mitad, wLbl, btnSize);
        
        //----------------------------------------------------------------------
        btnProgres.setBounds(lblEstadoProgres.getX() + lblEstadoProgres.getWidth() + (width / 200), mitad, btnSize, btnSize);
        btnOne.setBounds(btnProgres.getX() + (btnSize + (width / 200)), mitad + sizeEstandar, btnSize, btnSize);
        btnTwo.setBounds(btnOne.getX() + (btnSize + (width / 200)), btnOne.getY() , btnSize, btnSize);
        btnThree.setBounds(btnTwo.getX() + (btnSize + (width / 200)), btnTwo.getY() , btnSize, btnSize);
        btnFour.setBounds(btnThree.getX()+ (btnSize + (width / 200)), btnThree.getY() , btnSize, btnSize);
    }
    
    public jPeticionModific()
    {
        _Hilos hilo = new _Hilos(1);
        
        hilo.establecerAccion(new _Acciones(){
            public boolean acto() {

                inicio();

                //Saber que se realizo la accion
                return true;
            }
        });

        control.evaluarHilo(hilo);
    }
    
    //Cambiaremos de posicion los componentes
    private void changeList()
    {
        System.out.println(jdList.size());
        
        ArrayList<jInfoDialog> copiX = new ArrayList<jInfoDialog>();
        
        copiX.add(jdList.get(jdList.size() - 1));
        
        //Obtenemos los elementos para posicionarlos atrevez
        for (int i = 0; i <= (jdList.size() - 2); i++) {
            copiX.add(jdList.get(i));
        }
        
        //Cambiar la posicion
        int posY = 0;
        
        //Cambiar de posicion
        Iterator x = copiX.iterator();
        
        //Eliminar
        jdList.clear();
        
        while(x.hasNext())
        {
            jInfoDialog y = (jInfoDialog)x.next();

            //Le damos las dimenciones
            y.setBounds(0, posY, y.getWidth(), y.getHeight());

            //Obtenemos la nueva posicion
            posY += y.getHeight();
            
            jdList.add(y);
        }
        
        Iterator xi = jdList.iterator();
        int posYN = 0;
        
        while(xi.hasNext())
        {
            jInfoDialog element = (jInfoDialog)xi.next();
            
            element.setBounds(0, posYN - jScrollBarDialogos.getValue(), element.getWidth(), element.getHeight());
            
            posYN += element.getHeight() + posXNew;
        }
        
        //System.out.println("Valor = "+jScrollBarDialogos.getValue());
        
        //jScrollBarDialogos.setValue(0);
    }
    
    
    //Posicioinar la barra de progreso en los comentarios
    private void setValueScroll()
    {
        int heihgtComp = jComentarios.getHeight() - jEscribir.getHeight();
        
        if(heightDialog > heihgtComp)
        {
            jScrollBarDialogos.setVisible(true);
            
            //Obtener posicion en X
            int posX = jComentarios.getWidth() / 50;
            
            jScrollBarDialogos.setBounds(jComentarios.getWidth() - posX, 0, posX, heihgtComp);
            
            //Valor del scroll
            int value = 0;
            
            Iterator i = jdList.iterator();
            
            while(i.hasNext())
            {
                value += ((jInfoDialog)i.next()).getHeight();
            }
            
            jScrollBarDialogos.setMaximum(value);
        }
    }
    
    //Eliminar los botones que esten antes
    public void setValueBtn(byte indiceValue){
        
        //Valor
        for (int i = indiceValue; i < 5; i++) {

            stepEstado[i] = false;

            switch(i){
                case 0:
                    btnOne.setIcon(imgOff[0]);
                    break;

                case 1:
                    btnTwo.setIcon(imgOff[1]);
                    break;

                case 2:
                    btnThree.setIcon(imgOff[2]);
                    break;

                case 3:
                    btnFour.setIcon(imgOff[3]);
                    break;
            }
        }
        
    }
    
    //Cambiar el valor de los botones
    public void setStepValue(boolean stepValue, byte indiceValue)
    {
        //Le damos un nuevo valor
        stepEstado[indiceValue] = stepValue;
        
        if(stepValue)
        {
           switch(indiceValue){
               case 0:
                   btnOne.setIcon(imgOn[indiceValue]);
                   break;
                   
               case 1:
                   btnTwo.setIcon(imgOn[indiceValue]);
                   break;
                   
               case 2:
                   btnThree.setIcon(imgOn[indiceValue]);
                   break;
                   
               case 3:
                   btnFour.setIcon(imgOn[indiceValue]);
                   break;
           }
        }
        else
        {
            switch(indiceValue){
               case 0:
                   btnOne.setIcon(imgOff[indiceValue]);
                   break;
                   
               case 1:
                   btnTwo.setIcon(imgOff[indiceValue]);
                   break;
                   
               case 2:
                   btnThree.setIcon(imgOff[indiceValue]);
                   break;
                   
               case 3:
                   btnFour.setIcon(imgOff[indiceValue]);
                   break;
           }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jHeader = new javax.swing.JPanel();
        btnExit = new javax.swing.JButton();
        btnIcon = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        jOpcion = new javax.swing.JPanel();
        btnImg = new javax.swing.JButton();
        btnProgres = new javax.swing.JButton();
        btnOne = new javax.swing.JButton();
        btnTwo = new javax.swing.JButton();
        btnFour = new javax.swing.JButton();
        btnThree = new javax.swing.JButton();
        lblEstado = new javax.swing.JLabel();
        lblEstadoProgres = new javax.swing.JLabel();
        jComentarios = new javax.swing.JPanel();
        jEscribir = new javax.swing.JPanel();
        jScrollInfo = new javax.swing.JScrollPane();
        jTxtArea = new javax.swing.JTextArea();
        lblAccion = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        jScrollBarDialogos = new org.edisoncor.gui.scrollBar.ScrollBarCustom();
        lblUserName = new javax.swing.JLabel();
        lblCategory = new javax.swing.JLabel();
        lblConcejal = new javax.swing.JLabel();
        lblAsunto = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();

        setBackground(new java.awt.Color(69, 84, 194));

        jHeader.setBackground(new java.awt.Color(12, 22, 71));

        btnExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExitMouseClicked(evt);
            }
        });
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        btnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnBackMouseClicked(evt);
            }
        });
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        lblTitle.setBackground(new java.awt.Color(255, 255, 255));
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setText("Peticion");

        javax.swing.GroupLayout jHeaderLayout = new javax.swing.GroupLayout(jHeader);
        jHeader.setLayout(jHeaderLayout);
        jHeaderLayout.setHorizontalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jHeaderLayout.createSequentialGroup()
                .addGap(138, 138, 138)
                .addComponent(lblTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnExit)
                .addGap(60, 60, 60))
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(btnIcon)
                    .addContainerGap(347, Short.MAX_VALUE)))
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jHeaderLayout.createSequentialGroup()
                    .addContainerGap(250, Short.MAX_VALUE)
                    .addComponent(btnBack)
                    .addGap(117, 117, 117)))
        );
        jHeaderLayout.setVerticalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnExit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jHeaderLayout.createSequentialGroup()
                .addContainerGap(33, Short.MAX_VALUE)
                .addComponent(lblTitle)
                .addGap(28, 28, 28))
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(btnIcon, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(btnBack, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        jOpcion.setBackground(new java.awt.Color(0, 162, 211));

        btnImg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImgActionPerformed(evt);
            }
        });

        btnProgres.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnProgresMouseClicked(evt);
            }
        });

        btnOne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOneActionPerformed(evt);
            }
        });

        btnTwo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTwoActionPerformed(evt);
            }
        });

        btnFour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFourActionPerformed(evt);
            }
        });

        btnThree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThreeActionPerformed(evt);
            }
        });

        lblEstado.setText("jLabel1");

        lblEstadoProgres.setText("jLabel1");

        javax.swing.GroupLayout jOpcionLayout = new javax.swing.GroupLayout(jOpcion);
        jOpcion.setLayout(jOpcionLayout);
        jOpcionLayout.setHorizontalGroup(
            jOpcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jOpcionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnImg)
                .addGap(18, 18, 18)
                .addComponent(lblEstadoProgres)
                .addGap(135, 135, 135)
                .addComponent(btnProgres)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnOne)
                .addGap(18, 18, 18)
                .addComponent(btnTwo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnFour)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 178, Short.MAX_VALUE)
                .addComponent(btnThree)
                .addGap(18, 18, 18)
                .addComponent(lblEstado)
                .addGap(19, 19, 19))
        );
        jOpcionLayout.setVerticalGroup(
            jOpcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnProgres, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
            .addComponent(btnOne, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnTwo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnThree, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnFour, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jOpcionLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jOpcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jOpcionLayout.createSequentialGroup()
                        .addComponent(lblEstado)
                        .addGap(26, 26, 26))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jOpcionLayout.createSequentialGroup()
                        .addGroup(jOpcionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblEstadoProgres, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnImg, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(29, 29, 29))))
        );

        jComentarios.setBackground(new java.awt.Color(41, 51, 127));

        jEscribir.setBackground(new java.awt.Color(69, 84, 194));

        jScrollInfo.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTxtArea.setColumns(20);
        jTxtArea.setRows(5);
        jTxtArea.setWrapStyleWord(true);
        jScrollInfo.setViewportView(jTxtArea);

        lblAccion.setText("jLabel1");

        lblNombre.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNombre.setText("jLabel1");

        javax.swing.GroupLayout jEscribirLayout = new javax.swing.GroupLayout(jEscribir);
        jEscribir.setLayout(jEscribirLayout);
        jEscribirLayout.setHorizontalGroup(
            jEscribirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jEscribirLayout.createSequentialGroup()
                .addComponent(jScrollInfo, javax.swing.GroupLayout.DEFAULT_SIZE, 672, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jEscribirLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAccion)
                .addGap(58, 58, 58)
                .addComponent(lblNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jEscribirLayout.setVerticalGroup(
            jEscribirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jEscribirLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jEscribirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAccion)
                    .addComponent(lblNombre))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollBarDialogos.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jScrollBarDialogosPropertyChange(evt);
            }
        });
        jScrollBarDialogos.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
            public void adjustmentValueChanged(java.awt.event.AdjustmentEvent evt) {
                jScrollBarDialogosAdjustmentValueChanged(evt);
            }
        });
        jScrollBarDialogos.addVetoableChangeListener(new java.beans.VetoableChangeListener() {
            public void vetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {
                jScrollBarDialogosVetoableChange(evt);
            }
        });

        javax.swing.GroupLayout jComentariosLayout = new javax.swing.GroupLayout(jComentarios);
        jComentarios.setLayout(jComentariosLayout);
        jComentariosLayout.setHorizontalGroup(
            jComentariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jEscribir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jComentariosLayout.createSequentialGroup()
                .addContainerGap(490, Short.MAX_VALUE)
                .addComponent(jScrollBarDialogos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(175, 175, 175))
        );
        jComentariosLayout.setVerticalGroup(
            jComentariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jComentariosLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jScrollBarDialogos, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addComponent(jEscribir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        lblUserName.setText("jLabel1");

        lblCategory.setText("jLabel1");

        lblConcejal.setText("jLabel1");

        lblAsunto.setText("jLabel1");

        lblDate.setText("jLabel1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jOpcion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jComentarios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(145, 145, 145)
                        .addComponent(lblUserName))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(101, 101, 101)
                        .addComponent(lblConcejal)
                        .addGap(28, 28, 28)
                        .addComponent(lblCategory)
                        .addGap(48, 48, 48)
                        .addComponent(lblAsunto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblDate)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jOpcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblUserName)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblCategory)
                            .addComponent(lblAsunto)
                            .addComponent(lblDate))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 52, Short.MAX_VALUE)
                        .addComponent(lblConcejal)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComentarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnProgresMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnProgresMouseClicked
        //Evento
        if(can){
            timeEfect.start();
            can = false;
            step.setVisible(false);
        }
    }//GEN-LAST:event_btnProgresMouseClicked

    private void btnBackMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackMouseClicked
        peticion.setVisible(true);
        changeVisible(false);
        setIconStep();
    }//GEN-LAST:event_btnBackMouseClicked

    private void btnExitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExitMouseClicked
        changeVisible(false);
        this.peticion.setFormatear();
        setIconStep();
    }//GEN-LAST:event_btnExitMouseClicked

    //Eliminar los componentes
    private void resetValues(){
        Iterator i = jdList.iterator();
        
        while(i.hasNext())
        {
            jInfoDialog x = (jInfoDialog)i.next();
            
            jComentarios.remove(x);
        }
        
        jdList.clear();
        this.heightDialog = 0;
        jScrollBarDialogos.setValue(0);
        jScrollBarDialogos.setVisible(false);
    }
    
    private void resetValuesNone(){
        Iterator i = jdList.iterator();
        
        while(i.hasNext())
        {
            jInfoDialog x = (jInfoDialog)i.next();
            
            jComentarios.remove(x);
        }
        
        jdList.clear();
        this.heightDialog = 0;
        jScrollBarDialogos.setVisible(false);
        jComentarios.repaint();
    }
    
    private void jScrollBarDialogosAdjustmentValueChanged(java.awt.event.AdjustmentEvent evt) {//GEN-FIRST:event_jScrollBarDialogosAdjustmentValueChanged
        //Cambiar el posicionamiento
        Iterator i = jdList.iterator();

        int y = 0;
        
        while(i.hasNext())
        {
            try
            {
                //Paneles
                jInfoDialog D = (jInfoDialog)i.next();

                D.setBounds(D.getX(), y - evt.getValue(), D.getWidth(), D.getHeight());

                y += D.getHeight() + posXNew;
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null, e.getLocalizedMessage()+" "+y);
            }
        }
    }//GEN-LAST:event_jScrollBarDialogosAdjustmentValueChanged

    private void jScrollBarDialogosPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jScrollBarDialogosPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollBarDialogosPropertyChange

    private void jScrollBarDialogosVetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {//GEN-FIRST:event_jScrollBarDialogosVetoableChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollBarDialogosVetoableChange

    private void btnOneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOneActionPerformed
        if (false){
            //Posicionar el boton
            step.setLocation(btnOne.getX(), btnSize / 6);
            //Le ponemos la decoraciones
            step.setIcon(stepEstado[0], jPanelInfoStep.stepOne);
            //Cambiar el texto
            step.setTitleStep("Registrada");
            //Ver la peticion
            step.setVisible(true);
        }
    }//GEN-LAST:event_btnOneActionPerformed

    private void btnTwoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTwoActionPerformed
        if(stepEstado[0] && pet.getId_estadopet() < 2 && pet.getId_estadopet() < 5){
            //Posicionar el boton
            step.setLocation(btnTwo.getX(), btnSize / 6);
            //Le ponemos la decoraciones
            step.setIcon(stepEstado[1], jPanelInfoStep.stepTwo);
            //Cambiar el texto
            step.setTitleStep("Aceptar o denegar la peticion?");
            //Cambiar descripcion
            step.txtPrueba = "En este paso del proceso se acepta o deniega la peticion";
            step.jPanelInfo.repaint();
            //Ver la peticion
            step.setVisible(true);
        }
    }//GEN-LAST:event_btnTwoActionPerformed

    private void btnThreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThreeActionPerformed
        if(stepEstado[1] && pet.getId_estadopet() < 5){

            //Posicionar el boton
            step.setLocation(btnThree.getX(), btnSize / 6);
            //Le ponemos la decoraciones
            step.setIcon(stepEstado[2], jPanelInfoStep.stepThree);
            //Cambiar el texto
            step.setTitleStep("Asignada");
            //Cambiar descripcion
            step.txtPrueba = "Es la solicitud que ya ha sido asignada a un concejal y está haciendo las gestiones necesarias, de igual manera podría estar haciendo feedback entre el moderador y lo ciudadanos solicitante.";
            step.jPanelInfo.repaint();
            //Ver la peticion
            step.setVisible(true);
        }
    }//GEN-LAST:event_btnThreeActionPerformed

    private void btnFourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFourActionPerformed
        if(stepEstado[2] && pet.getId_estadopet() != 5){
            //Posicionar el boton
            step.setLocation(btnFour.getX(), btnSize / 6);
            //Le ponemos la decoraciones
            step.setIcon(stepEstado[3], jPanelInfoStep.stepFour);
            //Cambiar el texto
            step.setTitleStep("Finalizada");
            //Cambiar descripcion
            step.txtPrueba = "Es la solicitud que ha concluido de manera exitosa o que de alguna manera se da por finalizada por falta de recursos o cualquier detalle.";
            step.jPanelInfo.repaint();
            //Ver la peticion
            step.setVisible(true);
        }
    }//GEN-LAST:event_btnFourActionPerformed

    private void btnImgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImgActionPerformed
    try
        {
            if (spDoc.getItemCount() != 0){
                JFileChooser files = new JFileChooser();
                files.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int resultado = files.showOpenDialog(null);

                String destino = files.getSelectedFile().getAbsolutePath();
                destino += File.separator + "Concejal virtual";

                File prueba = null;

                if (resultado == 0) {
                    for (int i = 0; i < spDoc.getItemCount(); i++) {
                        String dox = (String)spDoc.getItemAt(i);

                        File f = new File(destino);
                        f.mkdir();

                        jBajarFtp.setPrimerPath("/FTP");
                        jBajarFtp.setSecundPath(destino);
                        jBajarFtp.setNombreArchivo(dox);
                        prueba = jBajarFtp.descargarArchivo();
                    }

                    if (prueba != null)
                    JOptionPane.showMessageDialog(null, "Archivos descargados exitosamente");
                }
            }
            else
            {
                JOptionPane.showMessageDialog(null, "No existen archivos enlazados");
            }
        }
        catch(Exception ex){}
    }//GEN-LAST:event_btnImgActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnBackActionPerformed
    
    sPerfil user = new sPerfil();
    
    public void changeVisible(boolean valor, sPeticiones pet)
    {
        this.pet = pet;
        changeVisible(valor);
        
        sConecxion.getInformation(user, sConectar.conectar(), sPerfil.getAllElement(), pet.getId_perfil());
        
        lblUserName.setIcon(new ImageIcon("Img/Admin/miniUser.png"));
        lblCategory.setIcon(new ImageIcon("Img/Admin/categoria16px.png"));
        lblConcejal.setIcon(new ImageIcon("Img/Admin/concejal16px.png"));
        lblAsunto.setIcon(new ImageIcon("Img/Admin/minMessage.png"));
        lblDate.setIcon(new ImageIcon("Img/Admin/minCalendar.png"));
        
        lblUserName.setText(user.getNombre());
        lblCategory.setText((String)sConecxion.getEspecificElement(sConectar.conectar(), sCategoria.getEspecificElement(), pet.getId_categoria()));
        lblConcejal.setText((String)sConecxion.getEspecificElement(sConectar.conectar(), sConcejales.getEapecificElement(), pet.getId_concejal()));
        descripcion = "Descripcion: " + pet.getDescripcion();
        
        int widthLbl = width / 4;
        
        FontMetrics fM = this.getFontMetrics(lblUserName.getFont());
        int leterWidth = fM.stringWidth(lblCategory.getText()) + 32;
        
        if (leterWidth < widthLbl) {
            widthLbl = leterWidth;
        }
        else
        {
            widthLbl = width / 4;
        }
        
        lblCategory.setBounds(jImg.getX() + jImg.getWidth() + (width / 100), jImg.getY(), widthLbl, lblUserName.getHeight());
        
        widthLbl = width / 4;
        
        leterWidth = fM.stringWidth(lblConcejal.getText()) + 32;
        
        if (leterWidth < widthLbl) {
            widthLbl = leterWidth;
        }
        else
        {
            widthLbl = width / 4;
        }
        
        lblConcejal.setBounds(lblCategory.getX() + lblCategory.getWidth() + (width / 100), lblCategory.getY(), widthLbl, lblCategory.getHeight());
        
        lblDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(pet.getFecha()));
        lblAsunto.setText(pet.getAsunto());
        lblAsunto.setForeground(Color.WHITE);
        lblDate.setForeground(Color.black);
        
        widthLbl = width / 4;
        
        leterWidth = fM.stringWidth(lblAsunto.getText()) + 32;
        
        if (leterWidth < widthLbl) {
            widthLbl = leterWidth;
        }
        else
        {
            widthLbl = (width / 4) * 3;
        }
        
        lblAsunto.setBounds(jImg.getX() + jImg.getWidth() + (width / 100), lblCategory.getY() + (lblCategory.getHeight() + (lblCategory.getHeight() / 2)), widthLbl, lblCategory.getHeight());
        
        widthLbl = width / 4;
        
        leterWidth = fM.stringWidth(lblDate.getText()) + 32;
        
        if (leterWidth < widthLbl) {
            widthLbl = leterWidth;
        }
        else
        {
            widthLbl = width / 4;
        }
        
        lblDate.setBounds(lblConcejal.getX() + lblConcejal.getWidth() + (width / 100), lblConcejal.getY(), widthLbl, lblConcejal.getHeight());
        
        panelPeticion.setBounds(jImg.getX() + jImg.getWidth() + (jImg.getX()), lblCategory.getY() + (lblCategory.getHeight() + (lblCategory.getHeight() / 2)), widthBox, heightBox);
       
        
       
        if (user.getFoto() == null) {
            imgx = "Img/modPeticiones/user.jpg";
        }
        else{
            imgx = user.getFoto();
        }
        
        jImg.repaint();
        
        spDoc.removeAllItems();
        sConecxion.getCombo(sConectar.conectar(), sDocumentos.getDocument(pet.getId_peticion()), spDoc);
        
        int Limit = pet.getId_estadopet();
        Integer ILimit = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sPeticiones.getEstadoPet(pet.getId_peticion()));
        Limit = ILimit.intValue();
                
        //if (Limit == 1) {
        //    sConecxion.setModific(sConectar.conectar(), sPeticiones.setInfo(2, pet.getId_peticion()));
        //    ILimit = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sPeticiones.getEstadoPet(pet.getId_peticion()));
        //    Limit = ILimit.intValue();
        //}
        
        if (Limit > 4) {
            Limit = 2;
            pet.setId_estadopet(6);
        }
        
        for (byte i = 0; i < (byte)(Limit); i++) {
            setStepValue(true, i);
        }
        
        lblUserName.setForeground(Color.WHITE);
        lblCategory.setForeground(Color.WHITE);
        lblConcejal.setForeground(Color.WHITE);
        lblAsunto.setForeground(Color.WHITE);
        lblDate.setForeground(Color.WHITE);
        
        posLbl();
        
        panelPeticion.setBounds(jImg.getX() + jImg.getWidth() + (jImg.getX()), lblAsunto.getY() + (lblAsunto.getHeight() + (lblAsunto.getHeight() / 2)), widthBox, heightBox - lblAsunto.getHeight());
        panelPeticion.repaint();
    }
    
    //Cambiar visibilidad
    public void changeVisible(boolean valor)
    {
        this.setVisible(valor);
        
        if(valor)
        {
            //Iniciar el timer
            tmBeta.start();
        }
        else
        {
            //Parar el timer
            tmBeta.stop();
        }
        
        doc = false;
        
        this.btnCancelDoc.setVisible(false);
        
        jTxtArea.setText("");
        
        resetValues();
        
        if(btnOne.getY() < jOpcion.getHeight())
            if(can){
                timeEfect.start();
                can = false;
            }
        
        step.setVisible(false);
        
        //canTimeUse = valor;
    }
    

    public void setVisible(boolean bln) {
        super.setVisible(bln); //To change body of generated methods, choose Tools | Templates.
    
        if (!direccion.equals("")) {
            File x = new File(direccion);
            if (x.exists()) {
                x.delete();
                direccion = "";
            }
        }
    }
    //Resetear los icono
    public void setIconStep(){
        for (int i = 0; i < 5; i++) {

            stepEstado[i] = false;

            switch(i){
                case 0:
                    btnOne.setIcon(imgOff[0]);
                    break;

                case 1:
                    btnTwo.setIcon(imgOff[1]);
                    break;

                case 2:
                    btnThree.setIcon(imgOff[2]);
                    break;

                case 3:
                    btnFour.setIcon(imgOff[3]);
                    break;
            }
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnFour;
    private javax.swing.JButton btnIcon;
    private javax.swing.JButton btnImg;
    private javax.swing.JButton btnOne;
    private javax.swing.JButton btnProgres;
    private javax.swing.JButton btnThree;
    private javax.swing.JButton btnTwo;
    private javax.swing.JPanel jComentarios;
    private javax.swing.JPanel jEscribir;
    private javax.swing.JPanel jHeader;
    private javax.swing.JPanel jOpcion;
    private org.edisoncor.gui.scrollBar.ScrollBarCustom jScrollBarDialogos;
    private javax.swing.JScrollPane jScrollInfo;
    private javax.swing.JTextArea jTxtArea;
    private javax.swing.JLabel lblAccion;
    private javax.swing.JLabel lblAsunto;
    private javax.swing.JLabel lblCategory;
    private javax.swing.JLabel lblConcejal;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblEstadoProgres;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblUserName;
    // End of variables declaration//GEN-END:variables

}
