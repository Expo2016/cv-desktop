/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Extras.jComboBox;
import Extras.jCustomFont;
import Extras.jTool;
import MVC.cComisiones;
import Conexion.xConexion;
import Hilos._Acciones;
import Hilos._Control;
import Hilos._Hilos;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Position;

/**
 *
 * @author kevin
 */
public class jComisiones extends javax.swing.JPanel {

    /**
     * Creates new form jComisiones
     */
    
    _Control control = new _Control();
    
    DefaultTableModel comisionesTabla;
     xConexion con=new xConexion();
    
    //Cambiar de posicion
    private boolean location = false;
    
    //Tamaños estandar
    private int btnSize = 24, elementSize = 34;
    
    //Combobox
    jComboBox scbCategoria = new jComboBox();
    
    
    
    
    //Instanciando clase CustomFont
    //final jCustomFont cf = new jCustomFont();
    
    
    public void inicio() {
        
        
        initComponents();
        
        comisionesTabla=new DefaultTableModel(null,getColumnas()){
            //Deshabilitando la edición de celdas
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        
        setFilas();
        //Configurando botones
        jTool.btnTransparente(btnExit, btnLogo, btnAll);
        
        
        //Invisivilizando lista de indices de comisiones
        scrIndex.setVisible(false); //scrIndex es el padre de lstIndex
        lstIndex.setVisible(false);
        
        
        
        /*
        //Aplicando fuentes
        lblComi1.setFont(cf.setMyriad(1, 17));
        lblComi2.setFont(cf.setBembo(1, 17));
        txtComi.setFont(cf.setMyriad(1, 20));
        txaDescripcion.setFont(cf.setBembo(1, 20));
        tblComi.setFont(cf.setMyriad(0, 12));
        lblCategoria.setFont(cf.setBembo(1, 17));
        */
        
        cComisiones obj = new cComisiones();
        
        //Llenando combobox
        obj.infoCMB();
        llenarCMB();
        
        //Llenando listas    ESTO IRA EN EL EVENTO EN EL QUE SE SELECCIONE UNA COMISION
        // EL 2 DE LA VARIABLE id ES PROVISIONAL, SE LLENARA CON EL ID DE LA COMISION SELECCIONADA
        Long id = Long.valueOf(0);
        obj.setId_comiM(id);
        obj.infoList();
        llenarList();
        
        try 
            {
                obj.cn.close();
            } 
        
            catch (SQLException ex) 
            {
                //Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        
        //Añadiendo combobox
        add(scbCategoria);
        
        //Al cambiar el item del combobox 
        this.scbCategoria.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(scbCategoria.getSelectedIndex() != -1){
                    //variable que contiene el indice seleccionado
                    String Index = String.valueOf(scbCategoria.getSelectedItem());



                    //Si ya hay comisiones registradas
                    try{

                        //Variable que busca si lstIndex contiene un elemento cuyo texto es igual al indice seleccionado
                        int respuesta = lstCategoria.getNextMatch(Index, 0, Position.Bias.Forward);

                        //Verificando si el concejal no pertenece ya a la comision seleccionada
                        if(respuesta == -1)
                        {
                            //No pertenece, el indice es añadido
                            IndexModel.addElement(cComisiones.arrayCate[scbCategoria.getSelectedIndex()][0]);
                            ListModel.addElement(scbCategoria.getSelectedItem());

                            lstIndex.setModel(IndexModel);
                            lstCategoria.setModel(ListModel);

                        }

                        else
                        {
                            //Ya pertenece, mensaje de error
                            if(scbCategoria.getSelectedIndex() != -1)
                            JOptionPane.showMessageDialog(null, "La categoría ya pertenece a la comisión seleccionada");
                        }
                    }

                    //Si aún no hay comisiones registradas
                    catch (Exception ex)
                    {
                        //El indice es añadido
                        IndexModel.addElement(cComisiones.arrayCate[scbCategoria.getSelectedIndex()][0]);
                        ListModel.addElement(scbCategoria.getSelectedItem());
                        System.out.println(cComisiones.arrayCate[scbCategoria.getSelectedIndex()][0]);
                        lstIndex.setModel(IndexModel);
                        lstCategoria.setModel(ListModel);
                    }
                }
                
            }    
        });
    }
    
    public jComisiones()
    {
        _Hilos hilo = new _Hilos(1);
        
        control.estrablecerConstructor(true);
        
        hilo.establecerAccion(new _Acciones(){
            public boolean acto() {

                inicio();

                //Saber que se realizo la accion
                return true;
            }
        });

        control.evaluarHilo(hilo);
    }
    
    
    //Modelos para combobox
    DefaultComboBoxModel ModelCMB;
    
    //Método para llenar los combobox
    void llenarCMB()
    {
        ModelCMB = new DefaultComboBoxModel();
        
        //Agregando elementos a los tres modelos
        for(int cont = 0; cont < cComisiones.lengthCate; cont++)
        {
            ModelCMB.addElement(cComisiones.arrayCate[cont][1]);
        }
        
        
        
        //Implementando modelos en combobox
        scbCategoria.setModel(ModelCMB);
    }
    
    
    
    //Modelos para listas
    DefaultListModel IndexModel;
    DefaultListModel ListModel;
    
    //Método para llenar las listas
    void llenarList()
    {
        //Inicializando modelos para las listas
        IndexModel = new DefaultListModel();
        ListModel = new DefaultListModel();
        
        //Agregando elementos a los tres modelos
        for(int cont = 0; cont < cComisiones.ListX; cont++)
        {
            
            IndexModel.addElement(cComisiones.List[cont][0]);
            ListModel.addElement(cComisiones.List[cont][1]);
        }
        
        
        
        //Implementando modelos en listas
        lstCategoria.setModel(ListModel);
        lstIndex.setModel(IndexModel);
    }
    
    public void limpiar()
    {
        //Regresando textFields a sus valores por defecto
        txtComi.setText("Nuevo nombre de comisión");
        txtComi.setForeground(new Color(153, 153, 153));
        
        txaDescripcion.setText("Descripción");
        txaDescripcion.setForeground(new Color(153, 153, 153));
        
        
        //Regresando listas y combobox a sus valores por defecto
        scbCategoria.setSelectedIndex(-1);
        
        IndexModel.removeAllElements();
        ListModel.removeAllElements();
        
        lstIndex.setModel(IndexModel);
        lstCategoria.setModel(ListModel);
        
        //Deshabilitando boton borrar
        btnBorrar.setEnabled(false);
        
        
        DefaultTableModel modelo = (DefaultTableModel)tblComi.getModel();
            
            int fila =comisionesTabla.getRowCount();
            
            for(int i=0; fila > i; i++)
            {
                comisionesTabla.removeRow(0);
            }
           
        //Deshabilitando modificar
        lblGuardar.setEnabled(true);
        lblModificar.setEnabled(false);
            
        setFilas();
        
        //Seleccionando primera fila
        tblComi.changeSelection(-1, -1, false, false);
    }
    
    private String[] getColumnas()
    {
        String columna[]= new String[]{"No.","id","Comision","Descripción"};
        return columna;
    }
      
    
    //Llena la tabla
    private void setFilas()
    {
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        try{
            String sql="SELECT committee_id, committee, committee_description FROM committees";

            PreparedStatement us=con.conectar().prepareStatement(sql);
            ResultSet res=us.executeQuery();

            Object datos[]=new Object[4];//Numero de columnas de la cosulta

            //El correlativo de la tabla
            int corr = 1;

            while (res.next())
            {
                datos[0] = corr;

                for (int i = 1; i < 4; i++) 
                {
                    datos[i]=res.getObject(i);
                }
                comisionesTabla.addRow(datos);

                corr++;


            }
            res.close();

            //Estableciendo modelo
            tblComi.setModel(comisionesTabla);

            //Dando tamaño a la columna del correlativo
            tblComi.getColumnModel().getColumn(0).setWidth(50);
            tblComi.getColumnModel().getColumn(0).setMinWidth(50);
            tblComi.getColumnModel().getColumn(0).setMaxWidth(50);

            //Ocultando columna de ID
            tblComi.getColumnModel().getColumn(1).setWidth(0);
            tblComi.getColumnModel().getColumn(1).setMinWidth(0);
            tblComi.getColumnModel().getColumn(1).setMaxWidth(0);

            //Validando que solo se pueda seleccionar un registro a la vez
            tblComi.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

            //Fijando la posición de las columnas de la tabla
            tblComi.getTableHeader().setReorderingAllowed(false);

            //Deshabilitando navegación con teclado en la tabla
            InputMap iMap1 = tblComi.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

            KeyStroke stroke = KeyStroke.getKeyStroke("ENTER");
            iMap1.put(stroke, "none");
            stroke = KeyStroke.getKeyStroke("DOWN");
            iMap1.put(stroke, "none");
            stroke = KeyStroke.getKeyStroke("UP");
            iMap1.put(stroke, "none");

        }
        catch(SQLException ex)
        {
            //Logger.getLogger(jAdminUsuarios.class.getName()).log(Level.SEVERE,null,ex);
        }
                
    }
        
    
    
    
    //Variables que contienen el tamaño del panel
    int width, height;
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jHeader = new javax.swing.JPanel();
        btnExit = new javax.swing.JButton();
        btnLogo = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        scrTabla = new javax.swing.JScrollPane();
        tblComi = new javax.swing.JTable();
        lblComi1 = new javax.swing.JLabel();
        lblComi2 = new javax.swing.JLabel();
        txtComi = new javax.swing.JTextField();
        scrDescripcion = new javax.swing.JScrollPane();
        txaDescripcion = new javax.swing.JTextArea();
        scrCategoria = new javax.swing.JScrollPane();
        lstCategoria = new javax.swing.JList<>();
        scrIndex = new javax.swing.JScrollPane();
        lstIndex = new javax.swing.JList<>();
        btnBorrar = new javax.swing.JButton();
        lblCategoria = new javax.swing.JLabel();
        jElement = new javax.swing.JPanel();
        btnAll = new javax.swing.JButton();
        lblGuardar = new javax.swing.JLabel();
        lblModificar = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 162, 211));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentMoved(java.awt.event.ComponentEvent evt) {
                formComponentMoved(evt);
            }
        });

        jHeader.setBackground(new java.awt.Color(12, 22, 71));

        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        lblTitle.setBackground(new java.awt.Color(255, 255, 255));
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setText("Gestion Comisiones");

        javax.swing.GroupLayout jHeaderLayout = new javax.swing.GroupLayout(jHeader);
        jHeader.setLayout(jHeaderLayout);
        jHeaderLayout.setHorizontalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnLogo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnExit)
                .addContainerGap())
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addGap(336, 336, 336)
                    .addComponent(lblTitle)
                    .addContainerGap(337, Short.MAX_VALUE)))
        );
        jHeaderLayout.setVerticalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnExit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnLogo, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addGap(8, 8, 8)
                    .addComponent(lblTitle)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        tblComi.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblComi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblComiMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblComiMouseReleased(evt);
            }
        });
        scrTabla.setViewportView(tblComi);

        lblComi1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblComi1.setForeground(new java.awt.Color(255, 255, 255));
        lblComi1.setText("Comisión seleccionada:");

        lblComi2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblComi2.setForeground(new java.awt.Color(174, 174, 175));
        lblComi2.setText("Ninguno");

        txtComi.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtComi.setForeground(new java.awt.Color(153, 153, 153));
        txtComi.setText("Nuevo nombre de comisión");
        txtComi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtComiActionPerformed(evt);
            }
        });
        txtComi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtComiKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtComiKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtComiKeyTyped(evt);
            }
        });

        txaDescripcion.setColumns(20);
        txaDescripcion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txaDescripcion.setForeground(new java.awt.Color(153, 153, 153));
        txaDescripcion.setRows(5);
        txaDescripcion.setText("Descripción");
        txaDescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txaDescripcionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txaDescripcionKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txaDescripcionKeyTyped(evt);
            }
        });
        scrDescripcion.setViewportView(txaDescripcion);

        lstCategoria.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lstCategoria.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                lstCategoriaFocusLost(evt);
            }
        });
        lstCategoria.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstCategoriaMouseClicked(evt);
            }
        });
        lstCategoria.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstCategoriaValueChanged(evt);
            }
        });
        scrCategoria.setViewportView(lstCategoria);

        lstIndex.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        scrIndex.setViewportView(lstIndex);

        btnBorrar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBorrar.setText("Borrar categoría");
        btnBorrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBorrar.setEnabled(false);
        btnBorrar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                btnBorrarFocusGained(evt);
            }
        });
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });

        lblCategoria.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblCategoria.setForeground(new java.awt.Color(255, 255, 255));
        lblCategoria.setText("Seleccionar categorías:");

        jElement.setBackground(new java.awt.Color(12, 22, 71));

        btnAll.setForeground(new java.awt.Color(255, 255, 255));
        btnAll.setText("Limpiar");
        btnAll.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAll.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllActionPerformed(evt);
            }
        });

        lblGuardar.setForeground(new java.awt.Color(255, 255, 255));
        lblGuardar.setText("Guardar");
        lblGuardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblGuardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblGuardarMouseClicked(evt);
            }
        });

        lblModificar.setForeground(new java.awt.Color(255, 255, 255));
        lblModificar.setText("Modificar");
        lblModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblModificar.setEnabled(false);
        lblModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblModificarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jElementLayout = new javax.swing.GroupLayout(jElement);
        jElement.setLayout(jElementLayout);
        jElementLayout.setHorizontalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jElementLayout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addComponent(btnAll, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblGuardar)
                .addGap(18, 18, 18)
                .addComponent(lblModificar)
                .addGap(42, 42, 42))
        );
        jElementLayout.setVerticalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jElementLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jElementLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblModificar)
                            .addComponent(lblGuardar)))
                    .addComponent(btnAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(scrTabla))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(scrDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(170, 170, 170)
                                        .addComponent(scrCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(72, 72, 72)
                                        .addComponent(scrIndex, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(213, 213, 213)
                                        .addComponent(btnBorrar))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtComi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(163, 163, 163)
                                .addComponent(lblCategoria))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblComi1)
                                .addGap(47, 47, 47)
                                .addComponent(lblComi2))
                            .addComponent(jElement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 55, Short.MAX_VALUE)))
                .addGap(26, 26, 26))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(scrTabla, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jElement, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblComi1)
                            .addComponent(lblComi2))
                        .addGap(37, 37, 37)
                        .addComponent(txtComi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(scrDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(152, 152, 152))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(scrIndex, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblCategoria)
                                .addGap(59, 59, 59)
                                .addComponent(scrCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(26, 26, 26)
                        .addComponent(btnBorrar)
                        .addGap(127, 127, 127))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        setLocationActualy();
    }//GEN-LAST:event_btnExitActionPerformed

    private void txtComiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtComiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtComiActionPerformed

    private void txtComiKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtComiKeyPressed
        if(txtComi.getText().equals("Nuevo nombre de comisión"))
        {
            txtComi.setText(null);
            txtComi.setForeground(Color.black);
        }
    }//GEN-LAST:event_txtComiKeyPressed

    private void txtComiKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtComiKeyReleased
        if(txtComi.getText().equals(""))
        {
            txtComi.setText("Nuevo nombre de comisión");
            txtComi.setForeground(new Color(153, 153, 153));
        }
    }//GEN-LAST:event_txtComiKeyReleased

    private void txaDescripcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txaDescripcionKeyPressed
        //Efecto de textarea
        if(txaDescripcion.getText().equals("Descripción"))
        {
            txaDescripcion.setText(null);
            txaDescripcion.setForeground(Color.black);
        }
    }//GEN-LAST:event_txaDescripcionKeyPressed

    private void txaDescripcionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txaDescripcionKeyReleased
        //Efecto de textarea
        if(txaDescripcion.getText().equals(""))
        {
            txaDescripcion.setText("Descripción");
            txaDescripcion.setForeground(new Color(153, 153, 153));
        }
    }//GEN-LAST:event_txaDescripcionKeyReleased

    private void formComponentMoved(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentMoved
        //Al cambiar de posición

        //Obteniendo tamaño de panel
        width = this.getWidth();
        height = this.getHeight();
        
        ResizePnl();
    }//GEN-LAST:event_formComponentMoved

    private void lstCategoriaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstCategoriaMouseClicked

    }//GEN-LAST:event_lstCategoriaMouseClicked

    private void lstCategoriaValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstCategoriaValueChanged
        //Habilitando boton borrar
        btnBorrar.setEnabled(true);
    }//GEN-LAST:event_lstCategoriaValueChanged

    
    
    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed

        
        if(lstCategoria.getSelectedIndex() != -1){
            
            cComisiones obj = new cComisiones();
            
            try
            {
                obj.setNumcate(Long.valueOf(lstIndex.getModel().getElementAt(lstCategoria.getSelectedIndex())));
                obj.setNumcomi((Long)this.comisionesTabla.getValueAt(tblComi.getSelectedRow(), 1));
                
                //Debe validar la eliminación del registro de la relación
                
                if(obj.buscarRelacion())
                { 
                    if(JOptionPane.showConfirmDialog(null, "¿Desea desvincular la categoría seleccionada de la comisión?", "Atención", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0)
                    {
                        //Borrando registro de comision seleccionada
                        if(obj.eliminarCategoria())
                            JOptionPane.showMessageDialog(null, "Categoría eliminada");
                    
                        IndexModel.remove(lstCategoria.getSelectedIndex());
                        ListModel.remove(lstCategoria.getSelectedIndex());

                        lstIndex.setModel(IndexModel);
                        lstCategoria.setModel(ListModel);

                        //Deshabilitando boton
                        btnBorrar.setEnabled(false);
                    }
                }
            
                else
                {
                    //Al intentar borrar categorias previamente ingresadas
                    JOptionPane.showMessageDialog(null, "La categoría seleccionada no puede ser eliminada \nporque ya hay una petición que la involucra");
                }
            }
            
            catch(Exception e)
            {
                //En caso de que no haya ningún registro seleccionado, no hace contacto con la base
                
                if(JOptionPane.showConfirmDialog(null, "¿Desea desvincular la categoría seleccionada de la comisión?", "Atención", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0)
                    {
                        //Borrando registro de comision seleccionada
                        JOptionPane.showMessageDialog(null, "Categoría eliminada");
                    
                        IndexModel.remove(lstCategoria.getSelectedIndex());
                        ListModel.remove(lstCategoria.getSelectedIndex());

                        lstIndex.setModel(IndexModel);
                        lstCategoria.setModel(ListModel);

                        //Deshabilitando boton
                        btnBorrar.setEnabled(false);
                    }
            }
            
            
            try 
            {
                obj.cn.close();
            } 
        
            catch (SQLException ex) 
            {
                Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        else{
            //Mensaje de error
            JOptionPane.showMessageDialog(null, "Por favor seleccione una categoría a borrar.");
        }
        
        
    }//GEN-LAST:event_btnBorrarActionPerformed

    
    
    private void btnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllActionPerformed
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(2);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto()
                {
                    limpiar();
                    
                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }
    }//GEN-LAST:event_btnAllActionPerformed

    private void lblGuardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGuardarMouseClicked

        //Validando que el label esté habilitado
        if(lblGuardar.isEnabled())
        {
        
            String nombre = txtComi.getText().toString().trim();
            String descripcion = txaDescripcion.getText().toString().trim();
        
            final cComisiones obj = new cComisiones();

            if(!(nombre.equals("") || nombre.equals("Nuevo nombre de categoría") || descripcion.equals("") || descripcion.equals("Descripción")))
            {
            
                //Mensaje de confirmación
                if(JOptionPane.showConfirmDialog(null, "¿Deseas guardar? Recuerda que las comisiones no pueden \nser eliminadas o desactivadas una vez guardadas, y las \nrelaciones con categorías creadas podrían no poder ser \nborradas posteriormente", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
                {
            
                    obj.setComision(nombre);
                    obj.setDescripcion(descripcion);
        
                    if(obj.validarNombreGuardar())
                    {
                        JOptionPane.showMessageDialog(this,"Ya existe una comisión con el nombre ingresado");
                    }
            
                    else
                    {
                        if(obj.guardarComision())
                        {
                    
                            if (!control.isConstructor()) {
                                _Hilos hilo = new _Hilos(3);
                                
                                hilo.establecerAccion(new _Acciones(){
                                    public boolean acto()
                                    {
                                        //Obtiene el id de la comisión recién añadida
                                        obj.seleccionarComision();

                                        for (int i = 0; i <= ListModel.getSize(); i++)
                                        { 
                                            //System.out.println(ListModel.getSize());
                                            try
                                            {
                                                Long cate= Long.valueOf(lstIndex.getModel().getElementAt(i));

                                                obj.setNumcate(cate);

                                                if(obj.guardarOrgcomision()){}

                                                else
                                                {
                                                    JOptionPane.showMessageDialog(null,"Error al guardar categoria");
                                                }
                                            }

                                            catch(Exception e){}

                                        }
                                        
                                        limpiar();
                                        
                                        return true;
                                    }
                                });
                                
                                control.evaluarHilo(hilo);
                            }
                        
                            JOptionPane.showMessageDialog(this,"Comision guardada");
                        }
                
                        else
                        {
                            JOptionPane.showMessageDialog(this, "Error al guardar datos");
                        }
          
                    }
                }
            }

            else
            {
                JOptionPane.showMessageDialog(null, "Por favor rellene todos los campos");
            }
        
            try 
            {
                obj.cn.close();
            } 
        
            catch (SQLException ex) 
            {
                Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_lblGuardarMouseClicked

    private void lblModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblModificarMouseClicked

        //Validando que el label esté habilitado
        if(lblModificar.isEnabled())
        {
        
            String nombre = txtComi.getText().toString().trim();
            String descripcion = txaDescripcion.getText().toString().trim();

            final cComisiones obj= new cComisiones();
        
            //Validando que los campos no estén vacíos
            if(!(nombre.equals("") || nombre.equals("Nuevo nombre de comisión") || descripcion.equals("") || descripcion.equals("Descripción")))
            {
            
                //Mensaje de confirmación
                if(JOptionPane.showConfirmDialog(null, "¿Seguro que deseas modificar el registro seleccionado? \nRecuerda que las relaciones con categorías creadas podrían \nno poder ser borradas posteriormente", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
                {
                
                    final int indice= this.tblComi.getSelectedRow();
                    int cate= this.lstIndex.getSelectedIndex();
                    
                    //Enviando datos a la clase
                    obj.setComision(nombre);
                    obj.setDescripcion(descripcion);
                    obj.setId_comision((Long)this.comisionesTabla.getValueAt(indice, 1));
        
        
                    if(obj.validarNombreModificar())
                    {
                        JOptionPane.showMessageDialog(this,"Ya existe una comisión con el nombre ingresado");
                    }
            
                    else
                    {

                        if(obj.ModificarComision())
                        {
                            if (!control.isConstructor()) {
                                _Hilos hilo = new _Hilos(4);
                                
                                hilo.establecerAccion(new _Acciones(){
                                    public boolean acto()
                                    {
                                        obj.setNumcomi((Long)comisionesTabla.getValueAt(indice, 1));

                                        for(int i = 0; i <= ListModel.size(); i++)
                                        {   
                                            //Aqui pondran el insert, enviando como parametros dos datos:
                                            //Sammy -- el id del concejal que estamos modificando, y ListModel.getElementAt(cont)
                                            //Raul -- el id de la comision que estamos modificando, y ListModel.getElementAt(cont)

                                            //System.out.println(ListModel.getSize());
                                            try
                                            {
                                                Long catX= Long.valueOf(lstIndex.getModel().getElementAt(i));


                                                obj.setNumcate(catX);

                                                //Validando las categorías que ya están ingresadas
                                                if(!(obj.validarCategoria()))
                                                {

                                                    //Ingresando categoría
                                                    if(obj.guardarOrgcomision())
                                                    {
                                                        //esto es solo para ver si va añadiendo las categorias.

                                                    }

                                                    else
                                                    {
                                                        JOptionPane.showMessageDialog(null,"Error al modificar comisión");
                                                    }
                                                }



                                            }

                                            catch(Exception e){}

                                        }

                                        limpiar();
                                        
                                        return true;
                                    }
                                });
                                
                                control.evaluarHilo(hilo);
                            }
                            
                            JOptionPane.showMessageDialog(this,"Comisión modificada");

                            //Luego de haber hecho eso, deben hacer un INSERT a la tabla intermedia por cada nueva 
                            //categoria/comision que se haya añadido a la lista, para ello recomiendo usar la siguiente
                            //estructura:

                            //Deshabilitando modificar
                            lblGuardar.setEnabled(true);
                            lblModificar.setEnabled(false);

                        }

                        else
                        {
                            JOptionPane.showMessageDialog(this, "Error al modificar datos");
                        }

                    }
                
                
                }
            
            }
        
        
            else
            {
                JOptionPane.showMessageDialog(null, "Por favor rellene todos los campos");
            }
        
            try 
            {
                obj.cn.close();
            } 
        
            catch (SQLException ex) 
            {
                Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_lblModificarMouseClicked

    private void lstCategoriaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_lstCategoriaFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_lstCategoriaFocusLost

    private void btnBorrarFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_btnBorrarFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_btnBorrarFocusGained

    private void tblComiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblComiMouseClicked
        // TODO add your handling code here:
        /*for (int i = 0; i < tblComi.getRowCount(); i++) 
          {*/
              
            String comision = "", descripcion = "";
            Long id;

            comision = (String) tblComi.getValueAt(tblComi.getSelectedRow(), 2);
            descripcion = (String) tblComi.getValueAt(tblComi.getSelectedRow(), 3);
            id = (Long) tblComi.getValueAt(tblComi.getSelectedRow(), 1);
            
            txtComi.setText(comision);
            txaDescripcion.setText(descripcion);
            
            
            cComisiones obj = new cComisiones();
            
            obj.setId_comiM(id);
            obj.infoList();
            llenarList();
            
            try 
            {
                obj.cn.close();
            } 
        
            catch (SQLException ex) 
            {
                Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
            }
          //}
        
        //Colores de los textfields
        txtComi.setForeground(Color.black);
        txaDescripcion.setForeground(Color.black);
        
        //Deshabilitando guardar
        lblGuardar.setEnabled(false);
        lblModificar.setEnabled(true);
    }//GEN-LAST:event_tblComiMouseClicked

    private void txtComiKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtComiKeyTyped
        //Solo letras para el textbox
        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'á' || car > 'ú') && (car < 'A' || car > 'Z') && (car < 'Á' || car > 'Ú') 
            &&(car != ',') && (car != '.') && (car!=(char)KeyEvent.VK_BACK_SPACE) && (car!=(char)KeyEvent.VK_SPACE)){
            evt.consume();
        }
    }//GEN-LAST:event_txtComiKeyTyped

    private void tblComiMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblComiMouseReleased
        // TODO add your handling code here:
        
        /*for (int i = 0; i < tblComi.getRowCount(); i++) 
          {*/
             
        //Al hacer click en un registro
        
        String comision = "", descripcion = "";
        final Long id = (Long) tblComi.getValueAt(tblComi.getSelectedRow(), 1);

        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(5);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto()
                {
                    cComisiones obj = new cComisiones();
            
                    obj.setId_comiM(id);
                    obj.infoList();
                    llenarList();

                    try 
                    {
                        obj.cn.close();
                    }
                    catch (SQLException ex){}
                    
                    return true;
                }
            });
        }
        
        
        comision = (String) tblComi.getValueAt(tblComi.getSelectedRow(), 2);
        descripcion = (String) tblComi.getValueAt(tblComi.getSelectedRow(), 3);


        txtComi.setText(comision);
        txaDescripcion.setText(descripcion);

          
        
        //Colores de los textfields
        txtComi.setForeground(Color.black);
        txaDescripcion.setForeground(Color.black);
        
        //Deshabilitando guardar
        lblGuardar.setEnabled(false);
        lblModificar.setEnabled(true);
    }//GEN-LAST:event_tblComiMouseReleased

    private void txaDescripcionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txaDescripcionKeyTyped
        //Solo letras para el textbox
        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'á' || car > 'ú') && (car < 'A' || car > 'Z') && (car < 'Á' || car > 'Ú') 
            &&(car != ',') && (car != '.') && (car != ';') && (car != '-') && (car != '"') && (car != '(') && (car != ')') 
            &&(car!=(char)KeyEvent.VK_BACK_SPACE) && (car!=(char)KeyEvent.VK_SPACE)){
            evt.consume();
        }
    }//GEN-LAST:event_txaDescripcionKeyTyped

    
    
    private void ResizePnl(){
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(10);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto()
                {
                    limpiar();
                    
                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }
        
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(11);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto()
                {
                    cComisiones obj = new cComisiones();
                    //Llenando listas    ESTO IRA EN EL EVENTO EN EL QUE SE SELECCIONE UNA COMISION, NO AQUI
                    // EL 2 DE LA VARIABLE id ES PROVISIONAL, SE LLENARA CON EL ID DE LA COMISION SELECCIONADA
                    Long id = Long.valueOf(0);
                    obj.setId_comiM(id);
                    obj.infoList();
                    llenarList();

                    try{obj.cn.close();}catch (SQLException ex){}

                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }
        
        //Disposiciones
        setLayout(null);
        jHeader.setLayout(null);
        jElement.setLayout(null);
        
        //Configurando imagenes
        btnExit.setIcon(new ImageIcon("Img/Peticiones/Close.png"));
        btnLogo.setIcon(new ImageIcon("Img/Admin/comision24px.png"));
        
        //Posicionando y dando tamaño a componentes:
        
        //la cabecera 
        jHeader.setBounds(0, 0, width, 30);
        btnLogo.setBounds((width / 100), 15 - 12, 24, 24);
        btnExit.setBounds(width - 30, 15 - 12, 24, 24);
        
        //Cuerpo----------------------------------------------------------------
        
        //Tabla
        scrTabla.setBounds(0, jHeader.getHeight(), width, height / 3);  //scrTabla es el padre de tblComi
        tblComi.setBounds(0, jHeader.getHeight(), width, height / 3);
        
        //Barra intermedia
        jElement.setBounds(0, jHeader.getHeight() + scrTabla.getHeight(), width, elementSize);
        
        btnAll.setIcon(new ImageIcon("Img/Reportes/garbage.png"));
        
        int mitad = (elementSize / 2) - (btnSize / 2);
        int K = width / 100;
        
        lblGuardar.setBounds(K, mitad, 80, btnSize);
        lblGuardar.setIcon(new ImageIcon("Img/Admin/guardar24px.png"));
        
        
        //Botones inferiores
        
        lblModificar.setBounds(90, mitad, 80, btnSize);
        lblModificar.setIcon(new ImageIcon("Img/Admin/modificar24px.png"));
        
        btnAll.setBounds(180, mitad, 180, btnSize);
        
        
        //Demás componentes
        //lblComi1.setLocation(width / 20, height / 2);
        //lblComi2.setLocation((width / 20) + 180, height / 2);
        txtComi.setBounds(width / 20, height * 21 /40, width * 18 / 30, height / 17); 
        scrDescripcion.setBounds(width / 20, txtComi.getY() + txtComi.getHeight(), width * 18 / 30, height - txtComi.getY() - txtComi.getHeight() - lblModificar.getHeight() - 5); //scrDescripcion es el padre de txaDescripcion
        txaDescripcion.setBounds(width / 20, txtComi.getY() + txtComi.getHeight(), width * 18 / 30, height - txtComi.getY() - txtComi.getHeight() - lblModificar.getHeight() - 5);
        scbCategoria.setBounds(txtComi.getX() + txtComi.getWidth() + 10, txtComi.getY(), width * 4 / 15, height / 17);
        scrCategoria.setBounds(txtComi.getX() + txtComi.getWidth() + 10, txtComi.getY() + txtComi.getHeight(), width * 4 / 15, height * 9 / 32);
        lstCategoria.setBounds(txtComi.getX() + txtComi.getWidth() + 10, txtComi.getY() + txtComi.getHeight(), width * 4 / 15, height * 9 / 32);
        btnBorrar.setBounds(lstCategoria.getX(), lstCategoria.getY() + lstCategoria.getHeight() + 10, scbCategoria.getWidth(), height / 17);
        lblCategoria.setLocation(scbCategoria.getX(), scbCategoria.getY() - 20);
        
        scrIndex.setBounds(scbCategoria.getX() + scbCategoria.getWidth(), scbCategoria.getY() + scbCategoria.getHeight(), scbCategoria.getWidth(), height / 4); //scrComision es el padre de lstComision
        
        //scrIndex.setVisible(true);
        //lstIndex.setVisible(true);
    
        //Peticiones Title------------------------------------------------------
        int fontHeight = jHeader.getHeight() / 2;
        Font fuente = new Font("Consolas", Font.PLAIN, fontHeight);
        /*lblTitle.setFont(cf.setBembo(0, fontHeight));*/
        lblTitle.setBounds((btnLogo.getX() * 2) + btnLogo.getWidth(), fontHeight - (fontHeight / 2), fontHeight * lblTitle.getText().length(), fontHeight);
    }
    
    //Saber si esta posicionado
    public boolean getLocationActualy()
    {
        return location;
    }
    
    //Darle un valor
    public void setLovationValue(boolean location)
    {
        this.location = location;
        
        if (location) {
            llenarList();
            llenarCMB();
        }
    }
    
    //Cambiar de posicion
    public void setLocationActualy()
    {
        mConcejalVirtual.jComi.setBounds(-2000, -2000, 800, 600);
        setLovationValue(false);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAll;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnLogo;
    private javax.swing.JPanel jElement;
    private javax.swing.JPanel jHeader;
    private javax.swing.JLabel lblCategoria;
    private javax.swing.JLabel lblComi1;
    private javax.swing.JLabel lblComi2;
    private javax.swing.JLabel lblGuardar;
    private javax.swing.JLabel lblModificar;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JList<String> lstCategoria;
    private javax.swing.JList<String> lstIndex;
    private javax.swing.JScrollPane scrCategoria;
    private javax.swing.JScrollPane scrDescripcion;
    private javax.swing.JScrollPane scrIndex;
    private javax.swing.JScrollPane scrTabla;
    private javax.swing.JTable tblComi;
    private javax.swing.JTextArea txaDescripcion;
    private javax.swing.JTextField txtComi;
    // End of variables declaration//GEN-END:variables
}
