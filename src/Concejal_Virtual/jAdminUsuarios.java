/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Extras.jComboBox;
import Conexion.xConexion;
import Extras.jTool;
import Hilos._Acciones;
import Hilos._Control;
import Hilos._Hilos;
import MVC.cUsuarios;
import MVC.sCategoria;
import MVC.sConectar;
import MVC.sConecxion;
import MVC.sPerfil;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author kevin
 */
public class jAdminUsuarios extends javax.swing.JPanel {

    /**
     * Creates new form jAdmin_Usuarios
     */
    
    //Control de hilos
    _Control control = new _Control();
    
    DefaultTableModel usuariosTabla;
    xConexion con=new xConexion();
    
    //Cambiar de posicion
    private boolean location = false;
    
    //Variables que contienen el tamaño del panel
    int width, height;
    
    
    //Variables para hacer consulta por partes:
    //inicio: Inicio de rango de la consulta
    //fin: Fin de rango de la consulta (consulta de 100 en 100)
    //corr: El correlativo de los resultados (no confundir con id)
    private int inicio, corr;
    
    //Variable para ordenar por estado
    private String ordenar = " ORDER BY id ASC";
    
    
    //Combobox
    jComboBox scbTipo = new jComboBox();
    jComboBox scbFiltro = new jComboBox();
    jComboBox scbEstado = new jComboBox();
    
    
    //Variable para filtrar por campos
    String filtro = "";
    
    
    public jAdminUsuarios() {
        
        _Hilos hilo = new _Hilos(1);
        
        //Es el constructor
        control.estrablecerConstructor(true);
        
        hilo.establecerAccion(new _Acciones(){
            public boolean acto() {

                initComponents();
                jHeader2.setLayout(null);


                //Estableciendo el modelo de la tabla
                usuariosTabla=new DefaultTableModel(null,getColumnas()){

                    //Deshabilitando la edición de celdas
                    @Override
                    public boolean isCellEditable(int row, int column) {
                        //all cells false
                        return false;
                    }
                };

                final JScrollBar vBar = jScrollPane1.getVerticalScrollBar();

                vBar.addAdjustmentListener(new AdjustmentListener() 
                {
                    @Override
                    public void adjustmentValueChanged(AdjustmentEvent e) {

                        if(vBar.getValue() >= vBar.getMaximum() - vBar.getModel().getExtent() - 50)
                        {
                            if (!control.isConstructor()) {
                                _Hilos hilo = new _Hilos(4);
                                
                                hilo.establecerAccion(new _Acciones(){
                                    public boolean acto() {
                                        inicio += 100;

                                        setFilas();
                                        
                                        return true;
                                    }
                                });
                                
                                control.evaluarHilo(hilo);
                            }
                        }
                    }
                });
                //Estableciendo límites de la consulta
                inicio = 0;
                corr = 1;

                setFilas();

                //Ingresamos los valores de los combobox
                scbEstado.setModel(new DefaultComboBoxModel ( new String[] { "Activo", "Inactivo"}));
                scbTipo.setModel( new DefaultComboBoxModel( new String[] { "Usuario", "Operador", "Administrador" }));
                scbFiltro.setModel( new DefaultComboBoxModel( new String[] { "Todos", "Nombre", "Correo", "Teléfono", "Dirección", "Nacimiento", "Tipo", "Activo", "Inactivo"}));



                add(scbEstado);
                add(scbTipo);
                add(scbFiltro);



                //Aplicando fuentes

                //Configurando botón
                jTool.btnTransparente(btnLogo2, btnExit2, btnAll);

                //Al cambiar el item del combobox para filtrar, el textfield se debe vaciar
                scbFiltro.addItemListener(new ItemListener()
                {
                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        
                        if (!control.isConstructor()) {
                            _Hilos hilo = new _Hilos(5);
                            
                            hilo.establecerAccion(new _Acciones(){
                                public boolean acto()
                                {
                                    refresh();
                                    
                                    return true;
                                }
                            });
                            
                            control.evaluarHilo(hilo);
                        }
                        
                        txtBuscar.setText(null);
                        txtBuscar.grabFocus();
                    }
                });

                //Saber que se realizo la accion
                return true;
            }
        });

        control.evaluarHilo(hilo);
    }
    
    
    //Establece los encabezados de las columnas
     private String[] getColumnas()
    {
        String columna[]= new String[]{"No.", "Nombre","Correo","Telefono","Direccion","Fecha de Nacimiento","Tipo de Usuario", "ID", "Estado"};
        return columna;
    }
     
    //Llena la tabla
    private void setFilas()
    {
        try{
                    //Consulta la tab_perfil
                    /*String sql="SELECT * FROM (SELECT nombre,e_mail,telefono,direccion,nacimiento,tipousu,id_perfil, id_estado, " +
                               "ROW_NUMBER() OVER (" + ordenar + ") as row " +
                               "FROM tab_perfil,tab_tipousu WHERE tab_perfil.id_tipousu=tab_tipousu.id_tipousu AND tab_tipousu.tipousu != 'Admin Alfa'" + filtro +
                               ") a WHERE row >= " + inicio + " and row <= " + fin;*/
                    
                    String sql = "SELECT name, email, phone, address, birthdate, user_type, id, state "
                            + "FROM users AS u, user_types AS t "
                            + "WHERE u.user_type_id = t.user_type_id AND t.user_type != 'Admin Alfa' "
                            + filtro + ordenar + " LIMIT " + inicio + ", 100";
                    
                    //System.out.println(sql);
                    PreparedStatement us=con.conectar().prepareStatement(sql);
                    ResultSet res=us.executeQuery();

                    //Crea un array que contendrá todos los valores de una fila
                    Object datos[]=new Object[9];

                    //Llena las filas
                    while (res.next())
                    {
                        //correlativo
                        datos[0] = corr;

                        //Llena las columnas
                        for (int i = 1; i < 9; i++) 
                        {
                            if(!(i == 8))
                                datos[i] = res.getObject(i);

                            else
                            {
                                //El campo id_estado debe ser convertido de int a string
                                if(res.getInt(i) == 0)
                                {
                                    datos[i] = "Activo";
                                }

                                else
                                {
                                    datos[i] = "Inactivo";
                                }
                            }
                        }
                        usuariosTabla.addRow(datos);

                        corr++;
                    }
                    res.close();


                    tblUsuarios.setModel(usuariosTabla);

                    //Dando tamaño a la columna del correlativo
                    tblUsuarios.getColumnModel().getColumn(0).setPreferredWidth(50);
                    tblUsuarios.getColumnModel().getColumn(0).setMaxWidth(50);
                    tblUsuarios.getColumnModel().getColumn(0).setMinWidth(50);

                    //Ocultando columna de ID
                    tblUsuarios.getColumnModel().getColumn(7).setWidth(0);
                    tblUsuarios.getColumnModel().getColumn(7).setMinWidth(0);
                    tblUsuarios.getColumnModel().getColumn(7).setMaxWidth(0);

                    //Validando que solo se pueda seleccionar un registro a la vez
                    tblUsuarios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

                    //Fijando la posición de las columnas de la tabla
                    tblUsuarios.getTableHeader().setReorderingAllowed(false);

                    //Deshabilitando navegación con teclado en la tabla
                    InputMap iMap1 = tblUsuarios.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

                    KeyStroke stroke = KeyStroke.getKeyStroke("ENTER");
                    iMap1.put(stroke, "none");
                    stroke = KeyStroke.getKeyStroke("DOWN");
                    iMap1.put(stroke, "none");
                    stroke = KeyStroke.getKeyStroke("UP");
                    iMap1.put(stroke, "none");
                    
                    System.out.println("termino");
                }
                catch(SQLException ex)
                {
                    //Logger.getLogger(jAdminUsuarios.class.getName()).log(Level.SEVERE,null,ex);
                }
    }
    
    
    private void createKeybindings(JTable tblUsuarios) {
tblUsuarios.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
    tblUsuarios.getActionMap().put("Enter", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            //do something on JTable enter pressed
            JOptionPane.showMessageDialog(null, "aa");
        }

    });
}
    

    //Método para actualizar tabla con filtros aplicados
    private void refresh(){
        
        //Refrescando búsqueda
        int fila = tblUsuarios.getRowCount();

                for (int i = 0; fila  > i; i++) {
                    usuariosTabla.removeRow(0);
                }
                
                //Filtros seleccionados
                switch(scbFiltro.getSelectedIndex())
                {
                    case 0:  //Buscar por todos los campos
                        filtro = " AND (name LIKE '%" + txtBuscar.getText().toString().trim() + "%'" +
                                 " OR email LIKE '%" + txtBuscar.getText().toString().trim() + "%'" +
                                 " OR phone LIKE '%" + txtBuscar.getText().toString().trim() + "%'" +
                                 " OR address LIKE '%" + txtBuscar.getText().toString().trim() + "%'" +
                                 " OR birthdate LIKE '%" + txtBuscar.getText().toString().trim() + "%'" +
                                 " OR user_type LIKE '%" + txtBuscar.getText().toString().trim() + "%'" +
                                 " OR state LIKE '%" + txtBuscar.getText().toString().trim() + "%')";
                        
                        ordenar = " ORDER BY id ASC";
                        break;
                        
                    case 1:  //Filtrar por nombre
                        filtro = " AND name LIKE '%" + txtBuscar.getText().toString().trim() + "%'";
                        
                        ordenar = " ORDER BY id ASC";
                        break;
                        
                    case 2:  //Filtrar por correo electrónico
                        filtro = " AND email LIKE '%" + txtBuscar.getText().toString().trim() + "%'";
                        
                        ordenar = " ORDER BY id ASC";
                        break;
                        
                    case 3:  //Filtrar por número de teléfono
                        filtro = " AND phone LIKE '%" + txtBuscar.getText().toString().trim() + "%'";
                        
                        ordenar = " ORDER BY id ASC";
                        break;
                        
                    case 4:  //Filtrar por dirección
                        filtro = " AND address LIKE '%" + txtBuscar.getText().toString().trim() + "%'";
                        
                        ordenar = " ORDER BY id ASC";
                        break;
                        
                    case 5:  //Filtrar por fecha de nacimiento
                        filtro = " AND birhtdate LIKE '%" + txtBuscar.getText().toString().trim() + "%'";
                        
                        ordenar = " ORDER BY id ASC";
                        break;
                        
                    case 6:  //Filtrar por tipo de usuario
                        filtro = " AND user_type LIKE '%" + txtBuscar.getText().toString().trim() + "%'";
                        
                        ordenar = " ORDER BY id ASC";
                        break;
                        
                    case 7:  //Filtrar por usuarios activos
                        ordenar = " ORDER BY state ASC, id";
                        break;
                        
                    case 8:  //Filtrar por usuarios inactivos
                        ordenar = " ORDER BY state DESC, id";
                        break;
                }
         
        //Reinicializando variables de filtro por partes
        inicio = 0;
        corr = 1;
               
        setFilas();
        
        //Seleccionando primera fila
        tblUsuarios.changeSelection(0, 0, false, false);
    }
    
    public void limpiar()
    {
        //Limpiando panel
        lblUsuario2.setText("Ninguno");
        lblCorreo2.setText("Ninguno");
        scbTipo.setSelectedIndex(0);
        scbFiltro.setSelectedIndex(0);
        scbEstado.setSelectedIndex(0);
        txtBuscar.setText(null);
        filtro = "";
        int fila = tblUsuarios.getRowCount();

                for (int i = 0; fila  > i; i++) {
                    usuariosTabla.removeRow(0);
                }
                
        inicio = 0;
        corr = 1;
                
        setFilas();
        
        //Seleccionando primera fila
        tblUsuarios.changeSelection(0, 0, false, false);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblUsuarios = new javax.swing.JTable();
        lblUsuario1 = new javax.swing.JLabel();
        lblUsuario2 = new javax.swing.JLabel();
        lblCorreo1 = new javax.swing.JLabel();
        lblEstado = new javax.swing.JLabel();
        lblTipo = new javax.swing.JLabel();
        lblCorreo2 = new javax.swing.JLabel();
        jHeader2 = new javax.swing.JPanel();
        btnExit2 = new javax.swing.JButton();
        btnLogo2 = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        jElement = new javax.swing.JPanel();
        btnAll = new javax.swing.JButton();
        btnGuardar = new javax.swing.JLabel();
        lblBuscar = new javax.swing.JLabel();
        lblFiltrar = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();

        setBackground(new java.awt.Color(69, 84, 194));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentMoved(java.awt.event.ComponentEvent evt) {
                formComponentMoved(evt);
            }
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        tblUsuarios.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Nombre de usuario", "Correo electrónico", "Tipo de usuario", "Fecha de nacimiento", "Teléfono", "Dirección"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblUsuarios.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tblUsuarios.setGridColor(new java.awt.Color(102, 204, 255));
        tblUsuarios.setSurrendersFocusOnKeystroke(true);
        tblUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblUsuariosMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblUsuariosMouseReleased(evt);
            }
        });
        tblUsuarios.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tblUsuariosKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(tblUsuarios);

        lblUsuario1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblUsuario1.setForeground(new java.awt.Color(255, 255, 255));
        lblUsuario1.setText("Usuario seleccionado:");

        lblUsuario2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblUsuario2.setForeground(new java.awt.Color(174, 174, 175));
        lblUsuario2.setText("Ninguno");

        lblCorreo1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblCorreo1.setForeground(new java.awt.Color(255, 255, 255));
        lblCorreo1.setText("Correo electrónico:");

        lblEstado.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblEstado.setForeground(new java.awt.Color(255, 255, 255));
        lblEstado.setText("Cambiar estado a:");

        lblTipo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblTipo.setForeground(new java.awt.Color(255, 255, 255));
        lblTipo.setText("Cambiar tipo a:");

        lblCorreo2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblCorreo2.setForeground(new java.awt.Color(174, 174, 175));
        lblCorreo2.setText("Ninguno");

        jHeader2.setBackground(new java.awt.Color(25, 31, 79));

        btnExit2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExit2ActionPerformed(evt);
            }
        });

        lblTitle.setBackground(new java.awt.Color(255, 255, 255));
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setText("Gestion usuario");

        javax.swing.GroupLayout jHeader2Layout = new javax.swing.GroupLayout(jHeader2);
        jHeader2.setLayout(jHeader2Layout);
        jHeader2Layout.setHorizontalGroup(
            jHeader2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jHeader2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnLogo2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnExit2)
                .addContainerGap())
            .addGroup(jHeader2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeader2Layout.createSequentialGroup()
                    .addGap(413, 413, 413)
                    .addComponent(lblTitle)
                    .addContainerGap(414, Short.MAX_VALUE)))
        );
        jHeader2Layout.setVerticalGroup(
            jHeader2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnExit2, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
            .addComponent(btnLogo2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jHeader2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeader2Layout.createSequentialGroup()
                    .addGap(7, 7, 7)
                    .addComponent(lblTitle)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jElement.setBackground(new java.awt.Color(33, 41, 103));

        btnAll.setForeground(new java.awt.Color(255, 255, 255));
        btnAll.setText("Limpiar");
        btnAll.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAll.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllActionPerformed(evt);
            }
        });

        btnGuardar.setForeground(new java.awt.Color(255, 255, 255));
        btnGuardar.setText("Guardar cambios");
        btnGuardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGuardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnGuardarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jElementLayout = new javax.swing.GroupLayout(jElement);
        jElement.setLayout(jElementLayout);
        jElementLayout.setHorizontalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jElementLayout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addComponent(btnAll, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(405, 405, 405)
                .addComponent(btnGuardar)
                .addContainerGap(636, Short.MAX_VALUE))
        );
        jElementLayout.setVerticalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jElementLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jElementLayout.createSequentialGroup()
                        .addComponent(btnGuardar)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        lblBuscar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblBuscar.setForeground(new java.awt.Color(255, 255, 255));
        lblBuscar.setText("Buscar:");

        lblFiltrar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblFiltrar.setForeground(new java.awt.Color(255, 255, 255));
        lblFiltrar.setText("Filtrar por:");

        txtBuscar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblUsuario1)
                            .addComponent(lblEstado))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblUsuario2)
                                .addGap(30, 30, 30)
                                .addComponent(lblCorreo1)
                                .addGap(35, 35, 35)
                                .addComponent(lblCorreo2))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblTipo)
                                .addGap(96, 96, 96)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblBuscar)
                                    .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(35, 35, 35)
                                .addComponent(lblFiltrar)))
                        .addGap(85, 748, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jHeader2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jElement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 736, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jHeader2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUsuario1)
                    .addComponent(lblUsuario2)
                    .addComponent(lblCorreo1)
                    .addComponent(lblCorreo2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEstado)
                    .addComponent(lblTipo)
                    .addComponent(lblBuscar)
                    .addComponent(lblFiltrar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
                .addComponent(jElement, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(273, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentMoved(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentMoved
        ResizePnl();
    }//GEN-LAST:event_formComponentMoved

    void ResizePnl(){
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(6);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto()
                {
                    limpiar();
                    
                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }
        
        //Obteniendo tamaño de panel
        width = this.getWidth();
        height = this.getHeight();
        
        //Tamaños estandar
        int btnSize = 24, elementSize = 34;
        int mitad = (elementSize / 2) - (btnSize / 2);
        int K = width / 100;
        
        //Disposiciones
        setLayout(null);
        jHeader2.setLayout(null);
        jElement.setLayout(null);
        
        //Configurando imagenes
        btnExit2.setIcon(new ImageIcon("Img/Peticiones/Close.png"));
        btnLogo2.setIcon(new ImageIcon("Img/Admin/adminUser24px.png"));
        btnGuardar.setIcon(new ImageIcon("Img/Admin/guardar.png"));
        
        //Posicionando y dando tamaño a componentes:
        
        //la cabecera 
        jHeader2.setBounds(0, 0, width, 30);
        btnLogo2.setBounds((width / 100), 15 - 12, 24, 24);
        btnExit2.setBounds(width - 30, 15 - 12, 24, 24);
        
        
        
        //Elementos del data grid
        int heightData = (height - elementSize) / 3;
        int kHeight = height / 100;
        
        //Tabla
        jScrollPane1.setBounds(0, heightData + elementSize, width, (heightData * 2));
        
        //Botones rapidos
        jElement.setBounds(0, jScrollPane1.getY() - elementSize, width, elementSize);
        
        //Posicionar los botones
        btnAll.setIcon(new ImageIcon("Img/Reportes/garbage.png"));
        btnGuardar.setIcon(new ImageIcon("Img/Admin/guardar24px.png"));
        
        
        //Botones inferiores
        btnGuardar.setBounds(K, mitad, 125, btnSize);
        btnAll.setBounds(135, mitad, 180, btnSize);
        
        
        //Componentes superiores
        lblUsuario1.setLocation(width / 75, height / 10);
        lblUsuario2.setBounds((width / 75) + 155, height / 10, (width / 2) - lblUsuario1.getX() - 155, lblUsuario2.getHeight());
        lblCorreo1.setLocation(width / 2, height / 10);
        lblCorreo2.setBounds((width / 2) + 155, height / 10, lblCorreo1.getX() - 155, lblCorreo2.getHeight());
        
        lblEstado.setLocation(width / 75, height * 1 / 6);
        scbEstado.setBounds(width / 75, height * 3 / 15, (width / 4) - 20, scbTipo.getHeight());
        lblTipo.setLocation(lblEstado.getX() + (width / 4), height * 1 / 6);
        scbTipo.setBounds(scbEstado.getX() + (width / 4), height * 3 / 15, (width / 4) - 20, scbTipo.getHeight());
        lblBuscar.setLocation(lblTipo.getX() + (width / 4), height * 1 / 6);
        txtBuscar.setBounds(scbTipo.getX() + (width / 4), height * 3 / 15, (width / 4) - 20, scbTipo.getHeight());
        lblFiltrar.setLocation(lblBuscar.getX() + (width / 4), height * 1 / 6);
        scbFiltro.setBounds(txtBuscar.getX() + (width / 4), height * 3 / 15, (width / 4) - 20, scbFiltro.getHeight());
        
        
        
        //btnGuardar.setBounds(width - (sizeDoc), height - (btnSize + kHeight), sizeDoc, btnSize);
        
        
        //tblUsuarios.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        
        //Usuarios Title------------------------------------------------------
        int fontHeight = jHeader2.getHeight() / 2;
        lblTitle.setFont(new Font("Arial", Font.PLAIN, fontHeight));
        lblTitle.setBounds((btnLogo2.getX() * 2) + btnLogo2.getWidth(), fontHeight - (fontHeight / 2), fontHeight * lblTitle.getText().length(), fontHeight);
    }
    
    
    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        
        
    }//GEN-LAST:event_formComponentResized

    
    private void btnExit2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExit2ActionPerformed
        setLocationActualy();
    }//GEN-LAST:event_btnExit2ActionPerformed

    private void btnGuardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGuardarMouseClicked
        // TODO add your handling code here:
        
        //Modificar el tipo de usuario
        
        //Validando que haya un registro seleccionado
        if(!(tblUsuarios.getSelectedRow() == -1))
        {
            //Mensaje de confirmación
            if(JOptionPane.showConfirmDialog(null, "¿Seguro que deseas modificar el registro seleccionado?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
            {
                try{
            
                    //Validando que el usuario en sesión no este intentando modificarse a sí mismo
                    if(mConcejalVirtual.perfil.getIdPerfil() != Integer.parseInt(tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 7).toString()))
                    {
                
            
                        //Llamando a clase intermedia
                        cUsuarios obj=new cUsuarios();
            
            
                        //Llenando parámetros
                        obj.setTipousuario(scbTipo.getSelectedIndex() + 1);  //Tipo de usuario
                        obj.setEstado(scbEstado.getSelectedIndex());         //Estado
                        obj.setId_perfil(Integer.parseInt(tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 7).toString()));   //Id
            
                        //Haciendo consulta
                        if(obj. ModificarUsuario())
                        {
            
                            DefaultTableModel modelo = (DefaultTableModel)tblUsuarios.getModel();
        
                            int fila = tblUsuarios.getRowCount();
                
                            //Reiniciando el modelo de la tabla
                            for (int i = 0; fila  > i; i++) {
                                modelo.removeRow(0);
                            }
        
                            if (!control.isConstructor()) {
                                _Hilos hilo = new _Hilos(3);
                                
                                hilo.establecerAccion(new _Acciones(){
                                    public boolean acto() {
                                        limpiar();
                                        
                                        return true;
                                    }
                                });
                                
                                control.evaluarHilo(hilo);
                            }
                        
                            JOptionPane.showMessageDialog(this,"Datos modificados");
                        }
                        else
                        {
                            //Error
                            JOptionPane.showMessageDialog(this,"Error al modificar datos");
                        }
                    }
            
                    else{
                        //El usuario en sesión intentó modificarse a sí mismo
                        JOptionPane.showMessageDialog(this,"¡No puedes modificarte a tí mismo!");
                    }
            
                }
        
                catch(Exception e){
                    //No se ha seleccionado ningún registro
                    JOptionPane.showMessageDialog(null, "Error: " + e);
                }
            }
        }
        else
        {
            //No se ha seleccionado ningún registro
                JOptionPane.showMessageDialog(null, "Por favor selecciona un registro a modificar");
        }
        
    }//GEN-LAST:event_btnGuardarMouseClicked

    private void tblUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblUsuariosMouseClicked
        // TODO add your handling code here:
        
        //for (int i = 0; i < tblUsuarios.getRowCount(); i++) 
          {
              
            String nombre = "", correo = "",direccion = "",  tipo_usuario = "", telefono = "", fecha = "", estado = "";

            nombre = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 1).toString();
            correo =  tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 2).toString();
            telefono = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 3).toString();
            direccion = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 4).toString();
            fecha = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 5).toString();
            tipo_usuario = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 6).toString();
            estado = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 8).toString();
            
             
             lblUsuario2.setText(nombre);
             lblCorreo2.setText(correo);
             scbEstado.setSelectedItem(estado);
             scbTipo.setSelectedItem(tipo_usuario);
             
          }
    }//GEN-LAST:event_tblUsuariosMouseClicked

    private void btnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllActionPerformed
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(2);
        
            hilo.establecerAccion(new _Acciones(){
                public boolean acto() {
                    limpiar();
                    return true;
                }
            });

            control.evaluarHilo(hilo);
        }
    }//GEN-LAST:event_btnAllActionPerformed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        // TODO add your handling code here:
        //Creamos el hilo
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(4);
        
            hilo.establecerAccion(new _Acciones(){
                public boolean acto() {
                    refresh();

                    return true;
                }
            });

            control.evaluarHilo(hilo);
        }
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void tblUsuariosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblUsuariosMouseReleased
        // TODO add your handling code here:
        String nombre = "", correo = "",direccion = "",  tipo_usuario = "", telefono = "", fecha = "", estado = "";

        nombre = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 1).toString();
        correo =  tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 2).toString();
        telefono = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 3).toString();
        direccion = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 4).toString();
        fecha = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 5).toString();
        tipo_usuario = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 6).toString();
        estado = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 8).toString();


         lblUsuario2.setText(nombre);
         lblCorreo2.setText(correo);
         scbEstado.setSelectedItem(estado);
         scbTipo.setSelectedItem(tipo_usuario);
    }//GEN-LAST:event_tblUsuariosMouseReleased

    private void tblUsuariosKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblUsuariosKeyTyped
        // TODO add your handling code here:
        
    }//GEN-LAST:event_tblUsuariosKeyTyped

    //Saber si esta posicionado
    public boolean getLocationActualy()
    {
        return location;
    }
    
    //Darle un valor
    public void setLovationValue(boolean location)
    {
        this.location = location;
    }
    
    //Cambiar de posicion
    public void setLocationActualy()
    {
        mConcejalVirtual.jAdmin.setBounds(-2000, -2000, 800, 600);
        setLovationValue(false);
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAll;
    private javax.swing.JButton btnExit2;
    private javax.swing.JLabel btnGuardar;
    private javax.swing.JButton btnLogo2;
    private javax.swing.JPanel jElement;
    private javax.swing.JPanel jHeader2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblBuscar;
    private javax.swing.JLabel lblCorreo1;
    private javax.swing.JLabel lblCorreo2;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblFiltrar;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblUsuario1;
    private javax.swing.JLabel lblUsuario2;
    private javax.swing.JTable tblUsuarios;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables
}
