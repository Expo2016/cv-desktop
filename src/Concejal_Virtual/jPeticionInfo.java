/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import MVC.sCategoria;
import MVC.sConcejales;
import MVC.sConectar;
import MVC.sConecxion;
import MVC.sPerfil;
import MVC.sPeticiones;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author Toshiba
 */
public class jPeticionInfo extends javax.swing.JPanel {

    /**
     * Creates new form jPeticionInfo
     */
    
    //Fuente
    private Font fuenteAcalde = null;
    
    //Paneles de manejo
    jPeticion peticion;
    jPeticionModific peticionModific;
    
    //La imagen
    javax.swing.JPanel imgComision;
    
    //Informacion
    private String usuario;
    private String descripcion; 
    private String categoria;
    private String consejal;
    
    public int getWidth() {
        return width;
    }

    //Tamaño
    public int getHeight() {
        return height;
    }
    int width;
    int height;
    
    //Imagen del usuario
    Image img;
    
    sPeticiones pet;
    
    public jPeticionInfo(int width, int height, jPeticion peticion, jPeticionModific peticionModific, sPeticiones pet){
        initComponents();
        
        this.pet =  pet;
        
        //Le quitamos la disposicion
        this.setLayout(null);
        
        //inicializamos los valores necesarios
        this.width = width;
        this.height = height;
        
        this.peticion = peticion;
        this.peticionModific = peticionModific;
        
        //Propiedades
        //Color
        this.setBackground(new Color(0,162,211));
        
        //Fuente
        lblFecha.setForeground(Color.WHITE);
        lblDescripcion.setForeground(Color.WHITE);
        lblCategoria.setForeground(Color.WHITE);
        lblConsejal.setForeground(Color.WHITE);
        
        this.setUsuario("Usuario: "+(String)sConecxion.getEspecificElement(sConectar.conectar(), sPerfil.getElement(), pet.getId_perfil()));
        this.setCategoria((String)sConecxion.getEspecificElement(sConectar.conectar(), sCategoria.getEspecificElement(), pet.getId_categoria()));
        this.setConsejal((String)sConecxion.getEspecificElement(sConectar.conectar(), sConcejales.getEapecificElement(), pet.getId_concejal()));
        this.setDescripcion("Asunto: "+pet.getAsunto());
    }
    
    //Metodo para inicializar los valores
    public void setImg(Image img)
    {
        this.img = img;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    public void setConsejal(String consejal) {
        this.consejal = consejal;
    }
    
    //imprimer el texto
    public void setPaint()
    {   
        //Apariencia del panel con el icono
        jImg.setBackground(this.getBackground());
        int imgX, imgY, imgWidth, imgHeight;
        
        imgHeight = height * 4 / 5;
        imgWidth = height * 4 / 5;
        imgY = (height / 2) - ((height * 4 / 5) / 2);
        imgX = width / 100;
        
        jImg.setIcon(new ImageIcon(img));
        
        //Borde inferios
        jBorder.setBounds(0, imgY + imgHeight, width, imgY);
        
        //Tamaño de la fuente
        int sizeFont = width / 50;
        
        jImg.setBounds(imgX, imgY, imgWidth, imgHeight - (jBorder.getHeight() / 2));
        
        //Fuente
        Font fuente = new Font("Consolas", Font.PLAIN, sizeFont);
        FontMetrics fM = this.getFontMetrics(fuente);
        //Fuente de los label
        lblFecha.setFont(fuente);
        lblDescripcion.setFont(fuente);
        lblCategoria.setFont(fuente);
        lblConsejal.setFont(fuente);
        
        //Texto de los label
        lblFecha.setText(pet.getFecha().toString());
        lblDescripcion.setText(this.descripcion);
        lblCategoria.setText(this.categoria);
        lblConsejal.setText(this.consejal);
        
        //Tamaño de los labels
        int K = (jImg.getWidth() / 10);
        int widthFont = width / 3;
        
        //Posicionamiento de los label
        //lblName.setBounds(jImg.getX() + jImg.getWidth() + K , height / 100, widthFont, height / 2);
        //lblConsejal.setBounds(lblCategoria.getX() + lblCategoria.getWidth() + K, height / 100, widthFont, height / 2);
        
        //Posicionamiento delos labels
        fuente = new Font("Consolas", Font.PLAIN, width / 60);
        
        lblDescripcion.setFont(fuente);
        lblDescripcion.setVerticalAlignment(JLabel.CENTER);
        lblDescripcion.setBounds((jImg.getX() * 2) + jImg.getWidth() + K , (height / 100), widthFont, height / 3);
        
        int widtFont = fM.stringWidth(this.categoria);
        
        if (widtFont < (width / 3)) {
            widthFont = widtFont + 32;
        }
        else
        {
            widthFont = width / 3;
        }
        
        lblCategoria.setBounds(width - widthFont, height - jBorder.getHeight() - (height / 3), widthFont, height / 3);
        try{lblCategoria.setIcon(new ImageIcon(ImageIO.read(new File("Img/Admin/categoria16px.png"))));}catch(Exception ex){}
        
//        widtFont = fM.stringWidth(this.consejal);
        
        if (widtFont < (width / 3)) {
            widthFont = widtFont + 32;
        }
        else
        {
            widthFont = width / 3;
        }
        
        lblConsejal.setBounds(lblCategoria.getX() - widthFont, height - jBorder.getHeight() - (height / 3), widthFont, height / 3);
        try{lblConsejal.setIcon(new ImageIcon(ImageIO.read(new File("Img/Admin/concejal16px.png"))));}catch(Exception ex){}
        //this.repaint();
        
        widtFont = fM.stringWidth(pet.getFecha().toString());
        
        if (widtFont < (width / 3)) {
            widthFont = widtFont;
        }
        else
        {
            widthFont = width / 3;
        }
        
        this.lblFecha.setBounds(lblDescripcion.getX(), height - jBorder.getHeight() - (height / 3), widthFont, height / 3);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jImg = new org.edisoncor.gui.panel.PanelImage();
        jBorder = new javax.swing.JPanel();
        lblDescripcion = new javax.swing.JLabel();
        lblCategoria = new javax.swing.JLabel();
        lblConsejal = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 162, 211));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        jImg.setBackground(new java.awt.Color(33, 41, 103));

        javax.swing.GroupLayout jImgLayout = new javax.swing.GroupLayout(jImg);
        jImg.setLayout(jImgLayout);
        jImgLayout.setHorizontalGroup(
            jImgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 68, Short.MAX_VALUE)
        );
        jImgLayout.setVerticalGroup(
            jImgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jBorder.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout jBorderLayout = new javax.swing.GroupLayout(jBorder);
        jBorder.setLayout(jBorderLayout);
        jBorderLayout.setHorizontalGroup(
            jBorderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jBorderLayout.setVerticalGroup(
            jBorderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 46, Short.MAX_VALUE)
        );

        lblDescripcion.setText("jLabel1");

        lblCategoria.setText("jLabel1");

        lblConsejal.setText("jLabel1");

        lblFecha.setText("jLabel1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jImg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblDescripcion)
                            .addComponent(lblCategoria)
                            .addComponent(lblConsejal)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(lblFecha)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 354, Short.MAX_VALUE)
                .addComponent(jBorder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(127, 127, 127))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jImg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(lblDescripcion)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                        .addComponent(jBorder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblCategoria)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblConsejal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblFecha)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        peticion.changeVisible(false);
        peticion.canTimeUse = false;
        peticionModific.changeVisible(true, pet);
        peticionModific.setComentarios();
    }//GEN-LAST:event_formMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jBorder;
    private org.edisoncor.gui.panel.PanelImage jImg;
    private javax.swing.JLabel lblCategoria;
    private javax.swing.JLabel lblConsejal;
    private javax.swing.JLabel lblDescripcion;
    private javax.swing.JLabel lblFecha;
    // End of variables declaration//GEN-END:variables
}
