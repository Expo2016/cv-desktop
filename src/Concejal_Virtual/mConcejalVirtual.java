/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Extras.jTool;
import MVC.sPerfil;
import com.alee.laf.WebLookAndFeel;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.UIManager;

/**
 *
 * @author Toshiba
 */
public class mConcejalVirtual extends javax.swing.JFrame {

    /**
     * Creates new form mConcejalVirtual
     */
    
    //Saber que tipo de menu sera
    private boolean administrador = true;
    
    //Paneles de administrador
    public static jAdminUsuarios jAdmin = new jAdminUsuarios();
    public static jComisiones jComi = new jComisiones();
    public static jCategorias jCate = new jCategorias();
    public static jConcejales jConce = new jConcejales();
    public static jAuditoria jAudi;
    
    //Paneles
    jPeticionModific panelBusqueda = new jPeticionModific();
    javax.swing.JPanel panelDibujo;
    jPeticion jPeticion;
    jMenu panelMenu;
    jReportes reportes;
    
    //Botones
    static jDescripcion descripcion = new jDescripcion();
    
    //posicion en el marco del menu
    int menuHaderX, menuHaderY;
    
    //Tamano del formulario
    static int height, width;
    
    //Locacion del menuVersion 2
    int menuX, menuY;
    
    //Bajar
    Timer bajar = new Timer(10, new ActionListener()
    {
        public void actionPerformed(ActionEvent e) {
            if(panelMenu.estado.equals(panelMenu.UP))
            {
                //Cambiar la posicion
                panelMenu.setLocation(0, panelMenu.getY() + 5);
                //System.out.println("Mouse" + panelMenu.getY());
                if(panelMenu.getY() > 0){
                    panelMenu.setLocation(0, 0);
                    stop();
                    panelMenu.arriba = false;
                }
            }
            else if(panelMenu.estado.equals(panelMenu.LEFT))
            {
                //Cambiar la posicion
                panelMenu.setLocation(panelMenu.getX() + 5, 0);
                //System.out.println("Mouse" + panelMenu.getY());
                if(panelMenu.getX() > 0){
                    panelMenu.setLocation(0, 0);
                    stop();
                    panelMenu.arriba = false;
                }
            }
            else if(panelMenu.estado.equals(panelMenu.RIGHT))
            {
                //Cambiar la posicion
                panelMenu.setLocation(panelMenu.getX() - 5, 0);
                //System.out.println("Mouse" + panelMenu.getY());
                if(panelMenu.getX() < width - 40){
                    panelMenu.setLocation(width - 40, 0);
                    stop();
                    panelMenu.arriba = false;
                }
            }
            else if(panelMenu.estado.equals(panelMenu.DOWN))
            {
                //Cambiar la posicion
                panelMenu.setLocation(0, panelMenu.getY() - 5);
                //System.out.println("Mouse" + panelMenu.getY());
                if(panelMenu.getY() < height - 40){
                    panelMenu.setLocation(0, height - 40);
                    stop();
                    panelMenu.arriba = false;
                }
            }
        }
    });
    
    @Override
    public void dispose() {
        super.dispose(); //To change body of generated methods, choose Tools | Templates.
        
        //Cambiar la visibilidad
        jPeticion.setVisible(false);
        panelBusqueda.changeVisible(false);
        reportes.changeVisible(false);
                
            
        if(this.administrador)
        {
            mConcejalVirtual.jAdmin.setLocationActualy();

            mConcejalVirtual.jComi.setLocationActualy();

            mConcejalVirtual.jCate.setLocationActualy();

            mConcejalVirtual.jConce.setLocationActualy();
            
            mConcejalVirtual.jAudi.setLocationActualy();
        }
        
        panelMenu.setHorizontal(panelMenu.UP);
        panelMenu.actualLocacion = panelMenu.UP;
        panelMenu.changeLocation();
    }
    
    //Ocultar
    public void stop()
    {
        bajar.stop();;
    }
    
    //PoderMoverElMenu
    private boolean Location = false;
    public static sPerfil perfil;
    
    public mConcejalVirtual(boolean type,sPerfil perfil) 
    {
        
        
        initComponents();
        mConcejalVirtual.perfil = perfil;
        
        administrador = type;
        
        //Lo inicializamos
        jPeticion = new jPeticion(panelBusqueda);
        panelBusqueda.setPeticion(jPeticion);
        //Poner las opciones
        int screnWindowHeight =  Toolkit.getDefaultToolkit().getScreenSize().height;
        int screnWindowWidth =  Toolkit.getDefaultToolkit().getScreenSize().width;
        this.setBounds(0, 0, screnWindowWidth, screnWindowHeight);
        
        //Poner Icono
        this.setIconImage(Toolkit.getDefaultToolkit().createImage("Img/IniciarSesion/shield(2).png"));
        
        //Medidas
        height = this.getHeight();
        width = this.getWidth();
        
        //Panel reportes
        reportes = new jReportes(width * 4 / 5, height * 4 / 5);
        
        //Menu
        panelMenu = new jMenu(jPeticion, panelBusqueda, reportes, administrador);
        //Caracteristicas del borde
        //borde.setBackground(new Color(183, 183, 183, 0));
        //borde.setBounds(0, 40, 40, height - 40);
        
        //PanelBorde
        
        jAudi = new jAuditoria(width * 1 / 2, height * 4 / 5);
        //Inicializamos el panel
        panelDibujo = new javax.swing.JPanel()
        {
            //Dibujar
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                
                //Dibujar el fondo
                //g.setColor(new Color(41,51,127));
                g.setColor(Color.white);
                g.fillRect(0, 0, width, height);
                
                //Poner icono (tamano)
                int mediaH = (height * 2/5);
                int mediaW = (width * 1/4);
                
                try {
                    g.drawImage(ImageIO.read(new File("Img/IniciarSesion/shield.png")),(width / 2) - (mediaW / 2), (height / 2) - (mediaH / 2), mediaW, mediaH, null);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        };
        
        //Caracteristicas del panel 
        panelDibujo.setLayout(null);
        panelDibujo.setBounds(0, 0, width, height);
        jTool.setLocationComponent(jPeticion, jTool.CENTER, jPeticion.height, jPeticion.width);
        jTool.setLocationComponent(panelBusqueda, jTool.CENTER, panelBusqueda.height, panelBusqueda.width);
        
        //Paneles
        panelMenu.cv = this;
        
        //Aregar los paneles
        this.setLayout(null);
        add(panelDibujo);
        
        
        //Agregar menu   
        panelMenu.setBounds(0, 0, panelMenu.width, panelMenu.height);
        
        descripcion.setVisible(false);
        panelDibujo.add(descripcion);
        panelDibujo.add(panelMenu);
        panelDibujo.add(jPeticion);
        panelDibujo.add(panelBusqueda);
        //panelDibujo.add(borde);
        
        //Visibilidad
        jPeticion.setVisible(false);
        panelBusqueda.setVisible(false);
        
        //Paneles para el maenjo de reportes
        jTool.setLocationComponent(reportes, jTool.CENTER, reportes.getHeight(), reportes.getWidth());
        panelDibujo.add(reportes);
        
        reportes.changeVisible(false);
        
        //Las opciones del admi
        panelDibujo.add(jAdmin);
        panelDibujo.add(jComi);
        panelDibujo.add(jCate);
        panelDibujo.add(jConce);
        panelDibujo.add(jAudi);
        
        jAudi.setLocation(10000, 10000);
    }
    
    public mConcejalVirtual() {
        
        
        initComponents();
        
        //Lo inicializamos
        jPeticion = new jPeticion(panelBusqueda);
        panelBusqueda.setPeticion(jPeticion);
        //Poner las opciones
        int screnWindowHeight =  Toolkit.getDefaultToolkit().getScreenSize().height;
        int screnWindowWidth =  Toolkit.getDefaultToolkit().getScreenSize().width;
        this.setBounds(0, 0, screnWindowWidth, screnWindowHeight);
        
        //Poner Icono
        this.setIconImage(Toolkit.getDefaultToolkit().createImage("Img/IniciarSesion/shield(2).png"));
        
        //Medidas
        height = this.getHeight();
        width = this.getWidth();
        
        //Panel reportes
        reportes = new jReportes(width * 4 / 5, height * 4 / 5);
        
        //Menu
        panelMenu = new jMenu(jPeticion, panelBusqueda, reportes, administrador);
        //Caracteristicas del borde
        //borde.setBackground(new Color(183, 183, 183, 0));
        //borde.setBounds(0, 40, 40, height - 40);
        
        //PanelBorde
        
        
        //Inicializamos el panel
        panelDibujo = new javax.swing.JPanel()
        {
            //Dibujar
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                
                //Dibujar el fondo
                //g.setColor(new Color(41,51,127));
                g.setColor(Color.white);
                g.fillRect(0, 0, width, height);
                
                //Poner icono (tamano)
                int mediaH = (height * 2/5);
                int mediaW = (width * 1/4);
                
                try {
                    g.drawImage(ImageIO.read(new File("Img/IniciarSesion/shield.png")),(width / 2) - (mediaW / 2), (height / 2) - (mediaH / 2), mediaW, mediaH, null);
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        };
        
        //Caracteristicas del panel 
        panelDibujo.setLayout(null);
        panelDibujo.setBounds(0, 0, width, height);
        jTool.setLocationComponent(jPeticion, jTool.CENTER, jPeticion.height, jPeticion.width);
        jTool.setLocationComponent(panelBusqueda, jTool.CENTER, panelBusqueda.height, panelBusqueda.width);
        
        //Paneles
        panelMenu.cv = this;
        
        //Aregar los paneles
        this.setLayout(null);
        add(panelDibujo);
        
        
        //Agregar menu   
        panelMenu.setBounds(0, 0, panelMenu.width, panelMenu.height);
        
        panelDibujo.add(panelMenu);
        panelDibujo.add(jPeticion);
        panelDibujo.add(panelBusqueda);
        //panelDibujo.add(borde);
        
        //Visibilidad
        jPeticion.setVisible(false);
        panelBusqueda.setVisible(false);
        
        //Paneles para el maenjo de reportes
        jTool.setLocationComponent(reportes, jTool.CENTER, reportes.getHeight(), reportes.getWidth());
        panelDibujo.add(reportes);
        
        reportes.changeVisible(false);
        
        //Las opciones del admi
        panelDibujo.add(jAdmin);
        panelDibujo.add(jComi);
        panelDibujo.add(jCate);
        panelDibujo.add(jConce);
        panelDibujo.add(jAudi);
        
        jAudi.setLocation(10000, 10000);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Consejal virtual");
        setUndecorated(true);
        addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                formMouseWheelMoved(evt);
            }
        });
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                formMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                formMouseReleased(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 422, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseEntered
        
    }//GEN-LAST:event_formMouseEntered

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        ///Location
        if((evt.getY() > 0 && evt.getY() < panelMenu.height) && panelMenu.estado.equals(panelMenu.UP))
        {
           if(panelMenu.arriba) bajar.start();
        }
        else if((evt.getX() > 0 && evt.getX() < 40) && panelMenu.estado.equals(panelMenu.LEFT))
        {
            
           if(panelMenu.arriba){ 
               bajar.start();
           }
        }
        else if((evt.getX() > width - 40 && evt.getX() < width) && panelMenu.estado.equals(panelMenu.RIGHT))
        {
            
           if(panelMenu.arriba){ 
               bajar.start();
           }
        }
        if((evt.getY() > (height - 42) && evt.getY() < height) && panelMenu.estado.equals(panelMenu.DOWN))
        {
           if(panelMenu.arriba) bajar.start();
        }
    }//GEN-LAST:event_formMouseClicked

    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
        //Precsionar
        /*
        if((evt.getX() > panelMenuV2.getX() && evt.getX() < (panelMenuV2.getX() + panelMenuV2.width)) && (evt.getY() > panelMenuV2.getY() && evt.getY() < (panelMenuV2.getY() + 30)))
        {
            Location = true;
            menuHaderX = evt.getX() - panelMenuV2.getX();
            menuHaderY = evt.getY() - panelMenuV2.getY();
            
            panelMenuV2.setLocation(evt.getX() - menuHaderX, evt.getY() - menuHaderY);
        }
        */
    }//GEN-LAST:event_formMousePressed
    
    private void formMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseReleased
        //Soltar
        /*
        if(Location)
        {
            Location = false;
            System.out.println("Width = "+width);
            if((evt.getX() > (panelMenuV2.width / 3) && evt.getX() < (width - (panelMenuV2.width / 3))) && (evt.getY() > panelMenu.height && evt.getY() < (height - panelMenuV2.height)))
            {
                panelMenuV2.setLocation(evt.getX() - menuHaderX, evt.getY() - menuHaderY);
            }
            else
            {
                if((evt.getX() > 0 && evt.getX() < (panelMenuV2.width / 3)) && (evt.getY() > 0 && (evt.getY() < height)) && !panelMenu.estado.equals(panelMenu.LEFT))
                {
                   panelMenu.setVisible(true);
                   panelMenuV2.setVisible(false);
                   panelMenu.setHorizontal(panelMenu.LEFT);
                   panelMenuV2.setBounds((width / 2) - (panelMenuV2.width / 2), (height / 2) - (panelMenuV2.height / 2), panelMenuV2.width, panelMenuV2.height);
                }
                else if((evt.getX() > 0 && evt.getX() < width) && (evt.getY() > 0 && (evt.getY() < 42)) && !panelMenu.estado.equals(panelMenu.UP))
                {
                    panelMenu.setVisible(true);
                    panelMenuV2.setVisible(false);
                    panelMenu.setVertical(panelMenu.UP);
                    panelMenuV2.setBounds((width / 2) - (panelMenuV2.width / 2), (height / 2) - (panelMenuV2.height / 2), panelMenuV2.width, panelMenuV2.height);
                }
                else if((evt.getX() > width -(panelMenuV2.width / 3) && evt.getX() < width) && (evt.getY() > 0 && (evt.getY() < height)) && !panelMenu.estado.equals(panelMenu.RIGHT))
                {
                   panelMenu.setVisible(true);
                   panelMenuV2.setVisible(false);
                   panelMenu.setHorizontalInversa(panelMenu.RIGHT);
                   panelMenuV2.setBounds((width / 2) - (panelMenuV2.width / 2), (height / 2) - (panelMenuV2.height / 2), panelMenuV2.width, panelMenuV2.height);
                }
                else if((evt.getX() > 0 && evt.getX() < width) && (evt.getY() > (height - 42) && (evt.getY() < height)) && !panelMenu.estado.equals(panelMenu.DOWN))
                {
                    panelMenu.setVisible(true);
                    panelMenuV2.setVisible(false);
                    panelMenu.setVerticalInversa(panelMenu.DOWN);
                    panelMenuV2.setBounds((width / 2) - (panelMenuV2.width / 2), (height / 2) - (panelMenuV2.height / 2), panelMenuV2.width, panelMenuV2.height);
                }
            }
        }
        */
    }//GEN-LAST:event_formMouseReleased

    private void formMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_formMouseWheelMoved
        
    }//GEN-LAST:event_formMouseWheelMoved

    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
       
    }//GEN-LAST:event_formMouseMoved

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        
    }//GEN-LAST:event_formKeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        
    }//GEN-LAST:event_formWindowClosed
    //Opciones de posicionamiento
    //Llamando paneles
    
    public static void adminUsuarios(){
        //Mostrando panel de administrar usuarios
        jAdmin.setBounds(width / 10, height / 10, width * 4 / 5, height * 4 / 5);
    }
    
    public static void mantenimientoComisiones(){
        //Mostrando panel de administrar categorías
        if(width > 1000)
            jComi.setBounds(width / 4, height / 10, width / 2, height * 3 / 4);
        else
            jComi.setBounds(width / 6, height / 10, width * 2 / 3, height * 3 / 4);
    }
    
    public static void mantenimientoCategorias(){
        //Mostrando panel de administrar categorías
        if(width > 1100)
            jCate.setBounds(width / 3, height / 10, width / 3, height * 3 / 4);
        else
            jCate.setBounds(width / 4, height / 10, width / 2, height * 3 / 4);
    }
    
    public static void mantenimientoConcejales(){
        //Mostrando panel de administrar concejales
        if(width > 1000)
            jConce.setBounds(width * 2 / 10, height / 10, width * 3 / 5, height * 4 / 5);
        else
            jConce.setBounds(width / 20, height / 10, width * 9 / 10, height * 4 / 5);
        /*jConce.setVisible(true);
        jConce.ResizePnl();*/
    }
    
    public static void mantenimientoAuditoria(){
        //Mostrando panel de administrar concejales
        jAudi.setSize(width * 1 / 2, height * 4 / 5);
        jTool.setLocationComponent(jAudi, jTool.CENTER, height * 4 / 5, width * 1 / 2);
        /*jConce.setVisible(true);
        jConce.ResizePnl();*/
    }
    
    //Crear hilos
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(mConcejalVirtual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(mConcejalVirtual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(mConcejalVirtual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(mConcejalVirtual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new mConcejalVirtual().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
