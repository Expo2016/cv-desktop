                                                                        /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Conexion.xConexion;
import Extras.jComboBox;
import Extras.jCustomFont;
import Extras.jTool;
import Hilos._Acciones;
import Hilos._Control;
import Hilos._Hilos;
import MVC.cCategorias;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author kevin
 */

public class jCategorias extends javax.swing.JPanel {

    /**
     * Creates new form jCategorias
     */
    _Control control = new _Control();
    DefaultTableModel CategoriaTabla;
    xConexion con = new xConexion();
    
    
    //Cambiar de posicion
    private boolean location = false;
    
    
    //Instanciando clase CustomFont
    //final jCustomFont cf = new jCustomFont();
    
    //Tamaños estandar
    private int btnSize = 24, elementSize = 34;
    
    int id = 0;
    
    //Combobox de estados
    jComboBox scbEstado = new jComboBox();
    
    
    public jCategorias() {
        
        _Hilos hilo = new _Hilos(1);
        
        control.estrablecerConstructor(true);
        
        hilo.establecerAccion(new _Acciones(){
            public boolean acto() {

                initComponents();
        
                //Para el jTable para ver las categorias
                CategoriaTabla = new DefaultTableModel(null, getColumnas()){

                    //Deshabilitando la edición de celdas
                    @Override
                    public boolean isCellEditable(int row, int column) {
                        //all cells false
                        return false;
                    }
                };

                setFilas();

                //Configurando botones
                jTool.btnTransparente(btnExit, btnLogo, btnAll);


                //Estableciendo modelo del combobox de estados
                scbEstado.setModel(new DefaultComboBoxModel (new String[] {"Activo", "Inactivo"}));

                //Añadiendo combobox
                add(scbEstado);

                /*
                //Aplicando fuentes
                lblCate1.setFont(cf.setMyriad(1, 17));
                lblCate2.setFont(cf.setBembo(1, 17));
                txtCate.setFont(cf.setMyriad(1, 20));
                txaDescripcion.setFont(cf.setBembo(1, 20));
                tblCate.setFont(cf.setMyriad(0, 12));
                */

                //Saber que se realizo la accion
                return true;
            }
        });

        control.evaluarHilo(hilo);
    }
    
    
    //Para el numero de columnas del jTable
    private String[] getColumnas(){
        String columna[] = new String[]{"No.", "Categoria", "Descripcion", "Id", "Estado"};
        return columna;
    }
    //Llena la tabla
    private void setFilas()
    {
            
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        try{
            String sql = "SELECT category, category_description, category_id, category_status FROM categories ORDER BY category_status";

            PreparedStatement us = con.conectar().prepareStatement(sql);
            ResultSet res = us.executeQuery();

            Object datos[] = new Object[5]; //Numero de columnas de la cosulta

            //El correlativo de la tabla
            int corr = 1;

            //Llena las dilas
            while (res.next()) {

                datos[0] = corr;

                //Llena las columnas
                for (int i = 1; i < 5; i++) {

                    if(!(i == 4))
                        datos[i] = res.getObject(i);

                    else
                    {
                        //El campo id_EstCategoria debe ser convertido de int a string
                        if(res.getInt(i) == 0)
                        {
                            datos[i] = "Activo";
                        }

                        else
                        {
                            datos[i] = "Inactivo";
                        }
                    }
                }
                CategoriaTabla.addRow(datos);

                corr++;
            }

            res.close();


            tblCate.setModel(CategoriaTabla);

            //Dando tamaño a la columna del correlativo
            tblCate.getColumnModel().getColumn(0).setWidth(50);
            tblCate.getColumnModel().getColumn(0).setMinWidth(50);
            tblCate.getColumnModel().getColumn(0).setMaxWidth(50);

            //Ocultando columna de ID
            tblCate.getColumnModel().getColumn(3).setWidth(0);
            tblCate.getColumnModel().getColumn(3).setMinWidth(0);
            tblCate.getColumnModel().getColumn(3).setMaxWidth(0);

            //Validando que solo se pueda seleccionar un registro a la vez
            tblCate.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

            //Fijando la posición de las columnas de la tabla
            tblCate.getTableHeader().setReorderingAllowed(false);

            //Deshabilitando navegación con teclado en la tabla
            InputMap iMap1 = tblCate.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

            KeyStroke stroke = KeyStroke.getKeyStroke("ENTER");
            iMap1.put(stroke, "none");
            stroke = KeyStroke.getKeyStroke("DOWN");
            iMap1.put(stroke, "none");
            stroke = KeyStroke.getKeyStroke("UP");
            iMap1.put(stroke, "none");

        }
        catch (SQLException ex) {
            Logger.getLogger(jCategorias.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("setFilas");
        }
                
    }
    

    //Variables que contienen el tamaño del panel
    int width, height;
    
    /**
     * This metlhod is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jHeader = new javax.swing.JPanel();
        btnExit = new javax.swing.JButton();
        btnLogo = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCate = new javax.swing.JTable();
        lblEstado = new javax.swing.JLabel();
        txtCate = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        txaDescripcion = new javax.swing.JTextArea();
        jElement = new javax.swing.JPanel();
        btnAll = new javax.swing.JButton();
        lblGuardar = new javax.swing.JLabel();
        lblModificar = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 162, 211));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentMoved(java.awt.event.ComponentEvent evt) {
                formComponentMoved(evt);
            }
        });

        jHeader.setBackground(new java.awt.Color(12, 22, 71));

        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        lblTitle.setBackground(new java.awt.Color(255, 255, 255));
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setText("Gestion Categoria");

        javax.swing.GroupLayout jHeaderLayout = new javax.swing.GroupLayout(jHeader);
        jHeader.setLayout(jHeaderLayout);
        jHeaderLayout.setHorizontalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnLogo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnExit)
                .addContainerGap())
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addGap(274, 274, 274)
                    .addComponent(lblTitle)
                    .addContainerGap(275, Short.MAX_VALUE)))
        );
        jHeaderLayout.setVerticalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnExit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnLogo, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addGap(8, 8, 8)
                    .addComponent(lblTitle)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        tblCate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblCate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCateMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblCateMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tblCate);

        lblEstado.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblEstado.setForeground(new java.awt.Color(255, 255, 255));
        lblEstado.setText("Cambiar estado a:");

        txtCate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtCate.setForeground(new java.awt.Color(153, 153, 153));
        txtCate.setText("Nuevo nombre de categoría");
        txtCate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCateActionPerformed(evt);
            }
        });
        txtCate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCateKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCateKeyTyped(evt);
            }
        });

        txaDescripcion.setColumns(20);
        txaDescripcion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txaDescripcion.setForeground(new java.awt.Color(153, 153, 153));
        txaDescripcion.setRows(5);
        txaDescripcion.setText("Descripción");
        txaDescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txaDescripcionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txaDescripcionKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txaDescripcionKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(txaDescripcion);

        jElement.setBackground(new java.awt.Color(12, 22, 71));

        btnAll.setForeground(new java.awt.Color(255, 255, 255));
        btnAll.setText("Limpiar");
        btnAll.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAll.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllActionPerformed(evt);
            }
        });

        lblGuardar.setForeground(new java.awt.Color(255, 255, 255));
        lblGuardar.setText("Guardar");
        lblGuardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblGuardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblGuardarMouseClicked(evt);
            }
        });

        lblModificar.setForeground(new java.awt.Color(255, 255, 255));
        lblModificar.setText("Modificar");
        lblModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblModificar.setEnabled(false);
        lblModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblModificarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jElementLayout = new javax.swing.GroupLayout(jElement);
        jElement.setLayout(jElementLayout);
        jElementLayout.setHorizontalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jElementLayout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addComponent(btnAll, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblGuardar)
                .addGap(18, 18, 18)
                .addComponent(lblModificar)
                .addGap(66, 66, 66))
        );
        jElementLayout.setVerticalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jElementLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jElementLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblModificar)
                            .addComponent(lblGuardar)))
                    .addComponent(btnAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jHeader, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtCate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 296, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jElement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(lblEstado)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jElement, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(lblEstado)
                .addGap(33, 33, 33)
                .addComponent(txtCate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(217, 217, 217))
        );
    }// </editor-fold>//GEN-END:initComponents

    
    public void limpiar(){
        //Regresando textfields a valores por defecto
        txtCate.setText("Nuevo nombre de categoría");
        txtCate.setForeground(new Color(153, 153, 153));
        
        txaDescripcion.setText("Descripción");
        txaDescripcion.setForeground(new Color(153, 153, 153));
        
        scbEstado.setSelectedIndex(0);
        
        
        
        DefaultTableModel modelo = (DefaultTableModel)tblCate.getModel();
        
        int fila = tblCate.getRowCount();

        for (int i = 0; fila  > i; i++) 
        {
            modelo.removeRow(0);
        }
        
        setFilas();
        
        //Deshabilitando modificar
        lblGuardar.setEnabled(true);
        lblModificar.setEnabled(false);
        
        //Seleccionando primera fila
        tblCate.changeSelection(0, 0, false, false);
        
    }
    
    
    private void formComponentMoved(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentMoved
        //Al cambiar de posición

        //Obteniendo tamaño de panel
        width = this.getWidth();
        height = this.getHeight();
        
        ResizePnl();
        
    }//GEN-LAST:event_formComponentMoved

    private void txtCateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCateActionPerformed

    private void txtCateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCateKeyPressed
        if(txtCate.getText().equals("Nuevo nombre de categoría"))
        {
            txtCate.setText(null);
            txtCate.setForeground(Color.black);
        }
    }//GEN-LAST:event_txtCateKeyPressed

    private void txtCateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCateKeyReleased
        if(txtCate.getText().equals(""))
        {
            txtCate.setText("Nuevo nombre de categoría");
            txtCate.setForeground(new Color(153, 153, 153));
        }
    }//GEN-LAST:event_txtCateKeyReleased

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        setLocationActualy();
    }//GEN-LAST:event_btnExitActionPerformed

    private void txaDescripcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txaDescripcionKeyPressed
        //Efecto de textarea
        if(txaDescripcion.getText().equals("Descripción"))
        {
            txaDescripcion.setText(null);
            txaDescripcion.setForeground(Color.black);
        }
    }//GEN-LAST:event_txaDescripcionKeyPressed

    private void txaDescripcionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txaDescripcionKeyReleased
        //Efecto de textarea
        if(txaDescripcion.getText().equals(""))
        {
            txaDescripcion.setText("Descripción");
            txaDescripcion.setForeground(new Color(153, 153, 153));
        }
    }//GEN-LAST:event_txaDescripcionKeyReleased

    private void btnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllActionPerformed

        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(2);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto()
                {
                    limpiar();
                    
                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }
        
        //Deshabilitando modificar
        lblGuardar.setEnabled(true);
        lblModificar.setEnabled(false);
        
    }//GEN-LAST:event_btnAllActionPerformed

    private void lblGuardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGuardarMouseClicked
        
        //Validando que el label esté habilitado
        if(lblGuardar.isEnabled())
        {
        
            String nombre = txtCate.getText().toString().trim();
            String descripcion = txaDescripcion.getText().toString().trim();
            int estado = scbEstado.getSelectedIndex();
            
            if(!(nombre.equals("") || nombre.equals("Nuevo nombre de categoría") || descripcion.equals("") || descripcion.equals("Descripción")))
            {
            
                //Mensaje de confirmación
                if(JOptionPane.showConfirmDialog(null, "¿Deseas guardar?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
                {
            
                    cCategorias obj = new cCategorias();
                
                    //obj.setCodigoCate(Integer.toString(txtCodigo.getText()));
                    obj.setCategoria(nombre);
                    obj.setDescripcion(descripcion);
                    obj.setEstado(estado);
                    
                    if(obj.validarNombreGuardar())
                    {
                        //Nombre repetido
                        JOptionPane.showMessageDialog(this,"Ya existe una categoría con el nombre ingresado");
                    }
            
                    else
                    {
            
                        //String txt1 = "", txt2 = "";
            
                        if(obj.guardarCategoria())
                        {
                            if (!control.isConstructor()) {
                                _Hilos hilo= new _Hilos(3);
                                
                                hilo.establecerAccion(new _Acciones(){
                                    public boolean acto()
                                    {
                                        limpiar();
                                        
                                        return true;
                                    }
                                });
                                
                                control.evaluarHilo(hilo);
                            }
                            
                            JOptionPane.showMessageDialog(this, "Categoría guardada");
                            /*txt1 = txtCate.getText();
                            txt2 = txaDescripcion.getText();*/
                        }
                
                        else
                        {
                            JOptionPane.showMessageDialog(this, "Error al guardar datos");
                        }
            
                        /*obj = new cCategorias();
                        obj.setCategoria(txt1);
                        obj.setDescripcion(txt2);
                        obj.consultarCategoria();*/
            
                    
            
            
                    }
                }
            }
        
            else
            {
                JOptionPane.showMessageDialog(null, "Por favor rellene todos los campos");
        
            }  
        }
    }//GEN-LAST:event_lblGuardarMouseClicked

    private void lblModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblModificarMouseClicked

        //Validando que el label esté habilitado
        if(lblModificar.isEnabled())
        {
        
            String nombre = txtCate.getText().toString().trim();
            String descripcion = txaDescripcion.getText().toString().trim();
            int estado = scbEstado.getSelectedIndex();
        
        
            if(!(nombre.equals("") || nombre.equals("Nuevo nombre de categoría") || descripcion.equals("") || descripcion.equals("Descripción")))
            {
            
                //Mensaje de confirmación
                if(JOptionPane.showConfirmDialog(null, "¿Seguro que deseas modificar el registro seleccionado?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
                {
            
                    cCategorias obj = new cCategorias();
                    obj.setCategoria(txtCate.getText());
                    obj.setDescripcion(txaDescripcion.getText());
                    obj.setEstado(estado);
                    obj.setCodigoCate((Long)tblCate.getValueAt(tblCate.getSelectedRow(), 3));
            
                    
                    if(obj.validarNombreModificar())
                    {
                        //Nombre repetido
                        JOptionPane.showMessageDialog(this,"Ya existe una categoría con el nombre ingresado");
                    }
            
                    else
                    {

                        if(obj.modificarCategoria())
                        {
                            if (!control.isConstructor()) {
                                _Hilos hilo = new _Hilos(4);
                                
                                hilo.establecerAccion(new _Acciones(){
                                    public boolean acto()
                                    {
                                        limpiar();
                                        
                                        return true;
                                    }
                                });
                                
                                control.evaluarHilo(hilo);
                            }
                            
                            JOptionPane.showMessageDialog(this, "Categoría modificada");

                        }

                        else
                        {
                            JOptionPane.showMessageDialog(this, "Error al modificar datos");
                        }
                    }
                }
            }
        
            else
            {
                JOptionPane.showMessageDialog(null, "Por favor rellene todos los campos");
            }
        }
    }//GEN-LAST:event_lblModificarMouseClicked

    private void tblCateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCateMouseClicked
        
        String categorias="", descripcion="", estado="";//codigo="";
            categorias  = (String)tblCate.getValueAt(tblCate.getSelectedRow(), 1);
            descripcion  = (String)tblCate.getValueAt(tblCate.getSelectedRow(), 2);
            estado = (String)tblCate.getValueAt(tblCate.getSelectedRow(), 4);
            //codigo = (String)tblCate.getValueAt(tblCate.getSelectedRow(), 2);
            
            txtCate.setText(categorias);
            txaDescripcion.setText(descripcion);
            scbEstado.setSelectedItem(estado);
            //txtCodigoCate.setText(codigo);
        
            //Cambio de color de textfields
            txtCate.setForeground(Color.black);
            txaDescripcion.setForeground(Color.black);
        
        //Deshabilitando guardar
        lblGuardar.setEnabled(false);
        lblModificar.setEnabled(true);
    }//GEN-LAST:event_tblCateMouseClicked

    
    private void txtCateKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCateKeyTyped
        //Solo letras para el textbox
        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'á' || car > 'ú') && (car < 'A' || car > 'Z') && (car < 'Á' || car > 'Ú') 
            &&(car != ',') && (car != '.') && (car!=(char)KeyEvent.VK_BACK_SPACE) && (car!=(char)KeyEvent.VK_SPACE)){
            evt.consume();
        }
    }//GEN-LAST:event_txtCateKeyTyped

    private void txaDescripcionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txaDescripcionKeyTyped
        //Solo letras para el textbox
        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'á' || car > 'ú') && (car < 'A' || car > 'Z') && (car < 'Á' || car > 'Ú') 
            &&(car != ',') && (car != '.') && (car != ';') && (car != '-') && (car != '"') && (car != '(') && (car != ')') 
            &&(car!=(char)KeyEvent.VK_BACK_SPACE) && (car!=(char)KeyEvent.VK_SPACE)){
            evt.consume();
        }
    }//GEN-LAST:event_txaDescripcionKeyTyped

    private void tblCateMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCateMouseReleased
            String categorias="", descripcion="", estado="";//codigo="";
            categorias  = (String)tblCate.getValueAt(tblCate.getSelectedRow(), 1);
            descripcion  = (String)tblCate.getValueAt(tblCate.getSelectedRow(), 2);
            estado = (String)tblCate.getValueAt(tblCate.getSelectedRow(), 4);
            //codigo = (String)tblCate.getValueAt(tblCate.getSelectedRow(), 2);

            txtCate.setText(categorias);
            txaDescripcion.setText(descripcion);
            scbEstado.setSelectedItem(estado);
            //txtCodigoCate.setText(codigo);

            //Cambio de color de textfields
            txtCate.setForeground(Color.black);
            txaDescripcion.setForeground(Color.black);

            //Deshabilitando guardar
            lblGuardar.setEnabled(false);
            lblModificar.setEnabled(true);
        
            
    }//GEN-LAST:event_tblCateMouseReleased
    
    
    //Método para posicionar y dar tamaño a los componentes del panel
    
    private void ResizePnl(){
        
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(5);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto()
                {
                    limpiar();
                    
                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }
        
        //Disposiciones
        setLayout(null);
        jHeader.setLayout(null);
        jElement.setLayout(null);
        
        //Configurando imagenes
        btnExit.setIcon(new ImageIcon("Img/Peticiones/Close.png"));
        btnLogo.setIcon(new ImageIcon("Img/Admin/categoria24px.png"));
        
        //Posicionando y dando tamaño a componentes:
        
        //la cabecera 
        jHeader.setBounds(0, 0, width, 30);
        btnLogo.setBounds((width / 100), 15 - 12, 24, 24);
        btnExit.setBounds(width - 30, 15 - 12, 24, 24);
        
        //Cuerpo----------------------------------------------------------------
        
        //Tabla
        jScrollPane1.setBounds(0, jHeader.getHeight(), width, height / 3);  //jScrollPane1 es el padre de tblCate
        tblCate.setBounds(0, height / 10, width, height / 3);
        
        
        //Barra intermedia
        jElement.setBounds(0, jHeader.getHeight() + jScrollPane1.getHeight(), width, elementSize);
        
        
        btnAll.setIcon(new ImageIcon("Img/Reportes/garbage.png"));
        
        int mitad = (elementSize / 2) - (btnSize / 2);
        int K = width / 100;
        
        
        //Botones inferiores
        lblGuardar.setBounds(K, mitad, 80, btnSize);
        lblGuardar.setIcon(new ImageIcon("Img/Admin/guardar24px.png"));
        
        lblModificar.setBounds(90, mitad, 80, btnSize);
        lblModificar.setIcon(new ImageIcon("Img/Admin/modificar24px.png"));
        
        btnAll.setBounds(180, mitad, 180, btnSize);
        
        //Demás componentes
        /*txtCate.setBounds(width / 20, height / 2, width * 9 / 20, height / 17);
        lblEstado.setLocation((width / 2) + 10, (height / 2) - 20); 
        scbEstado.setBounds((width / 2) + 10, height / 2, width * 17 / 40, height / 17);
        jScrollPane2.setBounds(width / 20, txtCate.getY() + txtCate.getHeight(), width * 9 / 10, height - txtCate.getY() - txtCate.getHeight() - lblModificar.getHeight() - 5); //jScrollPane2 es el padre de txaDescripcion
        txaDescripcion.setBounds(width / 20, txtCate.getY() + txtCate.getHeight(), width * 9 / 10, height / 3);*/
        
        
        txtCate.setBounds(width / 20, height * 21 / 40, width * 9 / 20, height / 17);
        lblEstado.setLocation((width / 2) + 10, (height * 21 / 40) - 20); 
        scbEstado.setBounds((width / 2) + 10, height * 21 / 40, width * 17 / 40, height / 17);
        jScrollPane2.setBounds(width / 20, txtCate.getY() + txtCate.getHeight(), width * 9 / 10, height - txtCate.getY() - txtCate.getHeight() - lblModificar.getHeight() - 5); //jScrollPane2 es el padre de txaDescripcion
        txaDescripcion.setBounds(width / 20, txtCate.getY() + txtCate.getHeight(), width * 9 / 10, height / 3);
       
        
        
        
        
        
        //Regresando textFields a sus valores por defecto

    
        //Peticiones Title------------------------------------------------------
        int fontHeight = jHeader.getHeight() / 2;
        Font fuente = new Font("Consolas", Font.PLAIN, fontHeight);
        /*lblTitle.setFont(cf.setBembo(0, fontHeight));*/
        lblTitle.setBounds((btnLogo.getX() * 2) + btnLogo.getWidth(), fontHeight - (fontHeight / 2), fontHeight * lblTitle.getText().length(), fontHeight);
    }
    
    //Saber si esta posicionado
    public boolean getLocationActualy()
    {
        return location;
    }
    
    //Darle un valor
    public void setLovationValue(boolean location)
    {
        this.location = location;
    }
    
    //Cambiar de posicion
    public void setLocationActualy()
    {
        mConcejalVirtual.jCate.setBounds(-2000, -2000, 800, 600); 
        setLovationValue(false);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAll;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnLogo;
    private javax.swing.JPanel jElement;
    private javax.swing.JPanel jHeader;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblGuardar;
    private javax.swing.JLabel lblModificar;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JTable tblCate;
    private javax.swing.JTextArea txaDescripcion;
    private javax.swing.JTextField txtCate;
    // End of variables declaration//GEN-END:variables
}
