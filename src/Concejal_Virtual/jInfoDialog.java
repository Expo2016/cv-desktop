/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Extras.jBajarFtp;
import MVC.sAuditoria;
import MVC.sComentarios;
import MVC.sConectar;
import MVC.sConecxion;
import MVC.sPerfil;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class jInfoDialog extends javax.swing.JPanel {
    
    //la fuente que se usara
    private Font fuenteAcalde = null;
    
    //Limite de caractheres
    int limiteChasracteres = 142;
    
    //Salto de linea
    public static final String linea = "sdf.sg.df,.gs,.,a..a,a.sdàf`psdfp";
    
    //Tamno de la linea
    private final byte lineaHeight = 4;
    
    //Dialogo
    private String Dialogo;
    
    //Tamaño
    private int width, height, fontSize;
    
    //Sabes si hay archivos adjuntos
    private boolean document = false, opcionCancel = false;
    
    sComentarios comX;
    jPeticionModific petX;
    
    public jInfoDialog(String Dialogo, int width, Color color, boolean document, boolean opcionCancel){
        initComponents();
        
        //Fuente
        try
        {
            InputStream is = getClass().getResourceAsStream("font/MYRIADPRO-REGULAR.tff");
            fuenteAcalde = Font.createFont(Font.TRUETYPE_FONT, is);
        }
        catch(Exception e)
        {
            fuenteAcalde = this.getFont();
        }
        
        //Nulo
        this.setLayout(null);
        
        //sabes si hay un archivo adjundo
        this.document = document;
        
        //Saber si se le puede borrar el comentario
        this.opcionCancel = opcionCancel;
        
        //El color de fondo
        this.setBackground(color);
        
        //Dialogos
        this.Dialogo = Dialogo;
        
        //Tamaño de la fuente
        fontSize = width / 60;
        
        //Tamaño
        this.width = width;
        this.height = (this.getHeihgtFont(Dialogo, width, fontSize)) * fontSize;
        
        //Saber si dejamos espacio para el documento
        if(this.document || opcionCancel)
            this.height += fontSize;
        
        this.height += (lineaHeight * 2);
        
        String infoTxt = "";
        //Ingresar el texto
        for(String text: Dialogo.split(linea))
        {
            infoTxt += text.trim()+"\n";
        }
        
        //Creamos los labeles
        if(this.document){
            JLabel lblDoc = new JLabel("ver documentos");
            lblDoc.setForeground(Color.WHITE);
            lblDoc.setBounds((this.width / 100), this.height - (this.getFontMetrics(this.getFont())).getHeight(), (this.width / 2) - (this.width / 100), (this.getFontMetrics(this.getFont())).getHeight());
            this.add(lblDoc);
        }
        if(this.opcionCancel)
        {
            JLabel lblCancel = new JLabel("Eliminar comentario");
            lblCancel.setForeground(Color.WHITE);
            int sepWidt = (this.getFontMetrics(this.getFont())).stringWidth("Eliminar comentario");
            lblCancel.setBounds(this.width - (sepWidt + (this.width / 100)), this.height - (this.getFontMetrics(this.getFont())).getHeight(), sepWidt, (this.getFontMetrics(this.getFont())).getHeight());
            this.add(lblCancel);
        }
    }

    public jInfoDialog(sComentarios com, int width, jPeticionModific pet){
        initComponents();
        
        //Fuentes
        this.setFont(new Font("Consolas", Font.PLAIN, 14));
        this.setForeground(Color.white);
        
        //Intanciamos los paneles
        this.comX = com;
        petX = pet;
        
        //Fuente
        try
        {
            InputStream is = getClass().getResourceAsStream("font/MYRIADPRO-REGULAR.tff");
            fuenteAcalde = Font.createFont(Font.TRUETYPE_FONT, is);
        }
        catch(Exception e)
        {
            fuenteAcalde = this.getFont();
        }
        
        //Nulo
        this.setLayout(null);
        
        //sabes si hay un archivo adjundo
        if (com.getArchivos() == null || com.getArchivos().equals("n/a"))
        {
            this.document = false;
        }
        else
        {
            this.document = true;
        }
        
        Integer tipoUsu = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sPerfil.getIdTipo(), new Integer(com.getId_perfil()));
        
        //Saber si se le puede borrar el comentario
        if (tipoUsu == 2 || tipoUsu == 3) {
            opcionCancel = true;
            
            //El color de fondo
            this.setBackground(new Color(49,61,151));
        }
        else
        {
            opcionCancel = false;
            
            //El color de fondo
            this.setBackground(new Color(80,95,198));
        }
        
        //Dialogos
        this.Dialogo = com.getComentario();
        
        //Tamaño de la fuente
        fontSize = width / 60;
        
        //Tamaño
        this.width = width;
        this.height = (this.getHeihgtFont(Dialogo, width, fontSize)) * fontSize;
        
        //Saber si dejamos espacio para el documento
        if(this.document || opcionCancel)
            this.height += fontSize;
        
        this.height += (lineaHeight * 2);
        
        String infoTxt = "";
        //Ingresar el texto
        for(String text: Dialogo.split(linea))
        {
            infoTxt += text.trim()+"\n";
        }
        
        //Creamos los labeles
        if(this.document){
            JLabel lblDoc = new JLabel("Descargar documentos");
            lblDoc.setForeground(Color.WHITE);
            lblDoc.setBounds((this.width / 100), this.height - (this.getFontMetrics(this.getFont())).getHeight(), (this.width / 2) - (this.width / 100), (this.getFontMetrics(this.getFont())).getHeight());
            this.add(lblDoc);
            lblDoc.addMouseListener(new MouseListener()
            {
                public void mouseClicked(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                
                    try
                    {
                        JFileChooser files = new JFileChooser();
                        files.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                        int resultado = files.showOpenDialog(null);
                        
                        if (resultado == 0) {
                            String destino = files.getSelectedFile().getAbsolutePath();
                            
                            destino += File.separator + "Concejal virtual";
                            
                            File f = new File(destino);
                            f.mkdir();
                            
                            jBajarFtp.setPrimerPath("/FTP");
                            jBajarFtp.setSecundPath(destino);
                            jBajarFtp.setNombreArchivo(comX.getArchivos());
                            jBajarFtp.descargarArchivo();
                            
                            JOptionPane.showMessageDialog(null, "Archivo descargado exitosamente");
                        }
                    }
                    catch(Exception ex){}
                }
                public void mousePressed(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
                public void mouseReleased(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
                public void mouseEntered(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
                public void mouseExited(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            });
        }
        if(this.opcionCancel)
        {
            //Borramos los comentarios al obtener el id y solamente dar click en el label
            
            JLabel lblCancel = new JLabel("Eliminar comentario");
            lblCancel.setForeground(Color.WHITE);
            int sepWidt = (this.getFontMetrics(this.getFont())).stringWidth("Eliminar comentario");
            lblCancel.setBounds(this.width - (sepWidt + (this.width / 100)), this.height - (this.getFontMetrics(this.getFont())).getHeight(), sepWidt, (this.getFontMetrics(this.getFont())).getHeight());
            this.add(lblCancel);
            lblCancel.addMouseListener(new MouseListener()
            {
                public void mouseClicked(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                
                    if (JOptionPane.showConfirmDialog(null, "Quieres borrar este mensaje") == 0) {
                        int idX = comX.getId_comentario();
                        
                        boolean r = sConecxion.deleteElement(sConectar.conectar(), sComentarios.deleteId(idX));
                        
                        petX.setComentarios();
                        
                        sConecxion.insertAll(sConectar.conectar(), sAuditoria.allInfo(), petX.pet.getId_peticion(), new Integer(7), mConcejalVirtual.perfil.getIdPerfil(), new Integer(0));
                        //JOptionPane.showMessageDialog(null, petX.pet.getDescripcion());
                    }
                }
                public void mousePressed(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
                public void mouseReleased(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
                public void mouseEntered(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
                public void mouseExited(MouseEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            });
        }
    }
    
    //Obtener la informacion
    public int getWidth(){ return width; }
    public int getHeight(){ return height; }
    
    //Calcular el tamaño de los elementos
    private int getHeihgtFont(String Dialogo, int width, int fontSize)
    {   
        //Lineas
        int numberLine = 0;
        
        //Creamos una Lista donde estaran toda la informacion
        ArrayList<String> txtDialogos = new ArrayList<String>();
        
        //Sabremos cuandos textos son
        for(String Prueba:Dialogo.split(linea))
        {
            //Aumentamos el numero de filas que usaremos
            //numberLine++;
            txtDialogos.add(Prueba);
        }
        
        //Quitar ese uno
        //numberLine--;
        
        //Obtenos el iterador
        Iterator i = txtDialogos.iterator();
        
        //Obtener la fontMetrica
        FontMetrics fm = this.getFontMetrics(this.getFont());
        
        //El ancho del texto
        int widthLeter = 0;
        
        //Sabremos el total de filas
        while(i.hasNext())
        {
            //Obtenemos los string
            String txt = (String)i.next();
            
            //Ahora sabremos cuandas veces hay que sumarle
            for (String prueba: txt.split("\n")) {
                //Le sumammos a la division
                numberLine++;
                
                widthLeter += fm.stringWidth(prueba);
                
                if(widthLeter >= (width - (lineaHeight * 2)))
                {
                    widthLeter = 0;
                    numberLine++;
                }
            }
        }
        
        return (numberLine);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
        
        g.setColor(Color.white);
        
        //Obtenemos los textos
        String txt = Dialogo;
        
        //Tamaño de los caracteres pero en la posision Y
        int infoPosY = fontSize;
        int infoPosX = lineaHeight;
        
        //El string con el caracteres
        String length = "";
        for(String infoTxtLine: txt.split(linea))
        for(String infoTxt: infoTxtLine.split("\n"))
        {
            //Obtenemos la simetria de la fuente
            FontMetrics fontM = g.getFontMetrics();
            
            //Obtenemos el ancho de la fuente
            int sepFont = fontM.stringWidth(" ");
            
            //Obtenemos cada letra
            for (int i = 0; i < infoTxt.length(); i++) {
                //Posicionamos la informacion
                g.drawString(""+infoTxt.charAt(i), infoPosX, infoPosY);
                
                //Obtenemos la posicion X del sigiente
                infoPosX += fontM.stringWidth(""+infoTxt.charAt(i));
                
                //Comprobamos sino sobre sale de los parametros
                if (infoPosX >= (width - (lineaHeight * 2))) {
                    infoPosX = lineaHeight;
                    infoPosY += fontSize;
                    
                    this.setSize(width,height+fontSize);
                }
                
            }
            
            //Reseterar todo papus
            infoPosX = lineaHeight;
            infoPosY += fontSize;
        }
        
        int heightRect = lineaHeight;
        
        if(document || opcionCancel){
            heightRect = g.getFontMetrics().getHeight();
        }

        //System.out.println("Size = " + heightRect);
        
        Color c = new Color(10,10,10,200);
        g.setColor(c);
        g.fillRect(0, height -heightRect, width, heightRect);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridLayout(1, 0));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
