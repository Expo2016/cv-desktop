/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Conexion.xConexion;
import Extras.jBajarFtp;
import Extras.jComboBox;
import Extras.jCustomFont;
import Extras.jSubirFtp;
import Extras.jTool;
import Hilos._Acciones;
import Hilos._Control;
import Hilos._Hilos;
import MVC.cConcejales;
import MVC.sConcejales;
import MVC.sConectar;
import MVC.sConecxion;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Position;

/**
 *
 * @author kevin
 */
public class jConcejales extends javax.swing.JPanel {

    /**
     * Creates new form jConcejales
     */
    _Control control = new _Control();
    //Fuente
    private Font fontAclade = null;
    
    //Cambiar de posicion
    private boolean location = false;
    
    public static ArrayList<String> urlFile = new ArrayList<String>();
    public static ArrayList<String> urlFile2 = new ArrayList<String>();
    
    
    //Tamaños estandar
    private int btnSize = 24, elementSize = 34;
    
    Connection cn = null;
    ResultSet rs = null;
    ResultSet rs1 = null;
    Statement st = null;
    
    Integer id = null;
    
    //Variable global para la imagen de Concejales
    Path FROM;
    //String imgConcejal = ("Img/Admin/user.jpg");
    String imgConcejal = "Img/Admin/user.jpg";;
    String imgModific = "";;
    
    DefaultTableModel ConcejalTabla;
    xConexion con = new xConexion();
    
    jComboBox scbEstConcejal = new jComboBox();
    jComboBox scbComision = new jComboBox();
    
    
    //Instanciando clase CustomFont
    //final jCustomFont cf = new jCustomFont();
    
    
    public void inicio() {
        initComponents();
        
        ConcejalTabla = new DefaultTableModel(null, getColumnas()){
            //Deshabilitando la edición de celdas
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        
        setFilas();
        
        //Configurando botones
        jTool.btnTransparente(btnExit, btnLogo, btnAll);
        
        //Invisivilizando lista de indices de comisiones
        scrIndex.setVisible(false); //scrIndex es el padre de lstIndex
        lstIndex.setVisible(false);
        
        //Inicializando modelos para las listas
        IndexModel = new DefaultListModel();
        ListModel = new DefaultListModel();
        
        
        //Valores del combobox de estado
        scbEstConcejal.setModel(new DefaultComboBoxModel(new String[]{ "Activo", "Inactivo"}));
        
        //Añadiendo combobox
        add(scbEstConcejal);
        add(scbComision);
        
        
        cConcejales obj = new cConcejales();
        //Llenando combobox
        obj.infoCMB();
        llenarCMB();

        //Llenando listas    ESTO IRA EN EL EVENTO EN EL QUE SE SELECCIONE UNA COMISION
        // EL 2 DE LA VARIABLE id ES PROVISIONAL, SE LLENARA CON EL ID DE LA COMISION SELECCIONADA

        Long id = Long.valueOf(0);
        obj.setId_comiM(id);
        obj.infoList();
        llenarList();
    
        try 
            {
                obj.cn.close();
            } 
        
            catch (SQLException ex) 
            {
                Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        
        //Al cambiar el item del combobox 
        this.scbComision.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                //Al seleccionar una comisión
                if(scbComision.getSelectedIndex() != -1)
                {
                    //variable que contiene el indice seleccionado
                    String Index = String.valueOf(scbComision.getSelectedItem());



                    //Si ya hay comisiones registradas
                    try
                    {
                        //Variable que busca si lstIndex contiene un elemento cuyo texto es igual al indice seleccionado
                        int respuesta = lstComision.getNextMatch(Index, 0, Position.Bias.Forward);

                        //Verificando si el concejal no pertenece ya a la comision seleccionada
                        if(respuesta == -1)
                        {
                            //No pertenece, el indice es añadido
                            IndexModel.addElement(cConcejales.Cate[scbComision.getSelectedIndex()][0]);
                            ListModel.addElement(scbComision.getSelectedItem());

                            lstIndex.setModel(IndexModel);
                            lstComision.setModel(ListModel);

                        }

                        else
                        {
                            //Ya pertenece, mensaje de error
                            if(scbComision.getSelectedIndex() != -1)
                            JOptionPane.showMessageDialog(null, "El concejal ya pertenece a la comisión seleccionada");
                        }
                    }

                    //Si aún no hay comisiones registradas
                    catch (Exception ex)
                    {
                        //El indice es añadido
                        IndexModel.addElement(cConcejales.Cate[scbComision.getSelectedIndex()][0]);
                        ListModel.addElement(scbComision.getSelectedItem());

                        lstIndex.setModel(IndexModel);
                        lstComision.setModel(ListModel);
                    }
                }


            }
            
        });
        
    }

    public jConcejales()
    {
        _Hilos hilo = new _Hilos(1);
        
        control.estrablecerConstructor(true);
        
        hilo.establecerAccion(new _Acciones(){
            public boolean acto() {

                inicio();

                //Saber que se realizo la accion
                return true;
            }
        });

        control.evaluarHilo(hilo);
    }
    
    //Para el numero de columnas del jTable
    private String[] getColumnas(){
        String columna[] = new String[]{"No.", "ID","Nombre", "Correo", "Descripción", "Estado", "Foto"};
        return columna;
        
    }
    //Para el numero de filas del jTable
    private void setFilas()
    {
            
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        try
        {
            String sql = "SELECT alderman_id, alderman_name, alderman_email, alderman_description, alderman_status, alderman_picture"
                    + " FROM alderman ORDER BY alderman_status";

            PreparedStatement us = con.conectar().prepareStatement(sql);
            ResultSet res = us.executeQuery();

            Object datos[] = new Object[7]; //Numero de columnas de la cosulta

            //El correlativo de la tabla
            int corr = 1;

            while (res.next()) {

                datos[0] = corr;

                for (int i = 1; i < 7; i++) {

                    if(!(i == 5))
                        datos[i] = res.getObject(i);

                    else
                    {
                        //El campo id_estado debe ser convertido de int a string
                        if(res.getInt(i) == 0)
                        {
                            datos[i] = "Activo";
                        }

                        else
                        {
                            datos[i] = "Inactivo";
                        }
                    }
                }

                ConcejalTabla.addRow(datos);


                corr++;
            }
            res.close();

            tblConce.setModel(ConcejalTabla);

            //Dando tamaño a la columna del correlativo
            tblConce.getColumnModel().getColumn(0).setWidth(50);
            tblConce.getColumnModel().getColumn(0).setMinWidth(50);
            tblConce.getColumnModel().getColumn(0).setMaxWidth(50);

            //Ocultando columna de ID
            tblConce.getColumnModel().getColumn(1).setWidth(0);
            tblConce.getColumnModel().getColumn(1).setMinWidth(0);
            tblConce.getColumnModel().getColumn(1).setMaxWidth(0);

            //Ocultando columna de foto
            tblConce.getColumnModel().getColumn(6).setWidth(0);
            tblConce.getColumnModel().getColumn(6).setMinWidth(0);
            tblConce.getColumnModel().getColumn(6).setMaxWidth(0);


            //Validando que solo se pueda seleccionar un registro a la vez
            tblConce.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

            //Fijando la posición de las columnas de la tabla
            tblConce.getTableHeader().setReorderingAllowed(false);

            //Deshabilitando navegación con teclado en la tabla
            InputMap iMap1 = tblConce.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

            KeyStroke stroke = KeyStroke.getKeyStroke("ENTER");
            iMap1.put(stroke, "none");
            stroke = KeyStroke.getKeyStroke("DOWN");
            iMap1.put(stroke, "none");
            stroke = KeyStroke.getKeyStroke("UP");
            iMap1.put(stroke, "none");
        }
        catch (SQLException ex) 
        {
            //Logger.getLogger(jConcejales.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //Para la foto del concejal
    DefaultTableModel TablaConcejal;
    
    void Registro() {
        TablaConcejal = new DefaultTableModel();
        TablaConcejal.addColumn("");
    }
    
    
        
    //Modelos para combobox
    DefaultComboBoxModel ModelCMB;
    
    //Método para llenar los combobox
    void llenarCMB()
    {
        ModelCMB = new DefaultComboBoxModel();
        
        //Agregando elementos a los tres modelos
        for(int cont = 0; cont < cConcejales.CateX; cont++)
        {
            ModelCMB.addElement(cConcejales.Cate[cont][1]);
        }
        
        
        
        //Implementando modelos en combobox
        scbComision.setModel(ModelCMB);
    }

    
    
    //Variables que contienen el tamaño del panel
    int width, height;
    
    //Modelos para listas
    DefaultListModel IndexModel;
    DefaultListModel ListModel;
    
    
    //Método para llenar las listas
    void llenarList()
    {
        //Inicializando modelos para las listas
        IndexModel = new DefaultListModel();
        ListModel = new DefaultListModel();
        
        //Agregando elementos a los tres modelos
        for(int cont = 0; cont < cConcejales.ListX; cont++)
        {
            
            IndexModel.addElement(cConcejales.List[cont][0]);
            ListModel.addElement(cConcejales.List[cont][1]);
        }
        
        
        
        //Implementando modelos en listas
        lstComision.setModel(ListModel);
        lstIndex.setModel(IndexModel);
    }
    
    
    
    
    public void limpiar(){
        //Regresando textFields a sus valores por defecto
        txtNombre.setText("Nombre");
        txtNombre.setForeground(new Color(153, 153, 153));
        txtCorreo.setText("E-mail");
        txtCorreo.setForeground(new Color(153, 153, 153));
        txaDescripcion.setText("Descripción");
        txaDescripcion.setForeground(new Color(153, 153, 153));


        //Regresando listas y combobox a sus valores por defecto
        scbComision.setSelectedIndex(-1);
        scbEstConcejal.setSelectedIndex(0);

        IndexModel.removeAllElements();
        ListModel.removeAllElements();

        lstIndex.setModel(IndexModel);
        lstComision.setModel(ListModel);


        imgConcejal = "Img/Admin/user.jpg";
        imgMod.setUrl(imgConcejal);
        imgMod.repaint();

        imgModific = "";

        imgConcejal = "Img/Admin/user.jpg";
        FROM = Paths.get("Img/Admin/user.jpg");

        //Limpiando tabla
        try
        {
            ConcejalTabla = (DefaultTableModel) tblConce.getModel();

            int a =ConcejalTabla.getRowCount();

            for(int i=0; i<a; i++)
                ConcejalTabla.removeRow(0);
        }

        catch(Exception e)
        {
            System.out.println(e);
        }

        //Deshabilitando boton borrar
        btnBorrar.setEnabled(false);

        //Deshabilitando modificar
        lblGuardar.setEnabled(true);
        lblModificar.setEnabled(false);


        setFilas();

        //Seleccionando primera fila
        tblConce.changeSelection(-1, -1, false, false);
    }
    
    
    private void buscarImagen(){
        
        // BUSCAR LA IMAGEN POR EL BROWSER
        JFileChooser selectorArchivos = new JFileChooser();//Creamos el objeto para obtener las direcciones
        
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Imagenes: ", "jpg");
        selectorArchivos.setFileFilter(filtro);

        selectorArchivos.setFileSelectionMode(JFileChooser.FILES_ONLY);
        
        int resultado = selectorArchivos.showOpenDialog(null);//Guardamos la opcion que se escogio
        File archivo = selectorArchivos.getSelectedFile();//Guardamos el archivo
        
        if ((archivo == null) || (archivo.getName().equals(""))) {
            //En caso de error le indicamos el error
            JOptionPane.showMessageDialog(null, "Nombre de archivo inválido", "Nombre de archivo inválido", JOptionPane.ERROR_MESSAGE);
        }
        else
        {
           //Le indicamos que imagen usara 
           imgMod.setUrl(archivo.getPath());
           imgMod.repaint();//Repintamos el panel
           
           try
           {
               //Obtenemos la direccion de origen
               FROM = Paths.get(archivo.getPath());
               //imgConcejal = "Img/Concejales/"+archivo.getName();
               imgConcejal = archivo.getAbsolutePath();
               imgModific = archivo.getAbsolutePath();
           }
           catch(Exception ex)
           {
               JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
           }
        }   
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jHeader = new javax.swing.JPanel();
        btnExit = new javax.swing.JButton();
        btnLogo = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        scrTabla = new javax.swing.JScrollPane();
        tblConce = new javax.swing.JTable();
        txtNombre = new javax.swing.JTextField();
        txtCorreo = new javax.swing.JTextField();
        scrDescripcion = new javax.swing.JScrollPane();
        txaDescripcion = new javax.swing.JTextArea();
        lblComision = new javax.swing.JLabel();
        scrComision = new javax.swing.JScrollPane();
        lstComision = new javax.swing.JList<>();
        scrIndex = new javax.swing.JScrollPane();
        lstIndex = new javax.swing.JList<>();
        btnBorrar = new javax.swing.JButton();
        jElement = new javax.swing.JPanel();
        btnAll = new javax.swing.JButton();
        lblGuardar = new javax.swing.JLabel();
        lblModificar = new javax.swing.JLabel();
        lblEstConcejal = new javax.swing.JLabel();
        lblCorreo = new javax.swing.JLabel();
        imgMod = new Extras.jPanelImage();
        btnImg = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 162, 211));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentMoved(java.awt.event.ComponentEvent evt) {
                formComponentMoved(evt);
            }
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });

        jHeader.setBackground(new java.awt.Color(12, 22, 71));

        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        lblTitle.setBackground(new java.awt.Color(255, 255, 255));
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setText("Gestion Concejales");

        javax.swing.GroupLayout jHeaderLayout = new javax.swing.GroupLayout(jHeader);
        jHeader.setLayout(jHeaderLayout);
        jHeaderLayout.setHorizontalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnLogo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnExit)
                .addContainerGap())
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addGap(316, 316, 316)
                    .addComponent(lblTitle)
                    .addContainerGap(317, Short.MAX_VALUE)))
        );
        jHeaderLayout.setVerticalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnExit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnLogo, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addGap(8, 8, 8)
                    .addComponent(lblTitle)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        tblConce.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblConce.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Categorías", "Correo electrónico", "Estado", "Descripción"
            }
        ));
        tblConce.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblConceMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tblConceMouseReleased(evt);
            }
        });
        scrTabla.setViewportView(tblConce);

        txtNombre.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtNombre.setForeground(new java.awt.Color(153, 153, 153));
        txtNombre.setText("Nombre");
        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNombreKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNombreKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        txtCorreo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtCorreo.setForeground(new java.awt.Color(153, 153, 153));
        txtCorreo.setText("Correo electrónico");
        txtCorreo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCorreoActionPerformed(evt);
            }
        });
        txtCorreo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCorreoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCorreoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCorreoKeyTyped(evt);
            }
        });

        txaDescripcion.setColumns(20);
        txaDescripcion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txaDescripcion.setForeground(new java.awt.Color(153, 153, 153));
        txaDescripcion.setRows(5);
        txaDescripcion.setText("Descripción");
        txaDescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txaDescripcionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txaDescripcionKeyReleased(evt);
            }
        });
        scrDescripcion.setViewportView(txaDescripcion);

        lblComision.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblComision.setForeground(new java.awt.Color(255, 255, 255));
        lblComision.setText("Seleccionar comisiones:");

        lstComision.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lstComision.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstComisionMouseClicked(evt);
            }
        });
        lstComision.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstComisionValueChanged(evt);
            }
        });
        scrComision.setViewportView(lstComision);

        lstIndex.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        scrIndex.setViewportView(lstIndex);

        btnBorrar.setBackground(new java.awt.Color(31, 150, 244));
        btnBorrar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBorrar.setForeground(new java.awt.Color(255, 255, 255));
        btnBorrar.setText("Borrar comisión");
        btnBorrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBorrar.setEnabled(false);
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });

        jElement.setBackground(new java.awt.Color(12, 22, 71));

        btnAll.setForeground(new java.awt.Color(255, 255, 255));
        btnAll.setText("Limpiar");
        btnAll.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAll.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllActionPerformed(evt);
            }
        });

        lblGuardar.setForeground(new java.awt.Color(255, 255, 255));
        lblGuardar.setText("Guardar");
        lblGuardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblGuardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblGuardarMouseClicked(evt);
            }
        });

        lblModificar.setForeground(new java.awt.Color(255, 255, 255));
        lblModificar.setText("Modificar");
        lblModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblModificar.setEnabled(false);
        lblModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblModificarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jElementLayout = new javax.swing.GroupLayout(jElement);
        jElement.setLayout(jElementLayout);
        jElementLayout.setHorizontalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jElementLayout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addComponent(btnAll, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblGuardar)
                .addGap(18, 18, 18)
                .addComponent(lblModificar)
                .addGap(25, 25, 25))
        );
        jElementLayout.setVerticalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jElementLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jElementLayout.createSequentialGroup()
                        .addGroup(jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblModificar)
                            .addComponent(lblGuardar))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(btnAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        lblEstConcejal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblEstConcejal.setForeground(new java.awt.Color(255, 255, 255));
        lblEstConcejal.setText("Seleccionar estado:");

        lblCorreo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblCorreo.setForeground(new java.awt.Color(255, 255, 255));
        lblCorreo.setText("@sansalvador.gob.sv");

        imgMod.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                imgModMouseReleased(evt);
            }
        });

        btnImg.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnImg.setText("Cambiar imagen");
        btnImg.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnImg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImgActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout imgModLayout = new javax.swing.GroupLayout(imgMod);
        imgMod.setLayout(imgModLayout);
        imgModLayout.setHorizontalGroup(
            imgModLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, imgModLayout.createSequentialGroup()
                .addContainerGap(39, Short.MAX_VALUE)
                .addComponent(btnImg)
                .addContainerGap())
        );
        imgModLayout.setVerticalGroup(
            imgModLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, imgModLayout.createSequentialGroup()
                .addGap(0, 77, Short.MAX_VALUE)
                .addComponent(btnImg))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(scrIndex, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(96, 96, 96))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(scrDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(122, 122, 122)
                                        .addComponent(btnBorrar))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(376, 376, 376)
                                        .addComponent(scrComision, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(225, 307, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(85, 85, 85)
                                .addComponent(lblCorreo)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(scrTabla, javax.swing.GroupLayout.PREFERRED_SIZE, 541, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(imgMod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jElement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(53, 53, 53))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblEstConcejal)
                        .addGap(34, 34, 34)
                        .addComponent(lblComision)
                        .addGap(169, 169, 169))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrTabla, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(imgMod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jElement, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblComision)
                            .addComponent(lblEstConcejal))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(scrComision, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(scrIndex, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(btnBorrar))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(lblCorreo)
                        .addGap(34, 34, 34)
                        .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(scrDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(117, 117, 117))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formComponentMoved(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentMoved
        //Al cambiar de posición

        //Obteniendo tamaño de panel
        width = this.getWidth();
        height = this.getHeight();
        
        ResizePnl();
        
        
    }//GEN-LAST:event_formComponentMoved

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyPressed
        //Efecto de textfield
        if(txtNombre.getText().equals("Nombre"))
        {
            txtNombre.setText(null);
            txtNombre.setForeground(Color.black);
        }
    }//GEN-LAST:event_txtNombreKeyPressed

    private void txtNombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyReleased
        //Efecto de textfield
        if(txtNombre.getText().equals(""))
        {
            txtNombre.setText("Nombre");
            txtNombre.setForeground(new Color(153, 153, 153));
        }
    }//GEN-LAST:event_txtNombreKeyReleased

    private void txaDescripcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txaDescripcionKeyPressed
        //Efecto de textfield
        if(txaDescripcion.getText().equals("Descripción"))
        {
            txaDescripcion.setText(null);
            txaDescripcion.setForeground(Color.black);
        }
    }//GEN-LAST:event_txaDescripcionKeyPressed

    private void txaDescripcionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txaDescripcionKeyReleased
        //Efecto de textfield
        if(txaDescripcion.getText().equals(""))
        {
            txaDescripcion.setText("Descripción");
            txaDescripcion.setForeground(new Color(153, 153, 153));
        }
    }//GEN-LAST:event_txaDescripcionKeyReleased

    private void txtCorreoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCorreoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCorreoActionPerformed

    private void txtCorreoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCorreoKeyPressed
        //Efecto de textfield
        if(txtCorreo.getText().equals("E-mail"))
        {
            txtCorreo.setText(null);
            txtCorreo.setForeground(Color.black);
        }
        
    }//GEN-LAST:event_txtCorreoKeyPressed

    private void txtCorreoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCorreoKeyReleased
        //Efecto de textfield
        if(txtCorreo.getText().equals(""))
        {
            txtCorreo.setText("E-mail");
            txtCorreo.setForeground(new Color(153, 153, 153));
        }
    }//GEN-LAST:event_txtCorreoKeyReleased

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        //ConsejalVirtual.jConce.setVisible(false);
        setLocationActualy();
    }//GEN-LAST:event_btnExitActionPerformed

    private void lstComisionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstComisionMouseClicked
        
    }//GEN-LAST:event_lstComisionMouseClicked

    private void lstComisionValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstComisionValueChanged
        //Habilitando boton borrar
        btnBorrar.setEnabled(true);
    }//GEN-LAST:event_lstComisionValueChanged

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        
        if(lstComision.getSelectedIndex() != -1){
            
            cConcejales obj = new cConcejales();
            
            try
            {
                obj.setNumcomi(Long.parseLong(lstIndex.getModel().getElementAt(lstComision.getSelectedIndex())));
                obj.setNumconce((Long)this.ConcejalTabla.getValueAt(tblConce.getSelectedRow(), 1));
                
                //Debe validar la eliminación del registro de la relación
                
                if(obj.buscarRelacion())
                { 
                    if(JOptionPane.showConfirmDialog(null, "¿Desea desvincular la comisión seleccionada del concejal?", "Atención", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0)
                    {
                        //Borrando registro de comision seleccionada
                        if(obj.eliminarComision())
                            JOptionPane.showMessageDialog(null, "Comisión eliminada");
                    
                        IndexModel.remove(lstComision.getSelectedIndex());
                        ListModel.remove(lstComision.getSelectedIndex());

                        lstIndex.setModel(IndexModel);
                        lstComision.setModel(ListModel);

                        //Deshabilitando boton
                        btnBorrar.setEnabled(false);
                    }
                }
            
                else
                {
                    //Al intentar borrar categorias previamente ingresadas
                    JOptionPane.showMessageDialog(null, "La comisión seleccionada no puede ser eliminada \nporque ya hay una petición que la involucra");
                }
            }
            
            catch(Exception e)
            {
                //En caso de que no haya ningún registro seleccionado, no hace contacto con la base
                
                if(JOptionPane.showConfirmDialog(null, "¿Desea desvincular la comisión seleccionada del concejal?", "Atención", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0)
                    {
                        //Borrando registro de comision seleccionada
                        JOptionPane.showMessageDialog(null, "Comisión eliminada");
                    
                        IndexModel.remove(lstComision.getSelectedIndex());
                        ListModel.remove(lstComision.getSelectedIndex());

                        lstIndex.setModel(IndexModel);
                        lstComision.setModel(ListModel);

                        //Deshabilitando boton
                        btnBorrar.setEnabled(false);
                    }
            }
            
            
            try 
            {
                obj.cn.close();
            } 
        
            catch (SQLException ex) 
            {
                Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        else{
            //Mensaje de error
            JOptionPane.showMessageDialog(null, "Por favor seleccione una comisión a borrar.");
        }
    }//GEN-LAST:event_btnBorrarActionPerformed

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        // TODO add your handling code here:
    }//GEN-LAST:event_formComponentShown

    private void btnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllActionPerformed
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(2);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto(){
                    
                    limpiar();
                    
                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }
        
    }//GEN-LAST:event_btnAllActionPerformed

    private void lblGuardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGuardarMouseClicked

        //Validando que el label esté habilitado
        if(lblGuardar.isEnabled())
        {
            //Obtiene valores de los campos de texto
            String nombre = txtNombre.getText().toString().trim();
            String correo = txtCorreo.getText().toString().trim();
            String descripcion = txaDescripcion.getText().toString().trim();

            cConcejales obj = new cConcejales();

            //Valida campos vacíos
            if(!(nombre.equals("") || nombre.equals("Nuevo nombre de categoría") || correo.equals("") || correo.equals("E-mail") || descripcion.equals("") || descripcion.equals("Descripción")))
            {

                File archivoImg = new File(imgConcejal);
                
                //Calcula el id que tendrá este registro
                obj.seleccionarConcejal();
                Long num = obj.getNumconce() + 1;
                
                obj.setNombreConcejal(nombre);
                obj.setCorreoConcejal(correo + "@sansalvador.gob.sv");
                obj.setDescripcionConcejal(descripcion);
                obj.setEstadoConcejal(scbEstConcejal.getSelectedIndex());
                obj.setFotoConcejal("");
                
                String e_mail = correo + "@sansalvador.gob.sv";
                String name = nombre;
                
                //Mensaje de confirmación
                if(JOptionPane.showConfirmDialog(null, "¿Deseas guardar? Recuerda que las relaciones con\n comisiones creadas podrían no poder ser borradas\n posteriormente", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
                {

                    //Valida que el nombre y correo no se repitan
                    if(obj.validarNombreGuardar())
                    {
                        JOptionPane.showMessageDialog(this,"Ya existe un concejal con el nombre ingresado");

                        if(obj.validarCorreoGuardar())
                            JOptionPane.showMessageDialog(this,"Ya existe un concejal con el correo electrónico ingresado");
                    }

                    else
                    {
                        if(obj.validarCorreoGuardar())
                        {
                            JOptionPane.showMessageDialog(this,"Ya existe un concejal con el correo electrónico ingresado");
                        }

                        else
                        {
                            //obj.setEstadoConcejal(id);
                            //String txt1 = "", txt2 = "";
                            if(obj.guardarConcejales())
                            {
                        //SUBIDA DE FOTOS
                        //COMENTARIADA HASTA NUEVO AVISO
                                /*Integer id = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sConcejales.getEapecificElementId(name, e_mail));
                                
                        
                                try
                                {
                                    File destino = new File("imgCom"+id.intValue()+".jpg");

                                    InputStream iN = new FileInputStream(archivoImg);
                                    OutputStream ouT = new FileOutputStream(destino);

                                    byte[] datos = new byte[1024];
                                    int len;

                                    while((len = iN.read(datos)) > 0)
                                    {
                                        ouT.write(datos, 0, len);
                                    }

                                    iN.close();
                                    ouT.close();
                                
                                    final File copyX = destino; 
                                    
                                    if (!control.isConstructor()) {
                                        _Hilos hilo = new _Hilos(3);
                                        
                                        hilo.establecerAccion(new _Acciones(){
                                            public boolean acto(){
                                                //Fotos:
                                                jSubirFtp.setDebug(true);
                                                jSubirFtp.setLocation("/FTP");
                                                jSubirFtp.setFile(copyX);
                                                jSubirFtp.subierArchivo();
                                                
                                                return true;
                                            }
                                        });
                                        
                                        control.evaluarHilo(hilo);
                                    }
                                    
                                    new cConcejales().modificarConcejalesFoto(destino.getName(), id);
                                    
                                    urlFile2.add(destino.getAbsolutePath());
                                }
                                catch(Exception ex){
                                    JOptionPane.showMessageDialog(null, ex);
                                }*/
                                //
                                obj.seleccionarConcejal();

                                for (int i = 0; i <= ListModel.getSize(); i++)
                                { 
                                    //System.out.println(ListModel.getSize());
                                    try
                                    {   
                                        Long comi= Long.parseLong(lstIndex.getModel().getElementAt(i));
                                        obj.setNumcomi(comi);

                                        if(obj.guardarOrgcomision())
                                        {
                                            //esto es solo para ver si va aÃ±adiendo las categorias.
                                            //JOptionPane.showMessageDialog(this,"comision guardada");
                                        }
                                        else
                                        {
                                            JOptionPane.showMessageDialog(this,"Error al guardar comision");
                                        }
                                    }

                                    catch(Exception e){}
                                }

                                
                                if (!control.isConstructor()) {
                                    _Hilos hilo = new _Hilos(4);
                                    
                                    hilo.establecerAccion(new _Acciones(){
                                        public boolean acto()
                                        {
                                            limpiar(); 
                                            
                                            return true;
                                        }
                                    });
                                    
                                    control.evaluarHilo(hilo);
                                }
                                
                                JOptionPane.showMessageDialog(null, "Concejal guardado");
                            }

                            else
                            {
                                JOptionPane.showMessageDialog(this, "Error al guardar datos");
                            }


                        }
                    }

                }
                /*obj = new cConcejales();
                obj.setNombreConcejal(txt1);
                obj.setDescripcionConcejal(txt2);
                obj.consultarConcejales();*/


            }

            else
            {
                JOptionPane.showMessageDialog(null, "Por favor rellene todos los campos");
            }


            try 
            {
                obj.cn.close();
            } 

            catch (SQLException ex) 
            {
                Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_lblGuardarMouseClicked

    private void lblModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblModificarMouseClicked

        //Validando que el label esté habilitado
        if(lblModificar.isEnabled())
        {
        
            String nombre = txtNombre.getText().toString().trim();
            String correo = txtCorreo.getText().toString().trim();
            String descripcion = txaDescripcion.getText().toString().trim();

            cConcejales obj= new cConcejales();

            if(!(nombre.equals("") || nombre.equals("Nuevo nombre de categoría") || correo.equals("") || correo.equals("E-mail") || descripcion.equals("") || descripcion.equals("Descripción")))
            {

                //Mensaje de confirmación
                if(JOptionPane.showConfirmDialog(null, "¿Seguro que deseas modificar el registro seleccionado? \nRecuerda que las relaciones con comisiones creadas podrían \nno poder ser borradas posteriormente", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
                {
                    String foto = (String)tblConce.getValueAt(tblConce.getSelectedRow(), 6);
                    int indice= this.tblConce.getSelectedRow();
                    Long comi= Long.valueOf(this.lstIndex.getSelectedIndex());

                    Long num = (Long)this.ConcejalTabla.getValueAt(indice, 1);
                    
                    final String fotox = foto;
                    
                    if (!imgModific.equals("")){                     
                        if (!control.isConstructor()) {
                            _Hilos hilo = new _Hilos(7);
                            
                            hilo.establecerAccion(new _Acciones(){
                                public boolean acto()
                                {
                                    
                        //SUBIDA DE FOTOS
                        //COMENTARIADA HASTA NUEVO AVISO
                                    /*try
                                    {                        
                                        File original = new File(imgModific);
                                        File destino = new File(fotox);

                                        InputStream iN = new FileInputStream(original);
                                        OutputStream ouT = new FileOutputStream(destino);

                                        byte[] datos = new byte[1024];
                                        int len;

                                        while((len = iN.read(datos)) > 0)
                                        {
                                            ouT.write(datos, 0, len);
                                        }

                                        iN.close();
                                        ouT.close();

                                        jSubirFtp.setDebug(true);
                                        jSubirFtp.setLocation("/FTP");
                                        jSubirFtp.setFile(destino);
                                        jSubirFtp.subierArchivo();

                                        urlFile2.add(fotox);
                                    }
                                    catch(Exception ex)
                                    {
                                        JOptionPane.showMessageDialog(null, "No se ha podido realizar los cambios");
                                    }*/
                                    
                                    return true;
                                }
                            });
                            
                            control.evaluarHilo(hilo);
                        }
                    }
                    obj.setNombreConcejal(nombre);
                    obj.setCorreoConcejal(correo + "@sansalvador.gob.sv");
                    obj.setDescripcionConcejal(descripcion);
                    obj.setEstadoConcejal(scbEstConcejal.getSelectedIndex());
                    obj.setFotoConcejal(foto);
                    obj.setCodigo((Long)this.ConcejalTabla.getValueAt(indice, 1));

                    //JOptionPane.showMessageDialog(null, obj.getCodigo());

                    
                    //Valida que el nombre y correo no se repitan
                    if(obj.validarNombreModificar())
                    {
                        JOptionPane.showMessageDialog(this,"Ya existe un concejal con el nombre ingresado");

                        if(obj.validarCorreoModificar())
                            JOptionPane.showMessageDialog(this,"Ya existe un concejal con el correo electrónico ingresado");
                    }

                    else
                    {
                        if(obj.validarCorreoModificar())
                        {
                            JOptionPane.showMessageDialog(this,"Ya existe un concejal con el correo electrónico ingresado");
                        }
                        
                        else
                        {
                    
                            if(obj.modificarConcejales())
                            {
                                txtNombre.setText("");
                                txaDescripcion.setText("");
                                
                                if (!imgModific.equals("")) {
                                    if (!control.isConstructor()) {
                                        _Hilos hilo = new _Hilos(8);
                                        
                                        hilo.establecerAccion(new _Acciones(){
                                            public boolean acto()
                                            {
                                                
                        //SUBIDA DE FOTOS (al parecer sin ftp)
                        //COMENTARIADA HASTA NUEVO AVISO
                                                
                                                /*try
                                                {
                                                    //Creamos un archivo donde se diga adonde lo meteremos
                                                    File file = new File(imgModific);

                                                    //Si el archivo existe lo creamos
                                                    //if (!file.exists()) 
                                                        file.createNewFile();

                                                    //Destino de la imagen
                                                    Path TO = Paths.get(imgModific);

                                                    //Copiamos
                                                    CopyOption[] options = new CopyOption[]{
                                                        StandardCopyOption.REPLACE_EXISTING,
                                                        StandardCopyOption.COPY_ATTRIBUTES
                                                    }; 

                                                    //Lo movemos
                                                    Files.copy(FROM, TO, options);
                                                }
                                                catch(Exception ex){}*/
                                                
                                                return true;
                                            }
                                        });
                                        
                                        control.evaluarHilo(hilo);
                                    }
                                }
                                indice= this.tblConce.getSelectedRow();
                                //System.out.println(""+indice);

                                Long id = (Long)this.ConcejalTabla.getValueAt(indice, 1);

                                obj.setNumconce(id);

                                for(int i = 0; i <= ListModel.size(); i++)
                                {
                                    //Aqui pondran el insert, enviando como parametros dos datos:
                                    //Sammy -- el id del concejal que estamos modificando, y ListModel.getElementAt(cont)
                                    //Raul -- el id de la comision que estamos modificando, y ListModel.getElementAt(cont)

                                    //System.out.println(ListModel.getSize());

                                    try
                                    {   
                                        comi= Long.parseLong(lstIndex.getModel().getElementAt(i));
                                        obj.setNumcomi(comi);



                                        if(!(obj.validarComision()))
                                        {
                                            if(obj.guardarOrgcomision())
                                            {
                                                //esto es solo para ver si va aÃ±adiendo las categorias.
                                                //JOptionPane.showMessageDialog(this,"Comision guardada");

                                            }

                                            else
                                            {
                                                JOptionPane.showMessageDialog(this,"Error al guardar Comision");
                                            }

                                        }
                                    }

                                    catch(Exception e){}


                                }

                                if (!control.isConstructor()) {
                                    _Hilos hilo = new _Hilos(9);
                                    
                                    hilo.establecerAccion(new _Acciones(){
                                        public boolean acto() {
                                            limpiar();
                                            
                                            return true;
                                        }
                                    });
                                    
                                    control.evaluarHilo(hilo);
                                }
                            
                                JOptionPane.showMessageDialog(this,"Concejal modificado");
                            }

                            else
                            {
                                JOptionPane.showMessageDialog(this, "Error al modificar datos datos");
                            }
                        }
                    }
                }
            }

            else
            {
                JOptionPane.showMessageDialog(null, "Por favor rellene todos los campos");
            }


            try 
            {
                obj.cn.close();
            } 

            catch (SQLException ex)
            {
                Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_lblModificarMouseClicked

    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        //Solo letras para el textbox
        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'á' || car > 'ú') && (car < 'A' || car > 'Z') && (car < 'Á' || car > 'Ú') 
            &&(car != ',') && (car != '.') && (car!=(char)KeyEvent.VK_BACK_SPACE) && (car!=(char)KeyEvent.VK_SPACE)){
            evt.consume();
        }
    }//GEN-LAST:event_txtNombreKeyTyped

    private void txaDescripcionKeyTyped(java.awt.event.KeyEvent evt) {                                        
        //Solo letras para el textbox
        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'á' || car > 'ú') && (car < 'A' || car > 'Z') && (car < 'Á' || car > 'Ú') 
            &&(car != ',') && (car != '.') && (car != ';') && (car != '-') && (car != '"') && (car != '(') && (car != ')') 
            &&(car!=(char)KeyEvent.VK_BACK_SPACE) && (car!=(char)KeyEvent.VK_SPACE)){
            evt.consume();
        }
    }
    
    
    private void tblConceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblConceMouseClicked
        
        /*String nombre="", correo="", descripcion="", foto="", estado="";
            int id;
            
            id = (Integer) tblConce.getValueAt(tblConce.getSelectedRow(), 1);
            nombre  = (String)tblConce.getValueAt(tblConce.getSelectedRow(), 2);
            correo = (String)tblConce.getValueAt(tblConce.getSelectedRow(), 3);
            descripcion = (String)tblConce.getValueAt(tblConce.getSelectedRow(), 4);
            estado = (String) tblConce.getValueAt(tblConce.getSelectedRow(), 5);
            foto = (String)tblConce.getValueAt(tblConce.getSelectedRow(), 6);
            
            txtNombre.setText(nombre);
            txtCorreo.setText(correo);
            txaDescripcion.setText(descripcion);
            scbEstConcejal.setSelectedItem(estado);
            imgMod.setIcon(new ImageIcon(foto));
            imgMod.repaint();
            
            
            cConcejales obj = new cConcejales();
            obj.setId_comiM(id);
            obj.infoList();
            llenarList();
            
            try 
            {
                obj.cn.close();
            } 
        
            catch (SQLException ex) 
            {
                Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    
        txtNombre.setForeground(Color.black);
        txtCorreo.setForeground(Color.black);
        txaDescripcion.setForeground(Color.black);
        
        //Deshabilitando guardar
        lblGuardar.setEnabled(false);
        lblModificar.setEnabled(true);*/
    }//GEN-LAST:event_tblConceMouseClicked

    private void tblConceMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblConceMouseReleased
        // TODO add your handling code here:
        
        String nombre="", correo="", descripcion="", foto="", estado="";
        Long id;

        id = (Long) tblConce.getValueAt(tblConce.getSelectedRow(), 1);
        nombre  = (String)tblConce.getValueAt(tblConce.getSelectedRow(), 2);
        correo = (String)tblConce.getValueAt(tblConce.getSelectedRow(), 3);
        descripcion = (String)tblConce.getValueAt(tblConce.getSelectedRow(), 4);
        estado = (String) tblConce.getValueAt(tblConce.getSelectedRow(), 5);
        foto = (String)tblConce.getValueAt(tblConce.getSelectedRow(), 6);

        //Buscarlos en el ftp
        if (foto != null) {
            if (!control.isConstructor()) {
                
                final String fX = foto;
                
                _Hilos hilo = new _Hilos(5);
                
                hilo.establecerAccion(new _Acciones(){
                    public boolean acto()
                    {
                        
                //BAJADA DE FOTOS
                //COMENTARIADA HASTA NUEVO AVISO
                        
                        /*jBajarFtp.setPrimerPath("/FTP");
                        jBajarFtp.setSecundPath("Img");
                        jBajarFtp.setNombreArchivo(fX);
                        File info = jBajarFtp.descargarArchivo();
                        String xfoto = info.getPath();
                        urlFile.add(xfoto);
                        imgMod.setUrl(xfoto);
                        imgMod.repaint();*/
                        
                        return true;
                    }
                });
                
                control.evaluarHilo(hilo);
            }
        }
        else
        {
            if (!control.isConstructor()) {
                _Hilos hilo = new _Hilos(5);
                
                hilo.establecerAccion(new _Acciones(){
                    public boolean acto(){
                        imgMod.setUrl("Img/modPeticiones/user.png");
                        imgMod.repaint();
                        
                        return true;
                    }
                });
            }
        }
        //

        txtNombre.setText(nombre);
        txtCorreo.setText(correo.replace("@sansalvador.gob.sv", ""));
        txaDescripcion.setText(descripcion);
        scbEstConcejal.setSelectedItem(estado);

        final Long idX = Long.valueOf(id);
        
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(6);
            
            hilo.setMultiple(true);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto()
                {
                    cConcejales obj = new cConcejales();
                    obj.setId_comiM(idX);
                    obj.infoList();
                    llenarList();

                    try 
                    {
                        obj.cn.close();
                    } 

                    catch (SQLException ex){}
                    
                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }


        txtNombre.setForeground(Color.black);
        txtCorreo.setForeground(Color.black);
        txaDescripcion.setForeground(Color.black);

        //Deshabilitando guardar
        lblGuardar.setEnabled(false);
        lblModificar.setEnabled(true);
        
    }//GEN-LAST:event_tblConceMouseReleased

    private void btnImgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImgActionPerformed
        
        buscarImagen();
    }//GEN-LAST:event_btnImgActionPerformed

    private void txtCorreoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCorreoKeyTyped
        //Validando caracteres del correo
        char car = evt.getKeyChar();
        if ((car < 'a' || car > 'z') && (car < 'á' || car > 'ú') && (car < 'A' || car > 'Z') && (car < 'Á' || car > 'Ú') 
            &&(car!=(char)KeyEvent.VK_BACK_SPACE)){
            evt.consume();
        }
    }//GEN-LAST:event_txtCorreoKeyTyped

    private void imgModMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imgModMouseReleased
        this.buscarImagen();
    }//GEN-LAST:event_imgModMouseReleased


    public void ResizePnl(){
        
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(10);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto()
                {
                    limpiar();
                    
                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }
        
        imgConcejal = "Img/Admin/user.jpg";
        imgModific = "";
        
        width = this.getWidth();
        height = this.getHeight();
        
        //Disposiciones
        setLayout(null);
        jHeader.setLayout(null);
        jElement.setLayout(null);
        
        //Configurando imagenes
        btnExit.setIcon(new ImageIcon("Img/Peticiones/Close.png"));
        btnLogo.setIcon(new ImageIcon("Img/Admin/concejal24px.png"));
        imgMod.setUrl("Img/Admin/user.jpg");
        
        //Posicionando y dando tamaño a componentes:
        
        //la cabecera 
        jHeader.setBounds(0, 0, width, 30);
        btnLogo.setBounds((width / 100), 15 - 12, 24, 24);
        btnExit.setBounds(width - 30, 15 - 12, 24, 24);
        
        
        //Cuerpo----------------------------------------------------------------
        
        
        //Tablaaa
        scrTabla.setBounds(0, jHeader.getHeight(), width * 17 / 24, height / 3);//scrTabla es el padre de tblConce
        //tblConce.setBounds(0, jHeader.getHeight(), width * 3 / 4, height / 3);
        
        //panelImage
        imgMod.setBounds(scrTabla.getWidth() + 10, scrTabla.getY(), (width - scrTabla.getWidth()) - 10, scrTabla.getHeight());
        
        //Barra intermedia
        jElement.setBounds(0, jHeader.getHeight() + scrTabla.getHeight(), width, elementSize);
        
        btnAll.setIcon(new ImageIcon("Img/Reportes/garbage.png"));
        
        int mitad = (elementSize / 2) - (btnSize / 2);
        int K = width / 100;

//1----------------------------------------------------
        /*btnAll.setBounds(K, mitad, btnSize, btnSize);
        
        
        //Botones inferiores
        
        //lblModificar.setBounds(width - sizeLeng, height - (btnSize + kHeight), sizeLeng, btnSize);
        lblModificar.setBounds(width - 85, mitad, 80, btnSize);
        lblModificar.setIcon(new ImageIcon("Img/Admin/modificar24px.png"));
        
        
        //lblGuardar.setBounds(width - (lblModificar.getWidth() + sizeDoc), height - (btnSize + kHeight), sizeDoc, btnSize);
        lblGuardar.setBounds(width - 170, mitad, 80, btnSize);
        lblGuardar.setIcon(new ImageIcon("Img/Admin/guardar24px.png"));*/
        
        
//2-----------------------------------------------------
        /*lblGuardar.setBounds(K, mitad, 80, btnSize);
        lblGuardar.setIcon(new ImageIcon("Img/Admin/guardar24px.png"));
        
        
        //Botones inferiores
        
        lblModificar.setBounds(90, mitad, 80, btnSize);
        lblModificar.setIcon(new ImageIcon("Img/Admin/modificar24px.png"));
        
        btnAll.setBounds(width - btnSize, mitad, btnSize, btnSize);*/
        
        
//3-----------------------------------------------------
        lblGuardar.setBounds(K, mitad, 80, btnSize);
        lblGuardar.setIcon(new ImageIcon("Img/Admin/guardar24px.png"));
        
        
        //Botones inferiores
        
        lblModificar.setBounds(90, mitad, 80, btnSize);
        lblModificar.setIcon(new ImageIcon("Img/Admin/modificar24px.png"));
        
        btnAll.setBounds(180, mitad, 180, btnSize);
        
        
        
//4-----------------------------------------------------
        /*lblGuardar.setBounds(K, mitad, btnSize, btnSize);
        lblGuardar.setIcon(new ImageIcon("Img/Admin/guardar24px.png"));
        
        
        //Botones inferiores
        
        lblModificar.setBounds(K + btnSize + 10, mitad, btnSize, btnSize);
        lblModificar.setIcon(new ImageIcon("Img/Admin/modificar24px.png"));
        
        btnAll.setBounds(K + btnSize + btnSize + 40, mitad, btnSize, btnSize);*/
        
        
        
        
        txtNombre.setBounds(width / 20, height / 2, (width * 9 / 20) - (width / 20), height / 17);
        txtCorreo.setBounds(width / 20, txtNombre.getY() + txtNombre.getHeight(), txtNombre.getWidth() / 3, height / 17);
        lblCorreo.setLocation(txtCorreo.getX() + txtCorreo.getWidth(), txtCorreo.getY() + (txtCorreo.getHeight() / 4));
        //txtCorreo1.setBounds(txtCorreo.getX() + txtCorreo.getWidth(), txtCorreo.getY(), txtCorreo.getWidth(), txtCorreo.getHeight());
        scrDescripcion.setBounds(width / 20, txtCorreo.getY() + txtCorreo.getHeight(), scrTabla.getWidth() - (width / 20), height - txtCorreo.getY() - txtCorreo.getHeight() - lblModificar.getHeight() - 5);  //scrDescripcion es el padre de txaDescripcion
        txaDescripcion.setBounds(width / 20, txtCorreo.getY() + txtCorreo.getHeight(), scrTabla.getWidth() - (width / 20), height - txtCorreo.getY() - txtCorreo.getHeight() - lblModificar.getHeight() - 5);
        scbComision.setBounds(imgMod.getX(), txtCorreo.getY(), imgMod.getWidth() - (width / 20), txtCorreo.getHeight());
        lblComision.setLocation(scbComision.getX(), scbComision.getY() - 20);
        scrComision.setBounds(imgMod.getX(), scbComision.getY() + scbComision.getHeight(), imgMod.getWidth() - (width / 20), height / 4); //scrComision es el padre de lstComision
        //lstComision.setBounds(imgMod.getX(), txtNombre.getY(), imgMod.getWidth(), txtCorreo.getY() - txtNombre.getY() + txtNombre.getHeight());
        
        btnBorrar.setBounds(scrComision.getX(), scrComision.getY() + scrComision.getHeight() + 10, scbComision.getWidth(), height / 17);
        
        scbEstConcejal.setBounds(txtNombre.getX() + txtNombre.getWidth() + 10, txtCorreo.getY(), scrTabla.getWidth() - (txtNombre.getX() + txtNombre.getWidth() + 10), txtCorreo.getHeight());
        lblEstConcejal.setLocation(scbEstConcejal.getX(), scbEstConcejal.getY() - 20);
        
        scrIndex.setBounds(scrComision.getX() + scrComision.getWidth(), scbComision.getY() + scbComision.getHeight(), imgMod.getWidth() - (width / 20), height / 4); //scrComision es el padre de lstComision
        
        //scrIndex.setVisible(true);
        //lstIndex.setVisible(true);
        
        
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(11);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto()
                {
                    cConcejales obj = new cConcejales();
                    //Llenando combobox
                    obj.infoCMB();
                    llenarCMB();

                    //Llenando listas    ESTO IRA EN EL EVENTO EN EL QUE SE SELECCIONE UNA COMISION
                    // EL 2 DE LA VARIABLE id ES PROVISIONAL, SE LLENARA CON EL ID DE LA COMISION SELECCIONADA

                    Long id = Long.valueOf(0);
                    obj.setId_comiM(id);
                    obj.infoList();
                    llenarList();
    
                    try 
                    {
                        obj.cn.close();
                    } 

                    catch (SQLException ex) 
                    {
                        Logger.getLogger(jComisiones.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }
        
    
        
        //Peticiones Title------------------------------------------------------
        int fontHeight = jHeader.getHeight() / 2;
        Font fuente = new Font("Consolas", Font.PLAIN, fontHeight);
        /*lblTitle.setFont(cf.setBembo(0, fontHeight));*/
        lblTitle.setBounds((btnLogo.getX() * 2) + btnLogo.getWidth(), fontHeight - (fontHeight / 2), fontHeight * lblTitle.getText().length(), fontHeight);
    }
    
    //Saber si esta posicionado
    public boolean getLocationActualy()
    {
        return location;
    }
    
    //Darle un valor
    public void setLovationValue(boolean location)
    {
        this.location = location;
    }
    
    //Cambiar de posicion
    public void setLocationActualy()
    {   
        mConcejalVirtual.jConce.setLocation(10000, 10000);
        setLovationValue(false);
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAll;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnImg;
    private javax.swing.JButton btnLogo;
    public Extras.jPanelImage imgMod;
    private javax.swing.JPanel jElement;
    private javax.swing.JPanel jHeader;
    private javax.swing.JLabel lblComision;
    private javax.swing.JLabel lblCorreo;
    private javax.swing.JLabel lblEstConcejal;
    private javax.swing.JLabel lblGuardar;
    private javax.swing.JLabel lblModificar;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JList<String> lstComision;
    private javax.swing.JList<String> lstIndex;
    private javax.swing.JScrollPane scrComision;
    private javax.swing.JScrollPane scrDescripcion;
    private javax.swing.JScrollPane scrIndex;
    private javax.swing.JScrollPane scrTabla;
    private javax.swing.JTable tblConce;
    private javax.swing.JTextArea txaDescripcion;
    private javax.swing.JTextField txtCorreo;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
