/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Extras.jTool;
import MVC.sAuditoria;
import MVC.sConectar;
import MVC.sConecxion;
import MVC.sPeticiones;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Maritza
 */

public class jPanelInfoStep extends javax.swing.JPanel {

    Point puntTxtArea;
    Point puntAcept;
    Point puntRemove;
    
    Point A;
    Point X;
    
    //Propiedades importantes
    public static final byte stepOne = 0, stepTwo = 1, stepThree = 2, stepFour = 3, stepFive = 4;
    
    //Indice del step
    private byte indiceStep = 0;
    
    //Imagenes
    private ImageIcon imgOff[] = { new ImageIcon("Img/Menu/oneOff.png"), new ImageIcon("Img/Menu/twoOff.png"), new ImageIcon("Img/Menu/threeOff.png"), new ImageIcon("Img/Menu/fourOff.png"), new ImageIcon("Img/Menu/fiveOff.png") };
    private ImageIcon imgOn[] = { new ImageIcon("Img/Menu/oneOn.png"), new ImageIcon("Img/Menu/twoOn.png"), new ImageIcon("Img/Menu/threeOn.png"), new ImageIcon("Img/Menu/fourOn.png"), new ImageIcon("Img/Menu/fiveOn.png") };
    
    //Tamaño del boto
    private byte btnSize = 24;
    
    //tamaño proporcional
    private byte div = 4;
    
    //Peticiones
    jPeticionModific modPet;
    
    //Panel del txexo
    javax.swing.JPanel jPanelInfo;
    
    //Texto
    protected String txtPrueba = "PRUEBA URBANA";
    
    private int widthX = 0;
    
    public jPanelInfoStep(jPeticionModific modPet) {
        initComponents();
        
        //Referencia
        this.modPet = modPet;
        
        //Quitar tamano
        jTool.btnTransparente(btnStep, btnExit, btnSalvar, btnEliminar);
        
        //Cambiar la disposicion
        this.setLayout(null);
        jHeader.setLayout(null);
        
        //Fondo
        //this.setBackground(Color.white);
        
        btnSalvar.setIcon(new ImageIcon("Img/Reportes/save.png"));
        btnEliminar.setIcon(new ImageIcon("Img/Reportes/delete.png"));
        
        jPanelInfo = new javax.swing.JPanel()
        {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
                g.setColor(Color.black);
                
                g.setFont(new Font("console",Font.BOLD,11));
                
                FontMetrics fm = g.getFontMetrics(new Font("console",Font.BOLD,11));
                int x = fm.stringWidth(" "), y = fm.getHeight(), xFalse = fm.stringWidth(" ");
                
                for (int i = 0; i < txtPrueba.length(); i++) {
                    String tx = ""+txtPrueba.charAt(i);
                    xFalse += fm.stringWidth(tx);
                    
                    if (xFalse < widthX) {
                        g.drawString(tx, x, y);
                        x += fm.stringWidth(tx);
                    }else{
                        x = fm.stringWidth(" ");
                        xFalse = fm.stringWidth(" ");
                        
                        y += fm.getHeight();
                        
                        g.drawString(tx, x, y);
                        
                        xFalse += fm.stringWidth(tx);
                        x += fm.stringWidth(tx);
                    }
                }
            }
        };
        
        
        //jPanelInfo.setBackground(Color.yellow);
        this.add(jPanelInfo);
    }

    //tamano
    public void setLocationWindow(int height, int width){
        //Posicionarlo
        height = height / div;
        width = width / div;
        
        widthX = width;
        
        int widthBtn = btnSize;
        int heightBtn = btnSize;
        int btnWidthE = btnSize;
        
        //Cambiar el tamano
        this.setBounds(0, -(100 + height), width, height);
        
        //Posicionar los demas paneles
        jHeader.setBounds(0, 0, width, btnSize);
        
        //Posicionar
        btnStep.setBounds(width / 100, 0, btnSize, btnSize);
        
        //Posicionar los demas botones
        btnSalvar.setBounds(width - widthBtn, height - heightBtn, widthBtn, heightBtn);
        btnEliminar.setBounds(btnSalvar.getX() - btnWidthE, btnSalvar.getY(), btnWidthE, heightBtn);
        
        //Le damos las propiedades al boton de salida
        btnExit.setBounds(width - btnSize, 0, btnSize, btnSize);
        
        //Poner el icono de salida
        btnExit.setIcon(new ImageIcon("Img/Menu/exitForm.png"));
        
        //Tamaño
        int jsWidht = width - ((width / 100) * 2);
        int jsHeight = height - ((btnSize * 2) + (btnSize / 2));
     
        puntAcept = btnSalvar.getLocation();
        puntRemove = btnEliminar.getLocation();
        
        int heightBTN = height * 2/ 3;
        int widthBTN = width / 3;
        int heightArea = height - jHeader.getHeight();
        int widthArea = width / 2;
        
        int yBTN = (heightArea / 2) - (heightBTN / 2);
        int xBTN = (widthArea / 2) - (widthBTN / 2);
        
        btnA.setBounds(8, 32, 32, 32);
        A = btnA.getLocation();
        btnA.setLocation(1000, 1000);
        
        btnX.setBounds(32+16+8, 32, 32, 32);
        X = btnX.getLocation();
        btnX.setLocation(1000, 1000);
        
        btnSalvar.setBounds(8, 32, 32, 32);
        puntAcept = btnSalvar.getLocation();
        
        btnEliminar.setBounds(32+16+8, 32, 32, 32);
        puntRemove = btnEliminar.getLocation();
        
        jTool.btnTransparente(btnX, btnA);
        
        btnX.setIcon(new ImageIcon("Img/Reportes/Nop.png"));
        btnA.setIcon(new ImageIcon("Img/Reportes/Yep.png"));
        
        jPanelInfo.setBounds(0, 32+32+8, width, height-(32+32+8));
    }
    
    //Escoger el icono y su tipo
    public void setIcon(boolean tipo, byte indice){
        //Iconos
        
        indiceStep = indice;
        
        if(tipo){
            btnStep.setIcon(imgOn[indice]);
        }
        else
        {
            btnStep.setIcon(imgOff[indice]);
        }
        
        if (indice != 1) {
            btnEliminar.setLocation(1000, 1000);
            btnSalvar.setLocation(1000, 1000);
            
            btnA.setLocation(A);
            btnX.setLocation(X);
        }
        else
        {
            btnSalvar.setLocation(puntAcept);
            btnEliminar.setLocation(puntRemove);
            
            btnA.setLocation(1000, 1000);
            btnX.setLocation(1000, 1000);
        }
        
        
    }
    
    //Escoger el texto
    public void setTitleStep(String title)
    {
        //Peticiones Title------------------------------------------------------
        lblTitle.setText(title);
        int fontHeight = jHeader.getHeight() / 2;
        Font fuente = new Font("Consolas", Font.PLAIN, fontHeight);
        lblTitle.setFont(fuente);
        lblTitle.setBounds((btnStep.getX() * 3) + btnStep.getWidth(), fontHeight - (fontHeight / 2), fontHeight * title.length(), fontHeight);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSalvar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        jHeader = new javax.swing.JPanel();
        btnStep = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        btnA = new javax.swing.JButton();
        btnX = new javax.swing.JButton();

        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnEliminar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        jHeader.setBackground(new java.awt.Color(25, 31, 79));

        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        lblTitle.setBackground(new java.awt.Color(255, 255, 255));
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setText("Peticiones");

        javax.swing.GroupLayout jHeaderLayout = new javax.swing.GroupLayout(jHeader);
        jHeader.setLayout(jHeaderLayout);
        jHeaderLayout.setHorizontalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnStep, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addGap(170, 170, 170)
                    .addComponent(lblTitle)
                    .addContainerGap(170, Short.MAX_VALUE)))
        );
        jHeaderLayout.setVerticalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnStep, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnExit, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jHeaderLayout.createSequentialGroup()
                    .addGap(26, 26, 26)
                    .addComponent(lblTitle)
                    .addContainerGap(27, Short.MAX_VALUE)))
        );

        btnA.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAActionPerformed(evt);
            }
        });

        btnX.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnEliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSalvar))
                    .addComponent(jHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addComponent(btnA, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnX, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(318, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(135, 135, 135)
                        .addComponent(btnA, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnX, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Desea aceptar la peticion?") == 0) {
            modPet.setStepValue(true, indiceStep);

            btnStep.setIcon(imgOn[indiceStep]);

            int id = modPet.pet.getId_peticion();

            sConecxion.setModific(sConectar.conectar(), sPeticiones.setInfo(indiceStep + 1, id));

            modPet.pet.setId_estadopet(indiceStep + 1);

            sConecxion.insertAll(sConectar.conectar(), sAuditoria.allInfo(), modPet.pet.getId_peticion(), new Integer(4), mConcejalVirtual.perfil.getIdPerfil(), new Integer(2));

            modPet.posLbl();
            
            this.setVisible(false);
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        //JOptionPane.showMessageDialog(null, "Gg");
       if (JOptionPane.showConfirmDialog(null, "Desea eliminar la peticion?") == 0) { 
            modPet.setStepValue(true, indiceStep);

            btnStep.setIcon(imgOn[indiceStep]);

            //modPet.setValueBtn(indiceStep);

            int id = modPet.pet.getId_peticion();

            sConecxion.setModific(sConectar.conectar(), sPeticiones.setInfo(5, id));

            this.setVisible(false);

            modPet.pet.setId_estadopet(5);

            sConecxion.insertAll(sConectar.conectar(), sAuditoria.allInfo(), null, new Integer(4), mConcejalVirtual.perfil.getIdPerfil(), new Integer(2));
       }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        //Eliminar elcoso
        this.setVisible(false);
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAActionPerformed
        Integer paso = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sPeticiones.getEstadoPet(modPet.pet.getId_peticion()));
        
        int petEstado = paso.intValue();
        
        if (petEstado < 4) {
            modPet.setStepValue(true, indiceStep);
        
            btnStep.setIcon(imgOn[indiceStep]);

            int id = modPet.pet.getId_peticion();

            sConecxion.setModific(sConectar.conectar(), sPeticiones.setInfo(indiceStep + 1, id));

            this.setVisible(false);

            modPet.pet.setId_estadopet(indiceStep + 1);

            sConecxion.insertAll(sConectar.conectar(), sAuditoria.allInfo(), modPet.pet.getId_peticion(), new Integer(4), mConcejalVirtual.perfil.getIdPerfil(), new Integer(2));

            modPet.posLbl();
        }
    }//GEN-LAST:event_btnAActionPerformed

    private void btnXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Desea descartar la peticion?") == 0) {
            int id = modPet.pet.getId_peticion();
        
            sConecxion.setModific(sConectar.conectar(), sPeticiones.setInfo(indiceStep, id));

            this.setVisible(false);

            modPet.pet.setId_estadopet(indiceStep);
            
            modPet.setValueBtn(indiceStep);

            sConecxion.insertAll(sConectar.conectar(), sAuditoria.allInfo(), modPet.pet.getId_peticion(), new Integer(4), mConcejalVirtual.perfil.getIdPerfil(), new Integer(2));
        
            modPet.posLbl();
        }
    }//GEN-LAST:event_btnXActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnA;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JButton btnStep;
    private javax.swing.JButton btnX;
    private javax.swing.JPanel jHeader;
    private javax.swing.JLabel lblTitle;
    // End of variables declaration//GEN-END:variables
}
