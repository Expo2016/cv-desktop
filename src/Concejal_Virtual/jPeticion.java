/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Extras.jComboBox;
import Extras.jTool;
import Hilos._Acciones;
import Hilos._Control;
import Hilos._Hilos;
import MVC.sCategoria;
import MVC.sComisiones;
import MVC.sConcejales;
import MVC.sConectar;
import MVC.sConecxion;
import MVC.sInfo;
import MVC.sOrganizacionComisiones;
import MVC.sOrganizacionConcejales;
import MVC.sPeticiones;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;

/**
 *
 * @author Toshiba
 */
public class jPeticion extends javax.swing.JPanel {

    /**
     * Creates new form jPeticion
     */
    
    boolean canTimeUse = false;
    
    //Codigo para saber si esta activo o no
    private boolean visibleCb = false, visibleC1 = false, visibleC2 = false;
    
    //Fyente privada
    private Font fontAclade = null;
    
    //Identificador del timer
    String idTimer = "";
    
    //Paneles
    JPanel fondo;
    
    //Nombre
    String fondoName = "";
    
    //Paneles para los step
    JPanel botones[];
    
    jPeticionModific peticionModific;
    
    //peticiones
    jPeticionInfo pInfo[];
    
    //Propiedades del panel
    //Tamano
    public int width, height;
    
    //tamano
    byte sizeEstandar = 34, btnSize = 24;
    
    //Labels
    JLabel[] jlCategorias;
    int categoriaCount, categoriaIndice;
    
    //El nombre de los label
    String Categoria[];
    
    //la ubicacioon de los iconos
    String imgUrl[];
    
    //Limire de elementos a la vista
    byte limite = 10;
    
    //saber que elemento es el que tiene el panel
    Integer indice;
    
    //Direccion del panel
    final byte UP = 0, DOWN = 1;
    byte dir = UP, dir2 = UP;
    
    //Saber si puede usar el timer
    boolean canTime = true, canStep = true;
    
    jComboBox spStep = new jComboBox();
    
    jComboBox cmbTipo = new jComboBox();
    jComboBox cmbCategoria = new jComboBox();
    jComboBox cmbConsejal = new jComboBox();
    
    //Filtro
    String cate = "";
    String conce = "";
    
    byte estadoFiltro = 0;
    
    //Cambiar el cmb
    private void resetCmb()
    {
        if (cmbTipo.getSelectedIndex() != 0) {
            cmbTipo.setSelectedIndex(0);
        }
        
        if (cmbCategoria.getSelectedIndex() != 0) {
            cmbCategoria.setSelectedIndex(0);
        }
        
        if (cmbConsejal.getSelectedIndex() != 0) {
            cmbConsejal.setSelectedIndex(0);
        }
    }
    
    
    //Dateformat df = DateFormat.getDateInstance();
    
    public void inicio(jPeticionModific peticionModific) {
        
        initComponents();
        
        dateInfo.setMaxSelectableDate(new Date());
        
        try{
            //Creamos la fuente
            InputStream is = this.getClass().getResourceAsStream("font/MYRIADPRO-REGULAR.tff");

            //Creamos la fuente segun lo importado
            fontAclade = Font.createFont(Font.TRUETYPE_FONT, is);
        }
        catch(Exception e)
        {
            fontAclade = this.getFont();
        }
        spStep.setModel( new DefaultComboBoxModel( new String[] { "Registrada", "Aceptada", "Asignada", "Finalizada" }));
        
        //ComboBox de la cabezera
        cmbTipo.setModel( new DefaultComboBoxModel( new String[] { "Todas las comisiones" }));
        cmbCategoria.setModel( new DefaultComboBoxModel( new String[] { "Todas las categorias" }));
        cmbConsejal.setModel( new DefaultComboBoxModel( new String[] { "Todos los concejales" }));
        
        this.peticionModific = peticionModific;
        
        //Lequitamos el layout
        this.setLayout(null);
        
        //Botones
        botones = new JPanel[5];
        
        //Inicializamos el tamano
        width = Toolkit.getDefaultToolkit().getScreenSize().width;
        height = Toolkit.getDefaultToolkit().getScreenSize().height;
        
        //Calculamos
        width = width * 4 / 5;
        height = height * 4 / 5;
        
        //Calcular el resto de paneles------------------------------------------
        jHeader.setBounds(0, 0, width, sizeEstandar);
        jFiltro.setBounds(0, jHeader.getHeight(), width, sizeEstandar);
        
        //Calcular el panel de categoria//Numero de elementos
        categoriaCount = 8;
        
        //Cambiar el tamano
        if (categoriaCount > limite)
            jSeleccion.setBounds(0, jFiltro.getY() + jFiltro.getHeight(), width / 5, height - (sizeEstandar * 2));
        else
            jSeleccion.setBounds(0, jFiltro.getY() + jFiltro.getHeight(), (width / 5) - (width / 60), height - (sizeEstandar * 2));
        
        //Anular las disposiciones
        jSeleccion.setLayout(null);
        jHeader.setLayout(null);
        jFiltro.setLayout(null);
        //Scroll bar busqueda
        jScrollBarFiltro.setBounds(jSeleccion.getWidth() - (width / 60), 0, width / 60, jSeleccion.getHeight());
        
        //Botones---------------------------------------------------------------
        jTool.btnTransparente(btnIcon, btnClose);
        
        //Imagenes
        btnIcon.setIcon(new ImageIcon("Img/Peticiones/Icon.png"));
        btnClose.setIcon(new ImageIcon("Img/Peticiones/Close.png"));
        
        //Tamano y posicion
        btnIcon.setBounds(width / 100, (jHeader.getHeight() / 2) - (btnSize / 2), btnSize, btnSize);
        btnClose.setBounds(width - (width / 100) - btnSize, (jHeader.getHeight() / 2) - (btnSize / 2), btnSize, btnSize);
        
        //Peparar los botones
        
        //Label de la categoria
        jlCategorias = new JLabel[categoriaCount];
        
        //Texto de la categoria
        Categoria = new String[categoriaCount];
        
        //Url de la imagen
        imgUrl = new String[categoriaCount];
        
        //Ingresar la informacion
        jTool.setComponentTxt(imgUrl,"Img/Peticiones/All.png", "Img/Peticiones/Aprovadas.png", "Img/Peticiones/Date.png", "Img/Peticiones/Stop.png", "Img/Peticiones/New.png", "Img/Peticiones/Progres.png", "Img/Peticiones/Fin.png", "Img/Peticiones/delete-bin.png");
        jTool.setComponentTxt(Categoria,"Ver todas", "Ver Aprobadas", "Ver por fechas", "Ver en espera", "Ver recientes", "Ver por progreso", "Ver finalizadas", "Ver Denegadas");
        
        for (int i = 0; i < jlCategorias.length; i++) {
            try
            {
                //Lo inicializamos
                jlCategorias[i] = new JLabel("");
                jlCategorias[i].setForeground(Color.white);
                fontAclade.deriveFont(Font.PLAIN, jSeleccion.getWidth() / 15);
                jlCategorias[i].setFont(fontAclade);
                jlCategorias[i].setText(Categoria[i]);
                jlCategorias[i].setVerticalTextPosition(JLabel.CENTER);
                jlCategorias[i].setIcon(new ImageIcon(imgUrl[i]));
                if (jScrollBarFiltro.isVisible())
                    jlCategorias[i].setBounds(jSeleccion.getWidth() / 20, (i * (jSeleccion.getHeight() / limite)), jSeleccion.getWidth() - jScrollBarFiltro.getWidth() - (jSeleccion.getWidth() / 20), jSeleccion.getHeight() / limite);
                else
                    jlCategorias[i].setBounds(jSeleccion.getWidth() / 20, (i * (jSeleccion.getHeight() / limite)), jSeleccion.getWidth() - (jSeleccion.getWidth() / 20), jSeleccion.getHeight() / limite);
                
                jSeleccion.add(jlCategorias[i]);
                
                jlCategorias[i].addMouseListener(new MouseListener()
                {
                    public void mouseClicked(MouseEvent e) {
                        
                        if (((JLabel)e.getSource()).getText().equals("Ver todas")) {
                            estadoFiltro = 1;
                            
                            removePeticiones();
                            setPeticionesCase();
                        }
                        
                        if (((JLabel)e.getSource()).getText().equals("Ver Aprobadas")) {
                            estadoFiltro = 2;
                            
                            removePeticiones();
                            setPeticionesCase();
                        }
                        if (((JLabel)e.getSource()).getText().equals("Ver por fechas")) {
                            estadoFiltro = 3;
                        }
                        if (((JLabel)e.getSource()).getText().equals("Ver en espera")) {
                            estadoFiltro = 4;
                            
                            removePeticiones();
                            setPeticionesCase();
                        }
                        if (((JLabel)e.getSource()).getText().equals("Ver recientes")) {
                            estadoFiltro = 5;
                            
                            removePeticiones();
                            setPeticionesCase();
                        }
                        if (((JLabel)e.getSource()).getText().equals("Ver por progreso")) {
                            estadoFiltro = 6;
                        }
                        if (((JLabel)e.getSource()).getText().equals("Ver finalizadas")) {
                            estadoFiltro = 7;
                            
                            removePeticiones();
                            setPeticionesCase();
                        }
                        if (((JLabel)e.getSource()).getText().equals("Ver Denegadas")) {
                            estadoFiltro = 8;
                            
                            removePeticiones();
                            setPeticionesCase();
                        }
                        
                        //timePanelStep
                        if(((JLabel)e.getSource()).getText().equals("Ver por fechas"))
                        {
                            if (canTime){
                                timePanel.start();
                                canTime = false;
                                idTimer = ((JLabel)e.getSource()).getText();
                            }
                        }
                        else
                        {
                            if (dir == UP){
                                timePanel.start();
                                canTime = false;
                                idTimer = "";
                            }
                        }
                        if(((JLabel)e.getSource()).getText().equals("Ver por progreso"))
                        {
                            if (canStep){
                                timePanelStep.start();
                                canStep = false;
                                idTimer = ((JLabel)e.getSource()).getText();
                            }
                        }
                        else
                        {
                            if (dir2 == UP){
                                timePanelStep.start();
                                canStep = false;
                                idTimer = "";
                            }
                        }
                        
                        //Obtenemos el boton
                        JLabel lbl = (JLabel)e.getSource();
                        
                        if(fondo.getY() != lbl.getY()){
                            //Guardamos su nombre
                            fondoName = lbl.getText();
                            
                            //le cambiamos de posicion
                            fondo.setLocation(0, lbl.getY());
                        }
                    }

                    public void mousePressed(MouseEvent e) {
                        
                    }

                    public void mouseReleased(MouseEvent e) {
                        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    public void mouseEntered(MouseEvent e) {
                        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }

                    public void mouseExited(MouseEvent e) {
                        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    }
                    
                });
            }
            catch(Exception e){System.out.println(e.getMessage());}
        }
        
        spStep.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            
               removePeticiones();
               setPeticionesCase();
            }
            
        });
        //Panel de dibujo de seleccion
        fondo = new JPanel()
        {
            protected void paintComponent(Graphics g) {
                super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
                
                //Dibujar
                int rectWidth = jSeleccion.getWidth();
                int rectHeight = (jSeleccion.getHeight() / limite) / 20;
                int rectY = (jSeleccion.getHeight() / limite) - rectHeight;
                
                //DIBUJAR
                g.fillRect(0, rectY, rectWidth, rectHeight);
            }
        };
        
        fondo.setBackground(new Color(0,0,0, (float) 0.4));
        
        fondo.setBounds(0, -100, jSeleccion.getWidth(), jSeleccion.getHeight() / limite);
        
        jSeleccion.add(fondo);
        
        //EL scroll
        setValue();
        
        //Cambiar el layout
        jInfo.setLayout(null);
        jStep.setLayout(null);
        
        //Colocar el panel
        panel((byte)4);
        panelStep((byte)4);
        
        //Posicional el date
        dateInfo.setBounds((jlCategorias[0].getWidth() / 2) - ((jlCategorias[0].getWidth() * 5 / 6) / 2), (jlCategorias[0].getHeight() / 2) - ((jlCategorias[0].getHeight() / 2) / 2), jlCategorias[0].getWidth() * 5 / 6, jlCategorias[0].getHeight() / 2);
    
        //Posicionar los cmb
        cmbCategoria.setBounds((jFiltro.getWidth() / 100), (jFiltro.getHeight() / 2) - ((jFiltro.getHeight() * 2 / 3) / 2), jFiltro.getWidth() * 2 / 10, jFiltro.getHeight() * 2 / 3);
        cmbTipo.setBounds( cmbCategoria.getX() + cmbCategoria.getWidth() + (jFiltro.getWidth() / 100), (jFiltro.getHeight() / 2) - ((jFiltro.getHeight() * 2 / 3) / 2), jFiltro.getWidth() * 2 / 10, jFiltro.getHeight() * 2 / 3);
        cmbConsejal.setBounds(cmbTipo.getX() + cmbTipo.getWidth() + (jFiltro.getWidth() / 100), (jFiltro.getHeight() / 2) - ((jFiltro.getHeight() * 2 / 3) / 2), jFiltro.getWidth() * 2 / 10, jFiltro.getHeight() * 2 / 3);
        
        
        cmbCategoria.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
                visibleCb = false;
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                visibleCb = false;
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
                visibleCb = true;
            }
        });
        
        cmbCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (visibleCb) {
                    
                    if (cmbCategoria.getSelectedIndex() == 0) {

                        cmbTipo.removeAllItems();
                        cmbTipo.addItem("Todas las comisiones");
                        sConecxion.getCombo(sConectar.conectar(), sComisiones.getAllComisiones(), cmbTipo);

                        cate = "";
                    }
                    else
                    {
                        try
                        {
                            if (isVisible() && cmbCategoria.getSelectedIndex() != 0) {
                                sCategoria kate = new sCategoria();

                                sConecxion.getInformationSin(kate, sConectar.conectar(), sCategoria.getElementCategoria(), (String)cmbCategoria.getSelectedItem());

                                int idCategoria = kate.getIdCategoria();

                                cmbTipo.removeAllItems();
                                cmbTipo.addItem("Todas las comisiones");
                                sConecxion.getCombo(sConectar.conectar(), sOrganizacionComisiones.getComision(idCategoria), cmbTipo);

                                cate = " id_categoria = " +idCategoria+ " ";
                            }
                        }
                        catch(Exception ex){}
                    }
                    removePeticiones();
                    createPeticiones();
                }
            }
        });
        
        cmbTipo.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
                visibleC1 = false;
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                visibleC1 = false;
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
                visibleC1 = true;
            }
        });
        
        cmbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (visibleC1) {
                    
                    if (cmbTipo.getSelectedIndex() == 0) {
                        cmbConsejal.removeAllItems();
                        cmbConsejal.addItem("Todos los concejales");
                        sConecxion.getCombo(sConectar.conectar(), sConcejales.getAllConcejal(), cmbConsejal);
                    }
                    else
                    {
                        try
                        {
                            if (isVisible() && cmbTipo.getSelectedIndex() != 0) {
                                sComisiones com = new sComisiones();

                                sConecxion.getInformationSin(com, sConectar.conectar(), sComisiones.getElementComiciones(), (String)cmbTipo.getSelectedItem());

                                int idComision = com.getIdComision();

                                cmbConsejal.removeAllItems();
                                cmbConsejal.addItem("Todos los concejales");
                                sConecxion.getCombo(sConectar.conectar(), sOrganizacionConcejales.getConcejal(idComision), cmbConsejal);
                            }
                        }
                        catch(Exception ex){}
                    }
                    removePeticiones();
                    createPeticiones();
                }
            }
        });
        
        cmbConsejal.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
                visibleC2 = false;
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                visibleC2 = false;
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
                visibleC2 = true;
            }
        });
        
        cmbConsejal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if (visibleC2) {
                    if (cmbConsejal.getSelectedIndex() == 0) {

                        conce = "";
                    }
                    else
                    {
                        try
                        {
                            if (isVisible() && cmbConsejal.getSelectedIndex() != 0) {
                                sConcejales com = new sConcejales();

                                sConecxion.getInformationSin(com, sConectar.conectar(), sConcejales.getEapecificElementId(), (String)cmbConsejal.getSelectedItem());

                                int idConcejal = com.getIdConcejal();

                                conce = " id_concejal = " +idConcejal+ " ";
                            }
                        }
                        catch(Exception ex){}
                    }
                
                
                    removePeticiones();
                    createPeticiones();
                }
            }
        });
        
        //Peticiones
        pInfo = new jPeticionInfo[new Random().nextInt(40)];
        setPeticiones();
        
        spStep.setBounds(((jlCategorias[0].getWidth()) / 2) - ((jlCategorias[0].getWidth() * 4 / 5) / 2), ((jlCategorias[0].getHeight()) / 2) - ((jlCategorias[0].getHeight() / 2) / 2), jlCategorias[0].getWidth() * 4 / 5, jlCategorias[0].getHeight() / 2 );
        jStep.add(spStep);
        
        jScrollBarPeticion.setBounds(width - (width / 50) - (width / 300), (jHeader.getHeight() + jFiltro.getHeight()), width / 50, height - (jHeader.getHeight() + jFiltro.getHeight()));
        setValuePeticiones();
    
        //Peticiones Title------------------------------------------------------
        int fontHeight = jHeader.getHeight() / 2;
        fontAclade.deriveFont(Font.PLAIN, fontHeight);
        lblTitle.setFont(fontAclade);
        lblTitle.setBounds((btnIcon.getX() * 2) + btnIcon.getWidth(), fontHeight - (fontHeight / 2), fontHeight * lblTitle.getText().length(), fontHeight);
    
        //La añadimos los combo a los filtros
        jFiltro.add(cmbTipo);
        jFiltro.add(cmbCategoria);
        jFiltro.add(cmbConsejal);
    }
    
    _Control control = new _Control();
    public jPeticion(final jPeticionModific peticionModific)
    {
        _Hilos hilo = new _Hilos(1);
        
        hilo.establecerAccion(new _Acciones(){
            public boolean acto() {

                inicio(peticionModific);

                //Saber que se realizo la accion
                return true;
            }
        });

        control.evaluarHilo(hilo);
    }
    
    //Posicionar e inicializar las peticiones
    private void setPeticiones()
    {
        ArrayList<sInfo> bast;
        
        if (!cate.equals("") || !conce.equals("")) {
            
            String var = "";
            
            if (cate.equals("") || conce.equals("")) {
                var = "WHERE"+cate+conce;
            }
            else
            {
                var = "WHERE" +cate +" AND " +conce;
            }
            
            bast = sConecxion.getInformation(new sPeticiones(), sConectar.conectar(), sPeticiones.getAllPeticiones(var));
        }
        else{
            bast = sConecxion.getInformation(new sPeticiones(), sConectar.conectar(), sPeticiones.getAllPeticiones());
        }
        
        pInfo = new jPeticionInfo[bast.size()];
        setValuePeticiones();
        
        //Creamos las peticiones
        for (int i = 0; i < pInfo.length; i++) {
            //pInfo[i] = new jPeticionInfo(this.width - jSeleccion.getWidth(), (this.height - jHeader.getHeight() - jFiltro.getHeight()) / 10, "Carlos Lemus", "Aqui se pondran las decripcion de la peticion que ha enviado el usuario con un limite de texto", "C", new Random().nextInt(8), this, peticionModific);
            //pInfo[i].setBounds(jSeleccion.getWidth(), (jHeader.getHeight() + jFiltro.getHeight()) + (((this.height - (jHeader.getHeight() + jFiltro.getHeight())) / 10) * i), this.width - jSeleccion.getWidth(), (this.height - jHeader.getHeight() - jFiltro.getHeight()) / 10);
            //this.add(pInfo[i]);
            
            sPeticiones pet = (sPeticiones)bast.get(i);
            
            //Paneles
            pInfo[i] = new jPeticionInfo(this.width - jSeleccion.getWidth(), (this.height - jHeader.getHeight() - jFiltro.getHeight()) / 10, this, peticionModific, pet);
            pInfo[i].setBounds(jSeleccion.getWidth(), (jHeader.getHeight() + jFiltro.getHeight()) + (((this.height - (jHeader.getHeight() + jFiltro.getHeight())) / 10) * i), this.width - jSeleccion.getWidth(), (this.height - jHeader.getHeight() - jFiltro.getHeight()) / 10);
            
            
            try{pInfo[i].setImg(ImageIO.read(new File("Img/Peticiones/avatar.png")));}catch(Exception ex){System.out.println(ex.getMessage());}
            
            pInfo[i].setPaint();
            
            this.add(pInfo[i]);
        }
    }
    
     private void setPeticionesCase(){
        ArrayList<sInfo> bast = new ArrayList<sInfo>();
        
        
        switch(estadoFiltro){
            case 1:
                bast = sConecxion.getInformation(new sPeticiones(), sConectar.conectar(), sPeticiones.getAllPeticiones());
                resetCmb();
                break;
            case 2:
                bast = sConecxion.getInformation(new sPeticiones(), sConectar.conectar(), sPeticiones.getPeticionesAproved());
                resetCmb();
                break;
            case 3:
                try{
                    
                    //JOptionPane.showMessageDialog(null, "uso");
                    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
                    dateInfo.setDateFormatString("yyyy-MM-dd");
                    String fecha = formatoFecha.format(dateInfo.getDate());
                    String date = "";
                    for (int i = 0; i < fecha.length(); i++) {
                        char x = fecha.charAt(i);    
                        if (x != '-') {
                            date += x;
                        }
                    }
                    bast = sConecxion.getInformationDate(new sPeticiones(), sConectar.conectar(), sPeticiones.getPeticionesFecha(date));
                    resetCmb();
                }catch(Exception e){}
                break;
            case 4:
                bast = sConecxion.getInformation(new sPeticiones(), sConectar.conectar(), sPeticiones.getPeticionesStop());
                resetCmb();
                break;
            case 5:
                bast = sConecxion.getInformation(new sPeticiones(), sConectar.conectar(), sPeticiones.getPeticionesFechaMax("(SELECT MAX(fecha) FROM tab_peticiones)"));
                resetCmb();
                break;
            case 6:
                bast = sConecxion.getInformation(new sPeticiones(), sConectar.conectar(), sPeticiones.getPeticionesStep(""+(this.spStep.getSelectedIndex() + 1)));
                resetCmb();
                break;
            case 7:
                bast = sConecxion.getInformation(new sPeticiones(), sConectar.conectar(), sPeticiones.getPeticionesFinish());
                resetCmb();
                break;
            case 8:
                bast = sConecxion.getInformation(new sPeticiones(), sConectar.conectar(), sPeticiones.getPeticionesDelete());
                resetCmb();
                break;
        }
        pInfo = new jPeticionInfo[bast.size()];
        setValuePeticiones();
        //Creamos las peticiones
        for (int i = 0; i < pInfo.length; i++) {
            sPeticiones pet = (sPeticiones)bast.get(i);
            pInfo[i] = new jPeticionInfo(this.width - jSeleccion.getWidth(), (this.height - jHeader.getHeight() - jFiltro.getHeight()) / 10, this, peticionModific, pet);
            pInfo[i].setBounds(jSeleccion.getWidth(), (jHeader.getHeight() + jFiltro.getHeight()) + (((this.height - (jHeader.getHeight() + jFiltro.getHeight())) / 10) * i), this.width - jSeleccion.getWidth(), (this.height - jHeader.getHeight() - jFiltro.getHeight()) / 10);
            try{pInfo[i].setImg(ImageIO.read(new File("Img/Peticiones/avatar.png")));}catch(Exception ex){System.out.println(ex.getMessage());}
            pInfo[i].setPaint();
            this.add(pInfo[i]);
        }
    }
    
    //Posicionar los paneles
    private void panel(byte estado)
    {
        switch(estado){
            case 1:
                
                for (int i = 0; i < jlCategorias.length; i++) {
                    if(jlCategorias[i].getText().equals("Ver por fechas")){
                        jInfo.setBounds(jlCategorias[i].getX(), jlCategorias[i].getY() + jlCategorias[i].getHeight(), jlCategorias[i].getWidth(), jlCategorias[i].getHeight());

                        if ((i + 1) < (jlCategorias.length))
                            jlCategorias[i + 1].setLocation(jInfo.getX(), jInfo.getY() + jInfo.getHeight());

                        for (int j = i + 2; j < jlCategorias.length; j++) {
                            jlCategorias[j].setLocation(jlCategorias[j - 1].getX(), jlCategorias[j - 1].getY() + jlCategorias[j - 1].getHeight());
                        }
                    }
                }
                
                break;
                
            case 2:
                for (int i = 0; i < jlCategorias.length; i++) {
                    if(jlCategorias[i].getText().equals("Ver por fechas")){
                        for (int j = i + 1; j < jlCategorias.length; j++) {
                            jlCategorias[j].setLocation(jlCategorias[j - 1].getX(), jlCategorias[j - 1].getY() + jlCategorias[j - 1].getHeight());
                            
                            if(fondoName.equals(jlCategorias[j].getText()))
                            {
                                fondo.setLocation(0, jlCategorias[j].getY());
                            }
                        }
                    }
                }
                break;
                
            case 3:
                
                for (int i = 0; i < jlCategorias.length; i++) {
                    if(jlCategorias[i].getText().equals("Ver por fechas")){
                        
                        if ((i + 1) < (jlCategorias.length)){
                            jlCategorias[i + 1].setLocation(jInfo.getX(), jInfo.getY() + jInfo.getHeight());
                            
                            if(fondoName.equals(jlCategorias[i + 1].getText()))
                            {
                                fondo.setLocation(0, jlCategorias[i + 1].getY());
                            }
                        }

                        for (int j = i + 2; j < jlCategorias.length; j++) {
                            jlCategorias[j].setLocation(jlCategorias[j - 1].getX(), jlCategorias[j - 1].getY() + jlCategorias[j - 1].getHeight());
                        
                            if(fondoName.equals(jlCategorias[j].getText()))
                            {
                                fondo.setLocation(0, jlCategorias[j].getY());
                            }
                        }
                    }
                }
                
                break;
                
            case 4:
                
                for (int i = 0; i < jlCategorias.length; i++) {
                    if(jlCategorias[i].getText().equals("Ver por fechas")){
                        jInfo.setBounds(jlCategorias[i].getX(), jlCategorias[i].getY() + jlCategorias[i].getHeight(), jlCategorias[i].getWidth(), 0);

                        if ((i + 1) < (jlCategorias.length))
                            jlCategorias[i + 1].setLocation(jInfo.getX(), jInfo.getY() + jInfo.getHeight());

                        for (int j = i + 2; j < jlCategorias.length; j++) {
                            jlCategorias[j].setLocation(jlCategorias[j - 1].getX(), jlCategorias[j - 1].getY() + jlCategorias[j - 1].getHeight());
                        }
                    }
                }
                
                dir = DOWN;
                
                break;
        }
    }
    
    //Segundo panel
    private void panelStep(byte estado)
    {
        switch(estado){
            case 1:
                
                for (int i = 0; i < jlCategorias.length; i++) {
                    if(jlCategorias[i].getText().equals("Ver por progreso")){
                        jStep.setBounds(jlCategorias[i].getX(), jlCategorias[i].getY() + jlCategorias[i].getHeight(), jlCategorias[i].getWidth(), jlCategorias[i].getHeight());

                        if ((i + 1) < (jlCategorias.length))
                            jlCategorias[i + 1].setLocation(jStep.getX(), jStep.getY() + jStep.getHeight());

                        for (int j = i + 2; j < jlCategorias.length; j++) {
                            jlCategorias[j].setLocation(jlCategorias[j - 1].getX(), jlCategorias[j - 1].getY() + jlCategorias[j - 1].getHeight());
                        }
                    }
                }
                
                break;
                
            case 2:
                for (int i = 0; i < jlCategorias.length; i++) {
                    if(jlCategorias[i].getText().equals("Ver por progreso")){
                        for (int j = i + 1; j < jlCategorias.length; j++) {
                            jlCategorias[j].setLocation(jlCategorias[j - 1].getX(), jlCategorias[j - 1].getY() + jlCategorias[j - 1].getHeight());
                        
                            if(fondoName.equals(jlCategorias[j].getText()))
                            {
                                fondo.setLocation(0, jlCategorias[j].getY());
                            }
                        }
                    }
                }
                break;
                
            case 3:
                
                for (int i = 0; i < jlCategorias.length; i++) {
                    if(jlCategorias[i].getText().equals("Ver por progreso")){
                        
                        if ((i + 1) < (jlCategorias.length)){
                            jlCategorias[i + 1].setLocation(jStep.getX(), jStep.getY() + jStep.getHeight());
                            
                            if(fondoName.equals(jlCategorias[i + 1].getText()))
                            {
                                fondo.setLocation(0, jlCategorias[i + 1].getY());
                            }
                        }

                        for (int j = i + 2; j < jlCategorias.length; j++) {
                            jlCategorias[j].setLocation(jlCategorias[j - 1].getX(), jlCategorias[j - 1].getY() + jlCategorias[j - 1].getHeight());
                        
                            if(fondoName.equals(jlCategorias[j].getText()))
                            {
                                fondo.setLocation(0, jlCategorias[j].getY());
                            }
                        }
                    }
                }
                
                break;
                
            case 4:
                
                for (int i = 0; i < jlCategorias.length; i++) {
                    if(jlCategorias[i].getText().equals("Ver por progreso")){
                        jStep.setBounds(jlCategorias[i].getX(), jlCategorias[i].getY() + jlCategorias[i].getHeight(), jlCategorias[i].getWidth(), 0);

                        if ((i + 1) < (jlCategorias.length))
                            jlCategorias[i + 1].setLocation(jStep.getX(), jStep.getY() + jStep.getHeight());

                        for (int j = i + 2; j < jlCategorias.length; j++) {
                            jlCategorias[j].setLocation(jlCategorias[j - 1].getX(), jlCategorias[j - 1].getY() + jlCategorias[j - 1].getHeight());
                        }
                    }
                }
                
                dir2 = DOWN;
                
                break;
        }
    }
    
    //Desaprarecer el panel
    Timer timePanel = new Timer(10, new ActionListener()
    {
        public void actionPerformed(ActionEvent e) {
            //Quitar posicion
            if (dir == UP){
                jInfo.setSize(jInfo.getWidth(), jInfo.getHeight() - 3);
                panel((byte)3);

                if (jInfo.getHeight() <= 0){
                    panelOut();
                    jInfo.setSize(jInfo.getWidth(), 0);
                    panel((byte)2);
                    dir = DOWN;
                    canTime = true;
                }
            }
            else if (dir == DOWN)
            {
                jInfo.setSize(jInfo.getWidth(), jInfo.getHeight() + 3);
                panel((byte)3);

                if (jInfo.getHeight() >= jlCategorias[0].getHeight()){
                    panelOut();
                    jInfo.setSize(jInfo.getWidth(), jlCategorias[0].getHeight());
                    panel((byte)1);
                    dir = UP;
                    canTime = true;
                }
            }
        }
    });
    
    Timer timePanelStep = new Timer(10, new ActionListener()
    {
        public void actionPerformed(ActionEvent e) {
            //Quitar posicion
            if (dir2 == UP){
                jStep.setSize(jStep.getWidth(), jStep.getHeight() - 3);
                panelStep((byte)3);

                if (jStep.getHeight() <= 0){
                    panelStepOut();
                    jStep.setSize(jStep.getWidth(), 0);
                    panelStep((byte)2);
                    dir2 = DOWN;
                    canStep = true;
                }
            }
            else if (dir2 == DOWN)
            {
                jStep.setSize(jStep.getWidth(), jStep.getHeight() + 3);
                panelStep((byte)3);

                if (jStep.getHeight() >= jlCategorias[0].getHeight()){
                    panelStepOut();
                    jStep.setSize(jStep.getWidth(), jlCategorias[0].getHeight());
                    panelStep((byte)1);
                    dir2 = UP;
                    canStep = true;
                }
            }
        }
    });
    
    //Quitar el Timer
    private void panelOut()
    {
        timePanel.stop();
    }
    
    private void panelStepOut()
    {
        timePanelStep.stop();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        basicTreeCellRenderer1 = new org.edisoncor.gui.tree.cellRenderer.BasicTreeCellRenderer();
        jHeader = new javax.swing.JPanel();
        btnIcon = new javax.swing.JButton();
        btnClose = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        jFiltro = new javax.swing.JPanel();
        jSeleccion = new javax.swing.JPanel();
        jScrollBarFiltro = new org.edisoncor.gui.scrollBar.ScrollBarCustom();
        jInfo = new javax.swing.JPanel();
        dateInfo = new com.toedter.calendar.JDateChooser();
        jStep = new javax.swing.JPanel();
        jScrollBarPeticion = new org.edisoncor.gui.scrollBar.ScrollBarCustom();

        setBackground(new java.awt.Color(41, 51, 127));

        jHeader.setBackground(new java.awt.Color(12, 22, 71));

        btnIcon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIconActionPerformed(evt);
            }
        });

        btnClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCloseMouseClicked(evt);
            }
        });
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });

        lblTitle.setBackground(new java.awt.Color(255, 255, 255));
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setText("Peticiones");

        javax.swing.GroupLayout jHeaderLayout = new javax.swing.GroupLayout(jHeader);
        jHeader.setLayout(jHeaderLayout);
        jHeaderLayout.setHorizontalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnIcon)
                .addGap(18, 18, 18)
                .addComponent(lblTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnClose)
                .addGap(19, 19, 19))
        );
        jHeaderLayout.setVerticalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnIcon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnClose, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
            .addGroup(jHeaderLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(lblTitle)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jFiltro.setBackground(new java.awt.Color(12, 22, 71));

        javax.swing.GroupLayout jFiltroLayout = new javax.swing.GroupLayout(jFiltro);
        jFiltro.setLayout(jFiltroLayout);
        jFiltroLayout.setHorizontalGroup(
            jFiltroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 599, Short.MAX_VALUE)
        );
        jFiltroLayout.setVerticalGroup(
            jFiltroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 44, Short.MAX_VALUE)
        );

        jSeleccion.setBackground(new java.awt.Color(69, 84, 194));

        jScrollBarFiltro.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
            public void adjustmentValueChanged(java.awt.event.AdjustmentEvent evt) {
                jScrollBarFiltroAdjustmentValueChanged(evt);
            }
        });
        jScrollBarFiltro.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jScrollBarFiltroPropertyChange(evt);
            }
        });
        jScrollBarFiltro.addVetoableChangeListener(new java.beans.VetoableChangeListener() {
            public void vetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {
                jScrollBarFiltroVetoableChange(evt);
            }
        });

        jInfo.setBackground(new java.awt.Color(69, 84, 194));

        dateInfo.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                dateInfoInputMethodTextChanged(evt);
            }
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        dateInfo.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dateInfoPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jInfoLayout = new javax.swing.GroupLayout(jInfo);
        jInfo.setLayout(jInfoLayout);
        jInfoLayout.setHorizontalGroup(
            jInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jInfoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(dateInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jInfoLayout.setVerticalGroup(
            jInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInfoLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(dateInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        jStep.setBackground(new java.awt.Color(69, 84, 194));

        javax.swing.GroupLayout jStepLayout = new javax.swing.GroupLayout(jStep);
        jStep.setLayout(jStepLayout);
        jStepLayout.setHorizontalGroup(
            jStepLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 101, Short.MAX_VALUE)
        );
        jStepLayout.setVerticalGroup(
            jStepLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 67, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jSeleccionLayout = new javax.swing.GroupLayout(jSeleccion);
        jSeleccion.setLayout(jSeleccionLayout);
        jSeleccionLayout.setHorizontalGroup(
            jSeleccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jSeleccionLayout.createSequentialGroup()
                .addGroup(jSeleccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jSeleccionLayout.createSequentialGroup()
                        .addContainerGap(11, Short.MAX_VALUE)
                        .addComponent(jStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8))
                    .addGroup(jSeleccionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jScrollBarFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jSeleccionLayout.setVerticalGroup(
            jSeleccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jSeleccionLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jSeleccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jSeleccionLayout.createSequentialGroup()
                        .addComponent(jScrollBarFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jSeleccionLayout.createSequentialGroup()
                        .addComponent(jStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(87, 87, 87)
                        .addComponent(jInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        jScrollBarPeticion.addAdjustmentListener(new java.awt.event.AdjustmentListener() {
            public void adjustmentValueChanged(java.awt.event.AdjustmentEvent evt) {
                jScrollBarPeticionAdjustmentValueChanged(evt);
            }
        });
        jScrollBarPeticion.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jScrollBarPeticionPropertyChange(evt);
            }
        });
        jScrollBarPeticion.addVetoableChangeListener(new java.beans.VetoableChangeListener() {
            public void vetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {
                jScrollBarPeticionVetoableChange(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jFiltro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jSeleccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollBarPeticion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addComponent(jSeleccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(jScrollBarPeticion, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jScrollBarFiltroPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jScrollBarFiltroPropertyChange
        
    }//GEN-LAST:event_jScrollBarFiltroPropertyChange

    private void jScrollBarFiltroVetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {//GEN-FIRST:event_jScrollBarFiltroVetoableChange
        
    }//GEN-LAST:event_jScrollBarFiltroVetoableChange

    private void jScrollBarFiltroAdjustmentValueChanged(java.awt.event.AdjustmentEvent evt) {//GEN-FIRST:event_jScrollBarFiltroAdjustmentValueChanged
        for (int i = 0; i < jlCategorias.length; i++) {
            jlCategorias[i].setBounds(jSeleccion.getWidth() / 20, (i * (jSeleccion.getHeight() / limite)) - (evt.getValue()), jSeleccion.getWidth() - jScrollBarFiltro.getWidth() - (jSeleccion.getWidth() / 20), jSeleccion.getHeight() / limite);
        }
    }//GEN-LAST:event_jScrollBarFiltroAdjustmentValueChanged

    private void jScrollBarPeticionAdjustmentValueChanged(java.awt.event.AdjustmentEvent evt) {//GEN-FIRST:event_jScrollBarPeticionAdjustmentValueChanged
        //Cmabio de posicion
        for (int i = 0; i < pInfo.length; i++) {
            try{
            pInfo[i].setBounds(jSeleccion.getWidth(), ((jHeader.getHeight() + jFiltro.getHeight()) + (((this.height - (jHeader.getHeight() + jFiltro.getHeight())) / 10) * i)) - (evt.getValue()), this.width - jSeleccion.getWidth(), (this.height - jHeader.getHeight() - jFiltro.getHeight()) / 10);
            }catch(Exception ex){  }
        }
    }//GEN-LAST:event_jScrollBarPeticionAdjustmentValueChanged

    private void jScrollBarPeticionPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jScrollBarPeticionPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollBarPeticionPropertyChange

    private void jScrollBarPeticionVetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {//GEN-FIRST:event_jScrollBarPeticionVetoableChange
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollBarPeticionVetoableChange

    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag); //To change body of generated methods, choose Tools | Templates.
    
        //Los combo box
        this.cmbConsejal.removeAllItems();
        this.cmbConsejal.addItem("Todos los concejales");
        sConecxion.getCombo(sConectar.conectar(), sConcejales.getAllConcejal(), this.cmbConsejal);
        
        this.cmbCategoria.removeAllItems();
        this.cmbCategoria.addItem("Todas las categorias");
        sConecxion.getCombo(sConectar.conectar(), sCategoria.getAllCategoria(), this.cmbCategoria);
        
        this.cmbTipo.removeAllItems();
        this.cmbTipo.addItem("Todas las comisiones");
        sConecxion.getCombo(sConectar.conectar(), sComisiones.getAllComisiones(), this.cmbTipo);
        
        
    }
    
    private void btnCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCloseMouseClicked
        // TODO add your handling code here:
        this.setVisible(false);
        resetPeticion();
        fondo.setLocation(0, -100);
        canTimeUse = false;
        //si no es igual a vacio
        if (!idTimer.equals(""))
        {
            if(idTimer.equals("Ver por fechas")){
                if (dir == UP){
                    timePanel.start();
                    canTime = false;
                    idTimer = "";
                }
            }
            else if(idTimer.equals("Ver por progreso")){
                if (dir2 == UP){
                    timePanelStep.start();
                    canStep = false;
                    idTimer = "";
                    spStep.setSelectedIndex(0);
                }
            }
            
            idTimer = "";
        }
    }//GEN-LAST:event_btnCloseMouseClicked

    public void setFormatear()
    {
        resetPeticion();
        fondo.setLocation(0, -100);
        
        //si no es igual a vacio
        if (!idTimer.equals(""))
        {
            if(idTimer.equals("Ver por fechas")){
                if (dir == UP){
                    timePanel.start();
                    canTime = false;
                    idTimer = "";
                }
            }
            else if(idTimer.equals("Ver por progreso")){
                if (dir2 == UP){
                    timePanelStep.start();
                    canStep = false;
                    idTimer = "";
                    spStep.setSelectedIndex(0);
                }
            }
            
            idTimer = "";
        }
    }
    
    private void btnIconActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIconActionPerformed
       
    }//GEN-LAST:event_btnIconActionPerformed

    private void dateInfoPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_dateInfoPopupMenuWillBecomeInvisible
        
    }//GEN-LAST:event_dateInfoPopupMenuWillBecomeInvisible

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCloseActionPerformed

    private void dateInfoInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_dateInfoInputMethodTextChanged
       
    }//GEN-LAST:event_dateInfoInputMethodTextChanged

    private void dateInfoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dateInfoPropertyChange
        if (canTimeUse) {
            removePeticiones();
            setPeticionesCase();
            //JOptionPane.showMessageDialog(null, "En uso");
        }
    }//GEN-LAST:event_dateInfoPropertyChange

    private void setValue()
    {
        if (categoriaCount > limite){
            //Obtenemos lo valores
            double valor = (double)categoriaCount/(double)limite;


           jScrollBarFiltro.setMaximum((jSeleccion.getHeight() * (int)valor));
           jScrollBarFiltro.setValue(0);
        }
        else
        {
            jScrollBarFiltro.setVisible(false);
        }
    }
    
    //Valor de la barra
    public void changeVisible(boolean state)
    {
        this.setVisible(state);
        jScrollBarPeticion.setValue(0);
        resetPeticion();
    }
    
    //Cambiar los elementos
    private void resetPeticion()
    {
        removePeticiones();
        createPeticiones();
        this.repaint();
    }
    
    //Valor del scroll bar
    private void setValuePeticiones()
    {
        if (pInfo.length > limite){
            //Obtenemos lo valores
            double valor = (double)pInfo.length/(double)limite;


           jScrollBarPeticion.setMaximum((jSeleccion.getHeight() * (int)valor));
           jScrollBarPeticion.setValue(0);
           jScrollBarPeticion.setVisible(true);
        }
        else
        {
            jScrollBarPeticion.setVisible(false);
        }
    }

    //--------------------------------------------------------------------------
    //Metodo que servira para elimar todos los elementos
    private void removePeticiones()
    {
        //El ciclo for para eliminar las periciones
        for (int i = 0; i < pInfo.length; i++) {
            this.remove(pInfo[i]);
        }
        
        //Eliminar los componentes de la matriz
        pInfo = null;
        
        //Lo repintamos
        this.repaint();
    }
    
    //Le creamos una cantidad nueva aleatoriamente
    private void createPeticiones()
    {
        setPeticiones();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.tree.cellRenderer.BasicTreeCellRenderer basicTreeCellRenderer1;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnIcon;
    private com.toedter.calendar.JDateChooser dateInfo;
    private javax.swing.JPanel jFiltro;
    private javax.swing.JPanel jHeader;
    private javax.swing.JPanel jInfo;
    private org.edisoncor.gui.scrollBar.ScrollBarCustom jScrollBarFiltro;
    private org.edisoncor.gui.scrollBar.ScrollBarCustom jScrollBarPeticion;
    private javax.swing.JPanel jSeleccion;
    private javax.swing.JPanel jStep;
    private javax.swing.JLabel lblTitle;
    // End of variables declaration//GEN-END:variables
}
