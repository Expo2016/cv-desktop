/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Conexion.xReportes;
import Extras.jBajarFtp;
import Extras.jComboBox;
import Extras.jGraficos;
import Extras.jReporteDetallado;
import Extras.jTool;
import Extras.jLista;
import Extras.jReporteListado;
import Hilos._Acciones;
import Hilos._Control;
import Hilos._Hilos;
import MVC.sInterfaz;
import MVC.sAuditoria;
import MVC.sCategoria;
import MVC.sComentarios;
import MVC.sConcejales;
import MVC.sConectar;
import MVC.sConecxion;
import MVC.sPerfil;
import MVC.sPeticiones;
import MVC.tabComentarios;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Maritza
 */
public class jReportes extends javax.swing.JPanel {

    /**
     * Creates new form jReportes
     */
    
    _Control control = new _Control();
    
    //Saber si el date esta en uso
    boolean dateUse = false, dateUse2 = false;
    
    //Fuente
    private Font fuenteAcalde = null;
    
    //Propiedades del tamaño
    private int width, height;
    
    //Tamaños estandar
    private int btnSize = 24, elementSize = 34;
    
    //Combo box
    jComboBox scbCategoria = new jComboBox();
    jComboBox scbConsejal = new jComboBox();
    jComboBox scbEtapas = new jComboBox();
    jComboBox scbOrdenar = new jComboBox();
    jComboBox scbFecha = new jComboBox();
    
    DefaultTableModel ModeloTabla;
    
    private String fecha1 = "";
    private String cate = "";
    private String conce = "";
    private String estado = "";
    private String fechaIngreso = "";
    private String fechaModificacion = "";
    private String Ordenar = "";
    private String allDate = "";
    
    //Parte del codigo
    private String[] getColumnas()
    {
        return new String[]{"No","Usuario","e_mail","Asunto","Categoria","Concejal","Descripcion","Estado de la peticion","Fecha"};
    }
    
    public void getTable()
    {
        //this.limpiarTabla(dtPeticion);
        
        ModeloTabla = new DefaultTableModel(null, getColumnas());
        //Filas
        sConecxion.tableInformation(sConectar.conectar(), sPeticiones.allTable(), getColumnas().length, ModeloTabla);
        
        
    }
    
    public void getTableInfo()
    {
        sConecxion.tableInformation(sConectar.conectar(), sPeticiones.allTable(cate+" "+conce+" "+estado+" "+fechaModificacion+" "+allDate, Ordenar), getColumnas().length, ModeloTabla);
    }
    
    boolean canUseDate = false;
    
    public void inicio(int width, int height) {
        getTable();
        
        initComponents();
        
        this.dtFecha1.setMaxSelectableDate(new Date());
        this.dtFecha2.setMaxSelectableDate(new Date());
        
        //Fuente
        try
        {
            InputStream is = getClass().getResourceAsStream("font/MYRIADPRO-REGULAR.tff");
            fuenteAcalde = Font.createFont(Font.TRUETYPE_FONT, is);
        }
        catch(Exception e)
        {
            fuenteAcalde = this.getFont();
        }
        
        //Ingresamos los valores de los combobox
        scbCategoria.setModel( new DefaultComboBoxModel( new String[] { "Escoger todos las categorias" }));
        scbConsejal.setModel( new DefaultComboBoxModel( new String[] { "Escoger todos los consejales" }));
        scbEtapas.setModel( new DefaultComboBoxModel( new String[] { "todas las etapas", "Registrada", "Aceptada", "Asignada", "Finalizada", "Rechazada"}));
        scbOrdenar.setModel( new DefaultComboBoxModel( new String[] { "Por orden alfabético", "Por orden alfabético a la inversa", "Por orden de indice", "Por ornden de indice a la inversa", "Por orden de fechas", "Por orden de fechas inversas" } ));
        scbFecha.setModel( new DefaultComboBoxModel( new String[] { "Todos los registros", "Ayer y hoy", "Esta semana", "Este mes", "Este trimestre", "Este año" }));
        
        scbCategoria.addItemListener(new ItemListener()
        {

            @Override
            public void itemStateChanged(ItemEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                
                if (scbCategoria.getSelectedIndex() == 0) {
                    cate = "";
                }
                else
                {
                    Integer id = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sCategoria.getElementCategoria(), (String)scbCategoria.getSelectedItem());
                
                    cate = "AND pet.id_categoria = " + id;
                }
                
                limpiarTabla(dtPeticion);
                getTableInfo();
                changeEstate();
            }
            
        });
        
        scbConsejal.addItemListener(new ItemListener()
        {

            @Override
            public void itemStateChanged(ItemEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                
                if (scbConsejal.getSelectedIndex() == 0) {
                    conce = "";
                }
                else
                {
                    Integer id = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sConcejales.getEapecificElementId(), (String)scbConsejal.getSelectedItem());
                
                    conce = "AND pet.id_concejal = " + id;
                }
                
                limpiarTabla(dtPeticion);
                getTableInfo();
                changeEstate();
            }
            
        });
        
        scbEtapas.addItemListener(new ItemListener()
        {

            @Override
            public void itemStateChanged(ItemEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                if (scbEtapas.getSelectedIndex() == 0) {
                    estado = "";
                }
                else
                {
                    int id = scbEtapas.getSelectedIndex();
                    estado = "AND pet.id_estadopet = " + id;
                }
                
                limpiarTabla(dtPeticion);
                getTableInfo();
                changeEstate();
            }
            
        });
        
        
        scbOrdenar.addItemListener(new ItemListener()
        {

            @Override
            public void itemStateChanged(ItemEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                switch(scbOrdenar.getSelectedIndex())
                {
                    case 0:
                        Ordenar = "ORDER BY per.nombre";
                        break;
                        
                    case 1:
                        Ordenar = "ORDER BY per.nombre DESC";
                        break;
                        
                    case 2:
                        Ordenar = "ORDER BY pet.id_peticion";
                        break;
                        
                    case 3:
                        Ordenar = "ORDER BY pet.id_peticion DESC";
                        break;
                        
                    case 4:
                        Ordenar = "ORDER BY pet.fecha";
                        break;
                        
                    case 5:
                        Ordenar = "ORDER BY pet.fecha DESC";
                        break;
                }
                
                limpiarTabla(dtPeticion);
                getTableInfo();
                changeEstate();
            }
            
        });
        //Propiedades del scb
        int scbWidht;
        
        //Informacion para el lbl
        int fontSize = width / 70;
        int sizeLeng = fontSize * lblGenerar.getText().length();
        
        sizeLeng = (sizeLeng * 3) / 4;
        
        int kHeight = height / 100;
        
        //Disposiciones
        this.setLayout(null);
        jHeader.setLayout(null);
        jElement.setLayout(null);
        
        //Propiedadel del tamaño
        this.width = width;
        this.height = height;
        
        //----------------------------------------------------------------------
        jHeader.setBounds(0, 0, width, elementSize);
        
        //Elementos del data grid
        int heightData = (height - elementSize) / 3;
        
        jScrollPane1.setBounds(0, heightData + elementSize, width, (heightData * 2) - (btnSize + kHeight));
        
        //Quitar las opciones por defecto 
        dtPeticion.getTableHeader().setReorderingAllowed(false);
        
        //Posicionarl el label
        fuenteAcalde.deriveFont(Font.PLAIN, fontSize);
        
        lblGenerar.setFont(fuenteAcalde);
        lblGenerar.setBounds(width - sizeLeng, height - (btnSize + kHeight), sizeLeng, btnSize);
        lblGenerar.setIcon(new ImageIcon("Img/Reportes/pdfOne.png"));
        
        int sizeDoc = lblDocumentos.getText().length() * fontSize;
        sizeDoc = sizeDoc * 3 / 4;
        
        lblDocumentos.setFont(fuenteAcalde);
        lblDocumentos.setBounds(width - (lblGenerar.getWidth() + sizeDoc), height - (btnSize + kHeight), sizeDoc, btnSize);
        lblDocumentos.setIcon(new ImageIcon("Img/Reportes/folder.png"));
        
        lblGraficos.setFont(fuenteAcalde);
        lblGraficos.setBounds(btnSize, height - (btnSize + kHeight), sizeDoc, btnSize);
        lblGraficos.setIcon(new ImageIcon("Img/Reportes/graphic.png"));
        
        lblGraficosEst.setFont(fuenteAcalde);
        lblGraficosEst.setBounds(lblGraficos.getX() + lblGraficos.getWidth(), height - (btnSize + kHeight), sizeDoc, btnSize);
        lblGraficosEst.setIcon(new ImageIcon("Img/Reportes/calendar.png"));

        lblControl.setFont(fuenteAcalde);
        lblControl.setBounds(lblGraficosEst.getX() + lblGraficosEst.getWidth(), height - (btnSize + kHeight), sizeDoc, btnSize);
        lblControl.setIcon(new ImageIcon("Img/Reportes/list.png"));
        
        //-------------------------------------------------------------------------
        jTool.btnTransparente(btnIcon, btnExit, btnLoad);
        
        btnIcon.setIcon(new ImageIcon("Img/Menu/Opcion1(2).png"));
        btnExit.setIcon(new ImageIcon("Img/Peticiones/Close.png"));
        
        //Lo posicionamos
        int mitad = (elementSize / 2) - (btnSize / 2);
        int K = width / 100;
        btnIcon.setBounds(K, mitad, btnSize, btnSize);
        btnExit.setBounds(width - (btnSize + K), mitad, btnSize, btnSize);
        
        //------------------------------------------------------------------------
        //Fuentes
        lblCategoria.setFont(fuenteAcalde);
        lblConsejal.setFont(fuenteAcalde);
        lblCategoria.setFont(fuenteAcalde);
        lblCategoria.setFont(fuenteAcalde);
        lblCategoria.setFont(fuenteAcalde);
        lblCategoria.setFont(fuenteAcalde);
        //Tamaño y posicion
        sizeLeng = ((fontSize * lblCategoria.getText().length()) * 3) / 4;
        lblCategoria.setBounds(K, jHeader.getHeight() + mitad, sizeLeng, btnSize);
        scbWidht = sizeLeng * 2;
        scbCategoria.setBounds(K, lblCategoria.getY() + lblCategoria.getHeight(), scbWidht, btnSize);
        
        sizeLeng = ((fontSize * lblConsejal.getText().length()) * 3) / 4;
        lblConsejal.setBounds(scbCategoria.getX() + scbCategoria.getWidth() + K, jHeader.getHeight() + mitad, sizeLeng, btnSize);
        scbConsejal.setBounds(scbCategoria.getX() + scbCategoria.getWidth() + K, lblCategoria.getY() + lblCategoria.getHeight(), scbWidht, btnSize);
        
        sizeLeng = ((fontSize * lblEstado.getText().length()) * 3) / 4;
        lblEstado.setBounds(scbConsejal.getX() + scbConsejal.getWidth() + K, jHeader.getHeight() + mitad, sizeLeng, btnSize);
        scbEtapas.setBounds(scbConsejal.getX() + scbConsejal.getWidth() + K, lblCategoria.getY() + lblCategoria.getHeight(), scbWidht, btnSize);
        
        sizeLeng = ((fontSize * lblOrden.getText().length()) * 3) / 4;
        lblFecha1.setBounds(scbEtapas.getX() + scbEtapas.getWidth() + K, jHeader.getHeight() + mitad, sizeLeng, btnSize);
        dtFecha1.setBounds(scbEtapas.getX() + scbEtapas.getWidth() + K, lblCategoria.getY() + lblCategoria.getHeight(), scbWidht, btnSize);
        
        sizeLeng = ((fontSize * lblFecha1.getText().length()) * 3) / 4;
        lblFecha2.setBounds(dtFecha1.getX() + dtFecha1.getWidth() + K, jHeader.getHeight() + mitad, sizeLeng, btnSize);
        dtFecha2.setBounds(dtFecha1.getX() + dtFecha1.getWidth() + K, lblCategoria.getY() + lblCategoria.getHeight(), scbWidht, btnSize);
        
        sizeLeng = ((fontSize * lblFecha2.getText().length()) * 3) / 4;
        lblOrden.setBounds(K, scbCategoria.getY() + scbCategoria.getHeight() + mitad, sizeLeng, btnSize);
        scbOrdenar.setBounds(K, lblOrden.getY() + lblOrden.getHeight(), scbWidht, btnSize);
        
        sizeLeng = ((fontSize * lblOrden.getText().length()) * 3) / 4;
        lblRango.setBounds((K * 2) + scbOrdenar.getWidth(), scbCategoria.getY() + scbCategoria.getHeight() + mitad, sizeLeng, btnSize);
        this.scbFecha.setBounds((K * 2) + scbOrdenar.getWidth(), lblRango.getY() + lblRango.getHeight(), scbWidht, btnSize);
        //----------------------------------------------------------------------
        add(scbCategoria);
        add(scbConsejal);
        add(scbEtapas);
        add(scbOrdenar);
        add(scbFecha);
        
        //Peticiones Title------------------------------------------------------
        int fontHeight = jHeader.getHeight() / 2;
        fuenteAcalde.deriveFont(Font.PLAIN, fontHeight);
        lblTitle.setFont(fuenteAcalde);
        lblTitle.setBounds((btnIcon.getX() * 2) + btnIcon.getWidth(), fontHeight - (fontHeight / 2), fontHeight * lblTitle.getText().length(), fontHeight);
    
        //Botones rapidos
        jElement.setBounds(0, jScrollPane1.getY() - elementSize, width, elementSize);
        
        //Posicionar los botones
        btnLoad.setIcon(new ImageIcon("Img/Reportes/garbage.png"));
        
        btnLoad.setBounds(K, mitad, btnSize, btnSize);
        
        scbFecha.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e) {
                //FILTRO POR RANGOS DE FECHAS
                
                
                //Formato para llamar a las fechas
                SimpleDateFormat form = new SimpleDateFormat("dd-MM-YY");
                DateFormat mes = new SimpleDateFormat("MM-YY");
                
                //Fecha actual
                Date fecha_actual = new Date();
                
                //Variable para hacer cálculos con respecto a la fecha actual
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(fecha_actual);
                int dias = 0;
                        
                switch (scbFecha.getSelectedIndex())
                {
                    
                    case 0:
                        //NINGÚN FILTRO
                        allDate = "";
                    break;
                    
                    case 1:
                        //De ayer y hoy
                        calendar.add(Calendar.DAY_OF_YEAR, -1);
                        allDate = " AND pet.fecha BETWEEN '" + form.format(calendar.getTime()) + "' AND '" + form.format(fecha_actual) + " 23:59'";
                    break;
                    
                    case 2:
                        //De esta semana
                        dias = calendar.get(Calendar.DAY_OF_WEEK) - 1;
                        calendar.add(Calendar.DAY_OF_YEAR, -dias);
                        allDate = " AND pet.fecha BETWEEN '" + form.format(calendar.getTime()) + "' AND '" + form.format(fecha_actual) + " 23:59'";
                    break;
                    
                    case 3:
                        //De este mes  
                        dias = calendar.get(Calendar.DAY_OF_MONTH) - 1;
                        calendar.add(Calendar.DAY_OF_YEAR, -dias);
                        allDate = " AND pet.fecha BETWEEN '" + form.format(calendar.getTime()) + "' AND '" + form.format(fecha_actual) + " 23:59'";
                    break;
                    
                    case 4:
                        //De este trimestre
                        int meses = 0;
                        int mes_actual = calendar.get(Calendar.MONTH);
                        //Los siguientes if clasifican en que "posición" del trimestre nos encontramos
                        if(mes_actual == 0 || mes_actual == 3 || mes_actual == 6 || mes_actual == 9)
                            meses = 0;
                        if(mes_actual == 1 || mes_actual == 4 || mes_actual == 7 || mes_actual == 10)
                            meses = 1;
                        if(mes_actual == 2 || mes_actual == 5 || mes_actual == 8 || mes_actual == 11)
                            meses = 2;
                        calendar.add(Calendar.MONTH, -meses);
                        //System.out.println(mes_actual + " " + meses);
                        dias = calendar.get(Calendar.DAY_OF_MONTH) - 1;
                        calendar.add(Calendar.DAY_OF_MONTH, -dias);
                        //System.out.println(form.format(calendar.getTime()));
                        allDate = " AND pet.fecha BETWEEN '" + form.format(calendar.getTime()) + "' AND '" + form.format(fecha_actual) + " 23:59'";
                    break;
                    
                    case 5:
                        //De este año
                        dias = calendar.get(Calendar.DAY_OF_YEAR) - 1;
                        calendar.add(Calendar.DAY_OF_YEAR, -dias);
                        allDate = " AND pet.fecha BETWEEN '" + form.format(calendar.getTime()) + "' AND '" + form.format(fecha_actual) + " 23:59'";
                    break;
                }
                limpiarTabla(dtPeticion);
                getTableInfo();
                changeEstate();
            
                if (dateUse) {
                    dtFecha1.setDate(null);
                    
                    dtFecha1.setMaxSelectableDate(new Date());
                    
                    fecha1 = "";
                    
                    dateUse=false;
                }
            }
        });
        
        //Validando que solo se pueda seleccionar un registro a la vez
            dtPeticion.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            
            //Fijando la posición de las columnas de la tabla
            dtPeticion.getTableHeader().setReorderingAllowed(false);
            
            //Deshabilitando navegación con teclado en la tabla
            InputMap iMap1 = dtPeticion.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
            
            KeyStroke stroke = KeyStroke.getKeyStroke("ENTER");
            iMap1.put(stroke, "none");
            stroke = KeyStroke.getKeyStroke("DOWN");
            iMap1.put(stroke, "none");
            stroke = KeyStroke.getKeyStroke("UP");
            iMap1.put(stroke, "none");
    }
    
    public jReportes(final int width, final int height)
    {
        _Hilos hilo = new _Hilos(1);
        
        hilo.establecerAccion(new _Acciones(){
            public boolean acto() {

                inicio(width, height);

                //Saber que se realizo la accion
                return true;
            }
        });

        control.evaluarHilo(hilo);
    }
    
    public String getFechaFiltroDay(String day, String Moth, String Year)
    {
        String filtro = "";
        
        String dia = day;
        String Mes = Moth;
        String año = Year;
        
        if (day.charAt(0) == '0') {
            dia = ""+day.charAt(1);
        }
        
        if (Moth.charAt(0) == '0') {
            Mes = ""+Moth.charAt(1);
        }
        
        if (Year.charAt(0) == '0') {
            año = ""+Year.charAt(1);
        }
        
        //Al tener ya sepadado los numeros lo convertimos
        int dayNum = Integer.parseInt(dia);
        int mothNum = Integer.parseInt(Mes);
        int yearNum = Integer.parseInt(año);
        
        //Al tenerlos convertidos hacemos el calculo
        int limiteDias[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        
        if (dayNum == 1) {
            if (mothNum == 1) {
                
            }
        }
        
        return filtro;
    }
    
    //Obtener la informacion esencia
    public int getHeight(){ return this.height; }
    public int getWidth(){ return this.width; }
    
    //Cambiar la visibilidad
    public void changeVisible(boolean estado){ 
        this.setVisible(estado); 
        
        this.getTableInfo();
        limpiarTabla(dtPeticion);
        this.getTableInfo();
        canUseDate = estado;
        changeEstate();
    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag); //To change body of generated methods, choose Tools | Templates.
    
        scbCategoria.removeAllItems();
        scbCategoria.addItem("Todas las categorias");
        sConecxion.getCombo(sConectar.conectar(), sCategoria.getAllCategoria(), scbCategoria);
        
        scbConsejal.removeAllItems();
        scbConsejal.addItem("Todos los concejales");
        sConecxion.getCombo(sConectar.conectar(), sConcejales.getAllConcejal(), scbConsejal);
        
/*this.scbEtapas.setSelectedIndex(0);
this.scbFecha.setSelectedIndex(0);
this.scbOrdenar.setSelectedIndex(0);*/
        
        fecha1 = "";
        cate = "";
        conce = "";
        estado = "";
        fechaIngreso = "";
        fechaModificacion = "";
        Ordenar = "";
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jHeader = new javax.swing.JPanel();
        btnExit = new javax.swing.JButton();
        btnIcon = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        dtPeticion = new javax.swing.JTable();
        lblGenerar = new javax.swing.JLabel();
        lblCategoria = new javax.swing.JLabel();
        lblConsejal = new javax.swing.JLabel();
        lblEstado = new javax.swing.JLabel();
        lblFecha1 = new javax.swing.JLabel();
        lblFecha2 = new javax.swing.JLabel();
        lblOrden = new javax.swing.JLabel();
        jElement = new javax.swing.JPanel();
        btnLoad = new javax.swing.JButton();
        lblDocumentos = new javax.swing.JLabel();
        lblRango = new javax.swing.JLabel();
        dtFecha2 = new com.toedter.calendar.JDateChooser();
        dtFecha1 = new com.toedter.calendar.JDateChooser();
        lblGraficos = new javax.swing.JLabel();
        lblGraficosEst = new javax.swing.JLabel();
        lblControl = new javax.swing.JLabel();

        setBackground(new java.awt.Color(69, 84, 194));

        jHeader.setBackground(new java.awt.Color(12, 22, 71));

        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        lblTitle.setBackground(new java.awt.Color(255, 255, 255));
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setText("Reportes");

        javax.swing.GroupLayout jHeaderLayout = new javax.swing.GroupLayout(jHeader);
        jHeader.setLayout(jHeaderLayout);
        jHeaderLayout.setHorizontalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jHeaderLayout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(btnExit)
                .addGap(63, 63, 63)
                .addComponent(btnIcon)
                .addGap(106, 106, 106)
                .addComponent(lblTitle)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jHeaderLayout.setVerticalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jHeaderLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jHeaderLayout.createSequentialGroup()
                .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblTitle)
                    .addComponent(btnIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jScrollPane1.setBackground(new java.awt.Color(41, 51, 127));

        dtPeticion.setModel(ModeloTabla);
        dtPeticion.setGridColor(new java.awt.Color(61, 76, 186));
        dtPeticion.setUpdateSelectionOnSort(false);
        jScrollPane1.setViewportView(dtPeticion);
        if (dtPeticion.getColumnModel().getColumnCount() > 0) {
            dtPeticion.getColumnModel().getColumn(0).setResizable(false);
            dtPeticion.getColumnModel().getColumn(1).setResizable(false);
            dtPeticion.getColumnModel().getColumn(2).setResizable(false);
            dtPeticion.getColumnModel().getColumn(3).setResizable(false);
            dtPeticion.getColumnModel().getColumn(4).setResizable(false);
            dtPeticion.getColumnModel().getColumn(5).setResizable(false);
        }

        lblGenerar.setForeground(new java.awt.Color(255, 255, 255));
        lblGenerar.setText("Generar reporte");
        lblGenerar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblGenerarMouseClicked(evt);
            }
        });

        lblCategoria.setForeground(new java.awt.Color(255, 255, 255));
        lblCategoria.setText("Categoria");

        lblConsejal.setForeground(new java.awt.Color(255, 255, 255));
        lblConsejal.setText("Consejal");

        lblEstado.setForeground(new java.awt.Color(255, 255, 255));
        lblEstado.setText("Estado");

        lblFecha1.setForeground(new java.awt.Color(255, 255, 255));
        lblFecha1.setText("Fecha de ingreso");

        lblFecha2.setForeground(new java.awt.Color(255, 255, 255));
        lblFecha2.setText("Ultima modificacion");

        lblOrden.setForeground(new java.awt.Color(255, 255, 255));
        lblOrden.setText("Opciones para ordenar");

        jElement.setBackground(new java.awt.Color(12, 22, 71));

        btnLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jElementLayout = new javax.swing.GroupLayout(jElement);
        jElement.setLayout(jElementLayout);
        jElementLayout.setHorizontalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jElementLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnLoad, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jElementLayout.setVerticalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnLoad, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        lblDocumentos.setForeground(new java.awt.Color(255, 255, 255));
        lblDocumentos.setText("Documento listado");
        lblDocumentos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblDocumentosMouseClicked(evt);
            }
        });

        lblRango.setForeground(new java.awt.Color(255, 255, 255));
        lblRango.setText("Ordenar por rangos de fechas");

        dtFecha2.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dtFecha2PropertyChange(evt);
            }
        });

        dtFecha1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dtFecha1PropertyChange(evt);
            }
        });

        lblGraficos.setForeground(new java.awt.Color(255, 255, 255));
        lblGraficos.setText("Grafico categoria");
        lblGraficos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblGraficosMouseClicked(evt);
            }
        });

        lblGraficosEst.setForeground(new java.awt.Color(255, 255, 255));
        lblGraficosEst.setText("Grafico estadistica");
        lblGraficosEst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblGraficosEstMouseClicked(evt);
            }
        });

        lblControl.setForeground(new java.awt.Color(255, 255, 255));
        lblControl.setText("Documento control");
        lblControl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblControlMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblControl)
                .addGap(26, 26, 26)
                .addComponent(lblGraficosEst)
                .addGap(18, 18, 18)
                .addComponent(lblGraficos)
                .addGap(18, 18, 18)
                .addComponent(lblDocumentos)
                .addGap(18, 18, 18)
                .addComponent(lblGenerar)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCategoria)
                    .addComponent(lblConsejal)
                    .addComponent(lblEstado))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblOrden)
                    .addComponent(lblFecha2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblFecha1)
                        .addGap(18, 18, 18)
                        .addComponent(lblRango)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dtFecha2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dtFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
                    .addComponent(jElement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 1, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblCategoria)
                            .addComponent(lblFecha1))
                        .addComponent(lblRango, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(dtFecha2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblConsejal)
                            .addComponent(lblFecha2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblEstado)
                            .addComponent(lblOrden)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(dtFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(jElement, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblGenerar)
                    .addComponent(lblDocumentos)
                    .addComponent(lblGraficos)
                    .addComponent(lblGraficosEst)
                    .addComponent(lblControl))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        //Le quitamos la visibilidad
        this.changeVisible(false);
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoadActionPerformed
        scbCategoria.removeAllItems();
        scbCategoria.addItem("Todas las cetegorias");
        sConecxion.getCombo(sConectar.conectar(), sCategoria.getAllCategoria(), scbCategoria);
        
        scbConsejal.removeAllItems();
        scbConsejal.addItem("Todos los concejales");
        sConecxion.getCombo(sConectar.conectar(), sConcejales.getAllConcejal(), scbConsejal);
        
        this.scbEtapas.setSelectedIndex(0);
        this.scbFecha.setSelectedIndex(0);
        this.scbOrdenar.setSelectedIndex(0);
        
        fecha1 = "";
        cate = "";
        conce = "";
        estado = "";
        fechaIngreso = "";
        fechaModificacion = "";
        Ordenar = "";
        allDate = "";
        
        this.getTableInfo();
        limpiarTabla(dtPeticion);
        this.getTableInfo();
        changeEstate();
        
        
    }//GEN-LAST:event_btnLoadActionPerformed

    private void changeEstate()
    {
        for (int i = 0; i < dtPeticion.getRowCount(); i++) {
            Integer estado = (Integer)dtPeticion.getValueAt(i, 7);
            
            int x = estado.intValue();
            
            switch(x)
            {
                case 1:
                    dtPeticion.setValueAt("Registrada", i, 7);
                    break;
                    
                case 2:
                    dtPeticion.setValueAt("Aceptada", i, 7);
                    break;
                
                case 3:
                    dtPeticion.setValueAt("Asignada", i, 7);
                    break;
                    
                case 4:
                    dtPeticion.setValueAt("Finalizada", i, 7);
                    break;
                    
                case 5:
                    dtPeticion.setValueAt("Rechazada", i, 7);
                    break;
            }
        }
    }
    
    private void dtFecha1PopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_dtFecha1PopupMenuWillBecomeInvisible
        // TODO add your handling code here:
        SimpleDateFormat formX = new SimpleDateFormat("YY-MM-dd");
        
        String fecha = "";
        
        try{
        fecha = formX.format(dtFecha1.getDate());}
        
        catch (Exception e){
            JOptionPane.showMessageDialog(null, e.toString());}
        
        fechaIngreso = "AND pet.fecha BETWEEN '" +fecha+ "' AND GETDATE()";
        
        limpiarTabla(dtPeticion);
                getTableInfo();
                changeEstate();
    }//GEN-LAST:event_dtFecha1PopupMenuWillBecomeInvisible

    private void dtFecha2PopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_dtFecha2PopupMenuWillBecomeInvisible
        SimpleDateFormat formX = new SimpleDateFormat("YY-MM-dd");
        
        String fecha = formX.format(dtFecha2.getDate());
        
        //JOptionPane.showMessageDialog(null, fecha);
        
        fechaIngreso = "AND pet.fecha BETWEEN GETDATE() AND '" +fecha+ "' 23:59";
        
        limpiarTabla(dtPeticion);
                getTableInfo();
                changeEstate();
    }//GEN-LAST:event_dtFecha2PopupMenuWillBecomeInvisible

    private void lblDocumentosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblDocumentosMouseClicked
        reporteListado();
        
        sConecxion.insertAll(sConectar.conectar(), sAuditoria.allInfo(), null, new Integer(10), mConcejalVirtual.perfil.getIdPerfil(), new Integer(1));
    }//GEN-LAST:event_lblDocumentosMouseClicked

    private void lblGenerarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGenerarMouseClicked
        reporteDetallado();
        
        sConecxion.insertAll(sConectar.conectar(), sAuditoria.allInfo(), null, new Integer(10), mConcejalVirtual.perfil.getIdPerfil(), new Integer(1));
    }//GEN-LAST:event_lblGenerarMouseClicked

    private void dtFecha2PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dtFecha2PropertyChange
        if (canUseDate) {
            SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
            
            dtFecha1.setMaxSelectableDate(dtFecha2.getDate());
            
            String dateNow = "";
            String fechaFormat = fm.format(dtFecha2.getDate());
            
            for (int i = 0; i < fechaFormat.length(); i++) {
                if (fechaFormat.charAt(i) != '-') {
                    dateNow += ""+fechaFormat.charAt(i);
                }
            }
            
            this.fechaIngreso = dateNow;
            
            if (!fecha1.equals("")) {
                allDate = "AND pet.fecha BETWEEN '"+fecha1+"' AND '"+fechaIngreso+"'";
            }
            else
            {
                allDate = "AND pet.fecha BETWEEN GETDATE() AND '"+fechaIngreso+"'";
            }
            
            dateUse2 = true;
            limpiarTabla(dtPeticion);
            getTableInfo();
            changeEstate();
        }
    }//GEN-LAST:event_dtFecha2PropertyChange

    private void dtFecha1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dtFecha1PropertyChange
        if (canUseDate) {
            SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
            
            dtFecha2.setMinSelectableDate(dtFecha1.getDate());
            
            String dateNow = "";
            String fechaFormat = fm.format(dtFecha1.getDate());
            
            for (int i = 0; i < fechaFormat.length(); i++) {
                if (fechaFormat.charAt(i) != '-') {
                    dateNow += ""+fechaFormat.charAt(i);
                }
            }
            
            this.fecha1 = dateNow;
            
            if (!fechaIngreso.equals("")) {
                allDate = "AND pet.fecha BETWEEN '"+fecha1+"' AND '"+fechaIngreso+"'";
            }
            else
            {
                allDate = "AND pet.fecha BETWEEN '"+fecha1+"' AND GETDATE()";
            }
            
            dateUse = true;
            limpiarTabla(dtPeticion);
            getTableInfo();
            changeEstate();
        }
    }//GEN-LAST:event_dtFecha1PropertyChange

    private void lblGraficosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGraficosMouseClicked
        jGraficos.generarCategorias();
    }//GEN-LAST:event_lblGraficosMouseClicked

    private void lblGraficosEstMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGraficosEstMouseClicked
        //reporteListado();
        
        //sConecxion.insertAll(sConectar.conectar(), sAuditoria.allInfo(), null, new Integer(10), mConcejalVirtual.perfil.getIdPerfil(), new Integer(1));
        
        //Creando array para combobox del prompt
        Integer[] years = new Integer[] {2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2030};
        
        Object prompt = JOptionPane.showInputDialog(this, "Seleccione el año a procesar:", "Conteo de Peticiones", JOptionPane.INFORMATION_MESSAGE, null, years, 0);
        Integer selected_year = Integer.valueOf(prompt.toString());
        try
        {
             String path = "src\\IText\\ConteoPeticiones.jasper";
             
             xReportes con = new xReportes();
             
             Map parametros = new HashMap();
             
             
             //Sin rango de fechas
             parametros.put("parameter1", selected_year);
             parametros.put("parameter2", selected_year + 1);
             
             
             JasperReport reporteJasper = (JasperReport)JRLoader.loadObject(path);
             JasperPrint mostrarReporte = JasperFillManager.fillReport(reporteJasper, parametros, con.conectar());
             JasperViewer visor = new JasperViewer(mostrarReporte, false);
             visor.setTitle("Conteo de peticiones");
             visor.setVisible(true);
        } 
        
        catch (JRException ex) {
            JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
        }
        
    }//GEN-LAST:event_lblGraficosEstMouseClicked

    private void lblControlMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblControlMouseClicked
        try
        {
             String path = "src\\IText\\prueba.jasper";             
             SimpleDateFormat form = new SimpleDateFormat("dd-MM-yy");
             xReportes con = new xReportes();
             Map parametros = new HashMap();
             if(dtFecha1.getDate() == null && dtFecha2.getDate() == null)
             {
                //Sin rango de fechas
                parametros.put("parameter1", mConcejalVirtual.perfil.getIdPerfil());
                parametros.put("parameter2", "1/1/1999");
                parametros.put("parameter3", "1/1/2100");
                parametros.put("parameter4", "Todos los resultados");
                parametros.put("parameter5", "");
             }
             else
             {
                //Con rangos de fechas
                parametros.put("parameter1", mConcejalVirtual.perfil.getIdPerfil());
                parametros.put("parameter2", form.format(dtFecha1.getDate()));
                parametros.put("parameter3", form.format(dtFecha2.getDate()) + " 23:59.99");
                parametros.put("parameter4", "Desde: " + form.format(dtFecha1.getDate()));
                parametros.put("parameter5", "Hasta: " + form.format(dtFecha1.getDate()));

             }
             
             
             JasperReport reporteJasper = (JasperReport)JRLoader.loadObject(path);
             JasperPrint mostrarReporte = JasperFillManager.fillReport(reporteJasper, parametros, con.conectar());
             JasperViewer visor = new JasperViewer(mostrarReporte, false);
             visor.setTitle("Record de usuario");
             visor.setVisible(true);
        } 
        
        catch (JRException ex) {
            JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
        }
    }//GEN-LAST:event_lblControlMouseClicked

    private void reporteDetallado()
    {
        try {
            
            File info = null;
            
            int indice = dtPeticion.getSelectedRow();
            
            SimpleDateFormat form = new SimpleDateFormat("YY-MM-dd");
            
            int id = (Integer)dtPeticion.getValueAt(indice, 0);
            String user = (String)dtPeticion.getValueAt(indice, 1);
            String e_mail = (String)dtPeticion.getValueAt(indice, 2);
            String asunto = (String)dtPeticion.getValueAt(indice, 3);
            String categoria = (String)dtPeticion.getValueAt(indice, 4);
            String concejal = (String)dtPeticion.getValueAt(indice, 5);
            String descripcion = (String)dtPeticion.getValueAt(indice, 6);
            String estado = (String)dtPeticion.getValueAt(indice, 7);
            String fecha = form.format(dtPeticion.getValueAt(indice, 8));
            
            sPerfil per = new sPerfil();
            sConecxion.getInformation(per ,sConectar.conectar(), sPerfil.getAllElementName(), e_mail);
            
            String foto = per.getFoto();
            
            if (foto.trim().equals("n/a")) {
                foto = "Img/modPeticiones/user.jpg";
            }
            else
            {
                jBajarFtp.setPrimerPath("/FTP");
                jBajarFtp.setSecundPath("Img");
                jBajarFtp.setNombreArchivo(foto);
                info = jBajarFtp.descargarArchivo();
                
                foto = info.getAbsolutePath();
            }
            
            String telefono = ""+per.getTelefono();
            String direccion = per.getDireccion();
            
            String date = fecha;
            
            //JOptionPane.showMessageDialog(null, foto);
            
            jReporteDetallado detalle = new jReporteDetallado();
            
            detalle.setName("Nombre: "+user);
            detalle.setAsunto("Asunto: "+asunto);
            detalle.setCategoria("Categoria: "+categoria);
            detalle.setConsejal("Concejal: "+concejal);
            detalle.setFecha("Fecha:" + date);
            detalle.setEstado("Estado: "+estado);
            detalle.setUrl("ReporteDetallado");
            detalle.setCorreo("Correo: "+e_mail);
            detalle.setTelefono("Telefono: "+telefono);
            detalle.setDireccion("Direccion: "+direccion);
            detalle.setDescripcion("Descripcion: "+descripcion);
            detalle.setFoto(foto);
            
            ArrayList<String> txt = new ArrayList<String>();
            txt = sConecxion.getComentStrind(sComentarios.selectPetOne(), id);
            
            ArrayList<Integer> num = new ArrayList<Integer>();
            num = sConecxion.getComentInteger(sComentarios.selectPetOneInt(), id);
            
            Iterator i = txt.iterator();
            int znex = 0;
            
            while(i.hasNext())
            {
                String text = (String)i.next();
                
                Integer idX = num.get(znex);
                
                Integer tipoUsu = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sPerfil.getIdTipo(), idX);
                String name = (String)sConecxion.getEspecificElement(sConectar.conectar(), sPerfil.getName(), idX);
                
                boolean resp = false;
                
                if (tipoUsu == 2 || tipoUsu == 3 || tipoUsu == 4) {
                    resp = false;
                }
                else
                {
                    resp = true;
                }
                
                detalle.addComentarios(text, new Boolean(resp), name);
                znex++;
            }
            
            detalle.generarReporte();
            
            if (info != null) {
                info.delete();
            }
        } catch (DocumentException ex) {
            JOptionPane.showMessageDialog(null, "Error de documento:" + ex.getMessage());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error de direccion:" + ex.getMessage());
        }
        catch (Exception ex){JOptionPane.showMessageDialog(null, "No se ha seleccionado nada");}
    }
    
    private void reporteListado(){
        ArrayList<File> fileList = new ArrayList<File>();
        int countRow = this.dtPeticion.getRowCount();
        jReporteListado lista = new jReporteListado();        
        SimpleDateFormat form = new SimpleDateFormat("YY-MM-dd");        
        try {
            for (int i = 0; i < countRow; i++) {
                jLista list = new jLista();                
                list.setName("Usuario: "+(String)dtPeticion.getValueAt(i, 1));
                String e_mail = (String)dtPeticion.getValueAt(i, 2);
                list.setComision("Cetegoria: "+(String)dtPeticion.getValueAt(i, 4));
                list.setConcejal("Concejal: "+(String)dtPeticion.getValueAt(i, 5));
                list.setDescripcion("Descripcion: "+(String)dtPeticion.getValueAt(i, 6));
                list.setEstado("Estado: "+(String)dtPeticion.getValueAt(i, 7));
                list.setFecha("Fecha:" + dtPeticion.getValueAt(i, 8).toString());                
                sPerfil per = new sPerfil();
                sConecxion.getInformation(per ,sConectar.conectar(), sPerfil.getAllElementName(), e_mail);
                String foto = per.getFoto();                
                if (foto.trim().equals("n/a")) {
                    foto = "Img/modPeticiones/user.jpg";                    
                    list.setImg(Image.getInstance(foto));
                }
                else{
                    jBajarFtp.setPrimerPath("/FTP");
                    jBajarFtp.setSecundPath("Img");
                    jBajarFtp.setNombreArchivo(foto);
                    File info = jBajarFtp.descargarArchivo();
                    fileList.add(info);
                    list.setImg(Image.getInstance(info.getAbsolutePath()));
                }
                lista.addJList(list);
            }
            Calendar cw = new GregorianCalendar();
            lista.setFecha("Fecha de creacion del documento: "+ form.format(cw.getTime()));
            lista.setUrl("ReporteListado");
            lista.generarReportes();
            Iterator i = fileList.iterator();
            while(i.hasNext()){
                File x = (File)i.next();
                x.delete();
            }
        } catch (BadElementException ex) {
           JOptionPane.showMessageDialog(null, ex.getMessage());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }
    
    public void limpiarTabla(JTable tabla){
        try {
            DefaultTableModel modelo=(DefaultTableModel) tabla.getModel();
            int filas=tabla.getRowCount();
            
            for (int i = 0;filas>i; i++) {
                modelo.removeRow(0);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnIcon;
    private javax.swing.JButton btnLoad;
    private com.toedter.calendar.JDateChooser dtFecha1;
    private com.toedter.calendar.JDateChooser dtFecha2;
    private javax.swing.JTable dtPeticion;
    private javax.swing.JPanel jElement;
    private javax.swing.JPanel jHeader;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCategoria;
    private javax.swing.JLabel lblConsejal;
    private javax.swing.JLabel lblControl;
    private javax.swing.JLabel lblDocumentos;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblFecha1;
    private javax.swing.JLabel lblFecha2;
    private javax.swing.JLabel lblGenerar;
    private javax.swing.JLabel lblGraficos;
    private javax.swing.JLabel lblGraficosEst;
    private javax.swing.JLabel lblOrden;
    private javax.swing.JLabel lblRango;
    private javax.swing.JLabel lblTitle;
    // End of variables declaration//GEN-END:variables
}
