/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Extras.jLocationPanel;
import Extras.jTool;
import MVC.sAuditoria;
import MVC.sConectar;
import MVC.sConecxion;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Toshiba  
 */
public class jMenu extends javax.swing.JPanel {

    /**
     * Creates new form jMenu
     */
    
    //Saber que tipo de menu sera
    private boolean administrador = true;
    
    //Panel
    jPeticion peticion;
    jPeticionModific peticionMod;
    jReportes reportes;
    
    //Estado
    final String UP = "UP", DOWN = "DOWN", RIGHT = "RIGHT", LEFT = "LEFT";
    String estado = UP;
    
    //Panel
    mConcejalVirtual cv = null;
    
    //Dimensiones del panel
    public int height = 42;
    public int width = (Toolkit.getDefaultToolkit().getScreenSize().width);
    
    //Cambiar de posicion
    public boolean Location = false;
    
    //Separacion de los botones
    private int btnSeparacion = width * 1/100;
    
    //Tamano de los botones
    private int btnSize = 32, btnSize2 = 24;
    private int btnMitad = (height / 2) - (btnSize / 2);
    
    //Estado
    public boolean arriba = false;
    
    //Posicionamiento
    //Timer
    Timer subir = new Timer(10, new ActionListener()
    {
        public void actionPerformed(ActionEvent e) {
            //Cambiar la posicion
            if (estado.equals(UP)){
                setLocation(0, getY() - 5);

                if(getY() < -height){
                    stop();
                    arriba = true;
                }
            }
            else if (estado.equals(LEFT)){
                setLocation(getX() - 5, 0);

                if(getX() < -42){
                    stop();
                    arriba = true;
                }
            }
            else if (estado.equals(RIGHT)){
                setLocation(getX() + 5, 0);

                if(getX() > Toolkit.getDefaultToolkit().getScreenSize().width){
                    stop();
                    arriba = true;
                }
            }
            else if (estado.equals(DOWN)){
                setLocation(0, getY() + 5);

                if(getY() > Toolkit.getDefaultToolkit().getScreenSize().height){
                    stop();
                    arriba = true;
                }
            }
        }
    });
    
    //tamano
    byte size = 42;
    
    //Ocultar
    public void stop()
    {
        subir.stop();;
    }
    
    
    //cambiar las dimensiones
    private void dimensiones()
    {
        int dW = Toolkit.getDefaultToolkit().getScreenSize().width;
        int dH = Toolkit.getDefaultToolkit().getScreenSize().height;
        System.out.println(dW);
        
        if (dW == 800 && dH == 600)
        {
            btnPeticiones.setIcon(new ImageIcon("Img/Menu/Opcion0(2).png"));
            btnReportes.setIcon(new ImageIcon("Img/Menu/Opcion1(2).png"));
            //btnConection.setIcon(new ImageIcon("Img/Menu/Conect(2).png"));
            btnHide.setIcon(new ImageIcon("Img/Menu/Hiden(2).png"));
            btnExit.setIcon(new ImageIcon("Img/Menu/Exit(2).png"));
            btnMin.setIcon(new ImageIcon("Img/Menu/Resizable.png"));
            
            //Cambiamos el tamano del menu
            size = 34;
            height = size;
            
            //el de los botones
            btnSize = 24; 
            btnSize2 = 16;
        }
        else
        {
            btnPeticiones.setIcon(new ImageIcon("Img/Menu/Opcion0.png"));
            btnReportes.setIcon(new ImageIcon("Img/Menu/Opcion1.png"));
            //btnConection.setIcon(new ImageIcon("Img/Menu/Conect.png"));
            btnHide.setIcon(new ImageIcon("Img/Menu/Hiden.png"));
            btnExit.setIcon(new ImageIcon("Img/Menu/Exit.png"));
            btnAdmin_Usuarios.setIcon(new ImageIcon("Img/Admin/adminUser.png"));
            btnCategorias.setIcon(new ImageIcon("Img/Admin/categoria.png"));
            btnConcejales.setIcon(new ImageIcon("Img/Admin/concejal.png"));
            btnComisiones.setIcon(new ImageIcon("Img/Admin/comision.png"));
            btnAuditoria.setIcon(new ImageIcon("Img/Menu/notebook.png"));
            btnMin.setIcon(new ImageIcon("Img/Menu/Resizable.png"));
        }
    }
    
    
    public jMenu(jPeticion peticion, jPeticionModific peticionMod, jReportes reportes, boolean administrador) {
        initComponents();
        
        this.administrador = administrador;
        
        this.peticion = peticion;
        this.peticionMod = peticionMod;
        this.reportes = reportes;
        //Iniciar el panel
        dimensiones();
        
        //Agrgar la imagen
        setLayout(null);
        
        //Botones Transparente
        jTool.btnTransparente(btnMin, btnAuditoria, btnPeticiones, btnReportes, btnHide, btnExit,btnAdmin_Usuarios,btnCategorias,btnConcejales,btnComisiones);
        
        //El tamano de los botones
        jTool.btnResizable(btnSize, btnAuditoria, btnPeticiones, btnReportes, btnAdmin_Usuarios,btnCategorias,btnConcejales,btnComisiones);
        jTool.btnResizable(btnSize2, btnHide, btnExit, btnMin);
        
        //La imagen de los botones
        
        
        //Locacion
        btnPeticiones.setLocation(btnSeparacion, btnMitad);
        btnReportes.setLocation((btnPeticiones.getX() + btnSize) + ((btnSeparacion)), btnMitad);
        
        //En el caso que sea un administrador
        if(administrador)
        {
            btnAuditoria.setLocation((btnReportes.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnAdmin_Usuarios.setLocation((btnAuditoria.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnCategorias.setLocation((btnAdmin_Usuarios.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnComisiones.setLocation((btnCategorias.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnConcejales.setLocation((btnComisiones.getX() + btnSize) + ((btnSeparacion) * 2), btnMitad);
            
            //btnConection.setLocation((btnConcejales.getX() + btnSize) + ((btnSeparacion) * 2), btnMitad);
        }
        else
        {
            //De caso contrario
            
            btnAdmin_Usuarios.setVisible(false);
            btnCategorias.setVisible(false);
            btnConcejales.setVisible(false);
            btnComisiones.setVisible(false);
            btnAuditoria.setVisible(false);
            //btnConection.setLocation((btnReportes.getX() + (btnSize * 2)) + ((btnSeparacion)), btnMitad);
        }
        
        //Location
        btnExit.setLocation(width - btnSize2, 0);
        btnHide.setLocation(btnExit.getX() - (btnSize2 + (btnSize2/4)), 0);
        btnMin.setLocation(btnHide.getX() - (btnSize2 + (btnSize2/4)), 0);
        //No hay conecion
        //btnConection.setEnabled(false);
        
        //Reloj Digital
        //rljDigital.setBounds((btnConection.getX() + 50), -5, 95, 30);
        
        //System.out.println(rljDigital.getY() + " , " + rljDigital.getWidth());
    }
    
    //Posicionarlo arriba
    public void setVertical(String estado)
    {
        //Cambiar SIZE
        setBounds(0, 0, Toolkit.getDefaultToolkit().getScreenSize().width, size);
        
        //Locacion
        btnPeticiones.setLocation(btnSeparacion, btnMitad);
        btnReportes.setLocation((btnPeticiones.getX() + btnSize) + ((btnSeparacion)), btnMitad);
        
        //En el caso que sea un administrador
        if(administrador)
        {
            btnAuditoria.setLocation((btnReportes.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnAdmin_Usuarios.setLocation((btnAuditoria.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnCategorias.setLocation((btnAdmin_Usuarios.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnComisiones.setLocation((btnCategorias.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnConcejales.setLocation((btnComisiones.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            
            //btnConection.setLocation((btnConcejales.getX() + (btnSize * 2)) + ((btnSeparacion) * 2), btnMitad);
        }
        else
        {
            //De caso contrario
            //btnConection.setLocation((btnReportes.getX() + (btnSize * 2)) + ((btnSeparacion)), btnMitad);
        }
        
        //Location
        btnExit.setLocation(width - btnSize2, 0);
        btnHide.setLocation(btnExit.getX() - (btnSize2 + (btnSize2/4)), 0);
        btnMin.setLocation(btnHide.getX() - (btnSize2 + (btnSize2/4)), 0);
        //Reloj Digital
        //rljDigital.setBounds((btnConection.getX() + 50), -5, 95, 30);
        
        this.estado = estado;
    }
    
    //Vertical
    public void setVerticalInversa(String estado)
    {
        //Cambiar SIZE
        setBounds(0, Toolkit.getDefaultToolkit().getScreenSize().height - size, Toolkit.getDefaultToolkit().getScreenSize().width, size);
        
        //Locacion
        btnPeticiones.setLocation(btnSeparacion, btnMitad);
        btnReportes.setLocation((btnPeticiones.getX() + btnSize) + ((btnSeparacion)), btnMitad);
        
        //En el caso que sea un administrador
        if(administrador)
        {
            btnAuditoria.setLocation((btnReportes.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnAdmin_Usuarios.setLocation((btnAuditoria.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnCategorias.setLocation((btnAdmin_Usuarios.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnComisiones.setLocation((btnCategorias.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            btnConcejales.setLocation((btnComisiones.getX() + btnSize) + ((btnSeparacion)), btnMitad);
            
            //btnConection.setLocation((btnConcejales.getX() + (btnSize * 2)) + ((btnSeparacion) * 2), btnMitad);
        }
        else
        {
            //De caso contrario
            //btnConection.setLocation((btnReportes.getX() + (btnSize * 2)) + ((btnSeparacion)), btnMitad);
        }
        
        //Location
        btnExit.setLocation(width - btnSize2, size / 10);
        btnHide.setLocation(btnExit.getX() - (btnSize2 + (btnSize2/4)),size / 10);
        btnMin.setLocation(btnHide.getX() - (btnSize2 + (btnSize2/4)),size / 10);
        //Reloj Digital
        //rljDigital.setBounds((btnConection.getX() + 50), -5, 95, 30);
        
        this.estado = estado;
    }
    
    //Posicionarlo a la derecha
    public void setHorizontal(String estado)
    {
        //Cambiar SIZE
       setBounds(0, 0, size, Toolkit.getDefaultToolkit().getScreenSize().height);
       
       //Botones
       btnPeticiones.setLocation(((size / 2) - (btnSize / 2)), btnSeparacion);
       btnReportes.setLocation(((size / 2) - (btnSize / 2)), btnPeticiones.getY() + (size + 2));
       
       //En el caso que sea un administrador
        if(administrador)
        {
            btnAuditoria.setLocation(((size / 2) - (btnSize / 2)), btnReportes.getY() + (size + 2));
            btnAdmin_Usuarios.setLocation(((size / 2) - (btnSize / 2)), btnAuditoria.getY() + (size + 2));
            btnCategorias.setLocation(((size / 2) - (btnSize / 2)), btnAdmin_Usuarios.getY() + (size + 2));
            btnComisiones.setLocation(((size / 2) - (btnSize / 2)), btnCategorias.getY() + (size + 2));
            btnConcejales.setLocation(((size / 2) - (btnSize / 2)), btnComisiones.getY() + (size + 2));
            
            //btnConection.setLocation(((size / 2) - (btnSize / 2)), btnConcejales.getY() + ((size * 2) + 2));
        }
        else
        {
            //De caso contrario
            
            //btnConection.setLocation(((size / 2) - (btnSize / 2)), btnReportes.getY() + (btnSeparacion * 2) + ((size * 2) + 2));
        }
       
       btnExit.setLocation(((size / 2) - (btnSize / 2)), (Toolkit.getDefaultToolkit().getScreenSize().height) - btnSize2);
       btnHide.setLocation(((size / 2) - (btnSize / 2)), btnExit.getY() - (btnSize2 + (btnSize2 / 4)));
       btnMin.setLocation(((size / 2) - (btnSize / 2)), btnHide.getY() - (btnSize2 + (btnSize2 / 4)));
       this.estado = estado;
    }
    
    public void setHorizontalInversa(String estado)
    {
        //Cambiar SIZE
       setBounds(Toolkit.getDefaultToolkit().getScreenSize().width - size, 0, size, Toolkit.getDefaultToolkit().getScreenSize().height);
       
       //Botones
       btnPeticiones.setLocation(((size / 2) - (btnSize / 2)), btnSeparacion);
       btnReportes.setLocation(((size / 2) - (btnSize / 2)), btnPeticiones.getY() + ((size + 2)));
       
       //En el caso que sea un administrador
        if(administrador)
        {
            btnAuditoria.setLocation(((size / 2) - (btnSize / 2)), btnReportes.getY() + (size + 2));
            btnAdmin_Usuarios.setLocation(((size / 2) - (btnSize / 2)), btnAuditoria.getY() + (size + 2));
            btnCategorias.setLocation(((size / 2) - (btnSize / 2)), btnAdmin_Usuarios.getY() + (size + 2));
            btnComisiones.setLocation(((size / 2) - (btnSize / 2)), btnCategorias.getY() + (size + 2));
            btnConcejales.setLocation(((size / 2) - (btnSize / 2)), btnComisiones.getY() + (size + 2));
            
            //btnConection.setLocation(((size / 2) - (btnSize / 2)), btnConcejales.getY() + ((size * 2) + 2));
        }
        else
        {
            //De caso contrario
            
            //btnConection.setLocation(((size / 2) - (btnSize / 2)), btnReportes.getY() + (btnSeparacion * 2) + ((size * 2) + 2));
        }
       
       btnExit.setLocation(((size / 2) - (btnSize / 2)), (Toolkit.getDefaultToolkit().getScreenSize().height) - btnSize2);
       btnHide.setLocation(((size / 2) - (btnSize / 2)), btnExit.getY() - (btnSize2 + (btnSize2 / 4)));
       btnMin.setLocation(((size / 2) - (btnSize / 2)), btnHide.getY() - (btnSize2 + (btnSize2 / 4)));
       this.estado = estado;
    }
    
    //Cambiar de posicion el menu
    private void changePosition(javax.swing.JPanel panel)
    {
        //Variables de posicon
        int height, width;
        
        if(estado.equals(UP) || estado.equals(DOWN))
        {
            height = Toolkit.getDefaultToolkit().getScreenSize().height;
            height -= this.getHeight();
            width = Toolkit.getDefaultToolkit().getScreenSize().width;
        }
        else
        {
            height = Toolkit.getDefaultToolkit().getScreenSize().height;
            width = Toolkit.getDefaultToolkit().getScreenSize().width;
            width -= this.getWidth();
        }
        
        int x = (width / 2) - (panel.getWidth() / 2);
        int y = (height / 2) - (panel.getHeight() / 2);
        
        //Para saber en que posicion actual esta
        panel.setBounds(x, y, panel.getWidth(), panel.getHeight());
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXDatePicker1 = new org.jdesktop.swingx.JXDatePicker();
        btnPeticiones = new javax.swing.JButton();
        btnReportes = new javax.swing.JButton();
        btnHide = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        btnAdmin_Usuarios = new javax.swing.JButton();
        btnComisiones = new javax.swing.JButton();
        btnCategorias = new javax.swing.JButton();
        btnConcejales = new javax.swing.JButton();
        btnAuditoria = new javax.swing.JButton();
        btnMin = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 162, 211));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                formMouseReleased(evt);
            }
        });

        btnPeticiones.setToolTipText(null);
        btnPeticiones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPeticionesMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPeticionesMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPeticionesMouseExited(evt);
            }
        });

        btnReportes.setToolTipText(null);
        btnReportes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnReportesMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnReportesMouseExited(evt);
            }
        });
        btnReportes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportesActionPerformed(evt);
            }
        });

        btnHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHideActionPerformed(evt);
            }
        });

        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        btnAdmin_Usuarios.setToolTipText(null);
        btnAdmin_Usuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAdmin_UsuariosMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAdmin_UsuariosMouseExited(evt);
            }
        });
        btnAdmin_Usuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdmin_UsuariosActionPerformed(evt);
            }
        });

        btnComisiones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnComisionesMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnComisionesMouseExited(evt);
            }
        });
        btnComisiones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnComisionesActionPerformed(evt);
            }
        });

        btnCategorias.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCategoriasMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCategoriasMouseExited(evt);
            }
        });
        btnCategorias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCategoriasActionPerformed(evt);
            }
        });

        btnConcejales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnConcejalesMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnConcejalesMouseExited(evt);
            }
        });
        btnConcejales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConcejalesActionPerformed(evt);
            }
        });

        btnAuditoria.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAuditoriaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAuditoriaMouseExited(evt);
            }
        });
        btnAuditoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAuditoriaActionPerformed(evt);
            }
        });

        btnMin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(115, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAdmin_Usuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnComisiones)
                        .addGap(10, 10, 10)
                        .addComponent(btnCategorias)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConcejales)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAuditoria))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnReportes)
                        .addGap(18, 18, 18)
                        .addComponent(btnPeticiones)
                        .addGap(249, 249, 249)
                        .addComponent(btnHide, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnMin, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnExit)))
                .addGap(225, 225, 225))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAuditoria, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnHide, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnReportes, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPeticiones, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnMin, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnComisiones, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnConcejales, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCategorias, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAdmin_Usuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(20, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    
    private void btnHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHideActionPerformed
        //Posicionamiento
        if(arriba == false)
           subir.start();
    }//GEN-LAST:event_btnHideActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        //Cerrar
        sConecxion.insertAll(sConectar.conectar(), sAuditoria.allInfo(), null, new Integer(2), mConcejalVirtual.perfil.getIdPerfil(), new Integer(1));
        mConcejalVirtual.jConce.imgMod.setUrl("Img/Admin/user.jpg");
        mIniciarSesion start = new mIniciarSesion();
        start.setVisible(true);
        cv.dispose();
    }//GEN-LAST:event_btnExitActionPerformed

    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
        //Presionar el menu para moverlo
        if (!Location){
            Location = true;
        }
    }//GEN-LAST:event_formMousePressed

    //Prueba
    String actualLocacion = "UP";
    //final String SUP = "UP", SDOWN = "DOWN", SRIGHT = "RIGHT", SLEFT = "LEFT";
    
    //Saber que panel posicionar
    public void changeLocation()
    {
        if (this.peticion.isVisible()) {
            this.changePosition(this.peticion);
        }
    }
    
    private void formMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseReleased
        //Quitar el menu para moverlo
        int width = Toolkit.getDefaultToolkit().getScreenSize().width;
        int height = Toolkit.getDefaultToolkit().getScreenSize().height;
        //Al saber que la location no esta para cambiarde
        
        //Obtener el tamaño del panel
        int x_Start = this.getX();
        int x_End = this.getX() + this.getWidth();
        int y_Start = this.getY();
        int y_End = this.getY() + this.getHeight();
        
        if (Location){
            Location = false;
            
            //Obtenemos un zona es decir donde esta el mouse en la pantalla
            boolean locationStade = ((evt.getXOnScreen() > x_Start && evt.getXOnScreen() < x_End) && (evt.getYOnScreen() > y_Start && evt.getYOnScreen() < y_End));
            
            //Posicionarlos dependiento de la posicion en x o y del propio mause
            if(!locationStade)
            if((evt.getXOnScreen() > 0 && evt.getXOnScreen() < (this.width / 3)) && (evt.getYOnScreen() > 0 && (evt.getYOnScreen() < height)) && !estado.equals(LEFT))
            {
               setHorizontal(LEFT);
               actualLocacion = LEFT;
               changeLocation();
            }
            else if((evt.getXOnScreen() > 0 && evt.getXOnScreen() < width) && (evt.getYOnScreen() > 0 && (evt.getYOnScreen() < 42)) && !estado.equals(UP))
            {
                setVertical(UP);
                actualLocacion = UP;
                changeLocation();
            }
            else if((evt.getXOnScreen() > width -(this.width / 3) && evt.getXOnScreen() < width) && (evt.getYOnScreen() > 0 && (evt.getYOnScreen() < height)) && !estado.equals(RIGHT))
            {
               setHorizontalInversa(RIGHT);
               actualLocacion = RIGHT;
               changeLocation();
            }
            else if((evt.getXOnScreen() > 0 && evt.getXOnScreen() < width) && (evt.getYOnScreen() > (height - 42) && (evt.getYOnScreen() < height)) && ! estado.equals(DOWN))
            {
                setVerticalInversa(DOWN);
                actualLocacion = DOWN;
                changeLocation();
            }
        }

    }//GEN-LAST:event_formMouseReleased

    private void btnPeticionesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPeticionesMouseClicked
        //Cambiar la visibilidad de lo que no son visibles y de lo que si son visibles
        if(!peticion.isVisible() && !peticionMod.isVisible()){
            peticion.setVisible(true);
            peticion.canTimeUse = true;
            peticionMod.changeVisible(false);
            
            if(reportes.isVisible())
                reportes.changeVisible(false);
            
            if(this.administrador)
            {
                if(mConcejalVirtual.jAdmin.getLocationActualy())
                {
                    mConcejalVirtual.jAdmin.setLocationActualy();
                }

                if(mConcejalVirtual.jComi.getLocationActualy())
                {
                    mConcejalVirtual.jComi.setLocationActualy();
                }

                if(mConcejalVirtual.jCate.getLocationActualy())
                {
                    mConcejalVirtual.jCate.setLocationActualy();
                }

                if(mConcejalVirtual.jConce.getLocationActualy())
                {
                    mConcejalVirtual.jConce.setLocationActualy();
                }
                if(mConcejalVirtual.jAudi.getLocationActualy())
                {
                    mConcejalVirtual.jAudi.setLocationActualy();
                }
            }
        }
    }//GEN-LAST:event_btnPeticionesMouseClicked

    private void btnReportesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportesActionPerformed
       if(!reportes.isVisible()){
           reportes.changeVisible(true);
           
           if(peticion.isVisible()){
              peticion.setVisible(false);
              peticion.canTimeUse = false;
           }
           if(peticionMod.isVisible())
              peticionMod.changeVisible(false);
       
           if(this.administrador)
            {
                if(mConcejalVirtual.jAdmin.getLocationActualy())
                {
                    mConcejalVirtual.jAdmin.setLocationActualy();
                }

                if(mConcejalVirtual.jComi.getLocationActualy())
                {
                    mConcejalVirtual.jComi.setLocationActualy();
                }

                if(mConcejalVirtual.jCate.getLocationActualy())
                {
                    mConcejalVirtual.jCate.setLocationActualy();
                }

                if(mConcejalVirtual.jConce.getLocationActualy())
                {
                    mConcejalVirtual.jConce.setLocationActualy();
                }
                if(mConcejalVirtual.jAudi.getLocationActualy())
                {
                    mConcejalVirtual.jAudi.setLocationActualy();
                }
            }
       }
    }//GEN-LAST:event_btnReportesActionPerformed

    private void btnAdmin_UsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdmin_UsuariosActionPerformed
        mConcejalVirtual.adminUsuarios();
        mConcejalVirtual.jAdmin.setLovationValue(true);
        
        if(mConcejalVirtual.jComi.getLocationActualy())
        {
            mConcejalVirtual.jComi.setLocationActualy();
        }
        
        if(mConcejalVirtual.jCate.getLocationActualy())
        {
            mConcejalVirtual.jCate.setLocationActualy();
        }
        
        if(mConcejalVirtual.jConce.getLocationActualy())
        {
            mConcejalVirtual.jConce.setLocationActualy();
        }
        if(mConcejalVirtual.jAudi.getLocationActualy())
        {
            mConcejalVirtual.jAudi.setLocationActualy();
        }
        
        if(peticion.isVisible()){
            peticion.setVisible(false);
            peticion.canTimeUse = false;
        }
        if(peticionMod.isVisible())
            peticionMod.changeVisible(false);
            
        if(reportes.isVisible())
            reportes.changeVisible(false);
    }//GEN-LAST:event_btnAdmin_UsuariosActionPerformed

    private void btnComisionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComisionesActionPerformed
        mConcejalVirtual.mantenimientoComisiones();
        mConcejalVirtual.jComi.setLovationValue(true);
        
        if(mConcejalVirtual.jAdmin.getLocationActualy())
        {
            mConcejalVirtual.jAdmin.setLocationActualy();
        }
        
        if(mConcejalVirtual.jCate.getLocationActualy())
        {
            mConcejalVirtual.jCate.setLocationActualy();
        }
        
        if(mConcejalVirtual.jConce.getLocationActualy())
        {
            mConcejalVirtual.jConce.setLocationActualy();
        }
        if(mConcejalVirtual.jAudi.getLocationActualy())
        {
            mConcejalVirtual.jAudi.setLocationActualy();
        }
        
        if(peticion.isVisible()){
            peticion.setVisible(false);
            peticion.canTimeUse = false;
        }
        
        if(peticionMod.isVisible())
            peticionMod.changeVisible(false);
            
        if(reportes.isVisible())
            reportes.changeVisible(false);
    }//GEN-LAST:event_btnComisionesActionPerformed

    private void btnCategoriasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCategoriasActionPerformed
        mConcejalVirtual.mantenimientoCategorias();
        mConcejalVirtual.jCate.setLovationValue(true);
        
        if(mConcejalVirtual.jAdmin.getLocationActualy())
        {
            mConcejalVirtual.jAdmin.setLocationActualy();
        }
        
        if(mConcejalVirtual.jComi.getLocationActualy())
        {
            mConcejalVirtual.jComi.setLocationActualy();
        }
        
        if(mConcejalVirtual.jConce.getLocationActualy())
        {
            mConcejalVirtual.jConce.setLocationActualy();
        }
        if(mConcejalVirtual.jAudi.getLocationActualy())
        {
            mConcejalVirtual.jAudi.setLocationActualy();
        }
        
        if(peticion.isVisible()){
            peticion.setVisible(false);
            peticion.canTimeUse = false;
        }
        
        if(peticionMod.isVisible())
            peticionMod.changeVisible(false);
            
        if(reportes.isVisible())
            reportes.changeVisible(false);
    }//GEN-LAST:event_btnCategoriasActionPerformed

    private void btnConcejalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConcejalesActionPerformed
        mConcejalVirtual.mantenimientoConcejales();
        mConcejalVirtual.jConce.setLovationValue(true);
        
        if(mConcejalVirtual.jAdmin.getLocationActualy())
        {
            mConcejalVirtual.jAdmin.setLocationActualy();
        }
        
        if(mConcejalVirtual.jComi.getLocationActualy())
        {
            mConcejalVirtual.jComi.setLocationActualy();
        }
        
        if(mConcejalVirtual.jCate.getLocationActualy())
        {
            mConcejalVirtual.jCate.setLocationActualy();
        }
        
        if(peticion.isVisible()){
            peticion.setVisible(false);
            peticion.canTimeUse = false;
        }
        
        if(peticionMod.isVisible())
            peticionMod.changeVisible(false);
            
        if(reportes.isVisible())
            reportes.changeVisible(false);
    }//GEN-LAST:event_btnConcejalesActionPerformed

    //Posicionar la descripcion
    private void setDescripcion(String txt, javax.swing.JButton btn)
    {
        mConcejalVirtual.descripcion.setDescripcion(txt);
        mConcejalVirtual.descripcion.setFunte(new Font("Consolas", Font.BOLD, 18));
        
        switch(estado)
        {
            case UP:
                mConcejalVirtual.descripcion.setLocation(txt, btn.getX(), (this.getHeight()));
                break;
            
            case DOWN:
                mConcejalVirtual.descripcion.setLocation(txt, btn.getX(), (this.getY() - (mConcejalVirtual.descripcion.getHeight())));
                break;
                
            case RIGHT:
                mConcejalVirtual.descripcion.setLocation(txt, this.getX() - mConcejalVirtual.descripcion.getWidth() , btn.getY() + (btn.getHeight() - mConcejalVirtual.descripcion.getHeight()));
                break;
            
            case LEFT:
                mConcejalVirtual.descripcion.setLocation(txt, this.getWidth(), btn.getY() + (btn.getHeight() - mConcejalVirtual.descripcion.getHeight()));
                break;
        }
        mConcejalVirtual.descripcion.setVisible(true);
    }
    
    private void btnPeticionesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPeticionesMouseEntered
        this.setDescripcion("Buscar peticiones", btnPeticiones);
    }//GEN-LAST:event_btnPeticionesMouseEntered

    private void btnPeticionesMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPeticionesMouseExited
        mConcejalVirtual.descripcion.setVisible(false);
    }//GEN-LAST:event_btnPeticionesMouseExited

    private void btnReportesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnReportesMouseEntered
        this.setDescripcion("Generar reportes", btnReportes);
    }//GEN-LAST:event_btnReportesMouseEntered

    private void btnReportesMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnReportesMouseExited
        mConcejalVirtual.descripcion.setVisible(false);
    }//GEN-LAST:event_btnReportesMouseExited

    private void btnAdmin_UsuariosMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAdmin_UsuariosMouseEntered
        this.setDescripcion("Gestionar usuarios", btnAdmin_Usuarios);
    }//GEN-LAST:event_btnAdmin_UsuariosMouseEntered

    private void btnAdmin_UsuariosMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAdmin_UsuariosMouseExited
        mConcejalVirtual.descripcion.setVisible(false);
    }//GEN-LAST:event_btnAdmin_UsuariosMouseExited

    private void btnComisionesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnComisionesMouseEntered
        this.setDescripcion("Gestionar comisiones", btnComisiones);
    }//GEN-LAST:event_btnComisionesMouseEntered

    private void btnCategoriasMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCategoriasMouseEntered
        this.setDescripcion("Gestionar categorias", btnCategorias);
    }//GEN-LAST:event_btnCategoriasMouseEntered

    private void btnConcejalesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnConcejalesMouseEntered
        this.setDescripcion("Gestionar concejales", btnConcejales);
    }//GEN-LAST:event_btnConcejalesMouseEntered

    private void btnConcejalesMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnConcejalesMouseExited
        mConcejalVirtual.descripcion.setVisible(false);
    }//GEN-LAST:event_btnConcejalesMouseExited

    private void btnCategoriasMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCategoriasMouseExited
        mConcejalVirtual.descripcion.setVisible(false);
    }//GEN-LAST:event_btnCategoriasMouseExited

    private void btnComisionesMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnComisionesMouseExited
        mConcejalVirtual.descripcion.setVisible(false);
    }//GEN-LAST:event_btnComisionesMouseExited

    private void btnAuditoriaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAuditoriaMouseEntered
        this.setDescripcion("Auditoria", btnAuditoria);
    }//GEN-LAST:event_btnAuditoriaMouseEntered

    private void btnAuditoriaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAuditoriaMouseExited
        mConcejalVirtual.descripcion.setVisible(false);
    }//GEN-LAST:event_btnAuditoriaMouseExited

    private void btnAuditoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAuditoriaActionPerformed
        mConcejalVirtual.mantenimientoAuditoria();
        mConcejalVirtual.jAudi.setLovationValue(true);
        
        if(mConcejalVirtual.jAdmin.getLocationActualy())
        {
            mConcejalVirtual.jAdmin.setLocationActualy();
        }
        
        if(mConcejalVirtual.jComi.getLocationActualy())
        {
            mConcejalVirtual.jComi.setLocationActualy();
        }
        
        if(mConcejalVirtual.jCate.getLocationActualy())
        {
            mConcejalVirtual.jCate.setLocationActualy();
        }
        if(mConcejalVirtual.jConce.getLocationActualy())
        {
            mConcejalVirtual.jConce.setLocationActualy();
        }
        
        if(peticion.isVisible()){
            peticion.setVisible(false);
            peticion.canTimeUse = false;
        }
        
        if(peticionMod.isVisible())
            peticionMod.changeVisible(false);
            
        if(reportes.isVisible())
            reportes.changeVisible(false);
    }//GEN-LAST:event_btnAuditoriaActionPerformed

    private void btnMinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinActionPerformed
        this.cv.setExtendedState(javax.swing.JFrame.ICONIFIED); 
    }//GEN-LAST:event_btnMinActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdmin_Usuarios;
    private javax.swing.JButton btnAuditoria;
    private javax.swing.JButton btnCategorias;
    private javax.swing.JButton btnComisiones;
    private javax.swing.JButton btnConcejales;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnHide;
    private javax.swing.JButton btnMin;
    private javax.swing.JButton btnPeticiones;
    private javax.swing.JButton btnReportes;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker1;
    // End of variables declaration//GEN-END:variables
}
