/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concejal_Virtual;

import Extras.jComboBox;
import Extras.jTool;
import Extras.jReporteDetallado;
import Extras.jLista;
import Extras.jReporteListado;
import MVC.sConexion;
import Extras.jInterfaz;
import MVC.tabAccion;
import MVC.tabComentarios;
import MVC.tabConcejal;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import Conexion.xConectar;
import Hilos._Acciones;
import Hilos._Control;
import Hilos._Hilos;
import java.awt.Font;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
import java.util.logging.*;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JTable;

/**
 *
 * @author Maritza
 */
public class jAuditoria extends javax.swing.JPanel {

    /**
     * Creates new form jReportes
     */
     _Control control = new _Control();
    DefaultTableModel modeloTabla;
    
    //Fuente
    private Font fuenteAcalde = null;
    
    //Propiedades del tamaño
    private int width, height;
    
    //Variables para hacer consulta por partes:
    //inicio: Inicio de rango de la consulta
    //fin: Fin de rango de la consulta (consulta de 100 en 100)
    //corr: El correlativo de los resultados (no confundir con id)
    private int inicio, corr;
    
    //Tamaños estandar
    private int btnSize = 24, elementSize = 34;
    
    //Esta variable ayuda al datepicker a decidir si limpiarse o no
    private boolean limpiar_date = false;
    
    //Esta variable evita que el evento de cambio de item de scbAccion
    //haga consultas cuando no debe
    private boolean hacer_accion = false;
    
    //Combo box
    jComboBox scbAccion = new jComboBox();
    jComboBox scbFecha = new jComboBox();
    jComboBox scbOrdenar = new jComboBox();
    
    private String[] getColumnas()
    {
        return new String[]{"No","E-mail Usuario","Acción","Fecha/Hora","Petición relacionada"};
    }
    
    private String accion = "";
    private String ordenar = " ORDER BY au.created_at DESC";
    private String fecha1 = "";
    
    private void setFilasX()
    {

        try
        {
            //String sql = "SELECT per.e_mail, acc.accion, aud.fecha, pet.asunto FROM tab_auditoria as aud LEFT JOIN tab_peticiones as pet ON aud.id_peticion = pet.id_peticion, tab_perfil as per, tab_accion as acc WHERE aud.id_accion = acc.id_accion AND aud.id_perfil = per.id_perfil AND id_auditoria BETWEEN " + inicio + " AND " + fin + accion + fecha1 + ordenar;
            /*String sql = "SELECT e_mail, accion, fecha, asunto FROM ( " +
                         "SELECT aud.id_auditoria, per.e_mail, acc.accion, aud.fecha, pet.asunto, " +
                         "ROW_NUMBER() OVER (" + ordenar + ") as row " +
                         "FROM tab_auditoria as aud LEFT JOIN tab_peticiones as pet ON aud.id_peticion = pet.id_peticion, tab_perfil as per, tab_accion as acc " +
                         "WHERE aud.id_accion = acc.id_accion AND aud.id_perfil = per.id_perfil" + accion + fecha1 +
                         ") a WHERE row >= " + inicio + " and row <= " + fin;*/
            
            String sql = "SELECT u.email, ac.action, au.created_at, r.request_title"
                    + " FROM audits as au LEFT JOIN requests as r ON au.request_id = r.request_id, users as u, actions as ac"
                    + " WHERE au.action_id = ac.action_id AND au.user_id = u.id"
                    + accion + fecha1 + ordenar + " LIMIT " + inicio + ", 100";
            
            Connection cn;
            System.out.println(sql + " fecha: " + fecha1);
            xConectar.setUser("TOSHIBA-PC\\SQLEXPRESS");
            xConectar.setDataBase("cvdbd");
            cn = xConectar.conectar();

            PreparedStatement us = cn.prepareStatement(sql);

            ResultSet res = us.executeQuery();

            Object datos[] = new Object[5];

            //int corr = 1;

            while(res.next())
            {
                //La primera columna tendrá un correlativo
                datos[0] = corr;

                for (int i = 1; i < 5; i++) {

                    if(res.getObject(i) != null)
                        datos[i] = res.getObject(i);
                    else
                        datos[i] = "N/A";
                }

                modeloTabla.addRow(datos);
                corr++;
            }

            res.close();

            cn.close();
        }
        catch(Exception ex)
        {

        }

    }
    
    /*private void setFilas()  //NO SE USA
    {
        try
        {
            String sql = "SELECT per.e_mail, acc.accion, aud.fecha, pet.asunto FROM tab_auditoria as aud LEFT JOIN tab_peticiones as pet ON aud.id_peticion = pet.id_peticion, tab_perfil as per, tab_accion as acc WHERE aud.id_accion = acc.id_accion AND aud.id_perfil = per.id_perfil AND id_auditoria BETWEEN " + inicio + " AND " + fin + " ORDER BY aud.fecha DESC";
            Connection cn;

            xConectar.setUser("TOSHIBA-PC\\SQLEXPRESS");
            xConectar.setDataBase("cvdbd");
            cn = xConectar.conectar();

            PreparedStatement us = cn.prepareStatement(sql);

            ResultSet res = us.executeQuery();

            Object datos[] = new Object[4];
            
            //int corr = 1;

            while(res.next())
            {
                //La primera columna tendrá un correlativo
                datos[0] = corr;
                
                for (int i = 1; i < 4; i++) {
                    datos[i] = res.getObject(i);
                }

                modeloTabla.addRow(datos);
                corr++;
            }

            res.close();
            
            cn.close();
        }
        catch(Exception ex)
        {
            
        }
    }*/
    
    
    //Método para realizar consultas de nuevo
    private void refresh(){
        DefaultTableModel modelo = (DefaultTableModel)dtAuditoria.getModel();
        
        int fila = dtAuditoria.getRowCount();
        
        for (int i = 0; fila  > i; i++) {
            modeloTabla.removeRow(0);
        }
        
        inicio = 0;
        corr = 1;
        
        
        
        setFilasX();System.out.println("refresh()");
        
        //Seleccionando primera fila
        dtAuditoria.changeSelection(0, 0, false, false);
    }
    
    private void iniciar(int width, int height)
    {
        
        modeloTabla = new DefaultTableModel(null, getColumnas()){
            
            //Deshabilitando la edición de celdas
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        
        //setFilas();
        
        //Eventos
        this.scbAccion.addItemListener(new ItemListener()
        {
            
            @Override
            public void itemStateChanged(ItemEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                //Si no esta activo el constructor
                
                final ItemEvent item = e;
                
                if (!control.isConstructor() && e.getStateChange() == 1)
                {
                    System.out.println("Valor: "+e.getItem());
                    
                    _Hilos hilo = new _Hilos(2);
                    
                    hilo.establecerAccion(new _Acciones(){
                        public boolean acto() {
                            if(item.getStateChange() == ItemEvent.SELECTED) {
                    
                                if(hacer_accion == true)
                                {

                                    if (scbAccion.getSelectedIndex() != 0)
                                    {
                                        accion = " AND ac.action = '"+scbAccion.getSelectedItem()+"'";
                                    }
                                    else
                                    {
                                        accion = "";
                                    }

                                    refresh();
                                    //Refrescando tabla

                                    System.out.println("scbAccion");
                                }
                            }
                            return true;
                        }
                    });
                    
                    control.evaluarHilo(hilo);
                }
            }    
        });
        
        this.scbFecha.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                
                final ItemEvent item = e;
                
                //FILTRO POR RANGOS DE FECHAS
                if (!control.isConstructor() && e.getStateChange() == 1) {
                    _Hilos hilo =new _Hilos(3);
                    
                    hilo.establecerAccion(new _Acciones(){
                        public boolean acto() {
                            if(item.getStateChange() == ItemEvent.SELECTED) {
                                if(hacer_accion == true)
                                {

                                    //Formato para llamar a las fechas
                                    SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
                                    //DateFormat mes = new SimpleDateFormat("MM-YY");

                                    //Fecha actual
                                    Date fecha_actual = new Date();

                                    //Variable para hacer cálculos con respecto a la fecha actual
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(fecha_actual);
                                    int dias = 0;

                                    switch (scbFecha.getSelectedIndex())
                                    {

                                        case 0:
                                            //NINGÚN FILTRO
                                            fecha1 = "";
                                        break;

                                        case 1:
                                            //De ayer y hoy
                                            calendar.add(Calendar.DAY_OF_YEAR, -1);
                                            fecha1 = " AND au.created_at BETWEEN '" + form.format(calendar.getTime()) + "' AND '" + form.format(fecha_actual) + " 23:59'";
                                        break;

                                        case 2:
                                            //De esta semana
                                            dias = calendar.get(Calendar.DAY_OF_WEEK) - 1;
                                            calendar.add(Calendar.DAY_OF_YEAR, -dias);
                                            fecha1 = " AND au.created_at BETWEEN '" + form.format(calendar.getTime()) + "' AND '" + form.format(fecha_actual) + " 23:59'";
                                        break;

                                        case 3:
                                            //De este mes  
                                            dias = calendar.get(Calendar.DAY_OF_MONTH) - 1;
                                            calendar.add(Calendar.DAY_OF_YEAR, -dias);
                                            fecha1 = " AND au.created_at BETWEEN '" + form.format(calendar.getTime()) + "' AND '" + form.format(fecha_actual) + " 23:59'";
                                        break;

                                        case 4:
                                            //De este trimestre
                                            int meses = 0;
                                            int mes_actual = calendar.get(Calendar.MONTH);
                                            //Los siguientes if clasifican en que "posición" del trimestre nos encontramos
                                            if(mes_actual == 0 || mes_actual == 3 || mes_actual == 6 || mes_actual == 9)
                                                meses = 0;
                                            if(mes_actual == 1 || mes_actual == 4 || mes_actual == 7 || mes_actual == 10)
                                                meses = 1;
                                            if(mes_actual == 2 || mes_actual == 5 || mes_actual == 8 || mes_actual == 11)
                                                meses = 2;
                                            calendar.add(Calendar.MONTH, -meses);
                                            //System.out.println(mes_actual + " " + meses);
                                            dias = calendar.get(Calendar.DAY_OF_MONTH) - 1;
                                            calendar.add(Calendar.DAY_OF_MONTH, -dias);
                                            //System.out.println(form.format(calendar.getTime()));
                                            fecha1 = " AND au.created_at BETWEEN '" + form.format(calendar.getTime()) + "' AND '" + form.format(fecha_actual) + " 23:59'";
                                        break;

                                        case 5:
                                            //De este año
                                            dias = calendar.get(Calendar.DAY_OF_YEAR) - 1;
                                            calendar.add(Calendar.DAY_OF_YEAR, -dias);
                                            fecha1 = " AND au.created_at BETWEEN '" + form.format(calendar.getTime()) + "' AND '" + form.format(fecha_actual) + " 23:59'";
                                        break;
                                    }

                                            System.out.println(fecha1);
                                    //Limpiando datepicker
                                    /*if(limpiar_date == true)
                                        dtFecha1.setDate(null);

                                    limpiar_date = true;*/

                                    dtFecha1.setDate(null);

                                    refresh();
                                    System.out.println(scbFecha.getSelectedIndex());
                                    System.out.println(fecha1);
                                }
                            }
                            
                            return true;
                        }
                    });
                    
                    control.evaluarHilo(hilo);
                }
            }
        });
        
        
        
        this.scbOrdenar.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            
                final ItemEvent item = e;
                
                if (!control.isConstructor() && e.getStateChange() == 1) {
                    _Hilos hilo = new _Hilos(4);
                    
                    hilo.establecerAccion(new _Acciones(){
                        public boolean acto() {
                            if(item.getStateChange() == ItemEvent.SELECTED) {
                
                            if(hacer_accion == true)
                            {

                                switch(scbOrdenar.getSelectedIndex())
                                {
                                    case 0:
                                        ordenar = " ORDER BY u.email, au.created_at DESC";
                                        break;

                                    case 1:
                                        ordenar = " ORDER BY u.email DESC, au.created_at DESC";
                                        break;

                                    case 2:
                                        ordenar = " ORDER BY ac.action, au.created_at DESC";
                                        break;

                                    case 3:
                                        ordenar = " ORDER BY ac.action DESC, au.created_at DESC";
                                        break;

                                    case 4:
                                        ordenar = " ORDER BY au.created_at";
                                        break;

                                    case 5:
                                        ordenar = " ORDER BY au.created_at DESC";
                                        break; 

                                    case 6:
                                        ordenar = " ORDER BY r.request_title, au.created_at DESC";
                                        break;

                                    case 7:
                                        ordenar = " ORDER BY r.request_title DESC, au.created_at DESC";
                                        break;
                                }

                                //Refrescando tabla
                                //Refrescando tabla
                                refresh();
                                System.out.println("scbOrdenar");
                            }
                        }
                            
                            return true;
                        }
                    });
                    
                    control.evaluarHilo(hilo);
                }
            }
        });
        
        initComponents();
        
        //Fuente
        try
        {
            InputStream is = getClass().getResourceAsStream("font/MYRIADPRO-REGULAR.tff");
            fuenteAcalde = Font.createFont(Font.TRUETYPE_FONT, is);
        }
        catch(Exception e)
        {
            fuenteAcalde = this.getFont();
        }
        
        //Ingresamos los valores de los combobox
        //scbAccion.setModel( new DefaultComboBoxModel( new String[] { "Escoger todos las categorias" }));
        scbFecha.setModel( new DefaultComboBoxModel( new String[] { "Todos los registros", "Ayer y hoy", "Esta semana", "Este mes", "Este trimestre", "Este año" }));
        
        
        scbOrdenar.setModel( new DefaultComboBoxModel( new String[] { "E_mail", "E-mail a la inversa", "Acción", "Acción a la inversa", "Orden de ingreso", "Orden de ingreso a la inversa", "Peticiones", "Peticiones a la inversa" } ));
        
        //Propiedades del scb
        int scbWidht;
        
        //Informacion para el lbl
        int fontSize = width / 70;
        int sizeLeng = fontSize * lblGenerar.getText().length();
        
        sizeLeng = (sizeLeng * 3) / 4;
        
        int kHeight = height / 100;
        
        //Disposiciones
        this.setLayout(null);
        jHeader.setLayout(null);
        jElement.setLayout(null);
        
        //Propiedadel del tamaño
        this.width = width;
        this.height = height;
        
        //----------------------------------------------------------------------
        jHeader.setBounds(0, 0, width, elementSize);
        
        //Elementos del data grid
        int heightData = (height - elementSize) / 3;
        
        jScrollPane1.setBounds(0, heightData + elementSize, width, (heightData * 2));
        
        //Quitar las opciones por defecto 
        dtAuditoria.getTableHeader().setReorderingAllowed(false);
        
        //Posicionarl el label
        fuenteAcalde.deriveFont(Font.PLAIN, fontSize);
        
        lblGenerar.setFont(fuenteAcalde);
        lblGenerar.setBounds(width - sizeLeng, height - (btnSize + kHeight), sizeLeng, btnSize);
        lblGenerar.setIcon(new ImageIcon("Img/Reportes/pdfOne.png"));
        lblGenerar.setVisible(false);
        
        int sizeDoc = lblDocumentos.getText().length() * fontSize;
        sizeDoc = sizeDoc * 3 / 4;
        
        lblDocumentos.setFont(fuenteAcalde);
        lblDocumentos.setBounds(width - (lblGenerar.getWidth() + sizeDoc), height - (btnSize + kHeight), sizeDoc, btnSize);
        lblDocumentos.setIcon(new ImageIcon("Img/Reportes/folder.png"));
        lblDocumentos.setVisible(false);
        //-------------------------------------------------------------------------
        jTool.btnTransparente(btnIcon, btnExit, btnAll);
        
        btnIcon.setIcon(new ImageIcon("Img/Menu/Opcion1(2).png"));
        btnExit.setIcon(new ImageIcon("Img/Peticiones/Close.png"));
        
        //Lo posicionamos
        int mitad = (elementSize / 2) - (btnSize / 2);
        int K = width / 100;
        btnIcon.setBounds(K, mitad, btnSize, btnSize);
        btnExit.setBounds(width - (btnSize + K), mitad, btnSize, btnSize);
        
        //------------------------------------------------------------------------
        //Fuentes
        lblAccion.setFont(fuenteAcalde);
        lblRegistro.setFont(fuenteAcalde);
        lblAccion.setFont(fuenteAcalde);
        lblAccion.setFont(fuenteAcalde);
        lblAccion.setFont(fuenteAcalde);
        lblAccion.setFont(fuenteAcalde);
        //Tamaño y posicion
        sizeLeng = ((fontSize * lblAccion.getText().length()) * 3) / 4;
        lblAccion.setBounds(K, jHeader.getHeight() + mitad, width / 3, btnSize);
        scbWidht = sizeLeng * 2;
        scbAccion.setBounds(K, lblAccion.getY() + lblAccion.getHeight(), width / 3, btnSize);
        
        sizeLeng = ((fontSize * lblRegistro.getText().length()) * 3) / 4;
        lblRegistro.setBounds(scbAccion.getX() + scbAccion.getWidth() + K, jHeader.getHeight() + mitad, width / 3, btnSize);
        scbFecha.setBounds(scbAccion.getX() + scbAccion.getWidth() + K, lblAccion.getY() + lblAccion.getHeight(), width / 3, btnSize);
        
        sizeLeng = ((fontSize * lblOrden.getText().length()) * 3) / 4;
        lblFecha1.setBounds(scbFecha.getX() + scbFecha.getWidth() + K, jHeader.getHeight() + mitad, width / 4, btnSize);
        dtFecha1.setBounds(scbFecha.getX() + scbFecha.getWidth() + K, lblAccion.getY() + lblAccion.getHeight(), width / 4, btnSize);
        
        lblOrden.setBounds(K, scbAccion.getY() + scbAccion.getHeight() + mitad, width / 3, btnSize);
        scbOrdenar.setBounds(K, lblOrden.getY() + lblOrden.getHeight(), width / 3, btnSize);
        
        //----------------------------------------------------------------------
        add(scbAccion);
        add(scbFecha);
        add(scbOrdenar);
        
        //Peticiones Title------------------------------------------------------
        int fontHeight = jHeader.getHeight() / 2;
        fuenteAcalde.deriveFont(Font.PLAIN, fontHeight);
        lblTitle.setFont(fuenteAcalde);
        lblTitle.setBounds((btnIcon.getX() * 2) + btnIcon.getWidth(), fontHeight - (fontHeight / 2), fontHeight * lblTitle.getText().length(), fontHeight);
    
        //Botones rapidos
        jElement.setBounds(0, jScrollPane1.getY() - elementSize, width, elementSize);
        
        //Posicionar los botones
        btnAll.setIcon(new ImageIcon("Img/Reportes/garbage.png"));
        
        btnAll.setBounds(K, mitad, 180, btnSize);
        
        
        
        
        
        final JScrollBar vBar = jScrollPane1.getVerticalScrollBar();
        
        vBar.addAdjustmentListener(new AdjustmentListener() 
        {
            public void adjustmentValueChanged(AdjustmentEvent e) {
                if(vBar.getValue() >= vBar.getMaximum() - vBar.getModel().getExtent() - 50){
                    if (!control.isConstructor()) {
                        _Hilos hilo = new _Hilos(7);

                        hilo.establecerAccion(new _Acciones(){
                            public boolean acto() {

                                    inicio += 100;
                                    setFilasX();System.out.println("JScrollBar");

                                return true;
                            }
                        });

                        control.evaluarHilo(hilo);
                    }
                }
            }
        });
        //Llenando combobox
        this.scbAccion.removeAllItems();
        this.scbAccion.addItem("Todas las acciones");
        jInterfaz.getCmb(this.scbAccion, tabAccion.select(tabAccion.Accion));
        
        //Habilitando las consultas hechas por scbAccion
        hacer_accion = true;
        
        //Estableciendo límites de la consulta
        inicio = 0;
        corr = 1;
        
        setFilasX();
        
        
        System.out.println("Constructor");
        
        dtAuditoria.setModel(modeloTabla);
        
        //Estableciendo tamaños de las columnas
        dtAuditoria.getTableHeader().setResizingAllowed(false);
        dtAuditoria.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        dtAuditoria.getColumnModel().getColumn(0).setPreferredWidth((width * 1 / 10) - 20);
        dtAuditoria.getColumnModel().getColumn(1).setPreferredWidth(width * 3 / 10);
        dtAuditoria.getColumnModel().getColumn(2).setPreferredWidth(width * 2 / 10);
        dtAuditoria.getColumnModel().getColumn(3).setPreferredWidth(width * 2 / 10);
        dtAuditoria.getColumnModel().getColumn(4).setPreferredWidth(width * 2 / 10);
    }
    
    public jAuditoria(final int width, final int height) {
        _Hilos hilo = new _Hilos(1);
        
        //Es el constructor
        control.estrablecerConstructor(true);
        
        hilo.establecerAccion(new _Acciones(){
            public boolean acto() {

                iniciar(width, height);

                //Saber que se realizo la accion
                return true;
            }
        });

        control.evaluarHilo(hilo);
        /*this.scbAccion.removeAllItems();
        this.scbAccion.addItem("Todas las acciones");
        jInterfaz.getCmb(this.scbAccion, tabAccion.select(tabAccion.Accion));  */
        
    }

    //Obtener la informacion esencia
    public int getHeight(){ return this.height; }
    public int getWidth(){ return this.width; }
    
    //Cambiar la visibilidad
    public void changeVisible(boolean estado){ 
        this.setVisible(estado);
        
        /*this.scbAccion.removeAllItems();
        this.scbAccion.addItem("Todas las acciones");
        jInterfaz.getCmb(this.scbAccion, tabAccion.select(tabAccion.Accion));*/
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jHeader = new javax.swing.JPanel();
        btnExit = new javax.swing.JButton();
        btnIcon = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        dtAuditoria = new javax.swing.JTable();
        lblGenerar = new javax.swing.JLabel();
        lblAccion = new javax.swing.JLabel();
        lblRegistro = new javax.swing.JLabel();
        lblFecha1 = new javax.swing.JLabel();
        lblOrden = new javax.swing.JLabel();
        jElement = new javax.swing.JPanel();
        btnAll = new javax.swing.JButton();
        lblDocumentos = new javax.swing.JLabel();
        dtFecha1 = new com.toedter.calendar.JDateChooser();

        setBackground(new java.awt.Color(0, 162, 211));

        jHeader.setBackground(new java.awt.Color(12, 22, 71));

        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        lblTitle.setBackground(new java.awt.Color(255, 255, 255));
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setText("Auditoría");

        javax.swing.GroupLayout jHeaderLayout = new javax.swing.GroupLayout(jHeader);
        jHeader.setLayout(jHeaderLayout);
        jHeaderLayout.setHorizontalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jHeaderLayout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(btnExit)
                .addGap(63, 63, 63)
                .addComponent(btnIcon)
                .addGap(106, 106, 106)
                .addComponent(lblTitle)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jHeaderLayout.setVerticalGroup(
            jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jHeaderLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jHeaderLayout.createSequentialGroup()
                .addGroup(jHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblTitle)
                    .addComponent(btnIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jScrollPane1.setBackground(new java.awt.Color(41, 51, 127));

        dtAuditoria.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        dtAuditoria.setGridColor(new java.awt.Color(61, 76, 186));
        dtAuditoria.setUpdateSelectionOnSort(false);
        jScrollPane1.setViewportView(dtAuditoria);
        if (dtAuditoria.getColumnModel().getColumnCount() > 0) {
            dtAuditoria.getColumnModel().getColumn(0).setResizable(false);
            dtAuditoria.getColumnModel().getColumn(1).setResizable(false);
            dtAuditoria.getColumnModel().getColumn(2).setResizable(false);
            dtAuditoria.getColumnModel().getColumn(3).setResizable(false);
            dtAuditoria.getColumnModel().getColumn(4).setResizable(false);
            dtAuditoria.getColumnModel().getColumn(5).setResizable(false);
        }

        lblGenerar.setForeground(new java.awt.Color(255, 255, 255));
        lblGenerar.setText("Generar reporte");
        lblGenerar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblGenerarMouseClicked(evt);
            }
        });

        lblAccion.setForeground(new java.awt.Color(255, 255, 255));
        lblAccion.setText("Acción   ");

        lblRegistro.setForeground(new java.awt.Color(255, 255, 255));
        lblRegistro.setText("Mostrar los registros de:");

        lblFecha1.setForeground(new java.awt.Color(255, 255, 255));
        lblFecha1.setText("Fecha de ingreso");

        lblOrden.setForeground(new java.awt.Color(255, 255, 255));
        lblOrden.setText("Opciones para ordenar");

        jElement.setBackground(new java.awt.Color(12, 22, 71));

        btnAll.setForeground(new java.awt.Color(255, 255, 255));
        btnAll.setText("Limpiar");
        btnAll.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAll.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnAll.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAllMouseClicked(evt);
            }
        });
        btnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jElementLayout = new javax.swing.GroupLayout(jElement);
        jElement.setLayout(jElementLayout);
        jElementLayout.setHorizontalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jElementLayout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addComponent(btnAll, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(378, Short.MAX_VALUE))
        );
        jElementLayout.setVerticalGroup(
            jElementLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jElementLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        lblDocumentos.setForeground(new java.awt.Color(255, 255, 255));
        lblDocumentos.setText("Generar documento");
        lblDocumentos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblDocumentosMouseClicked(evt);
            }
        });

        dtFecha1.setDateFormatString("yyyy-MM-dd");
        dtFecha1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dtFecha1PropertyChange(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblDocumentos)
                .addGap(18, 18, 18)
                .addComponent(lblGenerar)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblRegistro)
                    .addComponent(lblAccion, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblFecha1)
                        .addGap(95, 312, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblOrden)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dtFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(87, 87, 87))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
                    .addComponent(jElement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAccion)
                    .addComponent(lblFecha1))
                .addGap(23, 23, 23)
                .addComponent(lblRegistro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblOrden)
                    .addComponent(dtFecha1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jElement, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblGenerar)
                    .addComponent(lblDocumentos))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        //Le quitamos la visibilidad
        setLocationActualy();
    }//GEN-LAST:event_btnExitActionPerformed

    private void lblDocumentosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblDocumentosMouseClicked
        /*try {
            int indice = dtAuditoria.getSelectedRow();
            
            SimpleDateFormat form = new SimpleDateFormat("YY-MM-dd");
            
            int id = (Integer)dtAuditoria.getValueAt(indice, 0);
            String user = (String)dtAuditoria.getValueAt(indice, 1);
            String asunto = (String)dtAuditoria.getValueAt(indice, 2);
            String categoria = (String)dtAuditoria.getValueAt(indice, 3);
            String descripcion = (String)dtAuditoria.getValueAt(indice, 4);
            String fecha = form.format(dtAuditoria.getValueAt(indice, 5));
            String estado = Integer.toString((Integer)dtAuditoria.getValueAt(indice, 6));
            String concejal = (String)dtAuditoria.getValueAt(indice, 7);
            String foto = "Captura.PNG";
            String e_mail = (String)dtAuditoria.getValueAt(indice, 9);
            String telefono = Integer.toString((Integer)dtAuditoria.getValueAt(indice, 10));
            String direccion = (String)dtAuditoria.getValueAt(indice, 11);
            
            
        
            String date = fecha;
            
            jReporteDetallado detalle = new jReporteDetallado();
            detalle.setName("Nombre: "+user);
            detalle.setAsunto("Asunto: "+asunto);
            detalle.setCategoria("Categoria: "+categoria);
            detalle.setConsejal("Concejal: "+concejal);
            detalle.setFecha("Fecha:" + date);
            detalle.setEstado("Estado: "+estado);
            detalle.setUrl("ReporteDetallado");
            detalle.setCorreo("Correo: "+e_mail);
            detalle.setTelefono("Telefono: "+telefono);
            detalle.setDireccion("Direccion: "+direccion);
            detalle.setDescripcion("Descripcion: "+descripcion);
            detalle.setFoto(foto);
            
            ArrayList<String> txt = new ArrayList<String>();
            txt = jInterfaz.getComentStrind( tabComentarios.selectPetOne(tabComentarios.idPeticion), new Integer(id));
            
            Iterator i = txt.iterator();
            
            while(i.hasNext())
            {
                String text = (String)i.next();
                //detalle.addComentarios(text);
            }
            
            detalle.generarReporte();
        } catch (DocumentException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        catch (Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}*/
    }//GEN-LAST:event_lblDocumentosMouseClicked

    private void lblGenerarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGenerarMouseClicked
        /*int countRow = this.dtAuditoria.getRowCount();
        //JOptionPane.showMessageDialog(null, countRow);
        jReporteListado lista = new jReporteListado();
        
        SimpleDateFormat form = new SimpleDateFormat("YY-MM-dd");
        
        try {
            for (int i = 0; i < countRow; i++) {
                jLista list = new jLista();
                
                list.setName("Usuario: "+(String)dtAuditoria.getValueAt(i, 1));
                list.setImg(Image.getInstance("Captura.PNG"));
                list.setFecha("Fecha:" + dtAuditoria.getValueAt(i, 5).toString());
                list.setEstado("Estado: "+(Integer)dtAuditoria.getValueAt(i, 6));
                list.setDescripcion((String)dtAuditoria.getValueAt(i, 4));
                list.setConcejal("Concejal: "+(String)dtAuditoria.getValueAt(i, 7));
                list.setComision("Cetegoria: "+(String)dtAuditoria.getValueAt(i, 3));
                
                lista.addJList(list);
            }
            Calendar cw = new GregorianCalendar();
            
            lista.setFecha("Fecha de creacion del documento: "+ form.format(cw.getTime()));
            
            lista.setUrl("ReporteListado");
            try {
                lista.generarReportes();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        } catch (BadElementException ex) {
           JOptionPane.showMessageDialog(null, ex.getMessage());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }*/
    }//GEN-LAST:event_lblGenerarMouseClicked

    private void btnAllMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAllMouseClicked
        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(6);
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto() {
                    
                    DefaultTableModel modelo = (DefaultTableModel)dtAuditoria.getModel();
        
                    int fila = dtAuditoria.getRowCount();

                    for (int i = 0; fila  > i; i++) {
                        modelo.removeRow(0);
                    }

                    accion = "";
                    ordenar = " ORDER BY au.created_at DESC";
                    fecha1 = "";

                    setFilasX();

                    //Seleccionando primera fila
                    dtAuditoria.changeSelection(0, 0, false, false);
                    
                    return true;
                }
            });
            
            control.evaluarHilo(hilo);
        }
        
        hacer_accion = false;

        scbAccion.setSelectedIndex(0);
        scbFecha.setSelectedIndex(0);
        scbOrdenar.setSelectedIndex(0);
        fecha1 = "";
        dtFecha1.setDate(null);

        hacer_accion = true;

        inicio = 0;
        corr = 1;
    }//GEN-LAST:event_btnAllMouseClicked

    
    
    
    private void dtFecha1PopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_dtFecha1PopupMenuWillBecomeInvisible
        try
        {
            SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
        
            String date = form.format(dtFecha1.getDate());;
            String date2 = date + " 23:59";
            
            this.fecha1 = " AND au.created_at BETWEEN '"+date+"' AND '"+date2+"'";
            
            limpiar_date = false;
            scbFecha.setSelectedIndex(-1);
            
            refresh();
            
        }
        catch(Exception ex){ this.fecha1 = ""; }
    }//GEN-LAST:event_dtFecha1PopupMenuWillBecomeInvisible

    private void dtAuditoriaComponentMoved(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_dtAuditoriaComponentMoved
        
    }//GEN-LAST:event_dtAuditoriaComponentMoved

    private void btnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAllActionPerformed

    private void formComponentMoved(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentMoved
        
        //Llenando combobox
        /*this.scbAccion.removeAllItems();
        this.scbAccion.addItem("Todas las acciones");
        jInterfaz.getCmb(this.scbAccion, tabAccion.select(tabAccion.Accion));*/
        
        //Seleccionando primera fila
        dtAuditoria.changeSelection(0, 0, false, false);
        /*inicio = 1;
        fin = 100;
        corr = 1;*/
    }//GEN-LAST:event_formComponentMoved

    
    
    private void jScrollPane1MouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_jScrollPane1MouseWheelMoved
        
        //if(jScrollPane1.)
    }//GEN-LAST:event_jScrollPane1MouseWheelMoved

    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
        //Llenando combobox
        /*this.scbAccion.removeAllItems();
        this.scbAccion.addItem("Todas las acciones");
        jInterfaz.getCmb(this.scbAccion, tabAccion.select(tabAccion.Accion));*/
        
        inicio = 0;
        corr = 1;
        
        //Seleccionando primera fila
        dtAuditoria.changeSelection(0, 0, false, false);
    }//GEN-LAST:event_formComponentResized

    private void dtFecha1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dtFecha1PropertyChange

        if (!control.isConstructor()) {
            _Hilos hilo = new _Hilos(5);
        
            hilo.establecerAccion(new _Acciones(){

                @Override
                public boolean acto() {
                    try
                    {
                        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");

                        String date = form.format(dtFecha1.getDate());;
                        String date2 = date + " 23:59";

                        fecha1 = " AND au.created_at BETWEEN '"+date+"' AND '"+date2+"'";

                        limpiar_date = false;
                        scbFecha.setSelectedIndex(-1);

                        refresh();

                    }
                    catch(Exception e){}

                    return true;
                }

            });

            control.evaluarHilo(hilo);
        }
    }//GEN-LAST:event_dtFecha1PropertyChange

    public boolean getLocationActualy()
    {
        return location;
    }
    
    //Darle un valor
    public void setLovationValue(boolean location)
    {
        this.location = location;
    }
    
    //Cambiar de posicion
    public void setLocationActualy()
    {
        mConcejalVirtual.jAudi.setLocation(10000, 10000);
        setLovationValue(false);
    }
    
    private boolean location = false;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAll;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnIcon;
    private javax.swing.JTable dtAuditoria;
    private com.toedter.calendar.JDateChooser dtFecha1;
    private javax.swing.JPanel jElement;
    private javax.swing.JPanel jHeader;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAccion;
    private javax.swing.JLabel lblDocumentos;
    private javax.swing.JLabel lblFecha1;
    private javax.swing.JLabel lblGenerar;
    private javax.swing.JLabel lblOrden;
    private javax.swing.JLabel lblRegistro;
    private javax.swing.JLabel lblTitle;
    // End of variables declaration//GEN-END:variables
}
