/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

/**
 *
 * @author maritza-user
 */
public class tabPeticiones {

    public int getId_peticion() {
        return id_peticion;
    }

    public void setId_peticion(int id_peticion) {
        this.id_peticion = id_peticion;
    }

    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getId_estadopet() {
        return id_estadopet;
    }

    public void setId_estadopet(int id_estadopet) {
        this.id_estadopet = id_estadopet;
    }

    public int getId_concejal() {
        return id_concejal;
    }

    public void setId_concejal(int id_concejal) {
        this.id_concejal = id_concejal;
    }
    
    
    //Atributos
    private int id_peticion;
    private int id_perfil;
    private String asunto;
    private int id_categoria;
    private String descripcion;
    private String fecha;
    private int id_estadopet;
    private int id_concejal;
    
    //--------------------------------------------------------------------------
    public static final String idPeticion = "id_peticion";
    public static final String idPerfil = "id_perfil";
    public static final String tabAsunto = "asunto";
    public static final String idCategoria = "id_categoria";
    public static final String tabDescripcion = "descripcion";
    public static final String tabFecha = "fecha";
    public static final String idEstadoPet = "id_estadoPet";
    public static final String idConcejal = "id_concejal";
    
    public static String select(String get)
    {
        return "SELECT "+get+" FROM tab_peticiones ORDER BY "+tabPeticiones.tabFecha+" ASC";
    }
    public static String selectFor(String set)
    {
        return "SELECT * FROM tab_peticiones WHERE "+set+" = ?";
    }
    public static String selectFor(String set, String set1)
    {
        return "SELECT * FROM tab_peticiones WHERE "+set+" = ? AND "+set1+" = ?";
    }
    public static String selectFor(String set, String set1, String set2)
    {
        return "SELECT * FROM tab_peticiones WHERE "+set+" = ? AND "+set1+" = ? AND "+set2+" = ?";
    }
    public static String modificOne(String val1, String set)
    {
        return "UPDATE tab_peticiones SET "+val1+" = ? WHERE "+set+" = ?";
    }
}
