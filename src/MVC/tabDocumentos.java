/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

/**
 *
 * @author Toshiba
 */
public class tabDocumentos {

    public int getId_documento() {
        return id_documento;
    }

    public void setId_documento(int id_documento) {
        this.id_documento = id_documento;
    }

    public int getId_peticion() {
        return id_peticion;
    }

    public void setId_peticion(int id_peticion) {
        this.id_peticion = id_peticion;
    }

    public String getDocumentos() {
        return documentos;
    }

    public void setDocumentos(String documentos) {
        this.documentos = documentos;
    }
    
    //Documentos
    private int id_documento;
    private int id_peticion;
    private String documentos;

    //------------------------------------------------------------------------------------
    public static final String idDocumento = "id_documento";
    public static final String idPeticion = "id_peticion";
    public static final String tabDocumento = "documento";

    public static String selectDoc()
    {
        return "SELECT documento FROM tab_documentos WHERE id_peticion = ?";
    }
}
