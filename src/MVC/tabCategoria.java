/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

/**
 *
 * @author maritza-user
 */
public class tabCategoria {

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    
    
    //Atributos
    private int id_categoria;
    private String categoria;
    
    //--------------------------------------------------------------------------
    public static final String idCategoria = "id_categoria";
    public static final String Categoria = "categoria";
    
    public static String select(String get)
    {
        return "SELECT "+get+" FROM tab_categoria";
    }
    public static String selectOne(String get, String set)
    {
        return "SELECT "+get+" FROM tab_categoria WHERE "+set+" = ?";
    }
}
