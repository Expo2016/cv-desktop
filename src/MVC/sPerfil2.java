/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

//Importamos las clases
import java.sql.*;
import java.sql.Connection;

public class sPerfil2 {

    //Metodo Setter
    public void setId(int _id) {
        this._id = _id;
    }
    public void setName(String _Name) {
        this._Name = _Name;
    }
    public void setNacimiento(String _nacimiento) {
        this._nacimiento = _nacimiento;
    }
    public void setTelefono(String _telefono) {
        this._telefono = _telefono;
    }
    public void setDireccion(String _direccion) {
        this._direccion = _direccion;
    }
    public void setE_mail(String _e_mail) {
        this._e_mail = _e_mail;
    }
    public void setEstado(int _estado) {
        this._estado = _estado;
    }
    public void setTipoUser(int _tipoUser) {
        this._tipoUser = _tipoUser;
    }
    public void setFoto(String _foto) {
        this._foto = _foto;
    }
    public void setContra(String _contra) {
        this._contra = _contra;
    }

    //Obtener los atributos
    public int getId() {
        return _id;
    }
    public String getName() {
        return _Name;
    }
    public String getNacimiento() {
        return _nacimiento;
    }
    public String getTelefono() {
        return _telefono;
    }
    public String getDireccion() {
        return _direccion;
    }
    public String getE_mail() {
        return _e_mail;
    }
    public int getEstado() {
        return _estado;
    }
    public int getTipoUser() {
        return _tipoUser;
    }
    public String getFoto() {
        return _foto;
    }
    public String getContra() {
        return _contra;
    }
    
    //Atributos
    private int _id;
    private String _Name;
    private String _nacimiento;
    private String _telefono;
    private String _direccion;
    private String _e_mail;
    private int _estado;
    private int _tipoUser;
    private String _foto;
    private String _contra;
    
    //Escribir las consultas----------------------------------------------------
    public String iniciarSecion(){
        return "SELECT id_tipousu FROM tab_perfil WHERE nombre = ? and pass = ?";
    }
    public static String getTabName()
    {
        return "SELECT nombre FROM tab_perfil WHERE id_perfil = ?";
    }
}
