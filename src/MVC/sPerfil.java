/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author BastGame
 */
public class sPerfil implements sInfo{

    //Metodos de recoleccion de informacion
    public Long getIdPerfil() {
        return idPerfil;
    }
    public void setIdPerfil(Long idPerfil) {
        this.idPerfil = idPerfil;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public Object getNacimiento() {
        return nacimiento;
    }
    public void setNacimiento(Object nacimiento) {
        this.nacimiento = nacimiento;
    }
    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public String getE_mail() {
        return e_mail;
    }
    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }
    public int getIdEstado() {
        return idEstado;
    }
    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }
    public Long getIdTipoUsu() {
        return idTipoUsu;
    }
    public void setIdTipoUsu(Long idTipoUsu) {
        this.idTipoUsu = idTipoUsu;
    }
    public String getFoto() {
        return foto;
    }
    public void setFoto(String foto) {
        this.foto = foto;
    }
    public String getPass() {
        return pass;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
    
    //Atributos
    private Long idPerfil;
    private String nombre;
    private Object nacimiento;
    private String telefono;
    private String direccion;
    private String e_mail;
    private int idEstado;
    private Long idTipoUsu;
    private String foto;
    private String pass;
    
    //Consultas
    public static String getAllInformation()
    {//He cambiado esta a MySQL
        return "SELECT id, name, birthdate, phone, address, email, state, user_type_id, image, password"
                + " FROM users WHERE email = ? AND password = ? AND state = 0";
    }

    public static String getElement()
    {
        return "SELECT nombre FROM tab_perfil WHERE id_perfil = ?";
    }
    
    public static String getAllElement()
    {
        return "SELECT * FROM tab_perfil WHERE id_perfil = ?";
    }
    
    public static String getAllElementName()
    {
        return "SELECT * FROM tab_perfil WHERE e_mail = ?";
    }
    
    public static String getIdTipo()
    {
        return "SELECT id_tipousu FROM tab_perfil WHERE id_perfil = ?";
    }
    public static String getName()
    {
        return "SELECT nombre FROM tab_perfil WHERE id_perfil = ?";
    }
    
    @Override
    public void getInfo(ArrayList<Object> bast) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
        this.setIdPerfil((Long)bast.get(0));
        this.setNombre((String)bast.get(1));
        this.setNacimiento(bast.get(2));
        this.setTelefono((String)bast.get(3));
        this.setDireccion((String)bast.get(4));
        this.setE_mail((String)bast.get(5));
        this.setIdEstado((Integer)bast.get(6));
        this.setIdTipoUsu((Long)bast.get(7));
        this.setFoto((String)bast.get(8));
        this.setPass((String)bast.get(9));
    }

    @Override
    public sInfo clone() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        return new sPerfil();
    }
}