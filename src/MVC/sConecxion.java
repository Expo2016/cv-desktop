/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

/**
 *
 * @author BastGame
 */

import Conexion.xConectar;
import java.sql.Connection;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class sConecxion {
    public static boolean getExist(Connection cn, String consulta, Object... informacion){
        boolean exist = false;
        
        try {
            
            PreparedStatement cmd = cn.prepareStatement(consulta);
            
            for (int i = 1; i <= informacion.length; i++) {
                Object inf = informacion[i-1];
                
                if (inf instanceof String) {
                    String txt = (String)inf;
                    cmd.setString(i, txt);
                }
                else if (inf instanceof Integer) {
                    Integer Int = (Integer)inf;
                    cmd.setInt(i, Int);
                }
            }
            
            ResultSet rs = cmd.executeQuery();
            
            if (rs.next()) {
                exist = true;
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() );
        }
        
        return exist;
    }
    public static Object getEspecificElement(Connection cn, String consulta, Object... informacion){
        Object exist = null;
        
        try {
            
            PreparedStatement cmd = cn.prepareStatement(consulta);
            
            for (int i = 1; i <= informacion.length; i++) {
                Object inf = informacion[i-1];
                
                if (inf instanceof String) {
                    String txt = (String)inf;
                    cmd.setString(i, txt);
                }
                else if (inf instanceof Integer) {
                    Integer Int = (Integer)inf;
                    cmd.setInt(i, Int);
                }
            }
            
            ResultSet rs = cmd.executeQuery();
            
            if (rs.next()) {
                exist = rs.getObject(1);
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() );
        }
        
        return exist;
    }
    public static void getInformation(sInfo clase, Connection cn, String consulta, Object... informacion){
        ArrayList<Object> bast = new ArrayList<Object>();
        
        try {
            
            PreparedStatement cmd = cn.prepareStatement(consulta);
            
            for (int i = 1; i <= informacion.length; i++) {
                Object inf = informacion[i-1];
                
                if (inf instanceof String) {
                    String txt = (String)inf;
                    cmd.setString(i, txt);
                }
                else if (inf instanceof Integer) {
                    Integer Int = (Integer)inf;
                    cmd.setInt(i, Int);
                }
            }
            
            ResultSet rs = cmd.executeQuery();
            
            if (rs.next()) {
                boolean r = true;
                int i = 1;
                
                while(r)
                {
                    try
                    {
                        bast.add(rs.getObject(i));
                        i++;
                    }
                    catch(Exception ex)
                    {
                        r = false;
                    }
                }
                
                clase.getInfo(bast);
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() );
        }
    }
    public static void getInformationSin(sInfo clase, Connection cn, String consulta, Object... informacion){
        ArrayList<Object> bast = new ArrayList<Object>();
        
        try {
            
            PreparedStatement cmd = cn.prepareStatement(consulta);
            
            for (int i = 1; i <= informacion.length; i++) {
                Object inf = informacion[i-1];
                
                if (inf instanceof String) {
                    String txt = (String)inf;
                    cmd.setString(i, txt);
                }
                else if (inf instanceof Integer) {
                    Integer Int = (Integer)inf;
                    cmd.setInt(i, Int);
                }
            }
            
            ResultSet rs = cmd.executeQuery();
            
            if (rs.next()) {
                boolean r = true;
                int i = 1;
                
                while(r)
                {
                    try
                    {
                        bast.add(rs.getObject(i));
                        i++;
                    }
                    catch(Exception ex)
                    {
                        r = false;
                    }
                }
                
                clase.getInfo(bast);
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() );
        }
    }
    public static ArrayList<sInfo> getInformation(sInfo clase, Connection cn, String consulta){
        
        ArrayList<Object> bast = new ArrayList<Object>();
        ArrayList<sInfo> rep = new ArrayList<sInfo>();
        
        try {
            
            PreparedStatement cmd = cn.prepareStatement(consulta);
            
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next()) {
                boolean r = true;
                int i = 1;
                
                bast.clear();
                sInfo copiX = clase.clone();
                
                while(r)
                {
                    try
                    {
                        bast.add(rs.getObject(i));
                        i++;
                    }
                    catch(Exception ex)
                    {
                        r = false;
                    }
                }
                
                copiX.getInfo(bast);
                
                rep.add(copiX);
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() + consulta);
        }
        
        return rep;
    }
    public static ArrayList<sInfo> getInformationDate(sInfo clase, Connection cn, String consulta,Date hoy){
        
        ArrayList<Object> bast = new ArrayList<Object>();
        ArrayList<sInfo> rep = new ArrayList<sInfo>();
        
        try {
            
            PreparedStatement cmd = cn.prepareStatement(consulta);
            
            cmd.setDate(1, hoy);
            
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next()) {
                boolean r = true;
                int i = 1;
                
                sInfo copiX = clase.clone();
                
                while(r)
                {
                    try
                    {
                        bast.add(rs.getObject(i));
                        i++;
                    }
                    catch(Exception ex)
                    {
                        r = false;
                    }
                }
                
                copiX.getInfo(bast);
                
                rep.add(copiX);
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() + consulta);
        }
        
        return rep;
    }   
    public static ArrayList<sInfo> getInformationDate(sInfo clase, Connection cn, String consulta){
        
        ArrayList<Object> bast = new ArrayList<Object>();
        ArrayList<sInfo> rep = new ArrayList<sInfo>();
        
        try {
            System.out.println("Fecha: "+consulta);
            PreparedStatement cmd = cn.prepareStatement(consulta);
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next()) {
                boolean r = true;
                
                bast = new ArrayList<Object>();
                
                int i = 1;
                
                sInfo copiX = clase.clone();
                
                while(r)
                {
                    try
                    {
                        bast.add(rs.getObject(i));
                        //System.out.println("GGGG: "+rs.getObject(i));
                        i++;
                    }
                    catch(Exception ex)
                    {
                        r = false;
                    }
                }
                
                copiX.getInfo(bast);
                
                
                rep.add(copiX);
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() + consulta);
        }
        
        return rep;
    }
    public static void getCombo(Connection cn, String consulta, javax.swing.JComboBox cmb){
        try {
            
            PreparedStatement cmd = cn.prepareStatement(consulta);
            
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next()) {
                cmb.addItem(rs.getObject(1));
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() );
        }
    }
    public static boolean setModific(Connection cn, String sql){
        boolean r = false;
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            if(!cmd.execute())
            {
                r = false;
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){JOptionPane.showMessageDialog(null, ex.getMessage());}
        
        return r;
    }
    public static boolean insertAll(Connection cn, String sql, Object... informacion){
        boolean resp = false;
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            for (int i = 1; i <= informacion.length; i++) {
                Object inf = informacion[i-1];
                
                if (inf instanceof String) {
                    String txt = (String)inf;
                    cmd.setString(i, txt);
                }
                else if (inf instanceof Integer) {
                    Integer Int = (Integer)inf;
                    cmd.setInt(i, Int);
                }
                else if (inf instanceof Date) {
                    Date date = (Date)inf;
                    cmd.setDate(i, date);
                }
                else
                {
                    cmd.setObject(i, inf);
                }
            }
            
            if (!cmd.execute()) {
                resp = true;
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){JOptionPane.showMessageDialog(null, "Error inesperado: "+ex.getMessage());}
        
        return resp;
    }
    public static Object getEspecificElement(Connection cn, String consulta){
        Object exist = null;
        
        try {
            
            PreparedStatement cmd = cn.prepareStatement(consulta);
            
            ResultSet rs = cmd.executeQuery();
            
            if (rs.next()) {
                exist = rs.getObject(1);
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() );
        }
        
        return exist;
    }
    public static boolean deleteElement(Connection cn, String sql){
        boolean resp = false;
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
                
            if (!cmd.execute()) {
                resp = true;
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage());
        }
        
        return resp;
    }
    public static void tableInformation(Connection cn, String sql, int indice, DefaultTableModel modelo)
    {
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            ResultSet resp = cmd.executeQuery();
            
            Object Datos[] = new Object[indice];
            
            while(resp.next())
            {
                for (int i = 0; i < indice; i++) {
                    Datos[i] = resp.getObject(i+1);
                }
                
                modelo.addRow(Datos);
            }
            
            resp.close();
            cmd.close();
            cn.close();
        }
        catch(Exception ex){JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());}
    }
     public static ArrayList<String> getComentStrind(String sql, Object value)
    {
        //Valor a devolver
        ArrayList<String> obj = new ArrayList<String>();
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            if(value instanceof String)
            {
                //Si ambos son texto
                cmd.setString(1, (String)value);
            }
            else if (value instanceof Integer)
            {
                //Si es numero
                cmd.setInt(1, (Integer)value);
            }
            
            ResultSet rs = cmd.executeQuery();
            
            while(rs.next())
            {
                obj.add(rs.getString(1));
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage()); }
        
        return obj;
    }
     public static ArrayList<Integer> getComentInteger(String sql, Object value)
    {
        //Valor a devolver
        ArrayList<Integer> obj = new ArrayList<Integer>();
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            if(value instanceof String)
            {
                //Si ambos son texto
                cmd.setString(1, (String)value);
            }
            else if (value instanceof Integer)
            {
                //Si es numero
                cmd.setInt(1, (Integer)value);
            }
            
            ResultSet rs = cmd.executeQuery();
            
            while(rs.next())
            {
                obj.add(rs.getInt(1));
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage()); }
        
        return obj;
    }
     public static ArrayList<String> getInformationArray(Connection cn, String consulta){
         
        ArrayList<String> rep = new ArrayList<String>();
        
        try {
            
            PreparedStatement cmd = cn.prepareStatement(consulta);
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next()) { 
                rep.add(rs.getString(1));
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() + consulta);
        }
        
        return rep;
    }
     public static ArrayList<Integer> getInformationArrayInt(Connection cn, String consulta){
         
        ArrayList<Integer> rep = new ArrayList<Integer>();
        
        try {
            
            PreparedStatement cmd = cn.prepareStatement(consulta);
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next()) { 
                rep.add(rs.getInt(1));
            }
            
            cmd.close();
            cn.close();
            
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, "No se ha podido establecer la conecxion por el error: " + ex.getMessage() + consulta);
        }
        
        return rep;
    }
}
