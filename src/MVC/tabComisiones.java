/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

/**
 *
 * @author maritza-user
 */
public class tabComisiones {

    //Metodos para obtener y dar valores
    public int getId_comision() {
        return id_comision;
    }

    public void setId_comision(int id_comision) {
        this.id_comision = id_comision;
    }

    public String getComision() {
        return comision;
    }

    public void setComision(String comision) {
        this.comision = comision;
    }
    
    
    //Atributos
    private int id_comision;
    private String comision;
    
    //----------------------------------------------------------------------------------
    public static String selectOne(String get, String set)
    {
        return "SELECT "+get+" FROM tab_comisiones WHERE "+set+" = ?";
    }
    public static String select(String get)
    {
        return "SELECT "+get+" FROM tab_comisiones";
    }
    
    //Informacion sobre la tabla
    public static final String idComisiones = "id_comision";
    public static final String Comisione = "comision";
}
