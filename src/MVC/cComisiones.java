/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import Conexion.xConexion;
import javax.swing.JOptionPane;

/**
 *
 * @author kevin
 */
public class cComisiones {
    
    public Connection cn;
    
    public cComisiones()
    {
        //Establecemos la conexion 
        xConexion con = new xConexion();
        cn = con.conectar();
    }
    
    
    //Matrices para combobox
    public static String[][] arrayCate;
    
    //Variables que contienen la longitud de las matrices
    public static int lengthCate;
    
    //Tipo usuarios
    private Long id_comision;
    private String comision;
    private String descripcion;
    
    //Organización comisiones
    private Long numcomi;
    private Long numcate;
    
    public Long getNumcomi()
    {
        return numcomi;
    }
    public void setNumcomi(Long  numcomi)
    {
        this.numcomi=numcomi;
    }
    public Long  getNumcate()
    {
        return numcate;
    }
    public void setNumcate(Long  numcate)
    {
        this.numcate=numcate;
    }
    
     public Long getId_comision()
    {
        return id_comision;
    }
    public void setId_comision(Long id_comision)
    {
        this.id_comision=id_comision;
    }
   
    public String getDescripcion()
    {
        return descripcion;
    }
    public void setDescripcion(String descripcion)
    {
        this.descripcion=descripcion;
    }
      public String getComision()
    {
        return comision;
    }
    public void setComision(String comision)
    {
        this.comision=comision;
    }
    
    
    public boolean guardarComision()
    {
        boolean resp=false;
        try
        {
            String sql="INSERT INTO committees (committee, committee_description, created_at, updated_at) VALUES(?,?,NOW(),NOW())";
                    PreparedStatement cmd = cn.prepareStatement(sql);
                    
                    cmd.setString(1,comision);
                    cmd.setString(2,descripcion);
                     if(!cmd.execute())
            {
                resp = true;
            }
            cmd.close();
                        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        
        return resp;
        
       
    }
    
    
    
    public boolean seleccionarComision()
    {
        boolean resp=false;
        try{
            String sql="SELECT MAX(committee_id) FROM committees";
                    PreparedStatement cmd =cn.prepareStatement(sql);
                    ResultSet rs= cmd.executeQuery();
                    if(rs.next())
                        {
                      numcomi = rs.getLong (1);
                      resp=true;
                         }
                    
                    cmd.close();
        }
                    catch (Exception e)
                            {
                                System.out.println(e.toString());
                            }
                    return resp;
                    
                    
    }
     public boolean guardarOrgcomision()
        {
             boolean resp=false;
        try
        {
            String sql="INSERT INTO category_committee (category_id, committee_id, created_at, updated_at) VALUES(?,?,NOW(),NOW())";
                    PreparedStatement cmd = cn.prepareStatement(sql);
                    
                    cmd.setLong(1,numcate);
                    cmd.setLong(2,numcomi);
                     if(!cmd.execute())
            {
                resp = true;
            }
            cmd.close();
                        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        
        return resp;
        
       
        }
     
     
    public boolean validarCategoria()
    {
        //Método para verificar si ya existe una relación con la categoría ingresada
        
        boolean resp = false;
        
        try
        {
            String sql = "SELECT category_committee_id, category_id, committee_id FROM category_committee WHERE category_id = ? AND committee_id = ?";
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setLong(1, numcate);
            cmd.setLong(2, numcomi);
            
            ResultSet rs = cmd.executeQuery();
            
            if(rs.next())
            {
                resp = true;
            }
            
            cmd.close();
        }
        
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        
        return resp;
    }
     
     
     public boolean validarNombreGuardar()
     {
         boolean resp=false;
         try
         {
             String sql="SELECT committee FROM committees WHERE committee=?";
             PreparedStatement cmd= cn.prepareStatement(sql);
             cmd.setString(1,comision);
             ResultSet rs= cmd.executeQuery();
             
             if(rs.next())
                     {
                         resp=true;
                     }
             cmd.close();
         }
           catch (Exception e)
                            {
                                System.out.println(e.toString());
                            }
                    return resp;
        
         
         
     }
     
     public boolean validarNombreModificar()
     {
         boolean resp=false;
         try
         {
             String sql="SELECT committee FROM committees WHERE committee = ? AND committee_id NOT LIKE ?";
             PreparedStatement cmd= cn.prepareStatement(sql);
             cmd.setString(1,comision);
             cmd.setLong(2,id_comision);
             ResultSet rs= cmd.executeQuery();
             
             if(rs.next())
             {
                 resp=true;
             }
             
             cmd.close();
         }
           
         catch (Exception e)
                            
         {
             System.out.println(e.toString());
         }
                    
         return resp;
        
         
         
     }
     
     public boolean buscarRelacion()
    {
        //Método para decidir si se puede eliminar una relación categoría - comisión
        
        boolean resp = true;
        
        try
        {
            
            //Busca los concejales relacionados a la categoría seleccionada
            String sql = "SELECT A.alderman_id " +
                         "FROM category_committee as cc, alderman_committee as ac, alderman as A " +
                         "WHERE cc.categoria_id = ? AND cc.committee_id = ac.committee_id AND ac.alderman_id = A.alderman_id";
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setLong(1, numcate);
            
            ResultSet rs = cmd.executeQuery();
            
            int numconce = 0;
            
            PreparedStatement cmd2;
            ResultSet rs2;
            
            while(rs.next())
            {
                //Va comprobando concejal por concejal si alguna petición ya hace uso de esta relación
                
                numconce = rs.getInt(1);
                
                sql = "SELECT request_id FROM requests WHERE category_id = ? AND alderman_id = ?";
                cmd2 = cn.prepareStatement(sql);
                
                cmd2.setLong(1, numcate);
                cmd2.setLong(2, numconce);
                
                
                rs2 = cmd2.executeQuery();
                
                if(rs2.next())
                {
                    resp = false;
                }
                
                cmd.close();
                
            }
            
        }
        
        catch (Exception e)
        {
            
        }
        
        return resp;
    }
     
    public boolean eliminarCategoria()
    {
        //Método para eliminar una relación categoría - comisión, solo sucede si buscarRelacion() fue exitosa
        
        boolean resp = false;
        
        try
        {
            String sql = "DELETE FROM category_committee WHERE category_id = ? AND committee_id = ?";
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setLong(1, numcate);
            cmd.setLong(2, numcomi);
            
            if(!cmd.execute())
            {
                resp = true;
            }
            
            cmd.close();
        }
        
        catch(Exception e)
        {
            //Sólo debería llegar aquí en caso de que se borre una categoría de la lista sin que halla
            //un registro seleccionado. Pidamos a Dios que eso se cumpla y no surjan excepciones
            //inesperadas en este método xd
            JOptionPane.showMessageDialog(null, "Categoría eliminada");
        }
        
        return resp;
    }
     
    public boolean ModificarComision()
    {
        boolean resp= false;
        try
        {
            String sql= "UPDATE committees SET committee=?, committee_description=?, updated_at=NOW() WHERE committee_id=? ";
            PreparedStatement cmd= cn.prepareStatement(sql);
            
            cmd.setString(1,comision);
            cmd.setString(2, descripcion);
            cmd.setLong(3,id_comision);
            if (!cmd.execute())
            {
                resp=true;
            }
            cmd.close();
        }
            catch (Exception e)
                    {
                    System.out.println(e.toString());
                    }
        return resp;
    }
    
    
    //Método para recopilar informacion para los combobox
    public boolean infoCMB()
    {
        boolean resp = false;
        try
        {
            //Contador de fallos
            int respCont = 0;
            
            //Contador para matrices
            int cont = 0;
            
            //Consulta SELECT tabla proveedores
            String sql = "SELECT category_id, category FROM categories;";
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            //Ejecutando consulta para inicializar matriz
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next())
                cont++;
            
            //Inicializando matriz y guardando longitud
            arrayCate = new String[cont][2];
            lengthCate = cont;
            
            cont = 0;
            
            //Ejecutando consulta para llenar matriz
            rs = cmd.executeQuery();
            
            while(rs.next())
            {
                respCont++;
                arrayCate[cont][0] = rs.getString(1);
                arrayCate[cont][1] = rs.getString(2);
                cont++;
            }
            
            cont = 0;
            
            cmd.close();
            
        }
        
        catch(Exception e)
        {
            System.out.println(e.toString());
            
        }
        
        return resp;
    }
    
    
    
    //Matrices para listbox
    public static String[][] List;
    
    //Variables que contienen la longitud de las matrices
    public static int ListX;
    
    
    //Variable para el indice de la comision seleccionada
    private Long id_comiM;
    
    
    /**
     * @return the id_comiM
     */
    public Long getId_comiM() {
        return id_comiM;
    }

    /**
     * @param id_comiM the id_comiM to set
     */
    public void setId_comiM(Long id_comiM) {
        this.id_comiM = id_comiM;
    }
    
    
    //Método para recopilar informacion para los combobox
    public boolean infoList()
    {
        boolean resp = false;
        try
        {
            //Contador de fallos
            int respCont = 0;
            
            //Contador para matrices
            int cont = 0;
            
            //Consulta SELECT tabla proveedores
            String sql = "SELECT c.category_id, c.category "
                    + "FROM categories as c, category_committee as cc "
                    + "WHERE committee_id = ? AND c.category_id = cc.category_id;";
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setLong(1, id_comiM);
            
            //Ejecutando consulta para inicializar matriz
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next())
                cont++;
            
            //Inicializando matriz y guardando longitud
            List = new String[cont][2];
            ListX = cont;
            
            cont = 0;
            
            //Ejecutando consulta para llenar matriz
            rs = cmd.executeQuery();
            
            while(rs.next())
            {
                respCont++;
                List[cont][0] = rs.getString(1);
                List[cont][1] = rs.getString(2);
                cont++;
            }
            
            cont = 0;
            
            cmd.close();
            
        }
        
        catch(Exception e)
        {
            System.out.println(e.toString());
            
        }
        
        return resp;
    }

}
