/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import java.util.ArrayList;

/**
 *
 * @author BastGame
 */
public class sDocumentos implements sInfo{

    public int getId_documento() {
        return id_documento;
    }

    public void setId_documento(int id_documento) {
        this.id_documento = id_documento;
    }

    public int getId_peticion() {
        return id_peticion;
    }

    public void setId_peticion(int id_peticion) {
        this.id_peticion = id_peticion;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }
    
    private int id_documento;
    private int id_peticion;
    private String documento;

    @Override
    public void getInfo(ArrayList<Object> bast) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
        this.setId_documento((Integer)bast.get(0));
        this.setId_peticion((Integer)bast.get(1));
        this.setDocumento((String)bast.get(2));
    }

    @Override
    public sInfo clone() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
        return new sDocumentos();
    }
    
    //--------------------------------------------------------------------------
    public static String getAll()
    {
        return "SELECT * FROM tab_documentos WHERE id_peticion = ?";
    }
    
    public static String getDocument(int inf)
    {
        return "SELECT documento FROM tab_documentos WHERE id_peticion = "+inf;
    }
}
