/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

/**
 *
 * @author maritza-user
 */
public class tabDocument {

    public int getId_documento() {
        return id_documento;
    }

    public void setId_documento(int id_documento) {
        this.id_documento = id_documento;
    }

    public int getId_peticion() {
        return id_peticion;
    }

    public void setId_peticion(int id_peticion) {
        this.id_peticion = id_peticion;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }
    
    //Atributis
    private int id_documento;
    private int id_peticion;
    private String documento;

    //__________________________________________________________________________
    public static final String idDocumento = "id_documento";
    public static final String idPeticion = "id_peticion";
    public static final String Document = "documento";
    
    public static String selectDoc()
    {
        return "SELECT "+tabDocument.Document+" FROM tab_documentos WHERE "+tabDocument.idPeticion+" = ?";
    }
}