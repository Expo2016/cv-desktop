/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author BastGame
 */
public class sComentarios implements sInfo{

    public int getId_comentario() {
        return id_comentario;
    }

    public void setId_comentario(int id_comentario) {
        this.id_comentario = id_comentario;
    }

    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getArchivos() {
        return archivos;
    }

    public void setArchivos(String archivos) {
        this.archivos = archivos;
    }

    public int getId_peticion() {
        return id_peticion;
    }

    public void setId_peticion(int id_peticion) {
        this.id_peticion = id_peticion;
    }

    public Object getFecha() {
        return fecha;
    }

    public void setFecha(Object fecha) {
        this.fecha = fecha;
    }
    
    
    private int id_comentario;
    private int id_perfil;
    private String comentario;
    private String archivos;
    private int id_peticion;
    private Object fecha; 

    @Override
    public void getInfo(ArrayList<Object> bast) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        try{
            this.setId_comentario((Integer)bast.get(0));
            this.setId_perfil((Integer)bast.get(1));
            this.setComentario((String)bast.get(2));
            this.setArchivos((String)bast.get(3));
            this.setId_peticion((Integer)bast.get(4));
            this.setFecha(bast.get(5));
        }
        catch(Exception ex){
            this.setId_comentario((Integer)bast.get(0));
            this.setArchivos((String)bast.get(1));
            this.setId_peticion((Integer)bast.get(2));
            this.setId_perfil((Integer)bast.get(3));
            this.setComentario((String)bast.get(4));
            this.setFecha(bast.get(5));
        }
    }
    
    @Override
    public sInfo clone() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        return new sComentarios();
    }
    
    public static String insertComentario()
    {
        return "INSERT INTO tab_comentario (id_perfil, comentario, archivos, id_peticion, fecha) VALUES (?, ?, ?, ?, GETDATE())";
    }
    
    public static String insertComentarioSin()
    {
        return "INSERT INTO tab_comentario (id_perfil, comentario, id_peticion, fecha) VALUES (?, ?, ?, GETDATE())";
    }
    
    public static String getId()
    {
        return "SELECT Max(id_comentario) FROM tab_comentario";
    }
    
    public static String getAll(int idPet)
    {
        return "SELECT * FROM tab_comentario WHERE id_peticion = " +idPet;
    }
    
    public static String deleteId(int idCom)
    {
        return "DELETE FROM tab_comentario WHERE id_comentario = " +idCom;
    }
    public static String selectPetOne()
    {
        return "SELECT comentario FROM tab_comentario WHERE id_peticion = ?";
    }
    public static String selectPetOneInt()
    {
        return "SELECT id_perfil FROM tab_comentario WHERE id_peticion = ?";
    }
}
