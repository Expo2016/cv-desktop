/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;
import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import Conexion.xConexion;

/**
 *
 * @author Albie
 */
public class cUsuarios {
    private Connection cn;
    
    public cUsuarios()
    {
          xConexion con = new xConexion();
        cn = con.conectar();
    }
    //Tipo usuarios
    private Integer id_perfil;
    private String nombre;
    private Integer telefono;
    private String direccion;
    private String email;
    private Integer tipousuario;
    private Integer estado;
    
     
    
    
   public Integer getId_perfil()
   {
       return id_perfil;
   }
   public void setId_perfil(Integer id_perfil)
   {
       this.id_perfil=id_perfil;
   }
    
    public Integer getEstado()
    {
        return estado;
    }
    public void setEstado(Integer estado)
    {
        this.estado=estado;
    }
    public String getNombre()
    {
        return nombre;
    }
    public void setNombre(String nombre)
    {
        this.nombre=nombre;
    }
    public Integer getTelefono()
    {
        return telefono;
    }
    public void setTelefono(Integer telefono)
    {
        this.telefono=telefono;
    }
    public String getDireccion()
    {
        return direccion;
    }
    public void setDireccion(String direccion)
    {
        this.direccion=direccion;
    }
    public String getEmail()
    {
        return email;
    }
    public void setEmail(String email)
    {
        this.email=email;
    }
    public Integer getTipousuario()
    {
        return tipousuario;
    }
    public void setTipousuario(Integer tipousu)
    {
        this.tipousuario=tipousu;
    }
    
    public boolean ModificarUsuario()
    {
        boolean resp=false;
        try
        {
            String sql="UPDATE users SET user_type_id=?, state=?, updated_at=NOW() WHERE id=?";
                    PreparedStatement cmd = cn.prepareStatement(sql);
                    
                    cmd.setInt(1,tipousuario);
                    cmd.setInt(2, estado);
                    cmd.setInt(3,id_perfil);
                    if(!cmd.execute())
            {
                resp = true;
            }
            cmd.close();
            cn.close();
        }
        
        catch(Exception e)
        {
            System.out.println(e.toString());
        }
        return resp;
    }
    
}


         

