/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Conexion.xConexion;

/**
 *
 * @author User
 */
public class cCategorias {
    private Connection cn;
    private Long codigoCate;
    private String categoria;
    private String descripcion;
    private Integer estado;
    
    //return
    public Long getCodigoCate() {
        return codigoCate;
    }
    
    public void setCodigoCate(Long codigoCate) {
        this.codigoCate = codigoCate;
    }
    public String getCategoria() {
        return categoria;
    }
    
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }
    
    
    public cCategorias()
    {
        //Establecemos la conexion
        xConexion con = new xConexion();
        cn = con.conectar();
    }
    
    public boolean validarNombreGuardar()
     {
         boolean resp=false;
         try
         {
             String sql="SELECT category FROM categories WHERE category=?";
             
             PreparedStatement cmd= cn.prepareStatement(sql);
             cmd.setString(1,categoria);
             ResultSet rs= cmd.executeQuery();
             
             if(rs.next())
                     {
                         resp=true;
                     }
             System.out.println(sql);
             cmd.close();
         }
           catch (Exception e)
                            {
                                System.out.println(e.toString());
                            }
                    return resp;
        
         
         
     }
    
    
    public boolean validarNombreModificar()
     {
         boolean resp=false;
         try
         {
             String sql="SELECT category FROM categories WHERE category = ? AND category_id NOT LIKE ?";
             PreparedStatement cmd= cn.prepareStatement(sql);
             cmd.setString(1,categoria);
             cmd.setLong(2,codigoCate);
             ResultSet rs= cmd.executeQuery();
             
             if(rs.next())
             {
                 resp=true;
             }
             
             cmd.close();
         }
           
         catch (Exception e)
                            
         {
             System.out.println(e.toString());
         }
                    
         return resp;
        
         
         
     }
    
    
    public boolean guardarCategoria()
    {
        boolean resp = false;
        try
        {
            //Realizar consulta INSERT
            String sql = "INSERT INTO categories(category, category_description, category_status, created_at, updated_at)" + "VALUES(?,?,?, NOW(), NOW())";
            PreparedStatement cmd = cn.prepareStatement(sql);
            //Llenar los parametros de la clase
            cmd.setString(1, categoria);
            cmd.setString(2, descripcion);
            cmd.setInt(3, estado);
            
            //Si da error devuelve 1, caso contrario 0
            //Tomar en cuenta el "!" de negacion
            if(!cmd.execute())
            {
                resp = true;
            }
            cmd.close();
            cn.close();
            //System.out.println(sql);
        }
        catch(Exception e){
            System.out.println(e.toString());
            JOptionPane.showMessageDialog(null, "No guarda nada");
        }
        return resp;
    }
    
   /* public boolean consultarCategoria() {
        boolean resp = false;
        try {
            //Realizar consulta SELECT
            String sql = "SELECT * FROM categories WHERE category=?";
            PreparedStatement cmd = cn.prepareStatement(sql);
            //Llenar los parámetros de la clase
            
            cmd.setString(1, this.categoria);
            

            //Ejecutar la consulta
            ResultSet rs = cmd.executeQuery();
            //Recorrer la lista de registros
            if (rs.next()) {
                resp = true;
                //codigo_jugador = rs.getInt(1);
                descripcion = rs.getString(3);   
            }

            cmd.close();
            cn.close();
        } catch (Exception e) {
            System.out.println(e.toString());
            JOptionPane.showMessageDialog(null, "No consulta");
        }
        return resp;
    }*/
    
    public boolean modificarCategoria() {
        boolean resp = false;
        try {
            //Realizar consulta INSERT
            String sql = "UPDATE categories SET category = ?, category_description = ?, category_status = ?, updated_at = NOW() WHERE category_id = ?";
            //String sql = "UPDATE tab_categoria SET categoria = '" + categoria + "', descripcion = '" + descripcion + "' WHERE id_categoria = " + codigoCate;
            
            PreparedStatement cmd = cn.prepareStatement(sql);
            //Llenar los parámetros de la clase

            cmd.setString(1, categoria);
            cmd.setString(2, descripcion);
            cmd.setInt(3, estado);
            cmd.setLong(4, codigoCate);
                        
            if (!cmd.execute()) {
                resp = true;
            }
            cmd.close();
            cn.close();
            //System.out.println(sql + " " + categoria + " " + descripcion + " " + codigoCate);
        } catch (Exception e) {
            System.out.println(e.toString());
            JOptionPane.showMessageDialog(null, "No modifica nada");
        }
        return resp;
    }

    
}
