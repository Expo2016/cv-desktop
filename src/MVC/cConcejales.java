/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Conexion.xConexion;

/**
 *
 * @author User
 */
public class cConcejales {
    //GUARDAR CONCEJAL
    public Connection cn;
    private Long id_Concejal;
    private String nombreConcejal;
    private String descripcionConcejal;
    private String e_mailConcejal;
    private Integer id_estConcejal;
    private String fotoConcejal;
    
    
    private String comision;
    //return
    public Long getCodigo() {
        return id_Concejal;
    }
    
    public void setCodigo(Long id_Concejal) {
        this.id_Concejal = id_Concejal;
    }
    
    public String getNombreConcejal() {
        return nombreConcejal;
    }
    
    public void setNombreConcejal(String nombreConcejal) {
        this.nombreConcejal = nombreConcejal;
    }
    
    public String getDescripcionConcejal() {
        return descripcionConcejal;
    }
    
    public void setDescripcionConcejal(String descripcionConcejal){
        this.descripcionConcejal = descripcionConcejal;
    }
    
    public String getCorreoConcejal() {
        return e_mailConcejal;
    }
    
    public void setCorreoConcejal(String e_mailConcejal){
        this.e_mailConcejal = e_mailConcejal;
    }
    
    public Integer getEstadoConcejal() {
        return id_estConcejal;
    }
    
    public void setEstadoConcejal(Integer id_estConcejal) {
        this.id_estConcejal = id_estConcejal;
    }
    
    public String getFotoConcejal() {
        return fotoConcejal;
    }
    
    public void setFotoConcejal(String fotoConcejal){
        this.fotoConcejal = fotoConcejal;
    }
    
    
      public String getComision()
    {
        return comision;
    }
    public void setComision(String comision)
    {
        this.comision=comision;
    }
    
    //Matrices para combobox
    public static String[][] Cate;
    
    //Variables que contienen la longitud de las matrices
    public static int CateX;
    
    //Para el combobox de elegir comisiones
    private Long numconce;
    private Long numcomi;
    public Long getNumconce()
    {
        return numconce;
    }
    
    public void setNumconce(Long numconce)
    {
        this.numconce=numconce;
    }
    
    public Long getNumcomi()
    {
        return numcomi;
    }
    
    public void setNumcomi(Long numcomi)
    {
        this.numcomi=numcomi;
    }
    
    public cConcejales()
    {
        //Establecemos la conexion
        xConexion con = new xConexion();
        cn = con.conectar();
    }
    
    public boolean infoCMB()
    {
        boolean resp = false;
        try
        {
            //Contador de fallos
            int respCont = 0;
            
            //Contador para matrices
            int cont = 0;
            
            //Consulta SELECT tabla proveedores
            String sql = "SELECT committee_id, committee FROM committees;";
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            //Ejecutando consulta para inicializar matriz
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next())
                cont++;
            
            //Inicializando matriz y guardando longitud
            Cate = new String[cont][2];
            CateX = cont;
            
            cont = 0;
            
            //Ejecutando consulta para llenar matriz
            rs = cmd.executeQuery();
            
            while(rs.next())
            {
                respCont++;
                Cate[cont][0] = rs.getString(1);
                Cate[cont][1] = rs.getString(2);
                cont++;
            }
            
            cont = 0;
            
        }
        
        catch(Exception e)
        {
            System.out.println(e.toString());
            
        }
        
        return resp;
    }
    
    //Matrices para listbox
    public static String[][] List;
    
    //Variables que contienen la longitud de las matrices
    public static int ListX;
    
    
    //Variable para el indice de la comision seleccionada
    private Long id_comiM;
    
    
    /**
     * @return the id_comiM
     */
    public Long getId_comiM() {
        return id_comiM;
    }

    /**
     * @param id_comiM the id_comiM to set
     */
    public void setId_comiM(Long id_comiM) {
        this.id_comiM = id_comiM;
    }
    
    //Método para recopilar informacion para los combobox
    public boolean infoList()
    {
        boolean resp = false;
        try
        {
            //Contador de fallos
            int respCont = 0;
            
            //Contador para matrices
            int cont = 0;
            
            //Consulta SELECT tabla proveedores
            String sql = "SELECT c.committee_id, c.committee "
                    + "FROM committees as c, alderman_committee as a "
                    + "WHERE a.alderman_id = ? AND c.committee_id = a.committee_id;";
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setLong(1, id_comiM);
            
            //Ejecutando consulta para inicializar matriz
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next())
                cont++;
            
            //Inicializando matriz y guardando longitud
            List = new String[cont][2];
            ListX = cont;
            
            cont = 0;
            
            //Ejecutando consulta para llenar matriz
            rs = cmd.executeQuery();
            
            while(rs.next())
            {
                respCont++;
                List[cont][0] = rs.getString(1);
                List[cont][1] = rs.getString(2);
                cont++;
            }
            
            cont = 0;
            
        }
        
        catch(Exception e)
        {
            System.out.println(e.toString());
            
        }
        
        return resp;
    }
    
    public boolean guardarConcejales()
    {
        boolean resp = false;
        try
        {
            //Realizar consulta INSERT
            String sql = "INSERT INTO alderman(alderman_name, alderman_description, alderman_email, alderman_status, alderman_picture, created_at, updated_at) VALUES(?,?,?,?,?,NOW(),NOW())";
            PreparedStatement cmd = cn.prepareStatement(sql);
            //Llenar los parametros de la clase
            cmd.setString(1, nombreConcejal);
            cmd.setString(2, descripcionConcejal);
            cmd.setString(3, e_mailConcejal);
            cmd.setInt(4, id_estConcejal);
            cmd.setString(5, fotoConcejal);
            
            //Si da error devuelve 1, caso contrario 0
            //Tomar en cuenta el "!" de negacion
            if(!cmd.execute())
            {
                resp = true;
            }
            cmd.close();
        }
        catch(Exception e){
            System.out.println(e.toString());
            JOptionPane.showMessageDialog(null, "No guarda nada");
        }
        return resp;
    }
    
    public boolean consultarConcejales() {
        boolean resp = false;
        try {
            //Realizar consulta SELECT
            String sql = "SELECT alderman_id, alderman_name, alderman_description, alderman_email, alderman_status, alderman_picture"
                    + " FROM alderman WHERE alderman_name=?";
            PreparedStatement cmd = cn.prepareStatement(sql);
            //Llenar los parámetros de la clase
            
            cmd.setString(1, this.nombreConcejal);
            

            //Ejecutar la consulta
            ResultSet rs = cmd.executeQuery();
            //Recorrer la lista de registros
            if (rs.next()) {
                resp = true;
                //codigo_jugador = rs.getInt(1);
                descripcionConcejal = rs.getString(3);   
                JOptionPane.showMessageDialog(null, descripcionConcejal);
            }

            cmd.close();
            
        } catch (Exception e) {
            System.out.println(e.toString());
            JOptionPane.showMessageDialog(null, "No consulta");
        }
        return resp;
    }
    
    public boolean modificarConcejales() {
        boolean resp = false;
        try {
            //Realizar consulta INSERT
            String sql = "UPDATE alderman SET alderman_name =?, alderman_description = ?, alderman_email =?, alderman_status =?, alderman_picture=?, updated_at=NOW() WHERE alderman_id=?";
            PreparedStatement cmd = cn.prepareStatement(sql);
            //Llenar los parámetros de la clase

            cmd.setString(1, nombreConcejal);
            cmd.setString(2, descripcionConcejal);
            cmd.setString(3, e_mailConcejal);
            cmd.setInt(4, id_estConcejal);
            cmd.setString(5, fotoConcejal);
            cmd.setLong(6, id_Concejal);
                     //JOptionPane.showMessageDialog(null, cmd.toString());
            if (!cmd.execute()) {
                resp = true;
            }
            cmd.close();
            
        } catch (Exception e) {
            System.out.println(e.toString());
            JOptionPane.showMessageDialog(null, "No modifica nada");
        }
        return resp;
    }
    
    public boolean modificarConcejalesFoto(String foto, int id) {
        boolean resp = false;
        try {
            //Realizar consulta INSERT
            String sql = "UPDATE alderman SET alderman_picture=?, updated_at=NOW() WHERE alderman_id=?";
            PreparedStatement cmd = cn.prepareStatement(sql);
            //Llenar los parámetros de la clase

            cmd.setString(1, foto);
            cmd.setInt(2, id);
            
                     //JOptionPane.showMessageDialog(null, cmd.toString());
            if (!cmd.execute()) {
                resp = true;
            }
            cmd.close();
            
        } catch (Exception e) {
            System.out.println(e.toString());
            JOptionPane.showMessageDialog(null, "");
        }
        return resp;
    }
    
    public boolean seleccionarConcejal()
    {
        boolean resp=false;
        try{
            String sql="SELECT MAX(alderman_id) FROM alderman";
            PreparedStatement cmd =cn.prepareStatement(sql);
            ResultSet rs= cmd.executeQuery();
            if(rs.next())
            {
                numconce = rs.getLong(1);
                resp=true;
            }
            cmd.close();
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return resp;                    
    }
    
    public boolean guardarOrgcomision()
    {
        boolean resp=false;
        try
        {
            String sql="INSERT INTO alderman_committee (alderman_id, committee_id, created_at, updated_at) VALUES(?,?,NOW(),NOW())";
            PreparedStatement cmd = cn.prepareStatement(sql);
                   
            cmd.setLong(1,numconce);
            cmd.setLong(2,numcomi);
            if(!cmd.execute())
            {
                resp = true;
            }
            cmd.close();
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        
        return resp;
    }
    
    public boolean validarNombreGuardar()
    {
        boolean resp=false;
        try
        {
            String sql="SELECT alderman_name FROM alderman WHERE alderman_name=?";
            PreparedStatement cmd= cn.prepareStatement(sql);
            cmd.setString(1,nombreConcejal);
            ResultSet rs= cmd.executeQuery();
             
            if(rs.next())
            {
                resp=true;
            }
            
            cmd.close();
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return resp;
     }
    
    
    public boolean validarNombreModificar()
     {
         boolean resp=false;
         try
         {
             String sql="SELECT alderman_name FROM alderman WHERE alderman_name = ? AND alderman_id NOT LIKE ?";
             PreparedStatement cmd= cn.prepareStatement(sql);
             cmd.setString(1,nombreConcejal);
             cmd.setLong(2,id_Concejal);
             ResultSet rs= cmd.executeQuery();
             
             if(rs.next())
             {
                 resp=true;
             }
             
             cmd.close();
         }
           
         catch (Exception e)
                            
         {
             System.out.println(e.toString());
         }
                    
         return resp;
        
         
         
     }
    
    
    public boolean validarCorreoGuardar()
    {
        boolean resp=false;
        try
        {
            String sql="SELECT alderman_email FROM alderman WHERE alderman_email=?";
            PreparedStatement cmd= cn.prepareStatement(sql);
            cmd.setString(1,e_mailConcejal);
            ResultSet rs= cmd.executeQuery();
             
            if(rs.next())
            {
                resp=true;
            }
            
            cmd.close();
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return resp;
     }
    
    
    public boolean validarCorreoModificar()
     {
         boolean resp=false;
         try
         {
             String sql="SELECT alderman_email FROM alderman WHERE alderman_email = ? AND alderman_id NOT LIKE ?";
             PreparedStatement cmd= cn.prepareStatement(sql);
             cmd.setString(1,e_mailConcejal);
             cmd.setLong(2,id_Concejal);
             ResultSet rs= cmd.executeQuery();
             
             if(rs.next())
             {
                 resp=true;
             }
             
             cmd.close();
         }
           
         catch (Exception e)
                            
         {
             System.out.println(e.toString());
         }
                    
         return resp;
        
         
         
     }
    
    
    public boolean validarComision()
    {
        //Método para verificar si ya existe una relación con la categoría ingresada
        
        boolean resp = false;
        
        try
        {
            String sql = "SELECT * FROM alderman_committee WHERE committee_id = ? AND alderman_id = ?";
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setLong(1, numcomi);
            cmd.setLong(2, numconce);
            
            ResultSet rs = cmd.executeQuery();
            
            if(rs.next())
            {
                resp = true;
            }
            
            cmd.close();
        }
        
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        
        return resp;
    }
    
    
    public boolean buscarRelacion()
    {
        //Método para decidir si se puede eliminar una relación categoría - comisión
        
        boolean resp = true;
        
        try
        {
            
            //Busca los concejales relacionados a la categoría seleccionada
            String sql = "SELECT C.category_id " +
                         "FROM alderman_committee as ac, category_committee as cc, categories as C " +
                         "WHERE ac.alderman_id = ? AND ac.committee_id = cc.committee_id AND cc.category_id = C.category_id";
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setLong(1, numcomi);
            
            ResultSet rs = cmd.executeQuery();
            
            Long numcate = Long.valueOf(0);
            
            PreparedStatement cmd2;
            ResultSet rs2;
            
            while(rs.next())
            {
                //Va comprobando concejal por concejal si alguna petición ya hace uso de esta relación
                
                numcate = rs.getLong(1);
                
                sql = "SELECT request_id FROM requests WHERE category_id = ? AND alderman_id = ?";
                cmd2 = cn.prepareStatement(sql);
                
                cmd2.setLong(1, numcate);
                cmd2.setLong(2, numconce);
                
                
                rs2 = cmd2.executeQuery();
                
                if(rs2.next())
                {
                    resp = false;
                }
                
                cmd.close();
                
            }
            
        }
        
        catch (Exception e)
        {
            
        }
        
        return resp;
    }
    
    
    public boolean eliminarComision()
    {
        //Método para eliminar una relación categoría - comisión, solo sucede si buscarRelacion() fue exitosa
        
        boolean resp = false;
        
        try
        {
            String sql = "DELETE FROM alderman_committee WHERE committee_id = ? AND alderman_id = ?";
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setLong(1, numcomi);
            cmd.setLong(2, numconce);
            
            if(!cmd.execute())
            {
                resp = true;
            }
            
            cmd.close();
        }
        
        catch(Exception e)
        {
            //Sólo debería llegar aquí en caso de que se borre una categoría de la lista sin que halla
            //un registro seleccionado. Pidamos a Dios que eso se cumpla y no surjan excepciones
            //inesperadas en este método xd
            JOptionPane.showMessageDialog(null, "Comisión eliminada");
        }
        
        return resp;
    }
}
