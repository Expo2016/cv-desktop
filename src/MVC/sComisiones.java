/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import java.util.ArrayList;

/**
 *
 * @author BastGame
 */
public class sComisiones implements sInfo{

    public int getIdComision() {
        return idComision;
    }

    public void setIdComision(int idComision) {
        this.idComision = idComision;
    }
    
    private int idComision;
    
    public static String getAllComisiones()
    {
        return "SELECT comision FROM tab_comisiones";
    }

    public static String getElementComiciones()
    {
        return "SELECT id_comision FROM tab_comisiones WHERE comision = ?";
    }
    
    @Override
    public void getInfo(ArrayList<Object> bast) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        this.setIdComision((Integer)bast.get(0));
    }

    @Override
    public sInfo clone() {
        ///throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return new sComisiones();
    }
}
