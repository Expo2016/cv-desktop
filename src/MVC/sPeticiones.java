/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import java.util.ArrayList;

/**
 *
 * @author BastGame
 */
public class sPeticiones implements sInfo{

    public int getId_peticion() {
        return id_peticion;
    }

    public void setId_peticion(int id_peticion) {
        this.id_peticion = id_peticion;
    }

    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Object getFecha() {
        return fecha;
    }

    public void setFecha(Object fecha) {
        this.fecha = fecha;
    }

    public int getId_estadopet() {
        return id_estadopet;
    }

    public void setId_estadopet(int id_estadopet) {
        this.id_estadopet = id_estadopet;
    }

    public int getId_concejal() {
        return id_concejal;
    }

    public void setId_concejal(int id_concejal) {
        this.id_concejal = id_concejal;
    }
    
    private int id_peticion;
    private int id_perfil;
    private String asunto;
    private int id_categoria;
    private String descripcion;
    private Object fecha;
    private int id_estadopet;
    private int id_concejal;
    
    //--------------------------------
    public static String getCountPeticion(int id)
    {
        return "Select count(*) FROM tab_peticiones WHERE id_categoria = " + id;
    }
    public static String getCountPeticionConce(int id)
    {
        return "Select count(*) FROM tab_peticiones WHERE id_concejal = " + id;
    }
    public static String getCountPeticion()
    {
        return "Select count(*) FROM tab_peticiones";
    }
    public static String getAllPeticiones()
    {
        return "SELECT * FROM tab_peticiones ORDER BY fecha DESC";
    }
    
    public static String getAllPeticiones(String where)
    {
        return "SELECT * FROM tab_peticiones " +where+ " ORDER BY fecha DESC";
    }
    
    public static String getEstadoPet(String id)
    {
        return "SELECT pet.id_estadopet FROM tab_peticiones as pet WHERE pet.id_peticion = "+id;
    }
    
    //--------------------------------------------------------------------------
    public static String getPeticionesAproved()
    {
        return "SELECT * FROM tab_peticiones WHERE id_estadopet = 2 ORDER BY fecha DESC";
    }
    public static String getEstadoPet(int id)
    {
        return "SELECT id_estadopet FROM tab_peticiones WHERE id_peticion = "+id;
    }
    public static String getPeticionesFecha(String fecha)
    {
        
        System.out.println("SELECT * FROM tab_peticiones WHERE fecha = '" +fecha+ "'");
        return "SELECT * FROM tab_peticiones WHERE fecha BETWEEN '" +fecha+ "' AND '" + fecha + " 23:59'";
    }
    
     public static String getPeticionesFechaMax(String fecha)
    {
        return "SELECT top 10 * FROM tab_peticiones ORDER BY fecha DESC";
    }
    
    
     public static String getPeticionesFecha()
    {
        return "SELECT * FROM tab_peticiones WHERE fecha = ?";
    }
     
    public static String getPeticionesStop()
    {
        return "SELECT * FROM tab_peticiones WHERE id_estadopet = 1 ORDER BY fecha DESC";
    }
    
    public static String getPeticionesStep(String step)
    {
        return "SELECT * FROM tab_peticiones WHERE id_estadopet = " +step+ " ORDER BY fecha DESC";
    }
    
    public static String getPeticionesFinish()
    {
        return "SELECT * FROM tab_peticiones WHERE id_estadopet = 4 ORDER BY fecha DESC";
    }
    
    public static String getPeticionesDelete()
    {
        return "SELECT * FROM tab_peticiones WHERE id_estadopet = 5 ORDER BY fecha DESC";
    }
    
    public static String setInfo(int val, int ind)
    {
        return "UPDATE tab_peticiones set id_estadopet = " +val+ " WHERE id_peticion = " +ind;
    }
    
    public static String allTable()
    {
        return "SELECT pet.id_peticion, per.nombre, per.e_mail,pet.asunto,cate.categoria, conce.nombre, pet.descripcion, pet.id_estadopet, pet.fecha FROM tab_perfil as per, tab_peticiones as pet, tab_concejales as conce, tab_categoria as cate WHERE pet.id_perfil = per.id_perfil AND pet.id_concejal = conce.id_concejal AND pet.id_categoria = cate.id_categoria ORDER BY fecha DESC";
    }
    
    public static String allTable(String where, String group)
    {
        System.out.println("SELECT pet.id_peticion, per.nombre, per.e_mail,pet.asunto,cate.categoria, conce.nombre, pet.descripcion, pet.id_estadopet, pet.fecha FROM tab_perfil as per, tab_peticiones as pet, tab_concejales as conce, tab_categoria as cate WHERE pet.id_estadopet != 5 AND pet.id_perfil = per.id_perfil AND pet.id_concejal = conce.id_concejal AND pet.id_categoria = cate.id_categoria " +where+ " "+group);
        return "SELECT pet.id_peticion, per.nombre, per.e_mail,pet.asunto,cate.categoria, conce.nombre, pet.descripcion, pet.id_estadopet, pet.fecha FROM tab_perfil as per, tab_peticiones as pet, tab_concejales as conce, tab_categoria as cate WHERE pet.id_perfil = per.id_perfil AND pet.id_concejal = conce.id_concejal AND pet.id_categoria = cate.id_categoria " +where+ " "+group;
    }
    @Override
    public void getInfo(ArrayList<Object> bast) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  
        this.setId_peticion((Integer)bast.get(0));
        this.setId_perfil((Integer)bast.get(1));
        this.setAsunto((String)bast.get(2));
        this.setId_categoria((Integer)bast.get(3));
        this.setDescripcion((String)bast.get(4));
        this.setFecha(bast.get(5));
        this.setId_estadopet((Integer)bast.get(6));
        this.setId_concejal((Integer)bast.get(7));
    }

    @Override
    public sInfo clone() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
        return new  sPeticiones();
    }
}
