/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

//Importamos
import Conexion.xConectar;
import java.sql.*;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;

public class sConexion {
    //Conectard
    Connection cn;
    
    //conectar
    public sConexion()
    {
        //Obtenemos la sConexion
        xConectar.setUser("TOSHIBA-PC\\SQLEXPRESS");
        xConectar.setDataBase("cvdbd");
        cn = xConectar.conectar();
    }
    
    //Conection
    public Object Select(ArrayList<Object> elements, String sql)
    {
        Object info = null;
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            Iterator i = elements.iterator();
            int ind = 1;
            
            while(i.hasNext())
            {
                Object obj = i.next();
                
                if(obj.getClass().equals(String.class))
                {
                    cmd.setString(ind, (String)obj);
                }
                
                ind++;
            }
            
            //System.out.println(sql);
            
            ResultSet rs = cmd.executeQuery();
            
            if (rs.next())
            {
                info = rs.getObject(1);
                //JOptionPane.showMessageDialog(null, "Aceptar");
            }
            
            cmd.close();
            cn.close();
        }
        catch (Exception ex){ JOptionPane.showMessageDialog(null, "Error de inicio de sesion: "+ex.getMessage()); }
        
        return info;
    }
}
