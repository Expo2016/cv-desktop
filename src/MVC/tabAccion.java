/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

/**
 *
 * @author kevin
 */
public class tabAccion {
    
    public int getId_accion() {
        return id_accion;
    }

    public void setId_accion(int id_categoria) {
        this.id_accion = id_categoria;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String categoria) {
        this.accion = categoria;
    }
    
    
    //Atributos
    private int id_accion;
    private String accion;
    
    //--------------------------------------------------------------------------
    public static final String idAccion = "action_id";
    public static final String Accion = "action";
    
    
    public static String select(String get)
    {
        return "SELECT "+get+" FROM actions";
    }
    public static String selectOne(String get, String set)
    {
        return "SELECT "+get+" FROM actions WHERE "+set+" = ?";
    }
    
}
