/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

import java.util.ArrayList;

/**
 *
 * @author BastGame
 */
public class sCategoria implements sInfo{

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }
    
    private int idCategoria;
    
    public static String getAllCategoria()
    {
        return "SELECT categoria FROM tab_categoria WHERE id_estCategoria = 0";
    }
    
    public static String getAllCategoriaString()
    {
        return "SELECT categoria FROM tab_categoria WHERE id_estCategoria = 0 ORDER BY id_categoria";
    }
    
   public static String getAllCategoriaInt()
    {
        return "SELECT id_categoria FROM tab_categoria WHERE id_estCategoria = 0 ORDER BY id_categoria";
    }
    public static String getEspecificElement()
    {
        return "SELECT categoria FROM tab_categoria WHERE id_categoria = ? AND id_estCategoria = 0";
    }
    
    public static String getElementCategoria()
    {
        return "SELECT id_categoria FROM tab_categoria WHERE categoria = ? AND id_estCategoria = 0";
    }
    
    public static String getCountCategoria()
    {
        return "Select count(*) FROM tab_categoria WHERE id_estCategoria = 0";
    }

    @Override
    public void getInfo(ArrayList<Object> bast) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
        this.setIdCategoria((Integer)bast.get(0));
    }

    @Override
    public sInfo clone() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
        return new sCategoria();
    }
}
