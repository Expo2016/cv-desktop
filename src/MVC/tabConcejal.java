/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

/**
 *
 * @author maritza-user
 */
public class tabConcejal {

    public int getId_concejal() {
        return id_concejal;
    }

    public void setId_concejal(int id_concejal) {
        this.id_concejal = id_concejal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public int getId_estConcejal() {
        return id_estConcejal;
    }

    public void setId_estConcejal(int id_estConcejal) {
        this.id_estConcejal = id_estConcejal;
    }

    
    //Atributos
    private int id_concejal;
    private String nombre;
    private String foto;
    private String descripcion;
    private String e_mail;
    private int id_estConcejal;
    
    //--------------------------------------------------------------------------
    public static final String idConcejal = "id_concejal";
    public static final String tabNombre = "nombre";
    public static final String tabDescripcion = "descripcion";
    public static final String eMail = "e_mail";
    public static final  String idEstConcejal = "id_estConcejal";
    
    public static String select(String get)
    {
        return "SELECT "+get+" FROM tab_concejales";
    }
    public static String selectOne(String get, String set)
    {
        return "SELECT "+get+" FROM tab_concejales WHERE "+set+" = ?";
    }
}
