/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MVC;

/**
 *
 * @author maritza-user
 */
public class tabComentarios {

    public int getId_comentario() {
        return id_comentario;
    }

    public void setId_comentario(int id_comentario) {
        this.id_comentario = id_comentario;
    }

    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getArchivos() {
        return archivos;
    }

    public void setArchivos(String archivos) {
        this.archivos = archivos;
    }

    public int getId_peticion() {
        return id_peticion;
    }

    public void setId_peticion(int id_peticion) {
        this.id_peticion = id_peticion;
    }
    
    
    //Atributos
    private int id_comentario;
    private int id_perfil;
    private String comentario;
    private String archivos;
    private int id_peticion;
   
    //--------------------------------------------------------------------------
    public static final String idComentario = "id_comentario";
    public static final String idPeril = "id_perfil";
    public static final String Comentario = "comentario";
    public static final String Archivos = "archivos";
    public static final String idPeticion = "id_peticion";
    
    public static String selectPet(String set)
    {
        return "SELECT * FROM tab_comentario WHERE "+set+" = ? ";
    }
    
    public static String delate(String set)
    {
        return "DELETE FROM tab_comentario WHERE "+set+" = ?";
    }
    
    public static String selectPetOne(String set)
    {
        return "SELECT comentario FROM tab_comentario WHERE "+set+" = ? ";
    }
    public static String insert(){
        return"INSERT INTO tab_comentario (id_perfil, comentario, archivos, id_peticion) VALUES (?,?,?,?,?)";
    }
    
}
