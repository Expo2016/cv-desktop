/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

/**
 *
 * @author carlos
 */
public class _Hilos extends Thread{
    
    //Constructor
    public _Hilos(int id)
    {
        indice = id;
        useMul = false;
    }

    public boolean getMultiple()
    {
        return useMul;
    }
    
    public void setMultiple(boolean useMul)
    {
        this.useMul = useMul;
    }
    
    //Metodo que ejecuca el hilo
    public void run() {
        
        //Ejecutamos el hilo
        act.acto();
        
        //Detenemos
        this.stop();
        
    }

    public void establecerAccion(_Acciones act) {
        this.act = act;
    }

    public int getIndice() {
        return indice;
    }

    public _Acciones getActo() {
        return act;
    }
    
    //Este es el objeto de la accion
    private _Acciones act;
    
    //Indice del hilo para no repetir
    private int indice;

    private boolean useMul = false;
}
