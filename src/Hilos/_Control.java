/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

import Concejal_Virtual.mConcejalVirtual;
import Concejal_Virtual.mIniciarSesion;

/**
 *
 * @author carlos
 */
public class _Control{
    
    //Control para los contrustores
    private boolean constructor = false;

    public boolean isConstructor() {
        return constructor;
    }

    public void estrablecerConstructor(boolean constructor) {
        this.constructor = constructor;
    }
    
    //Esta clase servira para el control de los hilos
    public void evaluarHilo(_Hilos hilo)
    {
        //If el hilo anterir esta ejecutandose
        if (anterior != null && !hilo.getMultiple()) {
            //Si son el mismo hilo
            if (anterior.getIndice() == hilo.getIndice()) {
                System.out.println("Se quiso ejecutar el mismo hilo");
            }
            else//Si son hilos diferentes
            {
                //Obtenemos el acto
                final _Acciones actor = hilo.getActo();

                //Referenvia a si mismo
                _Control control = this;

                //Referencia
                final _Hilos referencia = anterior;

                final _Hilos actual = hilo;

                hilo.establecerAccion(new _Acciones(){
                    public boolean acto() {

                        while(referencia.isAlive())
                        {
                            System.out.println("Esperar...");
                        }

                        boolean r = actor.acto();

                        if (actual == anterior) {
                            System.out.println("Mision cumplida");
                            //carga.dispose();
                            
                            //Terminamos el constructor
                            if (constructor) {
                                constructor = false;
                            }
                            
                            ind--;
                        
                            if (ind == 0) {
                                //Solo pasara si esta iniciado
                                if (inicioSession) {
                                    cerrarInicio();
                                }
                            }
                            
                            anterior = null;
                        }

                        return r;
                    }
                });

                anterior = hilo;

                ind++;
                
                //Lo iniciamos despues
                hilo.start();
            }
        }
        else//Si no hay ningun hilo ejecutandose
        {
            //Obtenemos el acto
            final _Acciones actor = hilo.getActo();
            
            final _Hilos actual = hilo;
            
            hilo.establecerAccion(new _Acciones(){
                public boolean acto() {
                    boolean r = actor.acto();
                    
                    if (actual == anterior) {
                        System.out.println("Mision cumplida");
                        //carga.dispose();
                        
                        //Terminamos el constructor
                        if (constructor) {
                            constructor = false;
                        }
                        
                        ind--;
                        
                        if (ind == 0) {
                            if (ind == 0) {
                                //Solo pasara si esta iniciado
                                if (inicioSession) {
                                    cerrarInicio();
                                }
                            }
                        }
                        
                        anterior = null;
                    }
                    
                    return r;
                }
            });
            
            //Como no hay ningun hilo guardado lo guardamos
            anterior = hilo;
            
            //carga.setVisible(true);
            //Inicio el conteo
            ind++;
            
            //Lo iniciamos despues
            hilo.start();
        }
    }
    
    public void establecerLoad(_load load)
    {
        this.load = load;
    }
    
    //Saber si inicia session
    public static void establecerInicio(javax.swing.JFrame inicio)
    {
        inicioSession = true;
        _Control.inicio = inicio;
    }
    
    private void cerrarInicio()
    {
        inicioSession = false;
        
        _Control.inicio.dispose();
        _Control.inicio = null;
        ind = 0;
    }
    
    //Hilo anterior
    private _Hilos anterior;
    
    //Carga
    _load load = null;
    
    //Para saber si es el inicio
    private static boolean inicioSession = false;
    
    //El frame para cerrar
    private static javax.swing.JFrame inicio = null;
    
    //Iniciar cronometro
    private static int ind = 0;
}
