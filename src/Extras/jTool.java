/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Expo2016
 */
public class jTool {
    //Metodo para posicionar en medio el jFrame
   
    //Posicion de los componentes
    public static final byte CENTER = 0, LEFT = 1, UP = 2, RIGHT = 3, DOWN = 4;
    
    
    //Fuente
    Font fuente = new Font("Consolas", Font.PLAIN, 16);
    
    public static void setLocation(JFrame Frame, Dimension D)
    {
        //Posicion
        int y = (int)D.getHeight();
        int x = (int)D.getWidth();
        
        //Formula
        x = (x / 2) - (Frame.getWidth() / 2);
        y = (y / 2) - (Frame.getHeight() / 2);
        
        //Posicionar
        Frame.setLocation(x, y);
    }
    
    //Metodo de centrado arriba
    public static void setCenterUp(JFrame Frame, Dimension D)
    {
        //Posicion
        int y = 0;
        int x = (int)D.getWidth();
        
        //Formula
        x = (x / 2) - (Frame.getWidth() / 2);
        
        Frame.setLocation(x, y);
    }
    
    //Transparencia
    public static void btnTransparente(JButton... buton)
    {
        for (int i = 0; i < buton.length; i++){
            buton[i].setOpaque(false);
            buton[i].setContentAreaFilled(false);
            buton[i].setBorderPainted(false);
        }
    }
    
    //Ajustar el tamano de los botones
    public static void btnResizable(int size, JButton... buton)
    {
        for (int i = 0; i < buton.length; i++){
            //Ajustat el tamano
            buton[i].setSize(size, size);
        }
    }
    
    //Poner la imagen
    public static void img(String url, ImageIcon[] img)
    {
        //Imagenes
        for (int i = 0; i < img.length; i++){
            //Ajustat el tamano
            img[i] = new ImageIcon(url + i + ".png");
        }
    }
    
    //psocionar componentes
    public static void setLocationComponent(Component comp, byte posicion, int hComp, int wComp)
    {
        
        //Tamano
        int width = Toolkit.getDefaultToolkit().getScreenSize().width;
        int height = Toolkit.getDefaultToolkit().getScreenSize().height;
        
        switch(posicion)
        {
            case CENTER:
                //Lo centramos
                
                comp.setBounds((width / 2) - (wComp / 2), (height / 2) - (hComp / 2), wComp, hComp);
                
                break;
        }
    }
    
    //Poner texto a los componentes
    public static void setComponentTxt(String[] cmp, String... txt)
    {
        if (txt.length <= cmp.length)
        //Ciclo for para poner los textos
        for (int i = 0; i < txt.length; i++) {
             cmp[i] = txt[i];
        }
    }
}