/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JPanel;

/**
 *
 * @author Maritza
 */
public class jLocationPanel {

    //La lista que nos serivra para obtener los elementos
    private static ArrayList<JPanel> Opciones = new ArrayList<JPanel>();
    
    //contador que ayudara a separar los elementos
    private static int cont = 0;
    
    //Separacion de forma vertical
    private static int sepVertical = 34;
    
    //Separacion de forma horizontal 
    private static int sepHorizontal = 20;
    
    //
    private static int x = 0, y = 0;
    
    //Añadimos elementos
    public static void add(JPanel pnl){
        Opciones.add(pnl);
        cont++;
    }
    
    //Eliminar elemtos
    public static void delete(int contN){
        //Eliminar elementos
        if(contN > 0){
            Opciones.remove(contN);
            cont--;
        }
    }
    
    //Posicionar
    public static void setLocation(){
        //Posicionar
        Iterator i = Opciones.iterator();

        //
        int s = 0;

        //Posicionar
        while(i.hasNext()){
            JPanel panel = (JPanel)i.next();
        
            if(s == 0){
                x = panel.getX();
                y = panel.getY();
                
                s++;
            }
            else{
                x -= sepHorizontal;
                y += sepVertical;
            }
            
            //Location
            panel.setLocation(x, y);
        }
    }
    
    //
}
