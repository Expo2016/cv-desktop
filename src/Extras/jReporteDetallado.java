/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

//Importar elementos
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.FdfWriter;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import javax.swing.JOptionPane;

public class jReporteDetallado {
    
    //Construntor
    public jReporteDetallado()
    {
        this.fecha = "Fecha: 00/00/00";
        this.estado = "Estado: En progreso";
        this.asunto = "Asunto: documento de prueba";
        this.categoria = "Categoria: categoria falsa";
        this.consejal = "Concejal: concejal falso";
        this.name = "<inserte nombre aqui>";
        this.correo = "Correo: nombre_false@hotmail.com";
        this.telefono = "Telefono: 00000000";
        this.direccion = "Direccion: Avenida Simpre Viva nº 742";
        this.descripcion = "El sensor de la X-E1 presenta el mismo excelente rendimiento que el X-Trans CMOS "  
                        + "de 16 megapíxeles del modelo superior de la serie X, la X-Pro1. Gracias la matriz "  
                        + "de filtro de color con disposición aleatoria de los píxeles, desarrollada originalmente"  
                        + " por Fujifilm, el sensor X-Trans CMOS elimina la necesidad del filtro óptico de paso bajo"  
                        + " que se utiliza en los sistemas convencionales para inhibir el muaré a expensas de la"  
                        + " resolución. Esta matriz innovadora permite al sensor X-Trans CMOS captar la luz sin filtrar"  
                        + " del objetivo y obtener una resolución sin precedentes. La exclusiva disposición aleatoria de"  
                        + " la matriz de filtro de color resulta asimismo muy eficaz para mejorar la separación de ruido"  
                        + " en la fotografía de alta sensibilidad. Otra ventaja del gran sensor APS-C es la capacidad"  
                        + " para crear un hermoso efecto “bokeh”, el estético efecto desenfocado que se crea al disparar"  
                        + " con poca profundidad de campo.";
    
        
        this.url = "Beta";
        foto = "Img/Admin/user.jpg";
    }
    
    //Obtener informacion especifica
    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setCorreo(String coreo) {
        this.correo = coreo;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    public void setConsejal(String consejal) {
        this.consejal = consejal;
    }
    public void addComentarios(String txt, boolean tx, String name) {
        this.comentarios.add(name+": "+txt);
        comentariosBol.add(new Boolean(tx));
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    //Generar los reportes------------------------------------------------------
    private void addCeldaString(PdfPTable table, PdfPCell celda, com.itextpdf.text.Font font, int Aling,String txt)
    {
        //La informacion de la cabezera
        Paragraph parrafo = new Paragraph(new Phrase(txt, font));
        
        //Añadimos la celda
        celda = new PdfPCell(parrafo);
        celda.setHorizontalAlignment(Aling);
        celda.setBorder(Rectangle.NO_BORDER);
        table.addCell(celda);
    }
    
    private void addSeparator(Paragraph parrafo, int separacion)
    {
        for (int i = 0; i < separacion; i++) {
            parrafo.add(new Phrase(Chunk.NEWLINE));
        }
    }
    
    private void addParrafo(Document doc, Paragraph parrafo, com.itextpdf.text.Font font, int align, String txt) throws DocumentException
    {
        parrafo.add(new Phrase(txt, font));
        parrafo.setAlignment(align);
        doc.add(parrafo);
    }
    
    public void generarReporte() throws FileNotFoundException, DocumentException, BadElementException, IOException
    {
        //Creamos las partes importantes
        Document doc = new Document(PageSize.A4, 35, 30, 110, 50);
        FileOutputStream file = new FileOutputStream(url.trim()+".pdf");
        PdfWriter write = PdfWriter.getInstance(doc, file);
        
        jMembrete jM = new jMembrete("General");
        
        doc.setMarginMirroring(false);
        
        //Añadimos la fuente
        com.itextpdf.text.Font fuenteNormal = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.PLAIN, BaseColor.BLACK);
        com.itextpdf.text.Font fuenteNegrita = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font fuenteNegritaZ = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.HANGING_BASELINE, BaseColor.BLACK);
        com.itextpdf.text.Font fuenteNegritaX = FontFactory.getFont(FontFactory.TIMES_ROMAN, 15, Font.LAYOUT_NO_LIMIT_CONTEXT, BaseColor.BLACK);
        
        write.setPageEvent(jM);
        
        //Abrimos el documento
        doc.open();
        
        Rectangle rect= new Rectangle(0, 0, 36, 108);
        
        rect.setRight(30);
        rect.setTop(3000);
        
        rect.enableBorderSide(1);
        rect.enableBorderSide(2);
        rect.enableBorderSide(4);
        rect.enableBorderSide(8);
        rect.setBorder(2);
        
        rect.setBorderColor(BaseColor.BLACK);
        rect.setBackgroundColor(new BaseColor(33,41,103));
        
        doc.add(rect);
        
        //Creamos los reportes
        PdfPTable table = new PdfPTable(2);
        PdfPCell celda = new PdfPCell();
        
        //Tmaño
        table.setWidthPercentage(100);
        
        //Añadimos la celda
        this.addCeldaString(table, celda, fuenteNormal, Element.ALIGN_JUSTIFIED, this.fecha);
        this.addCeldaString(table, celda, fuenteNormal, Element.ALIGN_RIGHT, this.estado);
        
        //Añadimos las tablas
        doc.add(table);
        
        //Añadimos los parrafos
        Paragraph parrafo = new Paragraph();
        this.addSeparator(parrafo, 1);
        this.addParrafo(doc, parrafo, fuenteNegrita, Element.ALIGN_CENTER, this.asunto);
        
        //Creamos los espacios en blanco
        parrafo = new Paragraph();
        this.addParrafo(doc, parrafo, fuenteNegrita, Element.ALIGN_CENTER, " ");
        
        //Creamos una nueva tabla
        table = new PdfPTable(2);
        celda = new PdfPCell();
        
        //Tmaño
        table.setWidthPercentage(100);
        
        //Añadimos la celda
        this.addCeldaString(table, celda, fuenteNormal, Element.ALIGN_JUSTIFIED, this.categoria);
        this.addCeldaString(table, celda, fuenteNormal, Element.ALIGN_RIGHT, this.consejal);
        
        //Añadimos las tablas
        doc.add(table);
        
        //Creamos los demas parrafo
        parrafo = new Paragraph();
        this.addSeparator(parrafo, 2);
        this.addParrafo(doc, parrafo, fuenteNegrita, Element.ALIGN_JUSTIFIED, "Informacion sobre el solicitante");
        
        //Informacion
        parrafo = new Paragraph();
        this.addParrafo(doc, parrafo, fuenteNormal, Element.ALIGN_JUSTIFIED, "Nombre: "+this.name);
        parrafo = new Paragraph();
        this.addParrafo(doc, parrafo, fuenteNormal, Element.ALIGN_JUSTIFIED, this.correo);
        parrafo = new Paragraph();
        this.addParrafo(doc, parrafo, fuenteNormal, Element.ALIGN_JUSTIFIED, this.telefono);
        parrafo = new Paragraph();
        this.addParrafo(doc, parrafo, fuenteNormal, Element.ALIGN_JUSTIFIED, this.direccion);
        parrafo = new Paragraph();
        this.addSeparator(parrafo, 1);
        this.addParrafo(doc, parrafo, fuenteNormal, Element.ALIGN_JUSTIFIED, " ");
        //Imagen
        Image img = Image.getInstance(foto);
        img.scaleAbsolute(100, 100);
        img.setAlignment(Image.ALIGN_CENTER);
        
        //Creamos la tabla de la imagen
        table = new PdfPTable(1);
        celda = new PdfPCell(img);
        
        //Creamos la informacion de la tabla
        table.setWidthPercentage(100);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        //celda.setBorder(Rectangle.NO_BORDER);
        celda.setBorderColor(new BaseColor(153,153,153));
        celda.setBackgroundColor(new BaseColor(153,153,153));
        celda.setPaddingBottom(5);
        celda.setPaddingTop(5);
        
        //Añadimos las celdas
        table.addCell(celda);
        doc.add(table);
        
        //Le damos el resto de la informacion
        parrafo = new Paragraph();
        this.addSeparator(parrafo, 1);
        this.addParrafo(doc, parrafo, fuenteNormal, Element.ALIGN_JUSTIFIED, this.descripcion);
        
        //Apartado de comentario
        parrafo = new Paragraph();
        this.addSeparator(parrafo, 1);
        fuenteNegritaX.setColor(33,41,103);
        this.addParrafo(doc, parrafo, fuenteNegritaX, Element.ALIGN_CENTER, "Comentarios:");
        
        //Le damos a los comentario
        Iterator i = comentarios.iterator();
        int ix = 0;
        
        while(i.hasNext())
        {
            boolean tx = comentariosBol.get(ix);
            
            String txt = (String)i.next();
            
            parrafo = new Paragraph();
            
            this.addSeparator(parrafo, 1);
            fuenteNegritaZ.setColor(25,31,79);
            
            if (tx) {
                this.addParrafo(doc, parrafo, fuenteNegritaZ, Element.ALIGN_JUSTIFIED, txt);
            }
            else
            {
                this.addParrafo(doc, parrafo, fuenteNegritaZ, Element.ALIGN_RIGHT, txt);
            }
        
            ix++;
        }
        
        rect= new Rectangle(0, 0, 36, 108);
        
        rect.setRight(30);
        rect.setTop(3000);
        
        rect.enableBorderSide(1);
        rect.enableBorderSide(2);
        rect.enableBorderSide(4);
        rect.enableBorderSide(8);
        rect.setBorder(2);
        
        rect.setBorderColor(BaseColor.BLACK);
        rect.setBackgroundColor(new BaseColor(33,41,103));
        
        doc.add(rect);
        
        //Cerramos el documento
        doc.close();
        
        try {
            File objetofile = new File (url.trim()+".pdf");
            Desktop.getDesktop().open(objetofile);
         }catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error al abrir"+ex.getMessage());
         }
    }
    //Obtener los atributos
    private String fecha;
    private String estado;
    private String asunto;
    private String categoria;
    private String consejal;
    private String name;
    private String correo;
    private String telefono;
    private String direccion;
    private String descripcion;
    private String url;
    private String foto;
    //Comentarios
    ArrayList<String> comentarios = new ArrayList<String>();
    ArrayList<Boolean> comentariosBol = new ArrayList<Boolean>();
}
