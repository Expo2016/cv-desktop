/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

import MVC.sCategoria;
import MVC.sComentarios;
import MVC.sConcejales;
import MVC.sConectar;
import MVC.sConecxion;
import MVC.sPerfil;
import MVC.sPeticiones;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.statistics.HistogramType;

/**
 *
 * @author xarlos
 */
public class jGraficos{
    public static void generarCategorias()
    {
        generarCategoriasImg();
        generarConcejalImg();
        reporteImg();
    }
    
    private static void addCeldaString(PdfPTable table, PdfPCell celda, com.itextpdf.text.Font font, int Aling,String txt)
    {
        //La informacion de la cabezera
        Paragraph parrafo = new Paragraph(new Phrase(txt, font));
        
        //Añadimos la celda
        celda = new PdfPCell(parrafo);
        celda.setHorizontalAlignment(Aling);
        celda.setBorder(Rectangle.NO_BORDER);
        table.addCell(celda);
    }
    
    private static void addSeparator(Paragraph parrafo, int separacion)
    {
        for (int i = 0; i < separacion; i++) {
            parrafo.add(new Phrase(Chunk.NEWLINE));
        }
    }
    
    private static void addParrafo(Document doc, Paragraph parrafo, com.itextpdf.text.Font font, int align, String txt) throws DocumentException
    {
        parrafo.add(new Phrase(txt, font));
        parrafo.setAlignment(align);
        doc.add(parrafo);
    }
    
    private static void reporteImg()
    {
        try {
        String foto1, foto2;
            
        foto1 = "ReporteCategoria.PNG";
        foto2 = "ReporteConcejal.PNG";
            
        SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd");
            
        String date = fm.format(new Date());
            
            //Creamos las partes importantes
        Document doc = new Document(PageSize.A4, 35, 30, 110, 50);
        FileOutputStream file = new FileOutputStream("ReporteGraficos.pdf");
        PdfWriter write = PdfWriter.getInstance(doc, file);
        
        jMembrete jM = new jMembrete("Graficas");
        
        doc.setMarginMirroring(false);
        
        //Añadimos la fuente
        com.itextpdf.text.Font fuenteNormal = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.PLAIN, BaseColor.BLACK);
        com.itextpdf.text.Font fuenteNegrita = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, BaseColor.BLACK);
        com.itextpdf.text.Font fuenteNegritaZ = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.HANGING_BASELINE, BaseColor.BLACK);
        com.itextpdf.text.Font fuenteNegritaX = FontFactory.getFont(FontFactory.TIMES_ROMAN, 15, Font.LAYOUT_NO_LIMIT_CONTEXT, BaseColor.BLACK);
        
        write.setPageEvent(jM);
        
        //Abrimos el documento
        doc.open();
        
        //----------------------------------------------------------------------
        Rectangle rect= new Rectangle(0, 0, 36, 108);
        
        rect.setRight(30);
        rect.setTop(3000);
        
        rect.enableBorderSide(1);
        rect.enableBorderSide(2);
        rect.enableBorderSide(4);
        rect.enableBorderSide(8);
        rect.setBorder(2);
        
        rect.setBorderColor(BaseColor.BLACK);
        rect.setBackgroundColor(new BaseColor(33,41,103));
        
        doc.add(rect);
        
        
        //Creamos los reportes
        PdfPTable table = new PdfPTable(2);
        PdfPCell celda = new PdfPCell();
        
        //Tmaño
        table.setWidthPercentage(100);
        
        //Añadimos la celda
        addCeldaString(table, celda, fuenteNormal, Element.ALIGN_JUSTIFIED, date);
        addCeldaString(table, celda, fuenteNormal, Element.ALIGN_JUSTIFIED, "");
        
        //Añadimos las tablas
        doc.add(table);
        
        Image img = Image.getInstance(foto1);
        img.scaleAbsolute(400, 400);
        img.setAlignment(Image.ALIGN_CENTER);
        //img.setRotation(180f);
        
        //Creamos la tabla de la imagen
        table = new PdfPTable(1);
        celda = new PdfPCell(img);
        
        //Creamos la informacion de la tabla
        table.setWidthPercentage(100);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        //celda.setBorder(Rectangle.NO_BORDER);
        celda.setBorderColor(new BaseColor(153,153,153));
        celda.setBackgroundColor(new BaseColor(153,153,153));
        celda.setPaddingBottom(5);
        celda.setPaddingTop(5);
        
        //Añadimos las celdas
        table.addCell(celda);
        doc.add(table);
        
        img = Image.getInstance(foto2);
        img.scaleAbsolute(400, 400);
        img.setAlignment(Image.ALIGN_CENTER);
        
        //Creamos la tabla de la imagen
        table = new PdfPTable(1);
        celda = new PdfPCell(img);
        
        //Creamos la informacion de la tabla
        table.setWidthPercentage(100);
        celda.setHorizontalAlignment(Element.ALIGN_CENTER);
        //celda.setBorder(Rectangle.NO_BORDER);
        celda.setBorderColor(new BaseColor(153,153,153));
        celda.setBackgroundColor(new BaseColor(153,153,153));
        celda.setPaddingBottom(5);
        celda.setPaddingTop(5);
        
        //Añadimos las celdas
        table.addCell(celda);
        doc.add(table);
        
        //----------------------------------------------------------------------
        rect= new Rectangle(0, 0, 36, 108);
        
        rect.setRight(30);
        rect.setTop(3000);
        
        rect.enableBorderSide(1);
        rect.enableBorderSide(2);
        rect.enableBorderSide(4);
        rect.enableBorderSide(8);
        rect.setBorder(2);
        
        rect.setBorderColor(BaseColor.BLACK);
        rect.setBackgroundColor(new BaseColor(33,41,103));
        
        doc.add(rect);
        
        //Cerramos el documento
        doc.close();
        }
        catch (Exception ex){JOptionPane.showMessageDialog(null, "Error de vario:" + ex.getMessage());}
        
        try {
            File objetofile = new File ("ReporteGraficos.pdf");
            Desktop.getDesktop().open(objetofile);
         }catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error al abrir"+ex.getMessage());
         }
    }
    
    private static void generarCategoriasImg()
    {
        //Sabremos cuando 
        Integer countMax = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sPeticiones.getCountPeticion());
        
        //Por nombre
        ArrayList<String> nameCate = sConecxion.getInformationArray(sConectar.conectar(), sCategoria.getAllCategoriaString());
        
        //Por identificador
        ArrayList<Integer> numCate = sConecxion.getInformationArrayInt(sConectar.conectar(), sCategoria.getAllCategoriaInt());
         
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        for (int i = 0; i < nameCate.size(); i++) {
            int id = numCate.get(i).intValue();
            //JOptionPane.showConfirmDialog(null, id);
            Integer inT = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sPeticiones.getCountPeticion(id));
            
            int count = inT.intValue();
            int max = countMax.intValue();
            
            int porcentaje = (count * 100) / max;
            
            dataset.setValue(porcentaje, "Marks", nameCate.get(i));;
        }
        
        JFreeChart chart = ChartFactory.createBarChart("Total de categoria", "Nombre de las categorias", "Porcentajes de categorias", dataset, PlotOrientation.VERTICAL, false, true, false);
        
        int width = 900;
        int height = 900; 
        
        try {
            ChartUtilities.saveChartAsPNG(new File("ReporteCategoria.PNG"), chart, width, height);
            //JOptionPane.showMessageDialog(null, "Pdf creado");
        }
        catch (IOException e) {JOptionPane.showMessageDialog(null, e.getMessage());}
    }
    
    private static void generarConcejalImg()
    {
        //Sabremos cuando 
        Integer countMax = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sPeticiones.getCountPeticion());
        
        //Por nombre
        ArrayList<String> nameCate = sConecxion.getInformationArray(sConectar.conectar(), sConcejales.getAllConcejalString());
        
        //Por identificador
        ArrayList<Integer> numCate = sConecxion.getInformationArrayInt(sConectar.conectar(), sConcejales.getAllConcejalInt());
         
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        for (int i = 0; i < nameCate.size(); i++) {
            int id = numCate.get(i).intValue();
            //JOptionPane.showConfirmDialog(null, id);
            Integer inT = (Integer)sConecxion.getEspecificElement(sConectar.conectar(), sPeticiones.getCountPeticionConce(id));
            
            int count = inT.intValue();
            int max = countMax.intValue();
            
            int porcentaje = (count * 100) / max;
            
            dataset.setValue(porcentaje, "Marks", nameCate.get(i));;
        }
        
        JFreeChart chart = ChartFactory.createBarChart("Total de concejales", "Nombre de los concejales", "Porcentajes de concejales", dataset, PlotOrientation.VERTICAL, false, true, false);
        
        int width = 900;
        int height = 900; 
        
        try {
            ChartUtilities.saveChartAsPNG(new File("ReporteConcejal.PNG"), chart, width, height);
            //JOptionPane.showMessageDialog(null, "Pdf creado");
        }
        catch (IOException e) {JOptionPane.showMessageDialog(null, e.getMessage());}
    }
}
