/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import javax.swing.JOptionPane;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/**
 *
 * @author box
 */
public class jBajarFtp {

    
    public static File descargarArchivo() {
        File f_retorno = null;

        try {
            FTPClient ftpCliente = new FTPClient();
            int respuesta;

            //Clave de la ftp
            String ipFtp = jBajarFtp.ftpServer;
            String nameFile = jBajarFtp.nombreArchivo;
            String user = jBajarFtp.user;
            String clave = jBajarFtp.clave;
            String rutaOrigen = jBajarFtp.primePath;
            String rutaDestino = jBajarFtp.secundPath;

            ftpCliente.connect(ipFtp);

            if ((!user.equals("")) && (!clave.equals(""))) {
                ftpCliente.login(user, clave);

                respuesta = ftpCliente.getReplyCode();

                if (respuesta == 230) {
                    ftpCliente.setFileType(FTP.BINARY_FILE_TYPE);
                    ftpCliente.changeWorkingDirectory(rutaOrigen);
                    
                    respuesta = ftpCliente.getReplyCode();
                    //FTPReply.isPositiveCompletion(respuesta)
                    if (true) {
                        FTPFile archivoFtp[] = ftpCliente.listFiles();
                        respuesta = ftpCliente.getReplyCode();
                                
                        if (FTPReply.isPositiveCompletion(respuesta)) {
                            if (archivoFtp.length > 0) {
                                for (int i = 0; i < archivoFtp.length; i++) {
                                    String name = archivoFtp[i].getName();
                                    
                                    if (name.equals(nameFile)) {
                                        
                                        String destino = rutaDestino + File.separator + archivoFtp[i].getName();
                                        System.out.println(destino);
                                        boolean archivoReotrnado = ftpCliente.retrieveFile(archivoFtp[i].getName(), new FileOutputStream(destino));
                                    
                                        if (archivoReotrnado) {
                                            f_retorno = new File(destino);
                                            
                                            if (!(f_retorno.length() >= 0)) {
                                                JOptionPane.showMessageDialog(null, "Archivo con longitud "+f_retorno.length());
                                                f_retorno = null;
                                            }
                                        }
                                        else
                                        {
                                            JOptionPane.showMessageDialog(null, "No se pudo descargar");
                                        }
                                    }
                                    
                                    if (f_retorno != null) {
                                        break;
                                    }
                                }
                                
                                if (f_retorno == null) {
                                        JOptionPane.showMessageDialog(null, "No se pudo descargar el archivo");
                                }
                            }
                            else
                            {
                                JOptionPane.showMessageDialog(null, "Lista de archivo invalida");
                            }
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, "No se pudo leer los archivos");
                        }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "No se pudo cambiar los archivos");
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "No se pudo autentificar");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Error al acceder");
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getLocalizedMessage());
        }

        return f_retorno;
    }

    public static void setUser(String user) {
        jBajarFtp.user = user;
    }

    public static void setClave(String clave) {
        jBajarFtp.clave = clave;
    }

    public static void setNombreArchivo(String nombreArchivo) {
        jBajarFtp.nombreArchivo = nombreArchivo;
    }

    public static void setFtpServer(String ftpServer) {
        jBajarFtp.ftpServer = ftpServer;
    }

    public static void setPrimerPath(String primePath) {
        jBajarFtp.primePath = primePath;
    }

    public static void setSecundPath(String secundPath) {
        jBajarFtp.secundPath = secundPath;
    }
    
    //Informacion
    private static String user;
    private static String clave;
    private static String nombreArchivo;
    private static String ftpServer;
    private static String primePath;
    private static String secundPath;
}
