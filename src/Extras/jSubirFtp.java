/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;


/**
 *
 * @author box
 */
public class jSubirFtp {

    public static void subierArchivo()
    {
        /*
        try
        {
            //Si el archivo existe
            if (file.exists()) {
                //La conexion inicia
                Socket socket = new Socket(ftpServer, 21);
                
                //Sacer archivos
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                //Subir archivos
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
                //Escribimos
                bufferedWriter.write("USER "+user+"\r\n");
                bufferedWriter.flush();
                bufferedWriter.write("PASS "+clave+"\r\n");
                bufferedWriter.flush();
                bufferedWriter.write("CWD "+Location+"\r\n");
                bufferedWriter.flush();
                bufferedWriter.write("TYPE A\r\n");
                bufferedWriter.flush();
                bufferedWriter.write("PASV\r\n");
                bufferedWriter.flush();
                String response = null;
                
                while((response=bufferedReader.readLine())!=null)
                {
                    if(debug)
                        System.out.println("Debug: "+response);
                    
                    if (response.startsWith("530"))
                    {
                        System.err.println("Autentificacion fallida");
                        break;
                    }
                    
                    if (response.startsWith("227")) {
                        String address = null;
                        
                        int port = -1;
                        int opening = response.indexOf('(');
                        int closing = response.indexOf(')' , opening + 1);
                        
                        if (closing > 0) {
                            String dataLink = response.substring(opening + 1, closing);
                            StringTokenizer tokenizer = new StringTokenizer(dataLink, ",");
                        
                            try
                            {
                                address = tokenizer.nextToken() + "." + tokenizer.nextToken() + "." + tokenizer.nextToken() + "." + tokenizer.nextToken();
                                port = Integer.parseInt(tokenizer.nextToken()) * 256 +Integer.parseInt(tokenizer.nextToken());
                            }
                            catch(Exception ex)
                            {
                                ex.printStackTrace();
                            }
                            
                            try
                            {
                                Socket transfer = new Socket(address,port);
                                bufferedWriter.write("STOR "+file.getName()+"\r\n");
                                bufferedWriter.flush();
                                
                                response = bufferedReader.readLine();
                                System.out.println(response);
                                if (debug)
                                    System.out.println("antes 150: "+response);
                                
                                if (response.startsWith("125")) {
                                    
                                    FileInputStream fileInputStream= new FileInputStream(file);
                                    final int BUFFER_SIZE = 1024;
                                    byte buffer[] = new byte[BUFFER_SIZE];
                                    
                                    int len = 0, off = 0;
                                    
                                    if (debug) 
                                        System.out.println("Subuiendo archivo...");
                                    
                                    while((len=fileInputStream.read(buffer)) != -1)
                                        transfer.getOutputStream().write(buffer, off, len);
                                    
                                    transfer.getOutputStream().flush();
                                    transfer.close();
                                    socket.close();
                                    System.out.println("Los datos han sido enviados");
                                    break;
                                }
                            }
                            catch(Exception ex)
                            {
                                System.err.println(ex);
                            }
                        }
                    }
                }
            }
        }
        catch(MalformedURLException ex)
        {
            //Problemas en el url
            JOptionPane.showMessageDialog(null,"Error URL: "+ex.getMessage());
        }
        catch(IOException ex)
        {
            //Problemas en la lectura
            JOptionPane.showMessageDialog(null,"Error Lectura: "+ex.getMessage());
        }
        catch(Exception ex)
        {
            //Problemas en cualquier otra cosa
            JOptionPane.showMessageDialog(null,"Error: "+ex.getMessage());
        }
                
        */
        
        
        try
        {
            FTPClient ftpClient = new FTPClient();
            ftpClient.connect(InetAddress.getByName(jSubirFtp.ftpServer));
            ftpClient.login(jSubirFtp.user,jSubirFtp.clave);
            
            //Verificar conexión con el servidor.
            
            int reply = ftpClient.getReplyCode();
            
            System.out.println("Respuesta recibida de conexión FTP:" + reply);
            
            if(FTPReply.isPositiveCompletion(reply))
            {
                System.out.println("Conectado Satisfactoriamente");    
            }
            else
                {
                    System.out.println("Imposible conectarse al servidor");
                }
           
            //Verificar si se cambia de direcotirio de trabajo
            
            ftpClient.changeWorkingDirectory("/");//Cambiar directorio de trabajo
            System.out.println("Se cambió satisfactoriamente el directorio");
            
            //Activar que se envie cualquier tipo de archivo
            
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            System.out.println("Binario");
            BufferedInputStream buffIn = null;
            buffIn = new BufferedInputStream(new FileInputStream(jSubirFtp.file.getAbsolutePath()));//Ruta del archivo para enviar
            
            System.out.println("File");
            ftpClient.enterLocalPassiveMode();
            System.out.println("Modo pasivo");
            ftpClient.storeFile(jSubirFtp.file.getName(), buffIn);//Ruta completa de alojamiento en el FTP
            System.out.println("Ruta del ftp");
            buffIn.close(); //Cerrar envio de arcivos al FTP
            ftpClient.logout(); //Cerrar sesión
            ftpClient.disconnect();//Desconectarse del servidor
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null,"Erro: "+ex.getMessage());
        }
    }
    
    //Metodos para ingresar informacion
    public static void setFtpServer(String ftpServer) {
        jSubirFtp.ftpServer = ftpServer;
    }

    public static void setUser(String user) {
        jSubirFtp.user = user;
    }

    public static void setClave(String clave) {
        jSubirFtp.clave = clave;
    }

    public static void setLocation(String Location) {
        jSubirFtp.Location = Location;
    }

    public static void setFile(File file) {
        jSubirFtp.file = file;
    }
    
    public static void setDebug(boolean debug) {
        jSubirFtp.debug = debug;
    }
    
    //Informacion
    private static String ftpServer;
    private static String user;
    private static String clave;
    private static String Location;
    private static File file;
    private static boolean debug;
}
