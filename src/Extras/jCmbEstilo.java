/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;
import Extras.jTool;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicComboBoxUI;
/**
 *
 * @author Toshiba
 */
public class jCmbEstilo extends BasicComboBoxUI{
    //Creamos la clase
    
    //El icono del CMB
    private ImageIcon img = new ImageIcon("Img/Peticiones/0ne.png");
    private Color color = Color.WHITE;
    private Color colorSeleccion = new Color(31, 150, 244);
    
    //Cramos el objetos(este objeto)
    public static ComboBoxUI createUI(JComponent c)
    {
        return new jCmbEstilo();
    }
    
    //La flecha
    protected JButton createArrowButton()
    {
        //Creamos la flecha
        /*BasicArrowButton basicArrowButton = new BasicArrowButton(BasicArrowButton.SOUTH, Color.white, new Color(130, 7, 7), new Color(130, 7, 7), Color.white);
        
        //Le damos el efecto 2d
        basicArrowButton.setBorder(BorderFactory.createLineBorder(color, 2));
        basicArrowButton.setContentAreaFilled(false);
        return basicArrowButton;*/
        
        JButton boton = new JButton();
        boton.setText("");
        jTool.btnTransparente(boton);
        boton.setBorder(BorderFactory.createLineBorder(color, 2));
        boton.setContentAreaFilled(false);
        boton.setIcon(new ImageIcon("Img/Peticiones/arrowDown.png"));
        return boton;
    }
    
    //Ni diea
    public void paintCurrentValueBackground(Graphics g, Rectangle bounds,boolean hasFocus)
    {
        g.setColor(color);
        g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
    }

    protected ListCellRenderer createRenderer()
    {
        return new DefaultListCellRenderer()
        {
            public Component getListCellRendererComponent(JList list,Object value,int index,boolean isSelected,boolean cellHasFocus)
            {
               super.getListCellRendererComponent(list,value,index,isSelected,cellHasFocus);
               
               list.setSelectionBackground(color);
               
               if(isSelected)
               {
                   setBackground(colorSeleccion);
                   setForeground(Color.BLACK);
               }
               else
               {
                   setBackground( Color.WHITE );            
                   setForeground( new Color(70,70,70));
               }
               
               if (index!=-1) {          
                    setIcon(img);          
               }
               
               return this;
            } 
        };
    }
}
