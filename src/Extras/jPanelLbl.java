/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Toshiba
 */
public class jPanelLbl extends JPanel{
    
    //Tamano del panel
    int width, height;
    
    //Texto que saldra
    String txt;
    
    //Color del texto
    Color clr;
    
    //Constructor
    public jPanelLbl(int height, int width, String txt, Color color, Color clr)
    {
        //Pasamos la informacion
        this.height = height;
        this.width = width;
        this.txt = txt;
        this.setBackground(color);
        this.clr = clr;
    }
    
    //Paint
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        //Tamano del txt
        int Size = width * 2 / 3;
        
        //Color
        g.setColor(clr);
        
        //Dibujamos el texto
        g.setFont(new Font("Consolas", Font.PLAIN, Size));
        
        for (int i = 0; i < txt.length(); i++) {
                //Dibujamos las letras
                g.drawString("" + txt.charAt(i), (width / 2)  - (Size / 3), (i * Size) + Size);
        }
    }
}
