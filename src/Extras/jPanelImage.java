/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author carlos
 */
public class jPanelImage extends javax.swing.JPanel{
    private String url = "Img/Admin/user.jpg";
    private int width, height;
    
    public jPanelImage()
    {
        width = 0;
        height = 0;
    }
    
    public void setUrl(String url)
    {
        this.url = url;
    }
    
    public void setBounds(int i, int i1, int i2, int i3) {
        super.setBounds(i, i1, i2, i3); //To change body of generated methods, choose Tools | Templates.
        width = i2;
        height = i3;
    }
    
    public void setSize(int i, int i1) {
        super.setSize(i, i1); //To change body of generated methods, choose Tools | Templates.
        width = i;
        height = i1;
    }
    
    public void paintComponent(Graphics g) {
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
        try {
            g.drawImage(ImageIO.read(new File(url)), 0, 0, width, height, null);
        } catch (IOException ex) {}
    }
}
