/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

//Importacion

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import java.io.IOException;


public class jLista {
    //Constructor
    public jLista() throws BadElementException, IOException
    {
        this.name="Name: nombre falso";
        this.comision="Seguridad,Seguridad";
        this.concejal="Designado a: Concejal nombre_falso";
        this.img = Image.getInstance("Img/modPeticiones/user.jpg");
        this.descripcion = "El sensor de la X-E1 presenta el mismo excelente rendimiento que el X-Trans CMOS "  
                        + "de 16 megapíxeles del modelo superior de la serie X, la X-Pro1. Gracias la matriz "  
                        + "de filtro de color con disposición aleatoria de los píxeles, desarrollada originalmente"  
                        + " por Fujifilm, el sensor X-Trans CMOS elimina la necesidad del filtro óptico de paso bajo"  
                        + " que se utiliza en los sistemas convencionales para inhibir el muaré a expensas de la"  
                        + " resolución. Esta matriz innovadora permite al sensor X-Trans CMOS captar la luz sin filtrar"  
                        + " del objetivo y obtener una resolución sin precedentes. La exclusiva disposición aleatoria de"  
                        + " la matriz de filtro de color resulta asimismo muy eficaz para mejorar la separación de ruido"  
                        + " en la fotografía de alta sensibilidad. Otra ventaja del gran sensor APS-C es la capacidad"  
                        + " para crear un hermoso efecto “bokeh”, el estético efecto desenfocado que se crea al disparar"  
                        + " con poca profundidad de campo.";
        this.estado = "Estado: en progreso";
        this.fecha = "Fecha: 00/00/00";
    }
    
    //Setter y Getter
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getComision() {
        return comision;
    }
    public void setComision(String comision) {
        this.comision = comision;
    }
    public String getConcejal() {
        return concejal;
    }
    public void setConcejal(String concejal) {
        this.concejal = concejal;
    }
    public Image getImg() {
        return img;
    }
    public void setImg(Image img) {
        this.img = img;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public String getFecha() {
        return fecha;
    }
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    //Atributos
    private String name;
    private String comision;
    private String concejal;
    private Image img;
    private String descripcion;
    private String estado;
    private String fecha;
}
