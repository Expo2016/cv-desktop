/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Font;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdesktop.swingx.painter.AbstractLayoutPainter;

public class jMembrete extends PdfPageEventHelper{
    private Image imagen;
    PdfPTable table = new PdfPTable(2);
    
    public jMembrete(String reporte)
    {
        try
        {
            com.itextpdf.text.Font fuenteNegritaZ = FontFactory.getFont(FontFactory.TIMES_ROMAN, 12, Font.HANGING_BASELINE, BaseColor.BLACK);
            fuenteNegritaZ.setColor(25,31,79);
            
            PdfPCell titulo = new PdfPCell(new Phrase("alcaldía de san salvador", fuenteNegritaZ));
            PdfPCell tipoDeReporte = new PdfPCell(new Phrase("Reporte: " + reporte, fuenteNegritaZ));
            
            imagen = Image.getInstance("Img/IniciarSesion/shield.png");
            imagen.scaleAbsolute(100, 100);
            imagen.setAbsolutePosition(35, 745f);
            
            titulo.setBorder(Rectangle.BOTTOM);
            titulo.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
            
            tipoDeReporte.setBorder(Rectangle.BOTTOM);
            tipoDeReporte.setHorizontalAlignment(Rectangle.ALIGN_CENTER);
            
            //titulo.setBackgroundColor(new BaseColor(41,51,127));
            //tipoDeReporte.setBackgroundColor(new BaseColor(41,51,127));
            
            table.addCell(titulo);
            table.addCell(tipoDeReporte);
            
            table.setTotalWidth(350F);
            
        }
        catch(Exception ex)
        {
        
        }
    }
    
    public void onEndPage(PdfWriter write, Document doc)
    {
        try {
            doc.add(imagen);
            table.writeSelectedRows(0, -1, 140f, 800f, write.getDirectContent());
        } catch (DocumentException ex) {
            //Logger.getLogger(jMembrete.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
