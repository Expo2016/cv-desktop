/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

/**
 *
 * @author maritza-user
 */

//Informacion sql
import MVC.tabComentarios;
import Conexion.xConectar;
import java.sql.Connection;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import MVC.tabPeticiones;

public class jInterfaz {
    
    //Establecemos los valores
    public static void setValues()
    {
        xConectar.setUser("TOSHIBA-PC\\SQLEXPRESS");
        xConectar.setDataBase("cvdbd");
    }
    
    //Metodos estaicos para devolver valores
    public static boolean getCmb(javax.swing.JComboBox cmb, String sql,Object value)
    {
        //Valor que servira para saber si se ha establecido una coneccion
        boolean values = false;
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        //Posibles errore
        try
        {
           //Realizamos la consulta
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            JOptionPane.showMessageDialog(null, sql);
            
            //Le damos el valor
            if(value instanceof String)
            {
                //Si ambos son texto
                cmd.setString(1, (String)value);
            }
            else if (value instanceof Integer)
            {
                //Si es numero
                cmd.setInt(1, (Integer)value);
            }
            //Ejecutamos el sql
            ResultSet rs = cmd.executeQuery();
        
            while(rs.next())
            {
                cmb.addItem(rs.getObject(1));
            }
            
            values = true;
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error de los combos: " + ex.getMessage()); }
    
        //Devolvemos el estado de la accion
        return values;
    }
    
    public static boolean getCmb(javax.swing.JComboBox cmb, String sql)
    {
        //Valor que servira para saber si se ha establecido una coneccion
        boolean values = false;
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        //Posibles errore
        try
        {
           //Realizamos la consulta
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            
            
            //Ejecutamos el sql
            ResultSet rs = cmd.executeQuery();
        
            while(rs.next())
            {
                //JOptionPane.showMessageDialog(null, sql);
                cmb.addItem(rs.getObject(1));
            }
            
            cmd.close();
            cn.close();
            values = true;
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error de los combos: " + ex.getMessage()); }
    
        //Devolvemos el estado de la accion
        return values;
    }
    
    //Comentarios
    public static ArrayList<String> getComentStrind(String sql, Object value)
    {
        //Valor a devolver
        ArrayList<String> obj = new ArrayList<String>();
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            if(value instanceof String)
            {
                //Si ambos son texto
                cmd.setString(1, (String)value);
            }
            else if (value instanceof Integer)
            {
                //Si es numero
                cmd.setInt(1, (Integer)value);
            }
            
            ResultSet rs = cmd.executeQuery();
            
            while(rs.next())
            {
                obj.add(rs.getString(1));
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage()); }
        
        return obj;
    }
    
    //Comentarios
    public static ArrayList<tabComentarios> getComent(String sql, Object value)
    {
        //Valor a devolver
        ArrayList<tabComentarios> obj = new ArrayList<tabComentarios>();
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            if(value instanceof String)
            {
                //Si ambos son texto
                cmd.setString(1, (String)value);
            }
            else if (value instanceof Integer)
            {
                //Si es numero
                cmd.setInt(1, (Integer)value);
            }
            
            ResultSet rs = cmd.executeQuery();
            
            while(rs.next())
            {
                tabComentarios cmn = new tabComentarios();
                
                cmn.setId_comentario(rs.getInt(1));
                cmn.setId_perfil(rs.getInt(2));
                cmn.setComentario(rs.getString(3));
                cmn.setArchivos(rs.getString(4));
                cmn.setId_peticion(rs.getInt(5));
                
                obj.add(cmn);
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage()); }
        
        return obj;
    }
    
    public static Object getElement(String sql, Object value)
    {
        //Valor a devolver
        Object obj = null;
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            //JOptionPane.showMessageDialog(null, sql);
            
            if(value instanceof String)
            {
                //Si ambos son texto
                //JOptionPane.showMessageDialog(null, "Es un string");
                cmd.setString(1, (String)value);
            }
            else if (value instanceof Integer)
            {
                //Si es numero
                //JOptionPane.showMessageDialog(null, "Es un int");
                cmd.setInt(1, (Integer)value);
            }
            else
            {
                //Si es numero
                //JOptionPane.showMessageDialog(null, value.getClass().toString() +"="+ new Integer(0).getClass().toString());
            }
            
            
            ResultSet rs = cmd.executeQuery();
            
            if(rs.next())
            {
                obj = rs.getObject(1);
            }
            
            
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage()); }
        
        return obj;
    }
    
    public static ArrayList<tabPeticiones> getPetiones(String sql, String element)
    {
        //Lista
        ArrayList<tabPeticiones> peticiones = new ArrayList<tabPeticiones>();
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setString(1, element);
            
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next())
            {
                tabPeticiones pet = new tabPeticiones();
                
                //Le damos los valores
                pet.setId_peticion(rs.getInt(1));
                pet.setId_perfil(rs.getInt(2));
                pet.setAsunto(rs.getString(3));
                pet.setId_categoria(rs.getInt(4));
                pet.setDescripcion(rs.getString(5));
                pet.setFecha(rs.getString(6));
                pet.setId_estadopet(rs.getInt(7));
                pet.setId_concejal(rs.getInt(8));
                
                peticiones.add(pet);
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage()); }
        
        return peticiones;
    }
    
    public static ArrayList<tabPeticiones> getPetiones(String sql, int id)
    {
        //Lista
        ArrayList<tabPeticiones> peticiones = new ArrayList<tabPeticiones>();
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setInt(1, id);
            
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next())
            {
                tabPeticiones pet = new tabPeticiones();
                
                //Le damos los valores
                pet.setId_peticion(rs.getInt(1));
                pet.setId_perfil(rs.getInt(2));
                pet.setAsunto(rs.getString(3));
                pet.setId_categoria(rs.getInt(4));
                pet.setDescripcion(rs.getString(5));
                pet.setFecha(rs.getString(6));
                pet.setId_estadopet(rs.getInt(7));
                pet.setId_concejal(rs.getInt(8));
                
                peticiones.add(pet);
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage()); }
        
        return peticiones;
    }
    
    public static ArrayList<tabPeticiones> getPetiones(String sql, int id, int id2)
    {
        //Lista
        ArrayList<tabPeticiones> peticiones = new ArrayList<tabPeticiones>();
        //JOptionPane.showMessageDialog(null, sql+" Valor1 "+id+" Valor 2"+id2);
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setInt(1, id);
            cmd.setInt(2, id2);
            
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next())
            {
                tabPeticiones pet = new tabPeticiones();
                
                //Le damos los valores
                pet.setId_peticion(rs.getInt(1));
                pet.setId_perfil(rs.getInt(2));
                pet.setAsunto(rs.getString(3));
                pet.setId_categoria(rs.getInt(4));
                pet.setDescripcion(rs.getString(5));
                pet.setFecha(rs.getString(6));
                pet.setId_estadopet(rs.getInt(7));
                pet.setId_concejal(rs.getInt(8));
                
                peticiones.add(pet);
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage()); }
        
        return peticiones;
    }
    
    public static ArrayList<tabPeticiones> getPetiones(String sql)
    {
        //Lista
        ArrayList<tabPeticiones> peticiones = new ArrayList<tabPeticiones>();
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            ResultSet rs = cmd.executeQuery();
            
            while (rs.next())
            {
                tabPeticiones pet = new tabPeticiones();
                
                //Le damos los valores
                pet.setId_peticion(rs.getInt(1));
                pet.setId_perfil(rs.getInt(2));
                pet.setAsunto(rs.getString(3));
                pet.setId_categoria(rs.getInt(4));
                pet.setDescripcion(rs.getString(5));
                pet.setFecha(rs.getString(6));
                pet.setId_estadopet(rs.getInt(7));
                pet.setId_concejal(rs.getInt(8));
                
                peticiones.add(pet);
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage()); }
        
        return peticiones;
    }
    
    public static boolean eliminar(String sql, Object value)
    {
        boolean resp = false;
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
           PreparedStatement cmd = cn.prepareStatement(sql);
           
           if(value.getClass().equals("".getClass()))
            {
                //Si ambos son texto
                cmd.setString(1, (String)value);
            }
            else if (sql.getClass().equals(new Integer(0).getClass()))
            {
                //Si es numero
                cmd.setInt(1, (Integer)value);
            }
            
            
            if(!cmd.execute())
            {
                resp = true;
            }
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex){ JOptionPane.showMessageDialog(null, "Error: "+ex); }
        
        return resp;
    }
    
    public static boolean modificOne(String sql, int id, int id2)
    {
        boolean est = false;
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setInt(1, id);
            cmd.setInt(2, id2);
            
            if (!cmd.execute())
                est = true;
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
        }
        
        return est;
    }
    
    public static boolean InsertOne(String sql, int id_Perfil, String comentario, String archivo, int id_peticion)
    {
        boolean est = false;
        
        //Nos conectamos a la base
        Connection cn = xConectar.conectar();
        
        try
        {
            PreparedStatement cmd = cn.prepareStatement(sql);
            
            cmd.setInt(1, id_Perfil);
            cmd.setString(2, comentario);
            cmd.setString(3, archivo);
            cmd.setInt(4, id_peticion);
            
            if (!cmd.execute())
                est = true;
            
            cmd.close();
            cn.close();
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
        }
        
        return est;
    }
}
