/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

import java.awt.Font;
import java.io.InputStream;

/**
 *
 * @author kevin
 */
public class jCustomFont {
    
    private Font bembo = null;
    private Font myriad = null;

    public jCustomFont() {
        //Importando fuentes
        String fontBembo = "Bembo.ttf" ;
        String fontMyriad = "MyriadPro-Regular.otf" ;
        
        //Se carga bembo
        InputStream IS =  getClass().getResourceAsStream(fontBembo);
        
        try 
        {
            bembo = Font.createFont(Font.TRUETYPE_FONT, IS);
        } 
        
        catch (Exception ex) 
        {
            //Si existe un error se carga fuente por defecto ARIAL
            System.err.println(fontBembo + " No se cargo la fuente");
            bembo = new Font("Arial", Font.PLAIN, 14);            
        }
        
        
        //Se carga myriad
        IS =  getClass().getResourceAsStream(fontMyriad);
        
        try 
        {
            myriad = Font.createFont(Font.TRUETYPE_FONT, IS);
        } 
        
        catch (Exception ex) 
        {
            //Si existe un error se carga fuente por defecto ARIAL
            System.err.println(fontMyriad + " No se cargo la fuente");
            myriad = new Font("Arial", Font.PLAIN, 14);            
        }
    
    
    }
    
    
    /* Font.PLAIN = 0 , Font.BOLD = 1 , Font.ITALIC = 2
 * tamanio = float
 */
    
    
    //Método para aplicar Bembo
    public Font setBembo( int estilo, float tamanio)
    {
        Font tfont = bembo.deriveFont(estilo, tamanio);
        return tfont;
    }
    
    
    //Método para aplicar MMyriad
    public Font setMyriad( int estilo, float tamanio)
    {
        Font tfont = myriad.deriveFont(estilo, tamanio);
        return tfont;
    }
}
