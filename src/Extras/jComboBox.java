/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
/**
 *
 * @author Toshiba
 */
public class jComboBox extends JComboBox{
    public jComboBox()
    {
        Dimension dimension = new Dimension(200,32);
         setPreferredSize(dimension);
         setSize(dimension);      
         setForeground(Color.BLACK);        
         setBorder(BorderFactory.createLineBorder(new Color(71, 71, 71), 2));
         setUI(jCmbEstilo.createUI(this));                
         setVisible(true);
    }
}
