/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extras;

//Importar
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.PageSize;
import java.util.ArrayList;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

public class jReporteListado {
    
    //constructor
    public jReporteListado()
    {
        
        
        this.fecha = "Fecha del reporte: 00/00/00 00:00 pm";
    }
    
    //Añadir Atributo
    public void addJList(jLista lisTxt)
    {
        this.list.add(lisTxt);
    }
    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    String url = "";
    
    //Generar reporte-----------------------------------------------------------
    
    public void generarReportes() throws FileNotFoundException, DocumentException
    {
        Document doc = new Document(PageSize.A4, 35, 30, 110, 50);
        FileOutputStream file = new FileOutputStream(url+".pdf");
        PdfWriter write = PdfWriter.getInstance(doc, file);
        
        jMembrete jM = new jMembrete("Listado");
        
        //Fuentes
        com.itextpdf.text.Font fuenteNormal = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.PLAIN, BaseColor.BLACK);
        com.itextpdf.text.Font fuenteNegrita = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.BOLD, BaseColor.BLACK);
        
        write.setPageEvent(jM);
        
        //Abrir documento
        doc.open();
        
        Rectangle rect= new Rectangle(0, 0, 36, 108);
        
        rect.setRight(30);
        rect.setTop(3000);
        
        rect.enableBorderSide(1);
        rect.enableBorderSide(2);
        rect.enableBorderSide(4);
        rect.enableBorderSide(8);
        rect.setBorder(2);
        
        rect.setBorderColor(BaseColor.BLACK);
        rect.setBackgroundColor(new BaseColor(33,41,103));
        
        doc.add(rect);
        
        //Creamos la fecha
        Paragraph parrafo = new Paragraph();
        
        parrafo.setAlignment(Element.ALIGN_JUSTIFIED);
        
        parrafo.add(new Phrase(this.fecha, fuenteNormal));
        parrafo.add(new Phrase(Chunk.NEWLINE));
        parrafo.add(new Phrase(" ", fuenteNormal));
        
        doc.add(parrafo);
        
        Iterator i = list.iterator();
        
        while(i.hasNext())
        {
            //Obtenemos la lista
            jLista listado = (jLista)i.next();
            
            PdfPTable table = new PdfPTable(4);
            
            com.itextpdf.text.Font fuenteNegritaZ = FontFactory.getFont(FontFactory.TIMES_ROMAN, 11, Font.HANGING_BASELINE, BaseColor.BLACK);
            fuenteNegritaZ.setColor(BaseColor.WHITE);
            
            //Nombre del sujeto
            parrafo = new Paragraph(new Phrase(listado.getName(), fuenteNegritaZ));
            PdfPCell celda = new PdfPCell(parrafo);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            celda.setColspan(2);
            celda.setBorder(0);
            celda.setBackgroundColor(new BaseColor(69,84,194));
            table.addCell(celda);
            
            //Comision
            parrafo = new Paragraph(new Phrase(listado.getComision(), fuenteNegritaZ));
            celda = new PdfPCell(parrafo);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celda.setColspan(2);
            celda.setBorder(0);
            celda.setBackgroundColor(new BaseColor(69,84,194));
            table.addCell(celda);
            
            //Consejal que fue enviado
            parrafo = new Paragraph(new Phrase(listado.getConcejal(), fuenteNegritaZ));
            celda = new PdfPCell(parrafo);
            celda.setColspan(4);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            celda.setBorder(0);
            celda.setBackgroundColor(new BaseColor(69,84,194));
            table.addCell(celda);
            
            //Imagen
            Image img = listado.getImg();
            img.scaleAbsolute(100, 100);
            celda = new PdfPCell(img);
            celda.setColspan(1);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
            celda.setBorder(0);
            table.addCell(celda);
            
            //Descripcion
            parrafo = new Paragraph(new Phrase(listado.getDescripcion(), fuenteNormal));
            celda = new PdfPCell(parrafo);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            celda.setColspan(3);
            celda.setBorder(0);
            table.addCell(celda);
            
            //Estado
            parrafo = new Paragraph(new Phrase(listado.getEstado(), fuenteNegritaZ));
            celda = new PdfPCell(parrafo);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            celda.setColspan(2);
            celda.setBorder(0);
            celda.setBackgroundColor(new BaseColor(69,84,194));
            table.addCell(celda);
            
            //Fecha
            parrafo = new Paragraph(new Phrase(listado.getFecha(), fuenteNegritaZ));
            celda = new PdfPCell(parrafo);
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celda.setColspan(2);
            celda.setBorder(0);
            celda.setBackgroundColor(new BaseColor(69,84,194));
            table.addCell(celda);
            
            //Añadir la tabla
            doc.add(table);
            
            //Espacios
            parrafo = new Paragraph();
            parrafo.add(new Phrase(" ", fuenteNormal));
            doc.add(parrafo);
        }
        
        rect= new Rectangle(0, 0, 36, 108);
        
        rect.setRight(30);
        rect.setTop(3000);
        
        rect.enableBorderSide(1);
        rect.enableBorderSide(2);
        rect.enableBorderSide(4);
        rect.enableBorderSide(8);
        rect.setBorder(2);
        
        rect.setBorderColor(BaseColor.BLACK);
        rect.setBackgroundColor(new BaseColor(33,41,103));
        
        doc.add(rect);
        
        //Cerrar documento
        doc.close();
        
        try {
            File objetofile = new File (url.trim()+".pdf");
            Desktop.getDesktop().open(objetofile);
         }catch (IOException ex) {
            System.out.println(ex);
         }
    }
    
    //Atributo
    private ArrayList<jLista> list = new ArrayList<jLista>();
    private String fecha;
}
